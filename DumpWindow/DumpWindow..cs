﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DumpWindow
{
    /// <summary>
    /// ダンプ表示
    /// </summary>
    public partial class DumpWindow : Form
    {
        /// <summary>
        /// 環境保存用
        /// </summary>
        SVUtil.SVEnv mEnv;

        /// <summary>
        /// ダンプバッファ
        /// </summary>
        byte[] mDump = new byte[0];
        /// <summary>
        /// テキストの幅
        /// </summary>
        SizeF mTextSize;
        /// <summary>
        /// 表示行数
        /// </summary>
        int mVisivleLine = 0;
        /// <summary>
        /// ジャンプ値
        /// </summary>
        uint mJump = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DumpWindow(SVUtil.SVEnv env)
        {
            mEnv = env;
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void DumpWindow_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;

            // サイズ取得
            using (var g = panelDump.CreateGraphics())
            {

                mTextSize = g.MeasureString("A", panelDump.Font, 1000, new StringFormat());
            }
        }

        /// <summary>
        /// 閉じる処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void DumpWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// ダンプバッファ設定
        /// </summary>
        /// <param name="buf"></param>
        public void SetDumpBuffer(byte [] buf)
        {
            mDump = buf;
            ResetVScrollbar();
            PanelImage();
        }

        /// <summary>
        /// バーチャル表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void dgvDump_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if ( e.ColumnIndex >= 0 && e.RowIndex >= 0 )
            {
                int index = e.RowIndex * 16 + e.ColumnIndex;
                if (index < mDump.Length)
                {
                    if (index < mDump.Length)
                    {
                        e.Value = mDump[index].ToString("X2");
                    }
                    else
                    {
                        e.Value = "";
                    }
                }
            }
        }

        /// <summary>
        /// セルペイント表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void dgvDump_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if ( e.ColumnIndex < 0 && e.RowIndex >= 0)
            {
                //セルを描画する
                e.Paint(e.ClipBounds, DataGridViewPaintParts.All);

                int size = e.RowIndex * 16;
                string text = string.Format("{0:X4}:{1:X4}", (size >> 16), size & 0x0000ffff);

                //行番号を描画する範囲を決定する
                //e.AdvancedBorderStyleやe.CellStyle.Paddingは無視しています
                Rectangle indexRect = e.CellBounds;
                indexRect.Inflate(-2, -2);
                //行番号を描画する
                TextRenderer.DrawText(e.Graphics,
                    text,
                    e.CellStyle.Font,
                    indexRect,
                    e.CellStyle.ForeColor,
                    TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
                //描画が完了したことを知らせる
                e.Handled = true;
            }
        }

        /// <summary>
        /// トップへ移動
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void topMenuItem_Click(object sender, EventArgs e)
        {
            if (mDump != null)
            {
                vScrollBar.Value = 0;
            }

        }

        /// <summary>
        /// 最後へ移動
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void endMenuItem_Click(object sender, EventArgs e)
        {
            if (mDump != null)
            {
                vScrollBar.Value = vScrollBar.Maximum - mVisivleLine;
            }
        }

        /// <summary>
        /// +10000H進める
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void p10000HMenuItem_Click(object sender, EventArgs e)
        {
            if (mDump != null)
            {
                if ((vScrollBar.Value + 4096) > vScrollBar.Maximum)
                {
                    vScrollBar.Value = vScrollBar.Maximum-mVisivleLine;
                }
                else
                {
                    vScrollBar.Value += 4096;
                }
            }
        }

        /// <summary>
        /// -10000H進める
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void m10000HMenuItem_Click(object sender, EventArgs e)
        {
            if (mDump != null)
            {
                if ((vScrollBar.Value - 4096) < 0)
                {
                    vScrollBar.Value = 0;
                }
                else
                {
                    vScrollBar.Value -= 4096;
                }
            }
        }

        /// <summary>
        /// ダンプヘッダの表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void DumpWindow_Paint(object sender, PaintEventArgs e)
        {
            using (var g = this.CreateGraphics())
            {
                float x = panelDump.Location.X + 9 * mTextSize.Width / 2;
                float y = panelDump.Location.Y - mTextSize.Height - 1;
                for (int i = 0; i < 16; i++)
                {
                    g.DrawString(string.Format(" +{0:X1}", i), panelDump.Font, Brushes.Black, new PointF(x, y));
                    x += 3 * mTextSize.Width / 2;
                }
            }
        }

        /// <summary>
        /// ダンプデータの再表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void panelDump_Paint(object sender, PaintEventArgs e)
        {
            PanelImage();
        }

        /// <summary>
        /// ダンプ情報をビットマップで作成し貼り付ける
        /// </summary>
        void PanelImage()
        {
            using (Bitmap bmp = new Bitmap(panelDump.Size.Width, panelDump.Size.Height))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    string str;
                    int offset = vScrollBar.Value * 16;
                    float x = 0;
                    float y = 1;

                    g.FillRectangle(new SolidBrush(panelDump.BackColor), panelDump.ClientRectangle);


                    for (; offset < mDump.Length; offset += 16)
                    {
                        x = 0;
                        str = string.Format("{0:X4}:{1:X4}", offset >> 16, offset & 0x000ffff);
                        g.DrawString(str, panelDump.Font, Brushes.Black, new PointF(x, y));

                        x = 9 * mTextSize.Width / 2;
                        for (int i = offset; i < Math.Min(offset + 16, mDump.Length); i++)
                        {
                            g.DrawString(string.Format(" {0:X2}", mDump[i]), panelDump.Font, Brushes.Black, new PointF(x, y));
                            x += 3 * mTextSize.Width / 2;
                        }

                        y += mTextSize.Height + 1;

                        if (y > panelDump.ClientSize.Height) break;
                    }
                }

                using (var g = panelDump.CreateGraphics())
                {
                    g.DrawImage(bmp, new Point(0, 0));
                }
            }
        }


        /// <summary>
        /// ダンプウィンドウサイズ変更処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void DumpWindow_Resize(object sender, EventArgs e)
        {
            ResetVScrollbar();
        }


        /// <summary>
        /// スクロールバーの再計算
        /// </summary>
        void ResetVScrollbar()
        {
            // データが設定されている場合
            if (mDump != null)
            {
                int visible_line = (int)Math.Round((float)(panelDump.Height-1) / (mTextSize.Height+1)) + 1;
                int all_line = (mDump.Length - 1) / 16 + 2; // +2は最終行が見えるように

                // 表示ラインが全体ラインより大きい場合はスクロール無し
                if ( visible_line > all_line)
                {
                    vScrollBar.Value = 0;
                    vScrollBar.Enabled = false;
                }
                else
                {
                    vScrollBar.Enabled = true;
                    vScrollBar.Minimum = 0;
                    vScrollBar.Maximum = all_line; // - visible_line;
                    if (vScrollBar.Value > vScrollBar.Maximum)
                    {
                        vScrollBar.Value = vScrollBar.Maximum;
                    }
                    mVisivleLine = visible_line;
                    vScrollBar.LargeChange = mVisivleLine;
                }
            }
            else
            {
                vScrollBar.Enabled = false;
            }
        }

        /// <summary>
        /// スクロール処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void vScrollBar_ValueChanged(object sender, EventArgs e)
        {
            PanelImage();
        }

        /// <summary>
        /// ジャンプメニュー
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void jumpMenuItem_Click(object sender, EventArgs e)
        {
            using (var dlg = new Jump())
            {
                dlg.Address = mJump;
                if ( dlg.ShowDialog() != DialogResult.OK )
                {
                    return;
                }


                mJump = dlg.Address;

                uint index = mJump * 16;

                if (index >= mDump.Length)
                {
                    return;
                }
                SetPosition((int)index);
            }
        }

        /// <summary>
        /// ダンプ位置のスクロール処理
        /// </summary>
        /// <param name="pos">バイト位置</param>
        public void SetPosition(int pos)
        {
            int scroll = pos / 16;
            if ( (scroll+ mVisivleLine) >= vScrollBar.Maximum)
            {
                scroll = vScrollBar.Maximum - mVisivleLine;
            }
            // スクロールバー移動
            vScrollBar.Value = scroll;
        }
    }
}
