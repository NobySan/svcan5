﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DumpWindow
{
    public partial class Jump : Form
    {
        /// <summary>
        /// 数値の設定
        /// </summary>
        public uint Address
        {
            set
            {
                textAddress.Text = value.ToString("X7");
            }
            get
            {
                try
                {
                    return (Convert.ToUInt32(textAddress.Text,16));
                }
                catch
                {
                    return (0);
                }
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Jump()
        {
            InitializeComponent();
        }

        /// <summary>
        /// キーダウン（HEXのみ受付)
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void textAddress_KeyDown(object sender, KeyEventArgs e)
        {
            bool keyPressCk = true;

            if (('A' <= e.KeyValue && e.KeyValue <= 'F') // A～F
             || ('0' <= e.KeyValue && e.KeyValue <= '9') // 0～9 (キーボード)
             || ((int)Keys.NumPad0 <= e.KeyValue && e.KeyValue <= (int)Keys.NumPad9) // 1～9(テンキー) 
             || ((int)Keys.Left == e.KeyValue || (int)Keys.Right == e.KeyValue // ←→
             || (int)Keys.Delete == e.KeyValue || (int)Keys.Back == e.KeyValue))
            {
                keyPressCk = false;
            }

            // [KeyDown]後の[KeyPress]イベント
            e.SuppressKeyPress = keyPressCk; // true = 無効 / false = 許可
        }
    }
}
