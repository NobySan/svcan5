﻿namespace DumpWindow
{
    partial class DumpWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.topMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.endMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.p10000HMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m10000HMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelDump = new System.Windows.Forms.Panel();
            this.vScrollBar = new System.Windows.Forms.VScrollBar();
            this.menuStrip.SuspendLayout();
            this.panelDump.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.topMenuItem,
            this.endMenuItem,
            this.p10000HMenuItem,
            this.m10000HMenuItem,
            this.jumpMenuItem});
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(181, 136);
            // 
            // topMenuItem
            // 
            this.topMenuItem.Name = "topMenuItem";
            this.topMenuItem.Size = new System.Drawing.Size(121, 22);
            this.topMenuItem.Text = "Top";
            this.topMenuItem.Click += new System.EventHandler(this.topMenuItem_Click);
            // 
            // endMenuItem
            // 
            this.endMenuItem.Name = "endMenuItem";
            this.endMenuItem.Size = new System.Drawing.Size(121, 22);
            this.endMenuItem.Text = "End";
            this.endMenuItem.Click += new System.EventHandler(this.endMenuItem_Click);
            // 
            // p10000HMenuItem
            // 
            this.p10000HMenuItem.Name = "p10000HMenuItem";
            this.p10000HMenuItem.Size = new System.Drawing.Size(121, 22);
            this.p10000HMenuItem.Text = "+10000H";
            this.p10000HMenuItem.Click += new System.EventHandler(this.p10000HMenuItem_Click);
            // 
            // m10000HMenuItem
            // 
            this.m10000HMenuItem.Name = "m10000HMenuItem";
            this.m10000HMenuItem.Size = new System.Drawing.Size(121, 22);
            this.m10000HMenuItem.Text = "-10000H";
            this.m10000HMenuItem.Click += new System.EventHandler(this.m10000HMenuItem_Click);
            // 
            // jumpMenuItem
            // 
            this.jumpMenuItem.Name = "jumpMenuItem";
            this.jumpMenuItem.Size = new System.Drawing.Size(180, 22);
            this.jumpMenuItem.Text = "Jump";
            this.jumpMenuItem.Click += new System.EventHandler(this.jumpMenuItem_Click);
            // 
            // panelDump
            // 
            this.panelDump.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelDump.ContextMenuStrip = this.menuStrip;
            this.panelDump.Controls.Add(this.vScrollBar);
            this.panelDump.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panelDump.Location = new System.Drawing.Point(12, 27);
            this.panelDump.Name = "panelDump";
            this.panelDump.Size = new System.Drawing.Size(454, 379);
            this.panelDump.TabIndex = 2;
            this.panelDump.Paint += new System.Windows.Forms.PaintEventHandler(this.panelDump_Paint);
            // 
            // vScrollBar
            // 
            this.vScrollBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar.Location = new System.Drawing.Point(434, 0);
            this.vScrollBar.Name = "vScrollBar";
            this.vScrollBar.Size = new System.Drawing.Size(20, 379);
            this.vScrollBar.TabIndex = 3;
            this.vScrollBar.ValueChanged += new System.EventHandler(this.vScrollBar_ValueChanged);
            // 
            // DumpWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(478, 418);
            this.Controls.Add(this.panelDump);
            this.Location = new System.Drawing.Point(0, 26);
            this.Name = "DumpWindow";
            this.Text = "Dump";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DumpWindow_FormClosing);
            this.Load += new System.EventHandler(this.DumpWindow_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DumpWindow_Paint);
            this.Resize += new System.EventHandler(this.DumpWindow_Resize);
            this.menuStrip.ResumeLayout(false);
            this.panelDump.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem topMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endMenuItem;
        private System.Windows.Forms.ToolStripMenuItem p10000HMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m10000HMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jumpMenuItem;
        private System.Windows.Forms.Panel panelDump;
        private System.Windows.Forms.VScrollBar vScrollBar;
    }
}