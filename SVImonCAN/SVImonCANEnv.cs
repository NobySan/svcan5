﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SVImonCAN
{
    /// <summary>
    /// 環境設定
    /// </summary>
    [DataContract]
    public class SVImonCANEnv : SVUtil.SVEnv
    {
        /// <summary>
        /// 記録オプション
        /// </summary>
        [DataMember(Name = "RecOptionParam")]
        RecOptionParam _recoption = null;
        public RecOptionParam mRecOption
        {
            get
            {
                if (_recoption == null)
                {
                    _recoption = new RecOptionParam();
                }
                return (_recoption);
            }
            set
            {
                _recoption = value;
            }
        }
    }

    /// <summary>
    /// 記録－設定
    /// </summary>
    [DataContract]
    public class RecOptionParam
    {
        /// <summary>
        /// Data Folder
        /// </summary>
        [DataMember(Name = "DataFolder")]
        public string mDataFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        /// <summary>
        /// モード(0:Auto 1:Any)
        /// </summary>
        [DataMember(Name = "FileSetting")]
        public int mFileSetting = 0;
        /// <summary>
        /// Any Name
        /// </summary>
        [DataMember(Name = "AnyName")]
        public string mAnyName = "dump.dat";
        /// <summary>
        /// Video Color
        /// </summary>
        [DataMember(Name = "VideoColor")]
        public int mVideoColor = 0;
        /// <summary>
        /// Video VSyncLevel
        /// </summary>
        [DataMember(Name = "VideoVSyncLevel")]
        public int mVideoVSyncLevel = 0;
        /// <summary>
        /// Video HSyncLevel
        /// </summary>
        [DataMember(Name = "VideoHSyncLevel")]
        public int mVideoHSyncLevel = 0;
        /// <summary>
        /// CAN0
        /// </summary>
        [DataMember(Name = "VideoCAN0")]
        public bool mVideoCAN0 = true;
        /// <summary>
        /// CAN1
        /// </summary>
        [DataMember(Name = "VideoCAN1")]
        public bool mVideoCAN1 = true;
        /// <summary>
        /// Threshold1
        /// </summary>
        [DataMember(Name = "CanThreshold1")]
        public int mCanThreshold1 = 50;
        /// <summary>
        /// Threshold2
        /// </summary>
        [DataMember(Name = "CanThreshold2")]
        public int mCanThreshold2 = 5;
        /// <summary>
        /// CAN bps
        /// </summary>
        [DataMember(Name = "CANBps")]
        public int mCanBps = 500;
        /// <summary>
        /// CAN-FD bps
        /// </summary>
        [DataMember(Name = "CAN_FDbps")]
        public int mCAN_FDbps = 500;
        /// <summary>
        /// MAX Time
        /// </summary>
        [DataMember(Name = "MaxTime")]
        public bool mMaxTime = true;
        /// <summary>
        /// レコーディングタイム
        /// </summary>
        [DataMember(Name = "VideoRecoringTime")]
        public int mVideoRecoringTime = 600;
        /// <summary>
        /// LSB_Pack
        /// </summary>
        [DataMember(Name = "DisplayLsbPack")]
        public bool mDisplayLsbPack = false;
        /// <summary>
        /// Reverse
        /// </summary>
        [DataMember(Name = "DisplayReverse")]
        public bool mDisplayReverse = false;
        /// <summary>
        /// Preview
        /// </summary>
        [DataMember(Name = "Preview")]
        public bool mPreview = false;
        /// <summary>
        /// Picture Auto
        /// </summary>
        [DataMember(Name = "PictureAuto")]
        public bool mPictureAuto = true;
        /// <summary>
        /// Picture Width
        /// </summary>
        [DataMember(Name = "PictureWidth")]
        public int mPictureWidth = 1280;
        /// <summary>
        /// Picture Height
        /// </summary>
        [DataMember(Name = "PictureHeight")]
        public int mPictureHeight = 960;
        /// <summary>
        /// Picture FPS
        /// </summary>
        [DataMember(Name = "PictureFPS")]
        public int mPictureFPS = 30;
        /// <summary>
        /// Time Out
        /// </summary>
        [DataMember(Name = "TimeOut")]
        public int mTimeOut = 10;

        /// <summary>
        /// VSyncMask
        /// </summary>
        public ushort VSyncMask
        {
            get
            {
                //if (this.mVideoVSyncLevel == 1) return (0);
                return (SVCanData.SvCanBitMask.SYNCACTIVEMASK_V);
            }
        }

        /// <summary>
        /// HSyncMask
        /// </summary>
        public ushort HSyncMask
        {
            get
            {
                //if (this.mVideoHSyncLevel == 1) return (0);
                return (SVCanData.SvCanBitMask.SYNCACTIVEMASK_H);
            }
        }

        /// <summary>
        /// デシリアライズ
        /// </summary>
        /// <param name="sc">コンテキスト</param>
        [OnDeserializing]
        internal void OnDeserializing(StreamingContext sc)
        {
            // My Documents
            this.mDataFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            this.mAnyName = "dump.dat";
            this.mFileSetting = 0;
            this.mVideoColor = 0;
            this.mVideoVSyncLevel = 0;
            this.mVideoHSyncLevel = 0;
            this.mVideoCAN0 = true;
            this.mVideoCAN1 = true;
            this.mCanThreshold1 = 50;
            this.mCanThreshold2 = 5;
            this.mCanBps = 500;
            this.mCAN_FDbps = 500;
            this.mMaxTime = true;
            this.mVideoRecoringTime = 600;
            this.mDisplayLsbPack = false;
            this.mDisplayReverse = false;
            this.mPreview = false;
            this.mPictureAuto = true;
            this.mPictureWidth = 1280;
            this.mPictureHeight= 960;
            this.mPictureFPS = 30;
            this.mTimeOut = 10;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RecOptionParam()
        {
        }
    }
}
