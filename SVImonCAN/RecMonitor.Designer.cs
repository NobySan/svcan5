﻿namespace SVImonCAN
{
    partial class RecMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.labelAutoStopTime = new System.Windows.Forms.Label();
            this.textAutoStopTime = new System.Windows.Forms.TextBox();
            this.labelAutoStopTimeSec = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.labelErrorCount = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 149);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Preview picture";
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox.Location = new System.Drawing.Point(6, 17);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(229, 125);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // labelAutoStopTime
            // 
            this.labelAutoStopTime.AutoSize = true;
            this.labelAutoStopTime.Location = new System.Drawing.Point(12, 42);
            this.labelAutoStopTime.Name = "labelAutoStopTime";
            this.labelAutoStopTime.Size = new System.Drawing.Size(81, 12);
            this.labelAutoStopTime.TabIndex = 2;
            this.labelAutoStopTime.Text = "Auto stop time";
            // 
            // textAutoStopTime
            // 
            this.textAutoStopTime.Location = new System.Drawing.Point(125, 40);
            this.textAutoStopTime.Name = "textAutoStopTime";
            this.textAutoStopTime.ReadOnly = true;
            this.textAutoStopTime.Size = new System.Drawing.Size(57, 19);
            this.textAutoStopTime.TabIndex = 3;
            this.textAutoStopTime.Text = "100";
            // 
            // labelAutoStopTimeSec
            // 
            this.labelAutoStopTimeSec.AutoSize = true;
            this.labelAutoStopTimeSec.Location = new System.Drawing.Point(188, 43);
            this.labelAutoStopTimeSec.Name = "labelAutoStopTimeSec";
            this.labelAutoStopTimeSec.Size = new System.Drawing.Size(23, 12);
            this.labelAutoStopTimeSec.TabIndex = 4;
            this.labelAutoStopTimeSec.Text = "sec";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(14, 227);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(239, 23);
            this.progressBar.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(123, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Error Count:";
            // 
            // labelErrorCount
            // 
            this.labelErrorCount.Location = new System.Drawing.Point(195, 13);
            this.labelErrorCount.Name = "labelErrorCount";
            this.labelErrorCount.Size = new System.Drawing.Size(58, 14);
            this.labelErrorCount.TabIndex = 1;
            this.labelErrorCount.Text = "0";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(18, 270);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 7;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(107, 270);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // RecMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 307);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.labelErrorCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelAutoStopTimeSec);
            this.Controls.Add(this.textAutoStopTime);
            this.Controls.Add(this.labelAutoStopTime);
            this.Controls.Add(this.groupBox1);
            this.Name = "RecMonitor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recording-Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RecMonitor_FormClosing);
            this.Load += new System.EventHandler(this.RecMonitor_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelAutoStopTime;
        private System.Windows.Forms.TextBox textAutoStopTime;
        private System.Windows.Forms.Label labelAutoStopTimeSec;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelErrorCount;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnCancel;
    }
}