﻿namespace SVImonCAN
{
    partial class I2cWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.I2cTextBoxSlaveAdr = new System.Windows.Forms.TextBox();
            this.I2cTextBoxSubAdr = new System.Windows.Forms.TextBox();
            this.I2cTextBoxWriteVal = new System.Windows.Forms.TextBox();
            this.I2cTextBoxReadCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.I2cBtnSettingFileSend = new System.Windows.Forms.Button();
            this.I2cWrite = new System.Windows.Forms.Button();
            this.I2cRead = new System.Windows.Forms.Button();
            this.I2cWordAddressMode = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Slave Address (hex)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sub Address (hex)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Write Value (hex)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "Read Count (hex)";
            // 
            // I2cTextBoxSlaveAdr
            // 
            this.I2cTextBoxSlaveAdr.Location = new System.Drawing.Point(127, 12);
            this.I2cTextBoxSlaveAdr.Name = "I2cTextBoxSlaveAdr";
            this.I2cTextBoxSlaveAdr.Size = new System.Drawing.Size(100, 19);
            this.I2cTextBoxSlaveAdr.TabIndex = 4;
            // 
            // I2cTextBoxSubAdr
            // 
            this.I2cTextBoxSubAdr.Location = new System.Drawing.Point(127, 43);
            this.I2cTextBoxSubAdr.Name = "I2cTextBoxSubAdr";
            this.I2cTextBoxSubAdr.Size = new System.Drawing.Size(100, 19);
            this.I2cTextBoxSubAdr.TabIndex = 5;
            // 
            // I2cTextBoxWriteVal
            // 
            this.I2cTextBoxWriteVal.Location = new System.Drawing.Point(127, 78);
            this.I2cTextBoxWriteVal.Name = "I2cTextBoxWriteVal";
            this.I2cTextBoxWriteVal.Size = new System.Drawing.Size(100, 19);
            this.I2cTextBoxWriteVal.TabIndex = 6;
            // 
            // I2cTextBoxReadCount
            // 
            this.I2cTextBoxReadCount.Location = new System.Drawing.Point(127, 110);
            this.I2cTextBoxReadCount.Name = "I2cTextBoxReadCount";
            this.I2cTextBoxReadCount.Size = new System.Drawing.Size(100, 19);
            this.I2cTextBoxReadCount.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "Read Value (hex)";
            // 
            // I2cBtnSettingFileSend
            // 
            this.I2cBtnSettingFileSend.Location = new System.Drawing.Point(255, 12);
            this.I2cBtnSettingFileSend.Name = "I2cBtnSettingFileSend";
            this.I2cBtnSettingFileSend.Size = new System.Drawing.Size(109, 23);
            this.I2cBtnSettingFileSend.TabIndex = 10;
            this.I2cBtnSettingFileSend.Text = "Setting File Send";
            this.I2cBtnSettingFileSend.UseVisualStyleBackColor = true;
            this.I2cBtnSettingFileSend.Click += new System.EventHandler(this.I2cBtnSettingFileSend_Click);
            // 
            // I2cWrite
            // 
            this.I2cWrite.Location = new System.Drawing.Point(255, 46);
            this.I2cWrite.Name = "I2cWrite";
            this.I2cWrite.Size = new System.Drawing.Size(109, 23);
            this.I2cWrite.TabIndex = 11;
            this.I2cWrite.Text = "Write";
            this.I2cWrite.UseVisualStyleBackColor = true;
            this.I2cWrite.Click += new System.EventHandler(this.I2cWrite_Click);
            // 
            // I2cRead
            // 
            this.I2cRead.Location = new System.Drawing.Point(255, 81);
            this.I2cRead.Name = "I2cRead";
            this.I2cRead.Size = new System.Drawing.Size(109, 23);
            this.I2cRead.TabIndex = 12;
            this.I2cRead.Text = "Read";
            this.I2cRead.UseVisualStyleBackColor = true;
            this.I2cRead.Click += new System.EventHandler(this.I2cRead_Click);
            // 
            // I2cWordAddressMode
            // 
            this.I2cWordAddressMode.AutoSize = true;
            this.I2cWordAddressMode.Location = new System.Drawing.Point(255, 122);
            this.I2cWordAddressMode.Name = "I2cWordAddressMode";
            this.I2cWordAddressMode.Size = new System.Drawing.Size(118, 16);
            this.I2cWordAddressMode.TabIndex = 13;
            this.I2cWordAddressMode.Text = "Word Sub Address";
            this.I2cWordAddressMode.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(307, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 18);
            this.button1.TabIndex = 14;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(15, 170);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(349, 148);
            this.listBox1.TabIndex = 15;
            // 
            // I2cWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 331);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.I2cWordAddressMode);
            this.Controls.Add(this.I2cRead);
            this.Controls.Add(this.I2cWrite);
            this.Controls.Add(this.I2cBtnSettingFileSend);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.I2cTextBoxReadCount);
            this.Controls.Add(this.I2cTextBoxWriteVal);
            this.Controls.Add(this.I2cTextBoxSubAdr);
            this.Controls.Add(this.I2cTextBoxSlaveAdr);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "I2cWindow";
            this.Text = "I2cWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox I2cTextBoxSlaveAdr;
        private System.Windows.Forms.TextBox I2cTextBoxSubAdr;
        private System.Windows.Forms.TextBox I2cTextBoxWriteVal;
        private System.Windows.Forms.TextBox I2cTextBoxReadCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button I2cBtnSettingFileSend;
        private System.Windows.Forms.Button I2cWrite;
        private System.Windows.Forms.Button I2cRead;
        private System.Windows.Forms.CheckBox I2cWordAddressMode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
    }
}