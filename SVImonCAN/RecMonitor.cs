﻿using SVCanData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVImonCAN
{
    /// <summary>
    /// モニター表示
    /// </summary>
    public partial class RecMonitor : Form
    {
        /// <summary>
        /// 進捗バー
        /// </summary>
        public int ProgressPOS
        {
            set
            {
                progressBar.Value = value;
            }
        }

        /// <summary>
        /// RecordingTime
        /// </summary>
        public int RecordingTime
        {
            set
            {
                textAutoStopTime.Text = value.ToString();
            }
        }


        /// <summary>
        /// MAX Timeの表示有無
        /// </summary>
        public bool ActiveMaxTime
        {
            set
            {
                this.labelAutoStopTime.Visible = value;
                this.labelAutoStopTimeSec.Visible = value;
                this.textAutoStopTime.Visible = value;
                if (value)
                {
                    this.progressBar.Style = ProgressBarStyle.Blocks;
                }
                else
                {
                    this.progressBar.Style = ProgressBarStyle.Marquee;
                }
            }
        }


        /// <summary>
        /// エラーカウント
        /// </summary>
        public int ErrorCount
        {
            set
            {
                labelErrorCount.Text = value.ToString();
            }
        }

        /// <summary>
        /// プレビュー用のバッファに空きができたかどうか
        /// </summary>
        bool mNeedBuffer = true;
        public bool IsNeedBuffer
        {
            get
            {
                return (mNeedBuffer);
            }
        }
        /// <summary>
        /// フレームバッファ
        /// </summary>
        byte[] mFrameBuffer = null;

        /// <summary>
        /// ピクセルバッファ(3K)
        /// </summary>
        byte[] mPixels = new byte[3072 * 2048];

        /// <summary>
        /// キャンセルイベント
        /// </summary>
        public event EventHandler CancelEvent;

        /// <summary>
        /// 停止イベント
        /// </summary>
        public event EventHandler StopEvent;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RecMonitor()
        {
            InitializeComponent();

            //コントロールを初期化する
            progressBar.Minimum = 0;
            progressBar.Maximum = 100;
            progressBar.Value = 0;
        }

        /// <summary>
        /// STOP/CANCELボタンアクティブ
        /// </summary>
        public void ButtonActive()
        {
            this.btnCancel.Enabled = true;
            this.btnStop.Enabled = true;
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void RecMonitor_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;
        }

        /// <summary>
        /// キャンセル
        /// </summary>
        private void OnCancelEvent()
        {
            if (this.CancelEvent != null)
            {
                this.CancelEvent(this, new EventArgs());
            }
        }

        /// <summary>
        /// 停止イベント
        /// </summary>
        private void OnStopEvent()
        {
            if (this.StopEvent != null)
            {
                this.StopEvent(this, new EventArgs());
            }
        }

        /// <summary>
        /// クローズ処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void RecMonitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            Program.mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// フレームバッファの設定
        /// </summary>
        /// <param name="frame_buffer">フレームバッファ</param>
        /// <param name="start_pos">開始位置</param>
        public void PreviewFrameBuffer(byte[] frame_buffer, Size frame_size)
        {
            // プレビュー状態が無い場合は処理しない
            if (!Program.mEnv.mRecOption.mPreview || mNeedBuffer == false)
            {
                return;
            }

            // フレームバッファのコピー
            mFrameBuffer = new byte[frame_buffer.Length];
            Buffer.BlockCopy(frame_buffer, 0, mFrameBuffer, 0, mFrameBuffer.Length);

            // フレーム解析ビットマップ作成
            System.Threading.Tasks.Task.Run(() =>
            {
                // フレームビットマップ作成
                MakeFrameBitmap(frame_size);

                mNeedBuffer = true;
            });
        }

        /// <summary>
        /// フレームビットマップ作成
        /// </summary>
        void MakeFrameBitmap(Size frame_size)
        {
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            bool enableFrame = false;
            bool enableLine = false;
            int pixcelCount = 0;
            int lineCount = 0;
            int frameCount = 0;                         // フレームカウンター
            int vertical = 0;                           // Vブランキング
            int horizontal = 0;                         // Hブランキング
            bool enableVBlank = false;                  // Vブランキング期間判定

            wordSyncActive_V = Program.mEnv.mRecOption.VSyncMask;
            wordSyncActive_H = Program.mEnv.mRecOption.HSyncMask;
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            // 1フレーム目は読み飛ばす
            bool firstskip = true;

            ushort lsbBackShift = (ushort)((Program.mEnv.mRecOption.mDisplayLsbPack) ? 0 : 4);


            // 解析
            for (long i = 0; i < mFrameBuffer.Length; i += 2)
            {
                UInt16 wordCurrent = (UInt16)(((mFrameBuffer[i+1]<<8) & 0xff00) | (mFrameBuffer[i] & 0xff));
                if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                {
                    if (enableFrame == false)
                    {
                        //有効フレーム開始
                        enableFrame = true;
                        //画素数カウント初期化
                        pixcelCount = 0;
                        //有効ラインカウント初期化
                        lineCount = 0;
                        // フレームカウンタインクリメント
                        frameCount++;
                    }

                    if (enableLine == false)
                    {
                        //ライン有効
                        enableLine = true;
                    }

                    //画素数カウント
                    if (pixcelCount < mPixels.Length)
                    {
                        mPixels[pixcelCount] = (byte)(wordCurrent>> lsbBackShift);
                        pixcelCount++;
                    }
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V) //VSYNCのみ Active
                {
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        enableLine = false;
                        //有効ラインカウントアップ

                        lineCount++;
                        if ( (frame_size.Width * lineCount) != pixcelCount)
                        {
                            pixcelCount = 0;
                        }
                        pixcelCount = frame_size.Width * lineCount;
                    }
                    // 無効期間
                    horizontal++;
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_H) == wordSyncActive_H) //HSYNCのみ Active
                {
                    //Vブランキング期間
                    enableFrame = false;
                    // Vブランキング期間カウント判定
                    if (enableVBlank == true)
                    {
                        // カウント停止
                        enableVBlank = false;
                        // カウントUp
                        vertical++;
                    }
                }
                else
                {
                    //VHブランキング期間

                    // 2005.11.2 V3.31 VHハイ状態からいきなりVHロー状態になった時の対処
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        //有効ラインカウントアップ
                        lineCount++;
                    }

                    // Vブランキング区間カウント
                    if (enableVBlank == false)
                    {
                        // カウント開始フラグOn
                        enableVBlank = true;
                    }

                    if (firstskip)
                    {
                        firstskip = false;
                    }
                    else
                    {
                        // フレーム終了判定
                        if (enableFrame)
                        {
                            SVUtil.DIBitmap dib = new SVUtil.DIBitmap();
                            if (Program.mEnv.mRecOption.mDisplayReverse)
                            {
                                dib.CreateDIB(frame_size.Width, -frame_size.Height, 8);
                            }
                            else
                            {
                                dib.CreateDIB(frame_size.Width, frame_size.Height, 8);
                            }
                            dib.SetImage(mPixels);

                            // 貼付け
                            this.Invoke((MethodInvoker)delegate ()
                            {
                                pictureBox.Image = System.Drawing.Bitmap.FromHbitmap(dib.Handle);
                                pictureBox.Refresh();
                            });

                            dib.FreeBitmap();
                            dib = null;
                            break;
                        }
                    }
                    enableFrame = false;
                }
            }
        }

        /// <summary>
        /// 「STOP」ボタン押下
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void BtnStop_Click(object sender, EventArgs e)
        {
            this.btnCancel.Enabled = false;
            this.btnStop.Enabled = false;
            this.OnStopEvent();
        }

        /// <summary>
        /// 「CANCEL」ボタン押下
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.btnCancel.Enabled = false;
            this.btnStop.Enabled = false;
            this.OnCancelEvent();
        }
    }
}
