﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace sviusb20_dll
{
    /// <summary>
    /// DllImport属性を用いて定義したメソッドを格納するクラス
    /// </summary>
    public static class NativeMethods
    {
        /// <summary>
        /// SVIUSB20 API戻り値定義
        /// </summary>
        public const UInt32 RET_NORMAL = 0x1;
        public const UInt32 RET_ERROR_DEVICE = 0xe0000000;
        public const UInt32 RET_ERROR_DEVOPEN = 0xf0000001;
        public const UInt32 RET_ERROR_MULTIOPEN = 0xf0000002;
        public const UInt32 RET_ERROR_NOOPEN = 0xf0000003;
        public const UInt32 RET_ERROR_PARAMETER = 0xf0000004;
        public const UInt32 RET_ERROR_SVI = 0xf1000000;

        /// <summary>
        /// SVIUSB20 ステータス定義
        /// </summary>
        public const UInt32 STS_SUCCESS = 0x0001;
        public const UInt32 STS_FRAMEPENDING = 0x0002;
        public const UInt32 STS_CAPCANCELED = 0x0003;
        public const UInt32 STS_BUSY = 0x0011;
        public const UInt32 STS_RECOVERYMODE = 0x0012;
        public const UInt32 STS_I2CACTIVE = 0x0013;
        public const UInt32 STS_CMD_INVALID = 0x0101;
        public const UInt32 STS_PRM_INVALID = 0x0102;
        public const UInt32 STS_SEQ_INVALID = 0x0103;
        public const UInt32 STS_I2C_ACKTIMEOUT = 0x0601;
        public const UInt32 STS_I2C_PRETIMEOUT = 0x0602;
        public const UInt32 STS_I2C_POSTTIMEOUT = 0x0603;
        public const UInt32 STS_INTERNAL_ERROR = 0x0F01;
        public const UInt32 STS_RESOURCE_ERROR = 0x0F02;

        /// <summary>
        /// SVIUSB20_Open用定義
        /// </summary>
        public const UInt32 APP_WHO_REC = 1;
        public const UInt32 APP_WHO_CTL = 2;

        /// <summary>
        /// SVIUSB20_SetParam用定義
        /// </summary>
        public const UInt32 SETPARAM_BIT0 = 0x1;        // not use
        public const UInt32 SETPARAM_BIT1 = 0x2;        // not use
        public const UInt32 SETPARAM_BIT2 = 0x4;        // I2C Tranfer Spped setting
        public const UInt32 SETPARAM_BIT3 = 0x8;        // not use
        public const UInt32 SETPARAM_BIT4 = 0x10;       // not use
        public const UInt32 SETPARAM_BIT5 = 0x20;       // not use
        public const UInt32 SETPARAM_BIT6 = 0x40;       // not use
        public const UInt32 SETPARAM_CAMPOWER_OF = 0x0; // Camera Power OFF
        public const UInt32 SETPARAM_CAMPOWER_ON = 0x1; // Camera Power ON
        public const UInt32 SETPARAM_I2CSPEED_4 = 0x0;  // 400Kbps
        public const UInt32 SETPARAM_I2CSPEED_2 = 0x1;  // 200Kbps
        public const UInt32 SETPARAM_I2CSPEED_1 = 0x2;  // 100Kbps

        /// <summary>
        /// SVIUSB20_GetStatus用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct GET_STATUS
        {
            public UInt32 ulBasicStatus;        // 取込ハードウェアの基本ステータス（1H:正常、その他:異常）
            public UInt32 ulHWVersion;          // ハードウェアのバージョン番号
            public UInt32 ulFWVersion;          // ファームウェアのバージョン番号
            public UInt32 ulOPStatus;           // 現在の動作モード
                                                //	0:アイドル、1:モニタリング中
                                                //	2:レコーディング中、8:アップデート中
            public UInt32 ulStartupStatus;      // ファームウェア起動ステータス（0:ノーマル、1:リカバリ）
            public UInt32 ulOrgSizeW;           // カメラが出力している画像サイズの幅
            public UInt32 ulOrgSizeH;           // カメラが出力している画像サイズの高さ
            public UInt32 ulCutout_x;           // オリジナル画像の左上端からの切り出し範囲の左上端のX座標
            public UInt32 ulCutout_y;           // オリジナル画像の左上端からの切り出し範囲の左上端のY座標
            public UInt32 ulCutSizeW;           // 切り出し後の画像サイズの幅
            public UInt32 ulCutSizeH;           // 切り出し後の画像サイズの高さ
            public UInt32 ulCamStatus;          // カメラステータス（未使用）
            public UInt32 ulCamFps;             // カメラが出力しているフレームレート（100倍された値）
        }

        /// <summary>
        /// SVIUSB20_SetParam用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct SET_PARAM
        {
            public UInt32 ulParamBitFlag;   // 設定項目指定
            public UInt32 ulVsyncFlag;      // VSYNCデアサートフラグ	0　:　デアサートを待つ - 初期値
                                            //							1　:　デアサートを待たない
            public UInt32 Power;			// カメラ電源スイッチ（0:OFF、1:ON）
            public UInt32 ulI2CSpeed;       // I2C転送スピード設定
            public UInt32 ulMonMode;        // モニタリングモード（0 : ﾀﾞﾌﾞﾙﾊﾞｯﾌｧ、1 : ﾘﾝｸﾞﾊﾞｯﾌｧ）- 初期値：0
            public UInt32 ulPixelInfo;		// 画像情報通知	bit0-1 : 色成分数(1or2or3)
        }

        /// <summary>
        /// モニタリング用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct MON_PARAM
        {
            public UInt32 ulCutout_x;		// オリジナル画像の左上端からの切り出し範囲の左上端のX座標
            public UInt32 ulCutout_y;		// オリジナル画像の左上端からの切り出し範囲の左上端のY座標
            public UInt32 ulCutout_w;		// 切り出し範囲の幅（偶数）
            public UInt32 ulCutout_h;		// 切り出し範囲の高さ（偶数）
        }

        /// <summary>
        /// モニタリングヘッダー用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct MON_HEADER
        {
            public UInt32 ulStatus;			// 実行結果のステータス（1H:成功、その他:異常）
            public UInt32 ulPendingInfo;	// 保留フレーム数
            public UInt32 ulOrgSizeW;		// カメラが出力している画像サイズの幅
            public UInt32 ulOrgSizeH;		// カメラが出力している画像サイズの高さ
            public UInt32 ulCutout_x;		// オリジナル画像の左上端からの切り出し範囲の左上端のX座標
            public UInt32 ulCutout_y;		// オリジナル画像の左上端からの切り出し範囲の左上端のY座標
            public UInt32 ulMonSizeW;		// 切り出し後の画像サイズの幅
            public UInt32 ulMonSizeH;		// 切り出し後の画像サイズの高さ
            public UInt32 ulFrameRate;		// カメラが出力している画像のフレームレート
            public UInt32 ulLoseFrame;		// 前フレームから本フレーム間に破棄したフレーム数
        }

        /// <summary>
        /// 拡張レコーディング用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct REC_PARAM_EX
        {
            public UInt32 ulRecMode;        // レコーディングモード
                                            //  bit31（0:CAN-0 Not recording, 1:CAN-0 Recording）
                                            //  bit30（0:CAN-1 Not recording, 1:CAN-1 Recording）
                                            //  bit29（0:画像データLSB詰め, 1:MSB詰め）
                                            //  bit28-bit00（予約）
        }

        /// <summary>
        /// 拡張レコーディングヘッダー用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct REC_HEADER_EX
        {
            public UInt32 ulStatus;         // 実行結果のステータス（1H:ヘッダー有効、0H:ヘッダー無効）
            public UInt32 ulPendingInfo;    // 次に読出し可能なSubframeがある場合に'1'
            public UInt32 ulNumChHVerFVer;  // [ 7: 0]: FirmVer （過去との互換、現状で8’h00）
                                            // [15: 8]: HardVer （過去との互換、現状で8’h00）
                                            // [23:16]: NumChannel（過去との互換、現状で8’h00）
                                            // [26:24]: SmplClkSel 本サブフレーム取り込み時のDRB_SMPL_CLK_SELレジスタ設定の値
            public UInt32 ulNumScanCompFlagY;   // [ 7]: SLastFlag 一連のフレーム取り込みにおいて、最後のサブフレームかどうかを示します。
                                                // [31:16]: SCntNum サブフレームの番号を示します。一連のフレームの中で先頭のサブフレームは値’0’となります。
            public UInt32 ulDataWidth;      // [23: 0]: BgmSygSel 本サブフレーム取り込み時のDRB_BGM_SYG_SELレジスタ設定の値
                                            // [26:24]: BgpSygCnt 本サブフレーム取り込み時のDRB_BGP_SYG_CNTレジスタ設定の値
                                            // [31:28]: BgpError [28]: DRB_EXC_BGP_SRC_OVERFLOWレジスタの値を示します。
                                            //					[31]: DRB_BGP_EXC_FBP_LOGICレジスタの値を示します。
            public UInt32 ulHeaderSize;     // 32’d512固定
            public UInt32 ulRsv1;           // 予約
            public UInt32 ulRsv2;           // 予約
            public UInt32 ulRsv3;           // 予約
            public UInt32 ulRsv4;           // 予約
            public UInt32 ulRsv5;           // 予約
            public UInt32 ulRsv6;           // 予約
            public UInt32 ulRsv7;           // 予約
            public UInt32 ulRsv8;           // 予約
            public UInt32 ulRsv9;           // 予約
            public UInt32 ulRsv10;           // 予約
            public UInt32 ulSubframeSize;       // サブフレーム本体のサイズ、4Byte単位
            public UInt32 ulSubframeTime;       // [19: 0]: STime サブフレームのタイム・スタンプ
                                                // [26:24]: STimeUnit 上記タイム・スタンプの時間単位設定を示します。本サブフレーム取り込み時のDRB_SHG_STIME_UNITレジスタ設定の値
            public UInt32 ulSubframeCnt;        // [15: 0]: SCntNum サブフレームの番号を示します。一連のフレームの中で先頭のサブフレームは値’0’となります。
                                                // [31:16]: SCntLose 直前に失われたサブフレームの数を示します。
            public UInt32 ulFrameCnt;           // [15: 0]: FCntNum フレームの番号を示します。先頭のサブフレームは値’0’となります。
                                                // [23]: SLastFlag 一連のフレーム取り込みにおいて、最後のサブフレームかどうかを示します。
                                                // [26:24]: SmplClkSel 本サブフレーム取り込み時のDRB_SMPL_CLK_SELレジスタ設定の値
            public UInt32 ulFrameFmt;           // [23: 0]: BgmSygSel 本サブフレーム取り込み時のDRB_BGM_SYG_SELレジスタ設定の値
                                                // [26:24]: BgpSygCnt 本サブフレーム取り込み時のDRB_BGP_SYG_CNTレジスタ設定の値
                                                // [31:28]: BgpError [28]: DRB_EXC_BGP_SRC_OVERFLOWレジスタの値を示します。
                                                //					[31]: DRB_BGP_EXC_FBP_LOGICレジスタの値を示します。
            public UInt32 ulFrameSmplCnt;       // 一連のフレーム取り込みにおいてサンプリングしたデータの数を示します。
                                                // 途中のサブフレームの場合は、フレームの先頭からの累積数でおおよその値を示します。
                                                // 最後のサブフレームの場合には、フレーム全体での正確なサンプリング総数を示します。
            public UInt32 ulFrameTrgMode;       // 本サブフレーム取り込み時のトリガ・モードを示します。
                                                // （現状はモード’0’のみに対応なので、32’h0000_0000となります。）
            public UInt32 ulRsv11;           // 予約
            public UInt32 ulRsv12;           // 予約
            public UInt32 ulRsv13;           // 予約
            public UInt32 ulRsv14;           // 予約
            public UInt32 ulRsv15;           // 予約
            public UInt32 ulRsv16;           // 予約
            public UInt32 ulRsv17;           // 予約
            public UInt32 ulRsv18;           // 予約
            public UInt32 ulRsv19;           // 予約
            public UInt32 ulFitPriComStts;  // F-Info Table(Primary Side)のCommon Statusへの参照
            public UInt32 ulFitPriEachCnts; // [ 7: 0]:FRcnt Frame読出し側カウント値（新たに読出し可能なFrameの数）
                                    // [15: 8]:FWcnt Frame書込み側カウント値
                                    // [23:16]:FAcnt Frame Table上の実際のカウント値
                                    // [31:24]:FAcntMax Frame Table上で管理する最大数
        }

        /// <summary>
        /// SVIUSB20_Init API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_Init")]
        public static extern void Init();

        /// <summary>
        /// SVIUSB20_End API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_End")]
        public static extern void End();

        /// <summary>
        /// SVIUSB20_Open API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_Open")]
        public static extern UInt32 Open(UInt32 ulAppWho);

        /// <summary>
        /// SVIUSB20_Close API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_Close")]
        public static extern UInt32 Close(UInt32 ulAppWho);

        /// <summary>
        /// SVIUSB20_SetParam API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_SetParam")]
        public static extern UInt32 SetParam(ref SET_PARAM pParam);

        /// <summary>
        /// SVIUSB20_GetStatus API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_GetStatus")]
        public static extern UInt32 GetStatus(ref GET_STATUS pStatus);

        /// <summary>
        /// SVIUSB20_GetStatus2 API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_GetStatus2")]
        public static extern UInt32 GetStatus2(ref UInt32 pulStatus);

        /// <summary>
        /// SVIUSB20_ReadReg API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_ReadReg")]
        public static extern UInt32 ReadReg(UInt32 ulAdr, ref UInt32 pulStatus);

        /// <summary>
        /// SVIUSB20_RecStartEx API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_RecStartEx")]
        public static extern UInt32 RecStartEx(ref REC_PARAM_EX pParam);

        /// <summary>
        /// SVIUSB20_RecStartEx2 API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_RecStartEx2")]
        public static extern UInt32 RecStartEx2(ref REC_PARAM_EX pParam);

        /// <summary>
        /// SVIUSB20_RecStop API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_RecStop")]
        public static extern UInt32 RecStop();

        /// <summary>
        /// SVIUSB20_RecGetHeaderEx API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_RecGetHeaderEx")]
        public static extern UInt32 RecGetHeaderEx(ref REC_HEADER_EX pHeader);

        /// <summary>
        /// SVIUSB20_RecGetData API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_RecGetData")]
        public static extern UInt32 RecGetData(IntPtr lpDataBuf, UInt32 ulRcvLen, ref UInt32 pulRetRcvLen);

        /// <summary>
        /// SVIUSB20_I2CBlockWrite API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_I2COneBlockWrite")]
        public static extern UInt32 I2CBlockWrite(UInt32 ulSlaveAdr, UInt32 ulLen, UInt32 ulWriteMode, byte[] pucSendBuf);

        /// <summary>
        /// SVIUSB20_I2CBlockRead API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_I2COneBlockRead")]
        public static extern UInt32 I2CBlockRead(UInt32 ulSlaveAdr, UInt32 ulLen, UInt32 ulReadMode, byte[] pucRcvBuf);

        /// <summary>
        /// SVIUSB20_ReadReg API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_ReadReg")]
        public static extern UInt32 ReadReg(UInt32 ulFpgaAddress, UInt32[] pulBuf);

        /// <summary>
        /// SVIUSB20_PutReg API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_WriteReg")]
        public static extern UInt32 WriteReg(UInt32 ulFpgaAddress, UInt32 ulVal);

        /// <summary>
        /// SVIUSB20_GetVersion API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_GetVersion")]
        public static extern UInt32 GetVersion(byte[] pucVerBuf);

        /// <summary>
        /// SVIUSB20_GetLastErrorString API呼び出し定義
        /// </summary>
        [DllImport("SVIUSB20.dll", EntryPoint = "SVIUSB20_GetLastErrorString", CharSet = CharSet.Ansi)]
        public static extern void GetLastErrorString(UInt32 ulErrorCode, StringBuilder buf, ulong bufsize);
    }
}
