﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using _SVIControl;
using SVCanData;
using sviusb20_dll;
using SVUtil;

namespace SVImonCAN
{
    /// <summary>
    /// モニタリング用
    /// </summary>
    public partial class MainWindow : Form
    {
        /// <summary>
        /// フレームサイズ
        /// </summary>
        Size mFrameSize;
        /// <summary>
        /// 縮尺値
        /// </summary>
        double mScale = 1.0;
        /// <summary>
        /// 排他処理
        /// </summary>
        object mPaintLock = new object();

        /// <summary>
        /// 最大１個のキューバッファを作成する
        /// </summary>
        BlockingCollection<byte[]> mQueBuffer;

        /// <summary>
        /// CAN0レコード
        /// </summary>
        List<SvCanRecord> mCan0Record = new List<SvCanRecord>();
        /// <summary>
        /// CAN1レコード
        /// </summary>
        List<SvCanRecord> mCan1Record = new List<SvCanRecord>();

        /// <summary>
        /// ダンプ用メモリ
        /// </summary>
        System.IO.MemoryStream mDumpMemory = new System.IO.MemoryStream();

        /// <summary>
        /// ビットマップ
        /// </summary>
        public DIBitmap mDIBitmap = new DIBitmap();


        /// <summary>
        /// フレーム解析フラグ群
        /// </summary>
        class FrameAnalyzeFlag
        {
            /// <summary>
            /// FPSカウント開始時刻
            /// </summary>
            public DateTime mFpsDate;
            /// <summary>
            /// ピクセルデータ
            /// </summary>
            public byte[] mPixels = null;
            /// <summary>
            /// FPSカウンタ
            /// </summary>
            public int mFPSCount = 0;
            /// <summary>
            /// 前回のFPSカウンタ
            /// </summary>
            public int mOldFPSCount = 0;
            /// <summary>
            /// ビットシフト
            /// </summary>
            public int mRBitShift = 0;

            /// <summary>
            /// オリジナルFPS
            /// </summary>
            public double mOrgFPS;

            /// <summary>
            /// 時間差分
            /// </summary>
            TimeSpan mSpan;

            /// <summary>
            /// FPS時間の取得
            /// </summary>
            /// <returns>経過した場合はtrue,それ以外はfalse</returns>
            public bool IsFPSTime()
            {
                mSpan = DateTime.Now - mFpsDate;
                if (mSpan.TotalSeconds >= 4 )
                {
                    mFpsDate = DateTime.Now;
                    return (true);

                }
                return (false);
            }

            /// <summary>
            /// FPS値
            /// </summary>
            public double FPS
            {
                get
                {
                    if (mFPSCount == 0)
                        return (0);
                    double ret = (double)mFPSCount * 1000 / (double)mSpan.TotalMilliseconds;
                    return (ret);
                }
            }
        }

        /// <summary>
        /// モニタリングの開始
        /// </summary>
        void Monitoring()
        {
            // SVIコントロールクラスのオブジェクトを作成
            SVIControl svi_control = new SVIControl();

            // SVドライバーをオープンする
            int iret = SVIControl.Init();
            if (iret != 0)
            {
                Console.Out.WriteLine("Error SVIControl.Init() ret=" + iret.ToString());
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.Init() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                return;
            }
            Console.Out.WriteLine("SV driver open success");

            // VHsyncの極性指定
            uint uiVal = 0;
            if(Program.mEnv.mRecOption.mVideoVSyncLevel == 1)
            {
                uiVal |= 0x8;
            }
            if (Program.mEnv.mRecOption.mVideoHSyncLevel == 1)
            {
                uiVal |= 0x4;
            }
            UInt32 uret;
            UInt32[] uiBuff = new UInt32[8];
            uret = SVIControl.FpgaBlockRead(0x10000004, 1, uiBuff);
            uiBuff[0] &= 0xffeffff3; // bit20,bit3,bit2-clear
            uiBuff[0] |= uiVal;
            uret = SVIControl.FpgaBlockWrite(0x10000004, 1, uiBuff);

            // SVボードの情報を取得する
            NativeMethods.GET_STATUS sts = new NativeMethods.GET_STATUS();

            for( int i=0; i < Program.mEnv.mRecOption.mTimeOut; i++)
            {
                uret = SVIControl.GetStatusValue(out sts);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Console.Out.WriteLine("Error SVIControl.GetStatus() ret=" + uret.ToString());
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.GetStatus() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                    SVIControl.End();
                    return;
                }
                Console.Out.WriteLine("GetStatus ulCamFps={0}, ulOrgSizeW={1}, ulOrgSizeH={2}", sts.ulCamFps, sts.ulOrgSizeW, sts.ulOrgSizeH);
                if (sts.ulCamFps != 0 && sts.ulOrgSizeW != 0 && sts.ulOrgSizeH != 0) break;

                System.Threading.Thread.Sleep(1000);
            }

            if (sts.ulCamFps == 0 || sts.ulOrgSizeW == 0 || sts.ulOrgSizeH == 0)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.GetStatus() error capture" , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }

            // 画像サイズは自動以外は設定
            if (!Program.mEnv.mRecOption.mPictureAuto)
            {
                sts.ulOrgSizeW = (uint)Program.mEnv.mRecOption.mPictureWidth;
                sts.ulOrgSizeH = (uint)Program.mEnv.mRecOption.mPictureHeight;
                sts.ulCamFps = (uint)Program.mEnv.mRecOption.mPictureFPS * 100;
            }

            Console.Out.WriteLine("SVIControl.GetStatusValue() ok");
            Console.Out.WriteLine(" - basicStatus = " + sts.ulBasicStatus.ToString());
            Console.Out.WriteLine(" - orgWidth    = " + sts.ulOrgSizeW.ToString());
            Console.Out.WriteLine(" - orgHeight   = " + sts.ulOrgSizeH.ToString());
            Console.Out.WriteLine(" - camFps      = " + sts.ulCamFps.ToString());
            // ボードのアイドルと画像入力の有無を検証する
            if (sts.ulBasicStatus != 1 || sts.ulOrgSizeW == 0 || sts.ulOrgSizeH == 0)
            {
                Console.Out.WriteLine("Error can't capture");
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error can't capture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }

            //
            this.Invoke((MethodInvoker)delegate ()
            {
                statusLabelSize.Text = string.Format("Width:{0} Height:{1}", sts.ulOrgSizeW, sts.ulOrgSizeH);
                statusLabelFPS.Text = string.Format("fps : {0:N2}/0", (double)sts.ulCamFps/100.0);
            });


            // レコーディング用受信バッファ64MBの確保
            IntPtr SubFrameBuffer = Marshal.AllocHGlobal((IntPtr)SUB_FRAME_SIZE);

            // レコーディング開始を通知する
            uret = SVIControl.RecStart(Convert.ToUInt32(Program.mEnv.mRecOption.mVideoCAN0), Convert.ToUInt32(Program.mEnv.mRecOption.mVideoCAN1)); // CAN_0&CAN_1 Rec On
            if (uret != NativeMethods.RET_NORMAL)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.RecStart() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }

            // キューバッファの作成(1)
            mQueBuffer = new BlockingCollection<byte[]>(1);

            //
            // 記録開始
            //
            try
            {
                //
                FrameAnalyzeFlag frameAnalyzeFlag = new FrameAnalyzeFlag();
                NativeMethods.REC_HEADER_EX fmh;

                uint RcvSubFrameSize = 0;

                mMonitoringAbort = false;

                frameAnalyzeFlag.mFpsDate = DateTime.Now;
                frameAnalyzeFlag.mPixels = new byte[sts.ulOrgSizeW*sts.ulOrgSizeH];    // 256階調
                if (Program.mEnv.mRecOption.mDisplayReverse)
                {
                    mDIBitmap.CreateDIB((int)sts.ulOrgSizeW, -(int)sts.ulOrgSizeH, 8);
                }
                else
                {
                    mDIBitmap.CreateDIB((int)sts.ulOrgSizeW, (int)sts.ulOrgSizeH, 8);
                }
                frameAnalyzeFlag.mOrgFPS = (double)sts.ulCamFps / 100;
                frameAnalyzeFlag.mRBitShift = Program.mEnv.mRecOption.mDisplayLsbPack ? 0 : 4;
                mFrameSize = new Size((int)sts.ulOrgSizeW, (int)sts.ulOrgSizeH);

                // サイズ変更
                this.Invoke((MethodInvoker)delegate ()
                {
                    pictureBox.Width = (int)Math.Round(sts.ulOrgSizeW * mScale);
                    pictureBox.Height = (int)Math.Round(sts.ulOrgSizeH * mScale);
                });

                // 
                // レコーディングスタート(キューバッファ)
                //
                System.Threading.Tasks.Task.Run(() =>
                {
                    // mCanAnalyze.Can_serch_start();

                    candec.Candec cd = new candec.Candec();

                    bool first = true;

                    cd.Can_Start(Program.mEnv.mRecOption.mCanThreshold1, Program.mEnv.mRecOption.mCanThreshold2);
                    cd.Can_set_bitrate(0, Program.mEnv.mRecOption.mCanBps, Program.mEnv.mRecOption.mCAN_FDbps);
                    cd.Can_set_bitrate(1, Program.mEnv.mRecOption.mCanBps, Program.mEnv.mRecOption.mCAN_FDbps);
                    //cd.Can_Start(Program.mEnv.mRecOption.mCanThreshold1,Program.mEnv.mRecOption.mCanThreshold2);

                    do
                    {
                        byte[] buf;

                        // バッファが取得できるまで最大10msec待つ
                        if (mQueBuffer.TryTake(out buf, 10))
                        {
                            try
                            {
#if true
                                Parallel.Invoke
                                (
                                    () =>
                                    {
                                        MonitoringAnalyzeFrame(buf, (uint)buf.Length, ref frameAnalyzeFlag);
                                    },
                                    () =>
                                    {
                                        UInt16[] wbuf = new UInt16[buf.Length / 2];
                                        /// byte -> wbuf 変換
                                        for (int i = 0; i < buf.Length; i += 2)
                                        {
                                            wbuf[i / 2] = (UInt16)(((buf[i + 1] << 8)) | (buf[i] & 0xff));
                                        }

                                        // ビットレートの算出
                                        if ( first)
                                        {
                                            long[] can0_buf = new long[40960];
                                            long[] can1_buf = new long[40960];
                                            //最初のコールはCANのビットレートを求める
                                            cd.Can_mon_decode(ref wbuf, (uint)wbuf.Length, ref can0_buf, ref can1_buf);

                                            int ch0_bitrate = cd.Can_get_bitrate(0);
                                            int ch1_bitrate = cd.Can_get_bitrate(1);
                                            this.Invoke((MethodInvoker)delegate ()
                                            {
                                                mCan0Window.AppendTitle(string.Format(" / {0}K", ch0_bitrate));
                                                mCan1Window.AppendTitle(string.Format(" / {0}K", ch1_bitrate));
                                            });
                                            first = false;
                                            can0_buf = null;
                                            can1_buf = null;
                                        }

                                        // 解析
                                        CanAnalyzeFrame(cd,wbuf);
                                    }

                                );
#else
                                MonitoringAnalyzeFrame(buf, (uint)buf.Length, ref frameAnalyzeFlag);
#endif

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("error={0]", e.Message);
                            }
                            // メモリ強制開放
                            buf = null;
                        }

                    } while (!mMonitoringAbort);

                    cd.Can_End();
                });


                // 指定時間計測
                do
                {
                    // 中止
                    if (mMonitoringAbort) break;

                    uret = SVIControl.RecGetData(SubFrameBuffer, SUB_FRAME_SIZE, ref RcvSubFrameSize);
                    if (uret != NativeMethods.RET_NORMAL)
                    {
                        Console.Out.WriteLine("Error SVIControl.RecGetData() ret=" + uret.ToString());
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVIControl.RecGetData() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }

                    // フレーム
                    fmh = (NativeMethods.REC_HEADER_EX)Marshal.PtrToStructure(SubFrameBuffer, typeof(NativeMethods.REC_HEADER_EX));
                    if ( fmh.ulStatus == 0)
                    {
                        Console.WriteLine("fmh.ulStatus == 0");
                        continue;
                    }
                    Console.WriteLine("fmh.ulFrameSmplCnt={0}", fmh.ulFrameSmplCnt);
                   
                    byte[] SubFrameCopy = new byte[RcvSubFrameSize - 512];
                    Marshal.Copy(SubFrameBuffer + (512 / 1), SubFrameCopy, 0, (int)(RcvSubFrameSize - 512));

                    // エラー値のカウント
                    //if ((fmh.ulFrameFmt & (1 << 28)) != 0)
                    if ((fmh.ulSubframeCnt & 0xffff0000) != 0)
                    {
                        int err_count = (int)((fmh.ulSubframeCnt >> 16) & 0xffff);
                        Console.WriteLine("err_count={0}", err_count);
                    }

                    // バッファを最新にするため、キューを空にする
                    // ※キューは１個しかない
                    while (mQueBuffer.Count>0)
                    {
                        byte [] tmp;
                        mQueBuffer.TryTake(out tmp, 1);
                        tmp = null;
                    }
                    mQueBuffer.TryAdd(SubFrameCopy, 10);

                } while (true);

                // レコーディング停止を通知する
                uret = SVIControl.RecStop();
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Console.Out.WriteLine("Error SVIControl.RecStop() ret=" + uret.ToString());
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.RecStop() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }
                SVIControl.End();
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Console.Out.WriteLine("Exception =" + ex.Message);
                });
            }
            finally
            {

                mQueBuffer.Dispose();
            }

            // メモリ開放
            if (SubFrameBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(SubFrameBuffer);
            }

            Console.WriteLine("Monitoring End!!!");
        }

        /// <summary>
        /// モニタ表示用解析開始
        /// </summary>
        /// <param name="buffer">バッファ</param>
        /// <param name="buffer_size">bufferサイズ</param>
        /// <param name="frameAnalyzeFlag">フレーム解析用</param>
        void MonitoringAnalyzeFrame(byte[] buffer, uint buffer_size, ref FrameAnalyzeFlag frameAnalyzeFlag)
        {
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            wordSyncActive_V = Program.mEnv.mRecOption.VSyncMask;
            wordSyncActive_H = Program.mEnv.mRecOption.HSyncMask;
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            bool enableFrame = false;
            bool enableLine = false;
            int pixcelCount = 0;
            int lineCount = 0;
            int frameCount = 0;                         // フレームカウンター
            int vertical = 0;                           // Vブランキング
            int horizontal = 0;                         // Hブランキング
            bool enableVBlank = false;                  // Vブランキング期間判定

            IntPtr handle = mDIBitmap.Handle;

            double org_fps = frameAnalyzeFlag.mOrgFPS;

            // 解析
            for (uint i = 0; i < buffer_size; i += 2)
            {
                // ダンプ用バッファの追加
                if (mDumpWindow.Visible)
                {
                    mDumpMemory.WriteByte(buffer[i]);
                    mDumpMemory.WriteByte(buffer[i + 1]);
                }

                UInt16 wordCurrent = (UInt16)(((buffer[i + 1] << 8) & 0xff00) | (buffer[i] & 0xff));
                if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                {
                    if (enableFrame == false)
                    {
                        //有効フレーム開始
                        enableFrame = true;
                        //画素数カウント初期化
                        pixcelCount = 0;
                        //有効ラインカウント初期化
                        lineCount = 0;
                        // フレームカウンタインクリメント
                        frameCount++;

                        if (mDumpWindow.Visible)
                        {
                            mDumpMemory.SetLength(0);
                        }
                    }

                    if (enableLine == false)
                    {
                        //ライン有効
                        enableLine = true;
                    }

                    //画素数カウント
                    if (pixcelCount < frameAnalyzeFlag.mPixels.Length)
                    {
                        frameAnalyzeFlag.mPixels[pixcelCount] = (byte)(wordCurrent>> frameAnalyzeFlag.mRBitShift);
                        pixcelCount++;
                    }
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V) //VSYNCのみ Active
                {
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        enableLine = false;
                        //有効ラインカウントアップ

                        lineCount++;
                        pixcelCount = mFrameSize.Width * lineCount;
                    }
                    // 無効期間
                    horizontal++;
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_H) == wordSyncActive_H) //HSYNCのみ Active
                {
                    //Vブランキング期間
                    enableFrame = false;
                    // Vブランキング期間カウント判定
                    if (enableVBlank == true)
                    {
                        // カウント停止
                        enableVBlank = false;
                        // カウントUp
                        vertical++;
                    }
                }
                else
                {
                    //VHブランキング期間

                    // 2005.11.2 V3.31 VHハイ状態からいきなりVHロー状態になった時の対処
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        //有効ラインカウントアップ
                        lineCount++;
                        //Console.WriteLine("frameAnalyzeFlag.m64Pos={0},i={1}", frameAnalyzeFlag.m64Pos, i);
                    }

                    // Vブランキング区間カウント
                    if (enableVBlank == false)
                    {
                        // カウント開始フラグOn
                        enableVBlank = true;
                    }

                    // フレーム終了判定
                    if (enableFrame)
                    {
                        // 表示サイズが同じもの
                        if ( lineCount == mFrameSize.Height && (pixcelCount/ lineCount) == mFrameSize.Width )
                        {

                            //if (pixcelCount < frameAnalyzeFlag.mPixels.Length)
                            //{
                            //    Console.WriteLine("pixcelCount({0}) < frameAnalyzeFlag.mPixels.Length({1})", pixcelCount, frameAnalyzeFlag.mPixels.Length);
                            //    Console.WriteLine("frameAnalyzeFlag.m64Start={0},frameAnalyzeFlag.m64Count={1},frameAnalyzeFlag.m64Pos={2}", frameAnalyzeFlag.m64Start, frameAnalyzeFlag.m64Count, frameAnalyzeFlag.m64Pos);
                            //}

                            frameAnalyzeFlag.mFPSCount++;
                            int frame_count = frameCount;
                            double fps = -1;

                            mDIBitmap.SetImage(frameAnalyzeFlag.mPixels);

                            // FPS値の更新
                            if (frameAnalyzeFlag.IsFPSTime())
                            {
                                if (frameAnalyzeFlag.mFPSCount != frameAnalyzeFlag.mOldFPSCount)
                                {
                                    fps = frameAnalyzeFlag.FPS;
                                    frameAnalyzeFlag.mOldFPSCount = (int)fps;
                                    frameAnalyzeFlag.mFPSCount = 0;
                                }
                            }
                            // 画像更新
                            using (var g = pictureBox.CreateGraphics())
                            {
                                RedrawBitmap(g, handle);
                            }

                            // FPS値が更新されたら表示
                            if (fps != -1)
                            {
                                statusLabelFPS.Text = string.Format("fps : {0:N2}/{1:N2}", org_fps, fps);
                            }

                            // Dump/CAN0/CAN1ウィンドウのいずれかが表示されている場合のみInvoke処理
                            if (mDumpWindow.Visible)
                            {
                                // 貼付け
                                this.Invoke((MethodInvoker)delegate ()
                                {
                                    // Visible
                                    if (mDumpWindow.Visible)
                                    {
                                        mDumpWindow.SetDumpBuffer(mDumpMemory.ToArray());
                                    }
                                });
                            }
                        }
                    }
                    enableFrame = false;
                }
            }
        }

        /// <summary>
        /// 再表示処理
        /// </summary>
        /// <param name="g">グラフィック</param>
        void RedrawBitmap(Graphics g, IntPtr frame_handle)
        {
            // 排他ロック
            lock (mPaintLock)
            {
                IntPtr dc = Win32.User32.GetDC(IntPtr.Zero);
                IntPtr src = Win32.Gdi32.CreateCompatibleDC(dc);
                IntPtr dst = g.GetHdc();

                // ビットマップを張り付ける
                if (mScale < 1.0)
                {
                    Win32.Gdi32.SetStretchBltMode(dst, Win32.Gdi32.StrechModeFlags.HALFTONE);
                }
                Win32.Gdi32.SelectObject(src, frame_handle);
                Win32.Gdi32.StretchBlt(dst, 0, 0, (int)Math.Round(mFrameSize.Width * mScale), (int)Math.Round(mFrameSize.Height * mScale),
                                        src, 0, 0, mFrameSize.Width, mFrameSize.Height, Win32.Gdi32.TernaryRasterOperations.SRCCOPY);

                Win32.Gdi32.DeleteDC(src);
                g.ReleaseHdc(dst);

                Win32.User32.ReleaseDC(IntPtr.Zero, dc);
            }
        }

        /// <summary>
        /// 画像のスケール
        /// </summary>
        void ScalePicture()
        {
            // ロック
            lock (mPaintLock)
            {
                pictureBox.Width = (int)Math.Round(mFrameSize.Width * mScale);
                pictureBox.Height = (int)Math.Round(mFrameSize.Height * mScale);
                pictureBox.Refresh();
            }
            ScaleUpdate();
        }

        /// <summary>
        /// CANデータの計算
        /// </summary>
        /// <param name="buf"></param>
        void CanAnalyzeFrame(candec.Candec cd, UInt16[] wbuf)
        {
            long[] can0_buf = new long[40960];
            long[] can1_buf = new long[40960];

            // 取得
            int ret = cd.Can_Start_next(Program.mEnv.mRecOption.mCanThreshold1, Program.mEnv.mRecOption.mCanThreshold2);
            ret = cd.Can_mon_decode(ref wbuf, (uint)wbuf.Length, ref can0_buf, ref can1_buf);

            // クリア
            mCan0Record.Clear();
            mCan1Record.Clear();

            if ( (ret & 2) != 0 )
            {
                MakeCanRecord(can0_buf, mCan0Record);
            }
            if ( (ret & 4) != 0)
            {
                MakeCanRecord(can1_buf, mCan1Record);
            }

            // Dump/CAN0/CAN1ウィンドウのいずれかが表示されている場合のみInvoke処理
            if (mCan0Window.Visible || mCan1Window.Visible)
            {
                // 貼付け
                this.Invoke((MethodInvoker)delegate ()
                {
                    if (mCan0Window.Visible)
                    {
                        mCan0Window.SetCanRecord(mCan0Record);
                    }
                    if (mCan1Window.Visible)
                    {
                        mCan1Window.SetCanRecord(mCan1Record);
                    }
                });
            }
        }

        /// <summary>
        /// CAN　BIT バッファ
        /// </summary>
        /// <param name="can_buf">CANBIT バッファ</param>
        /// <param name="can_rec">CANレコード</param>
        /// <param name="max_rec">最大10レコード</param>
        void MakeCanRecord(long []can_buf, List<SvCanRecord> can_rec, int max_rec=10)
        {
            int top_index = 0;
            int last_index, length;
            SvCanRecord rec;

            do
            {
                // 終端検索
                last_index = Array.IndexOf(can_buf, -1, top_index);
                if (last_index < 0) break;

                length = last_index - top_index + 1; // デリミタ含む

                // 1パケット取得
                long[] can_packet = new long[length];
                Array.ConstrainedCopy(can_buf, top_index, can_packet, 0, length);

                rec = SvCapPacket.PacketConvert(can_packet);
                if (rec != null)
                {
                    can_rec.Add(rec);
                    if (can_rec.Count >= max_rec) break;
                }

                top_index = last_index + 1;
            }
            while (true);

        }
    }
}
