﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _SVIControl;
using sviusb20_dll;

namespace SVImonCAN
{
    /// <summary>
    /// レコーディング用
    /// </summary>
    public partial class MainWindow : Form
    {

        /// <summary>
        /// レコーディング開始
        /// </summary>
        /// <remarks>
        /// プレビュー処理部分については"this.Invoke((MethodInvoker)delegate ()"でGUIの処理を実行する
        /// </remarks>
        private void Recording(string file_name)
        {
            // SVIコントロールクラスのオブジェクトを作成
            SVIControl svi_control = new SVIControl();

            // SVドライバーをオープンする
            int iret = SVIControl.Init();
            if (iret != 0)
            {
                Console.Out.WriteLine("Error SVIControl.Init() ret=" + iret.ToString());
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.Init() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                return;
            }
            Console.Out.WriteLine("SV driver open success");

            // VHsyncの極性指定
            uint uiVal = 0;
            if (Program.mEnv.mRecOption.mVideoVSyncLevel == 1)
            {
                uiVal |= 0x8;
            }
            if (Program.mEnv.mRecOption.mVideoHSyncLevel == 1)
            {
                uiVal |= 0x4;
            }
            UInt32 uret;
            UInt32[] uiBuff = new UInt32[8];
            uret = SVIControl.FpgaBlockRead(0x10000004, 1, uiBuff);
            uiBuff[0] &= 0xffeffff3; // bit20,bit3,bit2-clear
            uiBuff[0] |= uiVal;
            uret = SVIControl.FpgaBlockWrite(0x10000004, 1, uiBuff);

            // SVボードの情報を取得する
            NativeMethods.GET_STATUS sts = new NativeMethods.GET_STATUS();

            // ボードステータス取得
            uret = SVIControl.GetStatusValue(out sts);
            if (uret != NativeMethods.RET_NORMAL)
            {
                Console.Out.WriteLine("Error SVIControl.GetStatus() ret=" + uret.ToString());
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.GetStatus() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }
            // 画像サイズは自動以外は設定
            if (!Program.mEnv.mRecOption.mPictureAuto)
            {
                sts.ulOrgSizeW = (uint)Program.mEnv.mRecOption.mPictureWidth;
                sts.ulOrgSizeH = (uint)Program.mEnv.mRecOption.mPictureHeight;
                sts.ulCamFps = (uint)Program.mEnv.mRecOption.mPictureFPS * 100;
            }
            // sステータスバー更新
            statusLabelSize.Text = string.Format("Width:{0} Height:{1}", sts.ulOrgSizeW, sts.ulOrgSizeH);
            statusLabelFPS.Text = string.Format("fps : {0:N2}", (double)sts.ulCamFps / 100.0);

#if false
            for (int i = 0; i < Program.mEnv.mRecOption.mTimeOut; i++)
            {
                uret = SVIControl.GetVideoMonitor(ref usts);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Console.Out.WriteLine("Error SVIControl.GetVideoMonitor() ret=" + uret.ToString());
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.GetVideoMonitor() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                    SVIControl.End();
                    return;
                }
                if ((usts & 0xc00) == 0)
                {
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }

                uret = SVIControl.GetStatusValue(out sts);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Console.Out.WriteLine("Error SVIControl.GetStatus() ret=" + uret.ToString());
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.GetStatus() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                    SVIControl.End();
                    return;
                }
                if (sts.ulCamFps != 0 && sts.ulOrgSizeW != 0 && sts.ulOrgSizeH != 0) break;

                System.Threading.Thread.Sleep(1000);
            }

            if (sts.ulCamFps == 0 || sts.ulOrgSizeW == 0 || sts.ulOrgSizeH == 0)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.GetStatus() error capture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }

            // 画像サイズは自動以外は設定
            if (!Program.mEnv.mRecOption.mPictureAuto)
            {
                sts.ulOrgSizeW = (uint)Program.mEnv.mRecOption.mPictureWidth;
                sts.ulOrgSizeH = (uint)Program.mEnv.mRecOption.mPictureHeight;
                sts.ulCamFps = (uint)Program.mEnv.mRecOption.mPictureFPS * 100;
            }

            Console.Out.WriteLine("SVIControl.GetStatusValue() ok");
            Console.Out.WriteLine(" - basicStatus = " + sts.ulBasicStatus.ToString());
            Console.Out.WriteLine(" - orgWidth    = " + sts.ulOrgSizeW.ToString());
            Console.Out.WriteLine(" - orgHeight   = " + sts.ulOrgSizeH.ToString());
            Console.Out.WriteLine(" - camFps      = " + sts.ulCamFps.ToString());
            // ボードのアイドルと画像入力の有無を検証する
            if (sts.ulBasicStatus != 1 || sts.ulOrgSizeW == 0 || sts.ulOrgSizeH == 0)
            {
                Console.Out.WriteLine("Error can't capture");
                Win32.SVMessageBox.Show(this, "Error can't capture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SVIControl.End();
                return;
            }

            //
            this.Invoke((MethodInvoker)delegate ()
            {
                statusLabelSize.Text = string.Format("Width:{0} Height:{1}", sts.ulOrgSizeW, sts.ulOrgSizeH);
                statusLabelFPS.Text = string.Format("fps : {0:N2}", (double)sts.ulCamFps/100.0);
                mRecMonitor.ErrorCount = 0;
            });
#endif

            // レコーディング用受信バッファ64MBの確保
            IntPtr SubFrameBuffer = Marshal.AllocHGlobal((IntPtr)SUB_FRAME_SIZE);

            // レコーディング開始を通知する
            uret = SVIControl.RecStart(Convert.ToUInt32(Program.mEnv.mRecOption.mVideoCAN0), Convert.ToUInt32(Program.mEnv.mRecOption.mVideoCAN1)); // CAN_0&CAN_1 Rec On
            if (uret != NativeMethods.RET_NORMAL)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVIControl.RecStart() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVIControl.End();
                return;
            }

            //
            // 記録開始
            //
            try
            {
                //
                SVCanData.SVCanRecData recData = new SVCanData.SVCanRecData();
                NativeMethods.REC_HEADER_EX fmh;
                int err_count = 0;

                // CAN閾値
                int[] threshold = new int[] { Program.mEnv.mRecOption.mCanThreshold1, Program.mEnv.mRecOption.mCanThreshold2 };
                int[] bps = new int[] { Program.mEnv.mRecOption.mCanBps, Program.mEnv.mRecOption.mCAN_FDbps };

                // 初期化
                //recData.FPS = (int)sts.ulCamFps / 10;
                recData.FPS = (int)sts.ulCamFps / 1;
                //recData.Width = (int)sts.ulCutSizeW;
                //recData.Height = (int)sts.ulCutSizeH;
                recData.Width = (int)sts.ulOrgSizeW;
                recData.Height = (int)sts.ulOrgSizeH;
                //recData.FPS = (int)300;
                //recData.Width = (int)1280;
                //recData.Height = (int)960;
                recData.HsyncPorarity = Program.mEnv.mRecOption.mVideoHSyncLevel;
                recData.VsyncPorarity = Program.mEnv.mRecOption.mVideoVSyncLevel;

                recData.VersionOfFirmware = (int)sts.ulFWVersion;
                recData.VersionOfHardware = (int)sts.ulHWVersion;

                Size frame_size = new Size((int)sts.ulCutSizeW, (int)sts.ulCutSizeH);

                int lostFrameCnt = 0;

                // ファイルオープンエラー
                if ( recData.Open(file_name, threshold, bps) == false )
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.RecStart() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                    SVIControl.End();
                    return;
                }

                // 記録時間タイマー起動
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
#if STOPW
                var sw2 = new System.Diagnostics.Stopwatch();
#endif
                uint RcvSubFrameSize = 0;
                int progress_pos = 0, progress_pos_old = 0;
                long total_msec = (Program.mEnv.mRecOption.mVideoRecoringTime * 1000);   // 記録時間(msec)

                // 最大時間が有効でない場合はtotal_msecを最大にする(無限)
                if (!Program.mEnv.mRecOption.mMaxTime)
                {
                    total_msec = int.MaxValue;
                }

                total_msec = (Program.mEnv.mRecOption.mTimeOut * 1000);
                int rec_start_flag = 0; // 0:録画開始前 1:録画実行中

                // 指定時間計測
                do
                {
                    // 中止
                    if (this.mRecoringAbort) break;
                    if (this.mRecoringStop) break;

#if STOPW
                    sw2.Reset();
                    sw2.Start();
#endif
                    uret = SVIControl.RecGetData(SubFrameBuffer, SUB_FRAME_SIZE, ref RcvSubFrameSize);

                    // タイマをデータ取得直後から開始する
                    if (!sw.IsRunning)
                    {
                        sw.Start();
                    }

                    if(RcvSubFrameSize > 0x200 && rec_start_flag == 0)
                    {
                        rec_start_flag = 1;
                        total_msec = sw.ElapsedMilliseconds + (long)(Program.mEnv.mRecOption.mVideoRecoringTime * 1000);
                        if (!Program.mEnv.mRecOption.mMaxTime)
                        {
                            total_msec = long.MaxValue;
                        }
                    }

#if STOPW
                    sw2.Stop();
                    Console.WriteLine("SVIControl.RecGetData[{0}] {1} msec", RcvSubFrameSize, sw2.ElapsedMilliseconds);
#endif
                    if (uret != NativeMethods.RET_NORMAL)
                    {
                        Console.Out.WriteLine("Error SVIControl.RecGetData() ret=" + uret.ToString());
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVIControl.RecGetData() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }

                    // フレーム
                    fmh = (NativeMethods.REC_HEADER_EX)Marshal.PtrToStructure(SubFrameBuffer, typeof(NativeMethods.REC_HEADER_EX));
                    if (fmh.ulStatus == 0)
                    {
                        uret++;
                        continue;
                    }

                    byte[] SubFrameCopy = new byte[RcvSubFrameSize - 512];
                    Marshal.Copy(SubFrameBuffer + (512 / 1), SubFrameCopy, 0, (int)(RcvSubFrameSize - 512));

                    // エラー値のカウント
                    //if ((fmh.ulFrameFmt & (1 << 28)) != 0)
                    if ((fmh.ulSubframeCnt & 0xffff0000) != 0)
                    {
                        err_count += (int)((fmh.ulSubframeCnt >> 16) & 0xffff);
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            mRecMonitor.ErrorCount = err_count;
                        });
                    }

                    // ファイル書込み
                    recData.Write(SubFrameCopy);

                    // 消失フレーム数
                    lostFrameCnt += (int)(fmh.ulSubframeCnt >> 16);

                    // Max Time
                    if(rec_start_flag != 0)
                    {
                        if (Program.mEnv.mRecOption.mMaxTime)
                        {
                            progress_pos = (int)Math.Min(100, (int)((double)((100 * sw.ElapsedMilliseconds) / total_msec)));
                            if (progress_pos != progress_pos_old)
                            {
                                // 0～100%の進捗情報
                                progress_pos_old = progress_pos;
                                this.Invoke((MethodInvoker)delegate ()
                                {
                                    mRecMonitor.ProgressPOS = progress_pos;
                                });
                            }
                        }
                    }

                    // バッファが空いてれば表示処理を更新
                    if (mRecMonitor.IsNeedBuffer)
                    {
                        mRecMonitor.PreviewFrameBuffer(SubFrameCopy, frame_size);
                    }

                } while (sw.ElapsedMilliseconds <= total_msec);

                // レコーディング停止を通知する
                uret = SVIControl.RecStop();
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Console.Out.WriteLine("Error SVIControl.RecStop() ret=" + uret.ToString());
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVIControl.RecStop() ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }
                SVIControl.End();

                // 欠落フレームの設定
                recData.LostFrameCnt = lostFrameCnt;

                recData.UpdateHeader();
                recData.Close();

                // 中止の場合はデータ削除
                if (this.mRecoringAbort)
                {
                    System.IO.File.Delete(file_name);
                }
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Console.Out.WriteLine("Exception =" + ex.Message);
                });
            }

            // メモリ開放
            if (SubFrameBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(SubFrameBuffer);
            }

            //　結果メッセージ
            this.Invoke((MethodInvoker)delegate ()
            {
                if (this.mRecoringAbort)
                {
                    Win32.SVMessageBox.Show(mRecMonitor, "Cancel Recording.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (this.mRecoringStop)
                {
                    Win32.SVMessageBox.Show(mRecMonitor, "Stop Recording.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Win32.SVMessageBox.Show(mRecMonitor, "Recording OK.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            });
        }
    }
}
