﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _SVIControl;
using sviusb20_dll;

namespace SVImonCAN
{
    public partial class I2cWindow : Form
    {
        private uint m_i2cSlaveAdr = 0;
        private uint m_i2cSubAdr = 0;
        private uint[] m_i2cWriteValue;
        private uint m_i2cReadCount = 0;
        private bool m_i2cWordAddressMode = false;
        private int m_i2cWriteValueCnt = 0;

        /// <summary>
        /// ダイアログに入力された値を読み込む
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void I2cGetParameter()
        {
            if (this.I2cTextBoxSlaveAdr.Text.Length != 0)
            {
                m_i2cSlaveAdr = (uint)Convert.ToInt32(this.I2cTextBoxSlaveAdr.Text, 16);
            }
            if (this.I2cTextBoxSubAdr.Text.Length != 0)
            {
                m_i2cSubAdr = (uint)Convert.ToInt32(this.I2cTextBoxSubAdr.Text, 16);
            }
            if (this.I2cTextBoxReadCount.Text.Length != 0)
            {
                m_i2cReadCount = (uint)Convert.ToInt32(this.I2cTextBoxReadCount.Text, 16);
            }
            m_i2cWordAddressMode = I2cWordAddressMode.Checked;

            m_i2cWriteValueCnt = 0;
            if (this.I2cTextBoxWriteVal.Text.Length != 0)
            {
                string[] splitStr = this.I2cTextBoxWriteVal.Text.Split(',');
                foreach (string s in splitStr)
                {
                    m_i2cWriteValue[m_i2cWriteValueCnt++] = (uint)Convert.ToInt32(s, 16);
                }
            }
        }
        /// <summary>
        /// 設定ファイルを読み込み、解析し、センサ、FPGAのレジスタライトを行う
        /// </summary>
        /// <param name="FileName"></param>
        private void I2cReadFile(string FileName)
        {
            uint uiSlave = m_i2cSlaveAdr;
            bool bI2cUnit = true; // false:10進, true:16進

            // SVIコントロールクラスのオブジェクトを作成
            SVIControl svi_control = new SVIControl();

            // SVドライバーをオープンする
            int iret = SVIControl.Init();
            if (iret != 0)
            {
                Win32.SVMessageBox.Show(this, "SVM-06 or SVI-09 board not connected to computer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Slave Addressの取得
            uiSlave = m_i2cSlaveAdr;

            // ファイルオープン、解析＆送信
            // StreamReaderクラスをインスタンス化
            StreamReader reader = new StreamReader(FileName, Encoding.GetEncoding("UTF-8"));

            // 最後まで読み込む
            while (reader.Peek() >= 0)
            {
                // 1行読み込む
                string line_str = reader.ReadLine();

                // 空白行か確認
                if (line_str.Length == 0) continue;

                // コメント行か確認
                if (line_str.IndexOf(';') == 0) continue;

                // 1行内のスペース文字とタブ文字を除去する
                line_str = line_str.Replace("\t", "");
                line_str = line_str.Replace(" ", "");

                // 行末にコメントがあれば、それ以降を除去
                int pos = line_str.IndexOf(';');
                if (pos != -1)
                {
                    line_str = line_str.Remove(pos, line_str.Length - pos);
                }

                // 英字を大文字にする
                line_str = line_str.ToUpper();

                // 文字列をカンマで区切る
                string[] line_splt = line_str.Split(',');

                // コマンド行の解析
                if(line_splt[0] == "UNIT")
                {
                    int val = Convert.ToInt32(line_splt[1], 10);
                    if (val == 10) bI2cUnit = false;
                    else if (val == 16) bI2cUnit = true;
                    else bI2cUnit = true;
                }
                else if (line_splt[0] == "SLAVE")
                {
                    if (bI2cUnit == false)
                    {
                        uiSlave = (uint)Convert.ToInt32(line_splt[1], 10);
                    }
                    else
                    {
                        uiSlave = (uint)Convert.ToInt32(line_splt[1], 16);
                    }
                }
                else if (line_splt[0] == "WAIT" || line_splt[0] == "WT")
                {
                    int wt = 0;
                    if (bI2cUnit == false)
                    {
                        wt = Convert.ToInt32(line_splt[1], 10);
                    }
                    else
                    {
                        wt = Convert.ToInt32(line_splt[1], 16);
                    }
                    System.Threading.Thread.Sleep(wt);
                }
                else if (line_splt[0] == "BYTE")
                {
                    m_i2cWordAddressMode = false;
                }
                else if (line_splt[0] == "WORD")
                {
                    m_i2cWordAddressMode = true;
                }
                else
                {
                    // センサまたFPGAのレジスタへライト
                    uint uiSub = 0;
                    if (bI2cUnit == false)
                    {
                        uiSub = (uint)Convert.ToInt32(line_splt[0], 10);
                    }
                    else
                    {
                        uiSub = (uint)Convert.ToInt32(line_splt[0], 16);
                    }
                    if (uiSlave != 0x8)
                    {
                        byte[] bBuff = new byte[256];
                        uint nWordAddressMode = 1;
                        uint uiVal = 0;
                        if (m_i2cWordAddressMode == true) nWordAddressMode = 2;
                        uint nSendCount = 0;
                        for (nSendCount = 1; nSendCount < line_splt.Length; nSendCount++)
                        {
                            if (bI2cUnit == false)
                            {
                                uiVal = (uint)Convert.ToInt32(line_splt[nSendCount], 10);
                            }
                            else
                            {
                                uiVal = (uint)Convert.ToInt32(line_splt[nSendCount], 16);
                            }
                            bBuff[nSendCount-1] = (byte)uiVal;
                        }
                        uint uret = SVIControl.DSPBlockWrite(uiSlave, uiSub, nWordAddressMode, nSendCount-1, bBuff);
                        if (uret != NativeMethods.RET_NORMAL)
                        {
                            Win32.SVMessageBox.Show(this, "I2C Write ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        uint[] uiBuff = new uint[256];
                        uint uiVal = 0;
                        uint nSendCount = 0;
                        for (nSendCount = 1; nSendCount < line_splt.Length; nSendCount++)
                        {
                            if (bI2cUnit == false)
                            {
                                uiVal = (uint)Convert.ToInt32(line_splt[nSendCount], 10);
                            }
                            else
                            {
                                uiVal = (uint)Convert.ToInt32(line_splt[nSendCount], 16);
                            }
                            uiBuff[nSendCount - 1] = uiVal;
                        }
                        uint uret = SVIControl.FpgaBlockWrite(uiSub, nSendCount-1, uiBuff);
                        if (uret != NativeMethods.RET_NORMAL)
                        {
                            Win32.SVMessageBox.Show(this, "FPGA Register Write ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }

                listBox1.Items.Add(line_str);
            }
            reader.Close();

            SVIControl.End();
        }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public I2cWindow()
        {
            InitializeComponent();
            m_i2cWriteValue = new uint[256];
        }
        /// <summary>
        /// 設定ファイルの内容をセンサまたはFPGAへライトする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void I2cBtnSettingFileSend_Click(object sender, EventArgs e)
        {
            // ダイアログ内の入力値を取得
            this.I2cGetParameter();

            // 設定ファイル選択
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                listBox1.Items.Add("Setting File = " + ofd.FileName);
            }
            else
            {
                return;
            }

            // 設定ファイル読み込み、解析、送信
            I2cReadFile(ofd.FileName);
        }
        /// <summary>
        /// センサまたはFPGAのレジスタへライトする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void I2cWrite_Click(object sender, EventArgs e)
        {
            // ダイアログ内の入力値を取得
            this.I2cGetParameter();

            // SVIコントロールクラスのオブジェクトを作成
            SVIControl svi_control = new SVIControl();

            // SVドライバーをオープンする
            int iret = SVIControl.Init();
            if (iret != 0)
            {
                Win32.SVMessageBox.Show(this, "SVM-06 or SVI-09 board not connected to computer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (m_i2cSlaveAdr != 0x8)
            {
                byte[] bBuff = new byte[256];
                uint nWordAddressMode = 1;
                if (m_i2cWordAddressMode == true) nWordAddressMode = 2;
                uint nSendCount = 0;
                for (nSendCount = 0; nSendCount < m_i2cWriteValueCnt; nSendCount++)
                {
                    bBuff[nSendCount] = (byte)m_i2cWriteValue[nSendCount];
                }
                uint uret = SVIControl.DSPBlockWrite((uint)m_i2cSlaveAdr, (uint)m_i2cSubAdr, nWordAddressMode, nSendCount, bBuff);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Win32.SVMessageBox.Show(this, "I2C Write ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                uint uret = SVIControl.FpgaBlockWrite(m_i2cSubAdr, (uint)m_i2cWriteValueCnt, m_i2cWriteValue);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Win32.SVMessageBox.Show(this, "FPGA Register Write ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            SVIControl.End();
            return;
        }
        /// <summary>
        /// センサまたはFPGAのレジスタをリードする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void I2cRead_Click(object sender, EventArgs e)
        {
            this.I2cGetParameter();
            if(m_i2cReadCount == 0)
            {
                Win32.SVMessageBox.Show(this, "Input please Read Count", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // SVIコントロールクラスのオブジェクトを作成
            SVIControl svi_control = new SVIControl();

            // SVドライバーをオープンする
            int iret = SVIControl.Init();
            if (iret != 0)
            {
                Win32.SVMessageBox.Show(this, "SVM-06 or SVI-09 board not connected to computer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (m_i2cSlaveAdr != 0x8)
            {

                byte[] bBuff = new byte[256];
                uint nWordAddressMode = 1;
                if (m_i2cWordAddressMode == true) nWordAddressMode = 2;
                uint uret = SVIControl.DSPBlockRead((uint)m_i2cSlaveAdr, (uint)m_i2cSubAdr, nWordAddressMode, (uint)m_i2cReadCount, bBuff);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Win32.SVMessageBox.Show(this, "I2C Read ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                // 結果を表示
                uint nRecvCount = 0;
                string str = "";
                for (nRecvCount = 0; nRecvCount < m_i2cReadCount; nRecvCount++)
                {
                    if (nRecvCount > 0)
                    {
                        str += ",";
                    }
                    str += bBuff[nRecvCount].ToString("x");
                }
                listBox1.Items.Add(str);
            }
            else
            {
                UInt32[] uiBuff = new UInt32[256];
                uint uret = SVIControl.FpgaBlockRead(m_i2cSubAdr, (uint)m_i2cReadCount, uiBuff);
                if (uret != NativeMethods.RET_NORMAL)
                {
                    Win32.SVMessageBox.Show(this, "FPGA Register Read ret=" + uret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                // 結果を表示
                uint nRecvCount = 0;
                string str = "";
                for (nRecvCount = 0; nRecvCount < m_i2cReadCount; nRecvCount++)
                {
                    if (nRecvCount > 0)
                    {
                        str += ",";
                    }
                    str += uiBuff[nRecvCount].ToString("x8");
                }
                listBox1.Items.Add(str);
            }

            SVIControl.End();
            return;
        }
        /// <summary>
        /// リード内容のリストボックスをクリアする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.I2cGetParameter();
            listBox1.Items.Clear();
        }
    }
}
