﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _SVIControl;
using sviusb20_dll;

namespace SVImonCAN
{
    /// <summary>
    /// メインウィンドウ
    /// </summary>
    public partial class MainWindow : Form
    {
        /// <summary>
        /// ダンプウィンドウ
        /// </summary>
        DumpWindow.DumpWindow mDumpWindow = new DumpWindow.DumpWindow(Program.mEnv);
        /// <summary>
        /// CAN0ウィンドウ
        /// </summary>
        CanWindow.CanWindow mCan0Window = new CanWindow.CanWindow(Program.mEnv, 0);
        /// <summary>
        /// CAN1ウィンドウ
        /// </summary>
        CanWindow.CanWindow mCan1Window = new CanWindow.CanWindow(Program.mEnv, 1);
        /// <summary>
        /// I2Cウィンドウ
        /// </summary>
        I2cWindow mI2cWindow = new I2cWindow();

        /// <summary>
        /// レコディングモニタ
        /// </summary>
        RecMonitor mRecMonitor = new RecMonitor();
        /// <summary>
        /// レコーディング中止
        /// </summary>
        volatile bool mRecoringAbort = false;
        /// <summary>
        /// レコーディング停止
        /// </summary>
        volatile bool mRecoringStop = false;
        /// <summary>
        /// レコーディング中フラグ
        /// </summary>
        volatile bool mRecoring = false;
        /// <summary>
        /// モニタリング中止
        /// </summary>
        volatile bool mMonitoringAbort = false;
        /// <summary>
        /// モニタリング中フラグ
        /// </summary>
        volatile bool mMonitoring = false;

        /// <summary>
        /// フレームサイズ(64M)
        /// </summary>
        private const UInt32 SUB_FRAME_SIZE = (64 * 1024 * 1024);

        /// <summary>
        /// DATチェックプログラム
        /// </summary>
        private string DAT_CHECK_EXE;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// オプション設定
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileOption_Click(object sender, EventArgs e)
        {
            using (var dlg = new RecOption())
            {
                dlg.ShowDialog();
                if (!Program.mEnv.mRecOption.mVideoCAN0)
                {
                    if (mCan0Window.Visible)
                    {
                        mCan0Window.Hide();
                    }
                }
                if (!Program.mEnv.mRecOption.mVideoCAN1)
                {
                    if (mCan1Window.Visible)
                    {
                        mCan1Window.Hide();
                    }
                }
            }
        }

        /// <summary>
        /// 閉じる
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MainWindow_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;

            ScaleUpdate();

            // デバッグ時にはボードをチェックしない
            string board_name;
            if (!GetBoardInfo(out board_name) )
            {
                Win32.SVMessageBox.Show(this, "The capture board is not connected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                this.Close();
            }
            else
            {
                this.Text = string.Format("SVImonCAN [{0}]", board_name);
            }

            // キャンセルイベント
            this.mRecMonitor.CancelEvent += this.MRecMonitor_CancelEvent;
            this.mRecMonitor.StopEvent += this.MRecMonitor_StopEvent;

            // チェック実行モジュール
            string path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            this.DAT_CHECK_EXE = System.IO.Path.Combine(path, "NVdatchk.exe");
        }

        /// <summary>
        /// レコーディングストップイベント
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MRecMonitor_StopEvent(object sender, EventArgs e)
        {
            this.mRecoringStop = true;
        }

        /// <summary>
        /// レコーディングキャンセルイベント
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MRecMonitor_CancelEvent(object sender, EventArgs e)
        {
            this.mRecoringAbort = true;
        }

        /// <summary>
        /// ボード情報の取得
        /// </summary>
        /// <param name="board_name">ボード名</param>
        /// <returns>正常ならtrue,それ以外ならfalseを返却</returns>
        bool GetBoardInfo(out string board_name)
        {
            board_name = "";
//#if DEBUG
//            return (true);
//#endif
            try
            {
                do
                {
                    // SVIコントロールクラスのオブジェクトを作成
                    SVIControl svi_control = new SVIControl();

                    // SVドライバーをオープンする
                    int iret = SVIControl.Init();
                    if (iret != 0)
                    {
                        Console.Out.WriteLine("Error SVIControl.Init() ret=" + iret.ToString());
                        Win32.SVMessageBox.Show(this, "SVI-09 or SVM-06 board not connected to computer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                    UInt32 boardKind = 0;
                    byte[] bBuff;
                    bBuff = new byte[256];
                    uint uret = SVIControl.DSPBlockRead(0x8, 0x23, 0x1, 0x1, bBuff);
                    if (uret != NativeMethods.RET_NORMAL)
                    {
                        Console.Out.WriteLine("Error SVIControl.DSPBlockReadt() ret=" + uret.ToString());
                        SVIControl.End();
                        break;
                    }
                    boardKind = bBuff[0];
                    Console.Out.WriteLine("SVIControl.DSPBlockReadt() ok boardKind=" + boardKind.ToString());

                    // ０：その他、１：ＳＶＩ－０９、２：ＳＶＭ－０６
                    switch (boardKind)
                    {
                        //
                        case 1:
                            board_name = "SVI-09";
                            break;
                        case 2:
                            board_name = "SVM-06";
                            break;
                        default:
                            board_name = "Other";
                            break;
                    }

                    SVIControl.End();

                    return (true);

                } while (false);
            }
            catch (Exception)
            {
            }
            return (false);
        }

        /// <summary>
        /// 閉じる処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }

            if (mDumpWindow.Visible) mDumpWindow.Close();

            Program.mEnv.SetWindowRect(this.Name, rc);
        }

        /// <summary>
        /// モニタリングメニュー
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuMonitoring_DropDownOpened(object sender, EventArgs e)
        {
            if (mRecoring)
            {
                menuMonitoringStart.Enabled = false;
                menuMonitoringStop.Enabled = false;
            }
            else
            {
                menuMonitoringStart.Enabled = !mMonitoring;
                menuMonitoringStop.Enabled = mMonitoring;
            }
        }

        /// <summary>
        /// レコーディングメニュー
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuRecording_DropDownOpened(object sender, EventArgs e)
        {
            if (mMonitoring)
            {
                menuRecordingStart.Enabled = false;
                //menuRecordingStop.Enabled = false;
            }
            else
            {
                menuRecordingStart.Enabled = !mRecoring;
                //menuRecordingStop.Enabled = mRecoring;
            }
        }

        /// <summary>
        /// レコーディング開始
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuRecordingStart_Click(object sender, EventArgs e)
        {
            // ファイル名
            string path = Program.mEnv.mRecOption.mDataFolder; ;
            string file_name;

            // ファイル名のコード判断(フォルダ指定)
            if (Program.mEnv.mRecOption.mFileSetting == 0)
            {
                // auto exepath + filename.dat
                file_name = System.IO.Path.Combine(path,DateTime.Now.ToString("yyyyMMddHHmmss") + ".dat");
            }
            else
            {
                // select
                file_name = System.IO.Path.Combine(path, Program.mEnv.mRecOption.mAnyName);
            }

            // 
            if ( file_name == string.Empty)
            {
                Win32.SVMessageBox.Show(this, "Recording file is not specified.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // モニタ非表示なら表示
            if (!mRecMonitor.Visible)
            {
                mRecMonitor.Show();
                mRecMonitor.ProgressPOS = 0;
                mRecMonitor.RecordingTime = Program.mEnv.mRecOption.mVideoRecoringTime;
                mRecMonitor.ActiveMaxTime = Program.mEnv.mRecOption.mMaxTime;
                mRecMonitor.ButtonActive();
            }
            mRecMonitor.BringToFront();

            mRecoring = true;
            mRecoringAbort = false;
            mRecoringStop = false;

            // レコーディングスタート
            System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    Recording(file_name);
#if true
                    // ファイルが作成されたので起動
                    if (System.IO.File.Exists(file_name))
                    {
                        // 画面を表示する
                        // [4]がdatサイズとフレーム数検出
                        string args = string.Format("\"{0}\" 10 {1}", file_name, Program.mEnv.mRecOption.mCanBps);
                        var pi = this.StartProcess(this.DAT_CHECK_EXE, args, out string msg, true);
                        if (pi == null)
                        {
                            this.Invoke((MethodInvoker)delegate ()
                            {
                                string text = string.Format("[{0}] exec error.\nmessage : {1}", this.DAT_CHECK_EXE, msg);
                                Win32.SVMessageBox.Show(this, text, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            });
                        }
                        else
                        {
                            pi.WaitForExit();

                            /// <summary>
                            /// dat　エラー
                            /// </summary>
                            Dictionary<int, string> DatErros = new Dictionary<int, string>()
                            {
                                { 0, "Normal" },
                                { 1, "File invalid" },
                                { 2, "File size error" },
                                { 3, "VsyncHigh error" },
                                { 4, "Discontinuity error" },
                                { 5, "Blank error" },
                                { 6, "DAT error" },
                                { 7, "NVCap error" },
                                { 8, "Disk error" },
                                { 9, "Skip" },
                                {10, "No check" },
                                {11, "Frame Count Mismatch" },
                                {12, "Skip Frame Count Mismatch" },
                                {13, "Skip All File Check" },
                            };

                            if (pi.ExitCode != 0)
                            {
                                this.Invoke((MethodInvoker)delegate ()
                                {
                                    string text = string.Format("[{0}] NVdatchk end.\nmessage : {1}", this.DAT_CHECK_EXE, DatErros[pi.ExitCode&0xff]);
                                    Win32.SVMessageBox.Show(this, text, "Information", MessageBoxButtons.OK);
                                });
                            }
                        }
                    }
#endif
                }
                catch
                {

                }
                mRecoring = false;

                this.Invoke((MethodInvoker)delegate ()
                {
                    mRecMonitor.Hide();
                });


            });
        }

        /// <summary>
        /// プロセスの開始
        /// </summary>
        /// <param name="exe_name">実行モジュールフルパス</param>
        /// <param name="arg">コマンドライン</param>
        /// <param name="msg">メッセージ</param>
        /// <param name="show">コマンドライン表示有無</param>
        /// <returns>プロセス情報(NULL失敗)</returns>
        System.Diagnostics.Process StartProcess(string exe_name, string arg, out string msg, bool show = false)
        {
            System.Diagnostics.Process pi = null;

            msg = string.Empty;

            try
            {
                pi = new System.Diagnostics.Process();
                pi.StartInfo.FileName = exe_name;
                pi.StartInfo.Arguments = arg;
                pi.StartInfo.CreateNoWindow = !show;
                pi.StartInfo.UseShellExecute = false;
                pi.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(exe_name);
                pi.Start();
            }
            catch (Exception e)
            {
                msg = string.Format("args : {0}\r\nMessage:{1}", arg, e.Message);
                return null;
            }

            return pi;
        }

        /// <summary>
        /// レコーディング停止
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuRecordingStop_Click(object sender, EventArgs e)
        {
            mRecoringAbort = true;
        }

        /// <summary>
        /// ダンプウィンドウ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolDump_Click(object sender, EventArgs e)
        {
            if ( !mDumpWindow.Visible)
            {
                mDumpWindow.Show();
            }
        }

        /// <summary>
        /// CAN0ウィンドウ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolCAN0Window_Click(object sender, EventArgs e)
        {
            if (!mCan0Window.Visible)
            {
                mCan0Window.Show();
            }
        }

        /// <summary>
        /// CAN1ウィンドウ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolCAN1Window_Click(object sender, EventArgs e)
        {
            if (!mCan1Window.Visible)
            {
                mCan1Window.Show();
            }
        }

        /// <summary>
        /// モニタリング開始
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuMonitoringStart_Click(object sender, EventArgs e)
        {
            mMonitoring= true;
            mMonitoringAbort = false;

            // レコーディングスタート
            System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    Monitoring();
                }
                catch( Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Monitoring Exception\r\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }
                mMonitoring = false;
            });

        }

        /// <summary>
        /// モニタリング停止
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuMonitoringStop_Click(object sender, EventArgs e)
        {
            mMonitoringAbort = true;
        }

        /// <summary>
        /// 1/4縮小
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_4_Click(object sender, EventArgs e)
        {
            mScale = 1.0 / 4.0;
            ScalePicture();

        }

        /// <summary>
        /// 1/2縮小
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_2_Click(object sender, EventArgs e)
        {
            mScale = 1.0 / 2.0;
            ScalePicture();
        }

        /// <summary>
        /// 1倍表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_Click(object sender, EventArgs e)
        {
            mScale = 1;
            ScalePicture();
        }

        /// <summary>
        /// 2倍表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_2_Click(object sender, EventArgs e)
        {
            mScale = 2;
            ScalePicture();

        }

        /// <summary>
        /// 4倍表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_4_Click(object sender, EventArgs e)
        {
            mScale = 4;
            ScalePicture();
        }

        /// <summary>
        /// ツールメニュー
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuTool_DropDownOpened(object sender, EventArgs e)
        {
            menuToolCAN0Window.Enabled = Program.mEnv.mRecOption.mVideoCAN0;
            menuToolCAN1Window.Enabled = Program.mEnv.mRecOption.mVideoCAN1;
        }


        /// <summary>
        /// スケール値更新
        /// </summary>
        private void ScaleUpdate()
        {
            statusLabelZoom.Text = string.Format("Zoom : 1/{0}", (int)Math.Round(1.0 / mScale));
        }

        /// <summary>
        /// アバウト
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuAbout_Click(object sender, EventArgs e)
        {
            using (var dlg = new AbotWindow())
            {
                dlg.ShowDialog();
            }
        }

        /// <summary>
        /// Fileメニュードロップダウン
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFile_DropDownOpened(object sender, EventArgs e)
        {
            if (mRecoring || mMonitoring )
            {
                menuFileOption.Enabled = false;
            }
            else
            {
                menuFileOption.Enabled = true;
            }
        }

        /// <summary>
        /// ピクチャボックスの再表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if ( mDIBitmap.Handle != null )
            {
                RedrawBitmap(e.Graphics, mDIBitmap.Handle);
            }
        }

        /// <summary>
        /// センサ＆FPGAの設定ファイル送信
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void settingFileWriteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!mI2cWindow.Visible)
            {
                mI2cWindow.Show();
            }
        }
    }
}
