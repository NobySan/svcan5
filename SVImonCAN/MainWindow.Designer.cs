﻿namespace SVImonCAN
{
    partial class MainWindow
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOption = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMonitoring = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMonitoringStart = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMonitoringStop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRecording = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRecordingStart = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRecordingStop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolDump = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolCAN0Window = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolCAN1Window = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuZoom_1_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_1_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarMenu = new System.Windows.Forms.ToolStrip();
            this.tspOption = new System.Windows.Forms.ToolStripButton();
            this.tspRec = new System.Windows.Forms.ToolStripButton();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabelSize = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelFPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelZoom = new System.Windows.Forms.ToolStripStatusLabel();
            this.settingFileWriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.toolBarMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuMonitoring,
            this.menuRecording,
            this.menuTool,
            this.menuHelp});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1024, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileOption,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(54, 20);
            this.menuFile.Text = "File (&F)";
            this.menuFile.DropDownOpened += new System.EventHandler(this.menuFile_DropDownOpened);
            // 
            // menuFileOption
            // 
            this.menuFileOption.Name = "menuFileOption";
            this.menuFileOption.Size = new System.Drawing.Size(180, 22);
            this.menuFileOption.Text = "Option (&O) ...";
            this.menuFileOption.Click += new System.EventHandler(this.menuFileOption_Click);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.Size = new System.Drawing.Size(180, 22);
            this.menuFileExit.Text = "End (&X)";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // menuMonitoring
            // 
            this.menuMonitoring.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMonitoringStart,
            this.menuMonitoringStop});
            this.menuMonitoring.Name = "menuMonitoring";
            this.menuMonitoring.Size = new System.Drawing.Size(101, 20);
            this.menuMonitoring.Text = "Monitoring (&M)";
            this.menuMonitoring.DropDownOpened += new System.EventHandler(this.menuMonitoring_DropDownOpened);
            // 
            // menuMonitoringStart
            // 
            this.menuMonitoringStart.Name = "menuMonitoringStart";
            this.menuMonitoringStart.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.menuMonitoringStart.Size = new System.Drawing.Size(180, 22);
            this.menuMonitoringStart.Text = "Monitoring Start";
            this.menuMonitoringStart.Click += new System.EventHandler(this.menuMonitoringStart_Click);
            // 
            // menuMonitoringStop
            // 
            this.menuMonitoringStop.Name = "menuMonitoringStop";
            this.menuMonitoringStop.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.menuMonitoringStop.Size = new System.Drawing.Size(180, 22);
            this.menuMonitoringStop.Text = "Monitoring Stop";
            this.menuMonitoringStop.Click += new System.EventHandler(this.menuMonitoringStop_Click);
            // 
            // menuRecording
            // 
            this.menuRecording.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuRecordingStart,
            this.menuRecordingStop});
            this.menuRecording.Name = "menuRecording";
            this.menuRecording.Size = new System.Drawing.Size(91, 20);
            this.menuRecording.Text = "Recording (&R)";
            this.menuRecording.DropDownOpened += new System.EventHandler(this.menuRecording_DropDownOpened);
            // 
            // menuRecordingStart
            // 
            this.menuRecordingStart.Name = "menuRecordingStart";
            this.menuRecordingStart.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.menuRecordingStart.Size = new System.Drawing.Size(180, 22);
            this.menuRecordingStart.Text = "Recording Start";
            this.menuRecordingStart.Click += new System.EventHandler(this.menuRecordingStart_Click);
            // 
            // menuRecordingStop
            // 
            this.menuRecordingStop.Enabled = false;
            this.menuRecordingStop.Name = "menuRecordingStop";
            this.menuRecordingStop.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.menuRecordingStop.Size = new System.Drawing.Size(180, 22);
            this.menuRecordingStop.Text = "Recording Stop";
            this.menuRecordingStop.Click += new System.EventHandler(this.menuRecordingStop_Click);
            // 
            // menuTool
            // 
            this.menuTool.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolDump,
            this.menuToolCAN0Window,
            this.menuToolCAN1Window,
            this.settingFileWriteToolStripMenuItem});
            this.menuTool.Name = "menuTool";
            this.menuTool.Size = new System.Drawing.Size(55, 20);
            this.menuTool.Text = "Tool(&T)";
            this.menuTool.DropDownOpened += new System.EventHandler(this.menuTool_DropDownOpened);
            // 
            // menuToolDump
            // 
            this.menuToolDump.Name = "menuToolDump";
            this.menuToolDump.Size = new System.Drawing.Size(191, 22);
            this.menuToolDump.Text = "Dump (&D)...";
            this.menuToolDump.Click += new System.EventHandler(this.menuToolDump_Click);
            // 
            // menuToolCAN0Window
            // 
            this.menuToolCAN0Window.Name = "menuToolCAN0Window";
            this.menuToolCAN0Window.Size = new System.Drawing.Size(191, 22);
            this.menuToolCAN0Window.Text = "CAN ch0 window (&0)...";
            this.menuToolCAN0Window.Click += new System.EventHandler(this.menuToolCAN0Window_Click);
            // 
            // menuToolCAN1Window
            // 
            this.menuToolCAN1Window.Name = "menuToolCAN1Window";
            this.menuToolCAN1Window.Size = new System.Drawing.Size(191, 22);
            this.menuToolCAN1Window.Text = "CAN ch1 window (&1)...";
            this.menuToolCAN1Window.Click += new System.EventHandler(this.menuToolCAN1Window_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(61, 20);
            this.menuHelp.Text = "Help(&H)";
            // 
            // menuAbout
            // 
            this.menuAbout.Name = "menuAbout";
            this.menuAbout.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.menuAbout.Size = new System.Drawing.Size(135, 22);
            this.menuAbout.Text = "About...";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.ContextMenuStrip = this.contextMenu;
            this.panel1.Controls.Add(this.toolBarMenu);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(0, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 693);
            this.panel1.TabIndex = 3;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuZoom_1_4,
            this.menuZoom_1_2,
            this.menuZoom_1,
            this.menuZoom_2,
            this.menuZoom_4});
            this.contextMenu.Name = "contextMenuStrip1";
            this.contextMenu.Size = new System.Drawing.Size(92, 114);
            // 
            // menuZoom_1_4
            // 
            this.menuZoom_1_4.Name = "menuZoom_1_4";
            this.menuZoom_1_4.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1_4.Text = "1/4";
            this.menuZoom_1_4.Click += new System.EventHandler(this.menuZoom_1_4_Click);
            // 
            // menuZoom_1_2
            // 
            this.menuZoom_1_2.Name = "menuZoom_1_2";
            this.menuZoom_1_2.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1_2.Text = "1/2";
            this.menuZoom_1_2.Click += new System.EventHandler(this.menuZoom_1_2_Click);
            // 
            // menuZoom_1
            // 
            this.menuZoom_1.Name = "menuZoom_1";
            this.menuZoom_1.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1.Text = "1/1";
            this.menuZoom_1.Click += new System.EventHandler(this.menuZoom_1_Click);
            // 
            // menuZoom_2
            // 
            this.menuZoom_2.Name = "menuZoom_2";
            this.menuZoom_2.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_2.Text = "2";
            this.menuZoom_2.Click += new System.EventHandler(this.menuZoom_2_Click);
            // 
            // menuZoom_4
            // 
            this.menuZoom_4.Name = "menuZoom_4";
            this.menuZoom_4.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_4.Text = "4";
            this.menuZoom_4.Click += new System.EventHandler(this.menuZoom_4_Click);
            // 
            // toolBarMenu
            // 
            this.toolBarMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBarMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspOption,
            this.tspRec});
            this.toolBarMenu.Location = new System.Drawing.Point(0, 0);
            this.toolBarMenu.Name = "toolBarMenu";
            this.toolBarMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolBarMenu.Size = new System.Drawing.Size(1024, 25);
            this.toolBarMenu.TabIndex = 1;
            this.toolBarMenu.Text = "toolStrip1";
            // 
            // tspOption
            // 
            this.tspOption.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tspOption.Image = ((System.Drawing.Image)(resources.GetObject("tspOption.Image")));
            this.tspOption.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspOption.Name = "tspOption";
            this.tspOption.Size = new System.Drawing.Size(23, 22);
            this.tspOption.Text = "toolStripButton1";
            this.tspOption.ToolTipText = "Option Setting";
            this.tspOption.Click += new System.EventHandler(this.menuFileOption_Click);
            // 
            // tspRec
            // 
            this.tspRec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tspRec.Image = ((System.Drawing.Image)(resources.GetObject("tspRec.Image")));
            this.tspRec.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspRec.Name = "tspRec";
            this.tspRec.Size = new System.Drawing.Size(23, 22);
            this.tspRec.Text = "toolStripButton2";
            this.tspRec.ToolTipText = "Recording Start";
            this.tspRec.Click += new System.EventHandler(this.menuRecordingStart_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox.ContextMenuStrip = this.contextMenu;
            this.pictureBox.Location = new System.Drawing.Point(0, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1, 1);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelSize,
            this.statusLabelFPS,
            this.statusLabelZoom});
            this.statusBar.Location = new System.Drawing.Point(0, 723);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1024, 22);
            this.statusBar.TabIndex = 4;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabelSize
            // 
            this.statusLabelSize.AutoSize = false;
            this.statusLabelSize.Name = "statusLabelSize";
            this.statusLabelSize.Size = new System.Drawing.Size(146, 17);
            this.statusLabelSize.Text = "Width : 0000 Height : 0000";
            // 
            // statusLabelFPS
            // 
            this.statusLabelFPS.AutoSize = false;
            this.statusLabelFPS.Name = "statusLabelFPS";
            this.statusLabelFPS.Size = new System.Drawing.Size(90, 17);
            this.statusLabelFPS.Text = "fps : 00.00/00.00";
            // 
            // statusLabelZoom
            // 
            this.statusLabelZoom.Name = "statusLabelZoom";
            this.statusLabelZoom.Size = new System.Drawing.Size(64, 17);
            this.statusLabelZoom.Text = "Zoom : 1/1";
            // 
            // settingFileWriteToolStripMenuItem
            // 
            this.settingFileWriteToolStripMenuItem.Name = "settingFileWriteToolStripMenuItem";
            this.settingFileWriteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.settingFileWriteToolStripMenuItem.Text = "I2C ...";
            this.settingFileWriteToolStripMenuItem.Click += new System.EventHandler(this.settingFileWriteToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 745);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuBar);
            this.MainMenuStrip = this.menuBar;
            this.Name = "MainWindow";
            this.Text = "SVImonCAN [no board]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.toolBarMenu.ResumeLayout(false);
            this.toolBarMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileOption;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.ToolStripMenuItem menuTool;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.ToolStripMenuItem menuToolDump;
        private System.Windows.Forms.ToolStripMenuItem menuToolCAN0Window;
        private System.Windows.Forms.ToolStripMenuItem menuToolCAN1Window;
        private System.Windows.Forms.ToolStripMenuItem menuRecording;
        private System.Windows.Forms.ToolStripMenuItem menuRecordingStart;
        private System.Windows.Forms.ToolStripMenuItem menuRecordingStop;
        private System.Windows.Forms.ToolStripMenuItem menuMonitoring;
        private System.Windows.Forms.ToolStripMenuItem menuMonitoringStart;
        private System.Windows.Forms.ToolStripMenuItem menuMonitoringStop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelSize;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFPS;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1_4;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1_2;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_2;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_4;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelZoom;
        private System.Windows.Forms.ToolStrip toolBarMenu;
        private System.Windows.Forms.ToolStripButton tspOption;
        private System.Windows.Forms.ToolStripButton tspRec;
        private System.Windows.Forms.ToolStripMenuItem settingFileWriteToolStripMenuItem;
    }
}

