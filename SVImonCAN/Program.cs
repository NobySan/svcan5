﻿using SVUtil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVImonCAN
{
    static class Program
    {
        /// <summary>
        /// プログラム環境
        /// </summary>
        static public SVImonCANEnv mEnv = new SVImonCANEnv();
        /// <summary>
        /// 環境変数ファイル名
        /// </summary>
        static public string mEnvFileName;

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Console.Writeをデバッグ画面へ表示
            System.Console.SetOut(Console.Out);
            Console.WriteLine("SVImonCAN Start");

            // exe.jsonのパス名作成
            mEnvFileName = Assembly.GetEntryAssembly().Location + ".json";

            // 環境の読込
            LoadEnv(mEnvFileName);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());

            // 環境の書込み
            SaveEnv(mEnvFileName);
        }

        /// <summary>
        /// 環境変数の読込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static void LoadEnv(string file_name)
        {
            try
            {
                using (var file = new FileStream(file_name, FileMode.Open, FileAccess.Read))
                {
                    mEnv = JsonSerializer.SerializerList<SVImonCANEnv>().ReadObject(file) as SVImonCANEnv;
                }
                // 必ずON
                mEnv.mRecOption.mVideoCAN0 = true;
                mEnv.mRecOption.mVideoCAN1 = true;
            }
            catch
            {
            }
        }

        /// <summary>
        /// 環境変数の保存
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static void SaveEnv(string file_name)
        {
            try
            {
                using (var file = new FileStream(file_name, FileMode.Create, FileAccess.Write))
                {
                    JsonSerializer.SerializerList<SVImonCANEnv>().WriteObject(file, mEnv);
                }
            }
            catch
            {
            }
        }
    }
}
