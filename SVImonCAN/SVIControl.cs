﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using sviusb20_dll;

namespace _SVIControl
{
    /// <summary>
    /// HDカメラアクセスモジュール
    /// </summary>
    public class SVIControl
    {
        /// <summary>
        /// クラス内変数定義
        /// </summary>
        private static bool bOpen = false;          // デバイスドライバオープンフラグ
        private const UInt32 m_BufMax = (64 * 1024 * 1024 * 4);  // 64MBx4の受信バッファサイズ
        private static NativeMethods.GET_STATUS sts;
        private static NativeMethods.REC_PARAM_EX rec_param;

        /// <summary>
        /// SVIコントロールクラスのコンストラクタ
        /// </summary>
        public SVIControl()
        {
            // 画像入力ライブラリーの初期化
            NativeMethods.Init();
        }

        /// <summary>
        /// SVI初期化、SVIデバイスドライバーオープン、センサー設定ファイルI2C送信
        /// </summary>
        /// <param name="stFileName"></param>
        /// <returns></returns>
        public static int Init()
        {
            UInt32 ret;

            ////////////////////////////////////////////////////////
            // SVIデバイスドライバのオープン
            ////////////////////////////////////////////////////////
            ret = NativeMethods.Open(NativeMethods.APP_WHO_REC);
            if (ret != NativeMethods.RET_NORMAL) return -1;
            bOpen = true;   // オープンフラグをオープン済みにする

            ////////////////////////////////////////////////////////
            // SVIハードウェア情報の設定（I2C転送速度設定, PixelInfo設定）
            ////////////////////////////////////////////////////////
            NativeMethods.SET_PARAM pParam = new NativeMethods.SET_PARAM();
            pParam.ulParamBitFlag = NativeMethods.SETPARAM_BIT0 | NativeMethods.SETPARAM_BIT2;
            pParam.ulI2CSpeed = NativeMethods.SETPARAM_I2CSPEED_2;
            pParam.ulPixelInfo = 0x1; // 入力8bit,カラー成分1
            ret = NativeMethods.SetParam(ref pParam);
            if (ret != NativeMethods.RET_NORMAL)
            {
                // デバイスドライバをクローズして抜ける
                ret = NativeMethods.Close(NativeMethods.APP_WHO_REC);
                bOpen = false;   // オープンフラグを未オープンにする
                return -3;
            }

            return 0;
        }

        /// <summary>
        /// SVIライブラリーを終了します
        /// </summary>
        public static void End()
        {
            // デバイスドライバがオープン済みであればクローズする
            if (bOpen != false)
            {
                UInt32 ret = NativeMethods.Close(NativeMethods.APP_WHO_REC);
                bOpen = false;   // オープンフラグをオープン未にする
            }
            // SVIライブラリーを終了します
            NativeMethods.End();
        }

        /// <summary>
        /// SVボードの情報を取得する
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static UInt32 GetStatusValue(out NativeMethods.GET_STATUS status)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // SVM-06ボードの情報を取得する
            uiRet = NativeMethods.GetStatus(ref sts);

            status = sts;

            return uiRet;
        }

        /// <summary>
        /// レコーディング開始を通知する
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static UInt32 RecStart(UInt32 can0_flag, UInt32 can1_flag)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // SVM-06ボードへレコーディングの開始を通知する
            rec_param.ulRecMode = (can0_flag << 31 | can1_flag << 30) | 0x4; // フレームメモリーのバッファ数を追加
            //uiRet = NativeMethods.RecStartEx(ref rec_param);
            uiRet = NativeMethods.RecStartEx(ref rec_param);
            uiRet = Open_SetFileWrite("nv.dat");
            uiRet = NativeMethods.RecStartEx2(ref rec_param);
            return uiRet;
        }

        /// <summary>
        /// レコーディング停止を通知する
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static UInt32 RecStop()
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // SVM-06ボードへレコーディングの停止を通知する
            uiRet = NativeMethods.RecStop();
            return uiRet;
        }

        /// <summary>
        /// レコーディングデータを取得します
        /// </summary>
        /// <param></param>
        /// <returns>
        /// </returns>
        public static UInt32 RecGetData(IntPtr pFrame, UInt32 uiLen, ref UInt32 uiRcvLen)
        {
            UInt32 dwRet = NativeMethods.RET_NORMAL;

            // データ受信
            unsafe
            {
                UInt32 ulGetSize2 = 0;
                dwRet = NativeMethods.RecGetData(pFrame, uiLen, ref ulGetSize2);
                uiRcvLen = ulGetSize2;
            }
            return dwRet;
        }

        /// <summary>
        /// センサーのレジスタを書き込む
        /// </summary>
        /// <param name="uiSlaveAddr"></param>
        /// <param name="uiSubAddr"></param>
        /// <param name="uiWordAddrMode"></param>
        /// <param name="uiSendCnt"></param>
        /// <param name="bBuf"></param>
        /// <returns></returns>
        public static UInt32 DSPBlockWrite(UInt32 uiSlaveAddr, UInt32 uiSubAddr, UInt32 uiWordAddrMode, UInt32 uiSendCnt, byte[] bBuf)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // センサーに対してI2C通信で1ブロック送信を行ないます
            UInt32 cnt = 1 + uiSendCnt;
            if (uiWordAddrMode == 2) cnt++;
            byte[] lpBuf = new byte[cnt];
            if (uiWordAddrMode == 1)
            {
                cnt = 1;
                lpBuf[0] = (byte)(uiSubAddr & 0xff);
            }
            else
            {
                cnt = 2;
                lpBuf[0] = (byte)((uiSubAddr >> 8) & 0xff);
                lpBuf[1] = (byte)((uiSubAddr >> 0) & 0xff);
            }
            Buffer.BlockCopy(bBuf, 0, lpBuf, (int)cnt, (int)uiSendCnt);
            uiRet = NativeMethods.I2CBlockWrite(uiSlaveAddr, cnt + uiSendCnt, 0x0, lpBuf);
            return uiRet;
        }

        /// <summary>
        /// センサーのレジスタを読み込む
        /// </summary>
        /// <param name="uiSlaveAddr"></param>
        /// <param name="uiSubAddr"></param>
        /// <param name="uiReadMode"></param>
        /// <param name="uiRcvCnt"></param>
        /// <param name="bBuf"></param>
        /// <returns></returns>
        public static UInt32 DSPBlockRead(UInt32 uiSlaveAddr, UInt32 uiSubAddr, UInt32 uiReadMode, UInt32 uiRcvCnt, byte[] bBuf)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // センサーに対してI2C通信で読み出しアドレスを指定します
            UInt32 cnt = 1;
            if (uiReadMode == 2) cnt++;
            byte[] lpBuf = new byte[cnt];
            if (uiReadMode == 1)
            {
                lpBuf[0] = (byte)(uiSubAddr & 0xff);
            }
            else
            {
                lpBuf[0] = (byte)((uiSubAddr >> 8) & 0xff);
                lpBuf[1] = (byte)((uiSubAddr >> 0) & 0xff);
            }
            uiRet = NativeMethods.I2CBlockWrite(uiSlaveAddr, cnt, 0x0, lpBuf);
            if (uiRet != NativeMethods.RET_NORMAL) return uiRet;

            // センサーに対してI2C通信で1ブロック受信を行ないます
            cnt = uiRcvCnt;
            byte[] lpBuf2 = new byte[cnt];
            uiRet = NativeMethods.I2CBlockRead(uiSlaveAddr, cnt, 0x80000000, lpBuf2);
            if (uiRet != NativeMethods.RET_NORMAL) return uiRet;
            Buffer.BlockCopy(lpBuf2, 0, bBuf, 0, (int)uiRcvCnt);
            return uiRet;
        }

        /// <summary>
        /// センサーのレジスタを書き込む
        /// </summary>
        /// <param name="uiRegAddr"></param>
        /// <param name="uiSendCnt"></param>
        /// <param name="uiBuf"></param>
        /// <returns></returns>
        public static UInt32 FpgaBlockWrite(UInt32 uiRegAddr, UInt32 uiSendCnt, UInt32[] uiBuf)
        {
            UInt32 uiRet = 0;
            UInt32 n_uiVal = 0;
            uiRet = NativeMethods.RET_NORMAL;
            UInt32 n_regAddr = uiRegAddr;
            for (int i = 0; i < uiSendCnt; i++)
            {
                n_uiVal = uiBuf[i];
                uiRet = NativeMethods.WriteReg(n_regAddr, n_uiVal);
                n_regAddr += 4;
            }
            return uiRet;
        }

        /// <summary>
        /// FPGAのレジスタを読み込む
        /// </summary>
        /// <param name="uiRegAddr"></param>
        /// <param name="uiRcvCnt"></param>
        /// <param name="uiBuf"></param>
        /// <returns></returns>
        public static UInt32 FpgaBlockRead(UInt32 uiRegAddr, UInt32 uiRcvCnt, UInt32[] uiBuf)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;
            UInt32 n_uiVal = 0;
            UInt32 n_regAddr = uiRegAddr;
            for (int i = 0; i < uiRcvCnt; i++)
            {
                uiRet = NativeMethods.ReadReg(n_regAddr, ref n_uiVal);
                if (uiRet != NativeMethods.RET_NORMAL) return uiRet;
                uiBuf[i] = n_uiVal;
                n_regAddr += 4;
            }
            return uiRet;
        }

        /// <summary>
        /// SVボードからVideo同期信号のモニター情報を取得する
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static UInt32 GetVideoMonitor(ref UInt32 pulTCB_CIP_SYNC_SIG)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // SVM-06ボードからレジスタを読み込む
            uiRet = NativeMethods.ReadReg(0x10000004, ref pulTCB_CIP_SYNC_SIG);
            return uiRet;
        }

        /// <summary>
        /// ラストエラー値取得
        /// </summary>
        /// <param name="ulErrorCode">エラーコード</param>
        /// <returns>エラー文字列</returns>
        public static string GetLastError(UInt32 ulErrorCode)
        {
            try
            {
                // 2K分のメモリを確保
                System.Text.StringBuilder sb = new System.Text.StringBuilder(2048);
                NativeMethods.GetLastErrorString(ulErrorCode, sb, (ulong)sb.Capacity);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /////////////////////////////////////////////////////
        /// <summary>
        /// SVIデバイスドライバのオープンと、センサー用I2C設定ファイルをセンサーに送信します
        /// </summary>
        /// <param name="stFileName">
        /// </param>
        /// <returns>
        /// </returns>
        public static UInt32 Open_SetFileWrite(String stFileName)
        {
            // SVデバイスドライバはオープン済みとします

            UInt32 ret = 0;
            StreamReader textFile;
            UInt32 uiSettingDataListNum = 0; // センサーレジスタリスト数
            String strReadLine;
            String strLine;
            int ipos;
            bool bWaitFlag = false;
            UInt32 uiSlaveAddr = 0;
            UInt32 uiSubAddr;
            UInt32 uiValue;
            UInt32 dwCmd = 0;
            char[] chFinds = { '\t', ' ', ',' };
            char[] chFinds2 = { '#', ';' };
            char[] chFinds3 = { '\t', ' ', ',', ';' };

            // 設定ファイルをオープンする
            try
            {
                textFile = new StreamReader(stFileName);
            }
            catch
            {
                return 0;
            }

            while (true)
            {

                // 1ライン読み込む
                strReadLine = textFile.ReadLine();
                if (strReadLine == null) break;

                // 全て小文字に変換する
                strReadLine.ToLower();

                // 改行を除去する
                strReadLine = strReadLine.Replace(Environment.NewLine, "");

                // TABを除去する
                strReadLine = strReadLine.Replace("\t", "");

                // 空行ならスキップする
                if (strReadLine.Length == 0) continue;

                // コメント文字を含むか判定
                if (strReadLine.IndexOf("#") < 0 && strReadLine.IndexOf(";") < 0)
                {
                    // 読み込んだ行がコメントではない場合
                    ipos = strReadLine.IndexOfAny(chFinds);
                    strLine = strReadLine.Substring(0, ipos);
                    if ((ipos + 1) >= strReadLine.Length)
                        // 区切り文字の最後にある場合
                        continue;
                    strLine.Insert(ipos, "\0");
                }
                else if (strReadLine.IndexOf("#") >= 0 || strReadLine.IndexOf(";") >= 0)
                {
                    // 読み込んだ行にコメントが存在するか？
                    ipos = strReadLine.IndexOfAny(chFinds2);
                    if (ipos > 0)
                    {
                        // コメントの前にデータが存在する
                        // コメントをスキップ
                        ipos = strReadLine.IndexOfAny(chFinds);
                        strLine = strReadLine.Substring(0, ipos);
                    }
                    else
                    {
                        // 先頭カラムからコメント場合はこの行をスキップする
                        continue;
                    }
                    // スペースが文字列の最後にある場合の対応
                    if ((ipos + 1) >= strReadLine.Length) continue;
                    strLine.Insert(ipos, "\0");
                }
                else continue;

                // ウェイト行対応
                if (strLine.Contains("wt") || strLine.Contains("wait"))
                {
                    // ウェイトフラグオン
                    bWaitFlag = true;
                }
                else
                {
                    // ウェイトフラグオフ
                    bWaitFlag = false;
                    // ウェイト行じゃなければスレーブアドレスとみなす
                    uiSlaveAddr = (UInt32)Convert.ToInt32(strLine, 16);
                }

                strLine = strReadLine.Substring(ipos + 1);
                int ipos2 = strLine.IndexOfAny(chFinds3);
                String s1;
                if (ipos2 < 0) s1 = strLine;
                else s1 = strReadLine.Substring(ipos + 1, ipos2);

                if (bWaitFlag)
                {
                    // ウェイトは10進でmsec単位
                    dwCmd = (UInt32)Convert.ToInt32(s1, 10);
                    // ウェイト実行
                    System.Threading.Thread.Sleep((int)dwCmd);
                    continue;
                }
                else
                {
                    // サブアドレスを取得
                    uiSubAddr = (UInt32)Convert.ToInt32(s1, 16);
                }

                String s2 = strLine.Substring(ipos2 + 1);
                int ipos3 = s2.IndexOfAny(chFinds3);
                String s3;
                if (ipos3 < 0)
                {
                    s3 = s2.Substring(0);
                }
                else
                {
                    s3 = s2.Substring(0, ipos3);
                }
                //s2.Insert(ipos3, "\0");
                // 書き込み値を取得
                uiValue = (UInt32)Convert.ToInt32(s3, 16);

                // センサーレジスタへの書き込み実行
                byte[] bBuf = new byte[16];

                // センサーレジスターライト
                if (uiSlaveAddr != 0x8)
                {
                    bBuf[0] = (byte)(uiValue & 0xff);
                    ret = DSPBlockWrite(uiSlaveAddr, uiSubAddr, 0, 0x1, bBuf);
                    //MessageBox.Show("I2C送信エラー発生", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textFile.Close(); ;
                    return NativeMethods.RET_NORMAL;
                }
                else
                {
                    ////////////////////////////////////////////////////////
                    // SVボードのFPGAレジスタへの書き込み
                    ret = NativeMethods.WriteReg(uiSubAddr, uiValue);
                    //if (ret != NativeMethods.RET_NORMAL) return ret;
                }
                if (ret != NativeMethods.RET_NORMAL)
                {
                }

                // センサーレジスタリストのポインタ、カウンタを更新
                uiSettingDataListNum++;
            }


            // 設定ファイルをクローズする
            textFile.Close();

            return NativeMethods.RET_NORMAL;
        }

#if false
        /// <summary>
        /// モニタリングを開始します
        /// </summary>
        /// <param></param>
        /// <returns>
        /// </returns>
        public static UInt32 MonStart()
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // 受信バッファの確保
            m_pUsbReadBuffer = Marshal.AllocHGlobal(3 * 256 * 1024 * 1024);

            // 画像データ受信開始を通知
            uiRet = NativeMethods.DataStart();
            if (uiRet != NativeMethods.RET_NORMAL)
            {
                MessageBox.Show("NativeMethods.DataStartエラー発生", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            unsafe
            {
                uint* mm;
                mm = (uint*)m_pUsbReadBuffer;
                for (int i = 0; i < (3 * 256 * 1024 * 1024) >> 2; i++)
                {
                    *mm++ = 0;
                }
            }

            return uiRet;
        }

        /// <summary>
        /// モニタリングを停止します
        /// </summary>
        /// <param></param>
        /// <returns>
        /// </returns>
        public static UInt32 MonStop()
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;

            // 画像データ受信開始を通知
            uiRet = NativeMethods.DataStop();
            if (uiRet != NativeMethods.RET_NORMAL)
            {
                MessageBox.Show("NativeMethods.DataStopエラー発生", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return uiRet;
        }

        /// <summary>
        /// IntPtr間でコピーをします
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="src"></param>
        /// <param name="size"></param>
        private static void CopyMemory(IntPtr dst, IntPtr src, int size)
        {
            byte[] temp = new byte[size];
            Marshal.Copy(src, temp, 0, size);
            Marshal.Copy(temp, 0, dst, size);
        }

        /// <summary>
        /// モニタリングデータを取得します
        /// </summary>
        /// <param></param>
        /// <returns>
        /// </returns>
        public static UInt32 MonGetData(IntPtr pFrame, IntPtr pHeader)
        {
            UInt32 dwRet = NativeMethods.RET_NORMAL;
            bool bSet = false;
        	UInt32 ulImageSize;
        	UInt32 ulRcvSize;
        	UInt32 ulGetSize;
        	UInt32 ulMonDataSize;
            NativeMethods.MON_PARAMQ m_monHeader = new NativeMethods.MON_PARAMQ();


        	// モニタリングフレーム取得ループ
        	while(true){

		        // 受信バッファのサイズを設定する
		        ulMonDataSize = m_BufMax;

        		// ヘッダ情報の取得
                unsafe
                {
                    UInt32 ulGetSize2=0;
		            dwRet = NativeMethods.DataRead(m_pUsbReadBuffer, ulMonDataSize, ref ulGetSize2);
                    if (dwRet != NativeMethods.RET_NORMAL)
                    {
                        // エラーなのでサイズ0を返す
                        return 0;
                    }
                    ulGetSize = ulGetSize2;
                }

        		// 受信バイト数が要求バイト数と違う時で受信バイト数はFPGAレジスタ(VPB)受信の受信バイト数と
		        // 同じであればFPGAレジスタ(VPB)受信とみなす
		        if(ulGetSize == 24){
                    CopyMemory(pFrame, m_pUsbReadBuffer, 24);
           			return 24;
		        }

                // Marshal.PtrToStructure()で、C#の構造体にコピーする
                IntPtr m_monHeaderPtr = Marshal.AllocCoTaskMem(512);
                m_monHeader = (NativeMethods.MON_PARAMQ)Marshal.PtrToStructure(m_pUsbReadBuffer, m_monHeader.GetType());
                // Marshal.AllocCoTaskMem()で確保したメモリを解放
                Marshal.FreeCoTaskMem(m_monHeaderPtr);
		
        		// SVIUSB30_DataReadで取得したヘッダー情報のエンディアンがビックエンディアンなのでリトルエンディアンに変換します。
                m_monHeader.ulStatus = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulStatus)>>32);
                m_monHeader.ulPendingInfo = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulPendingInfo)>>32);
        		m_monHeader.ulOrgSizeW = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulOrgSizeW)>>32);
        		m_monHeader.ulOrgSizeH = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulOrgSizeH)>>32);
        		m_monHeader.ulCutout_x = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulCutout_x)>>32);
        		m_monHeader.ulCutout_y = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulCutout_y)>>32);
        		m_monHeader.ulMonSizeW = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulMonSizeW)>>32);
        		m_monHeader.ulMonSizeH = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulMonSizeH)>>32);
        		m_monHeader.ulFrameRate = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulFrameRate)>>32);
        		m_monHeader.ulLoseFrame = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulLoseFrame)>>32);
        		m_monHeader.ulRecQuadCount = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulRecQuadCount)>>32);
                m_monHeader.ulRecStatus = (UInt32)(System.Net.IPAddress.NetworkToHostOrder(m_monHeader.ulRecStatus) >> 32);
#if false // RAW12orRAW10 16bit-in
                UInt32 ulMonSize = (m_monHeader.ulOrgSizeW * 2 * m_monHeader.ulOrgSizeH) + 512;
#endif
#if false // RAW8
                UInt32 ulMonSize = (m_monHeader.ulOrgSizeW * 1 * m_monHeader.ulOrgSizeH) + 512;
#endif
#if true // 2014.11.21 32bit-in,RAW8対応
                UInt32 ulMonSize = (m_monHeader.ulOrgSizeW * 4 * m_monHeader.ulOrgSizeH) + 512;
#endif
                if (ulGetSize != ulMonSize)
                {
		        	return 0;
		        }

                // アイドルフラグ以外であればデータ取得処理を行います。
		        if(m_monHeader.ulRecStatus != 0) {

                    // 実際のデータサイズの計算
#if false // RAW12orRAW10 16bit-in
                    ulImageSize = m_monHeader.ulMonSizeW * 2 * m_monHeader.ulMonSizeH;
#endif
#if false // RAW8
    				ulImageSize = m_monHeader.ulMonSizeW * 1 * m_monHeader.ulMonSizeH;
#endif
#if true // 2014.11.21 32bit-in,RAW8対応
                    ulImageSize = ulMonSize;
#endif
                    ulRcvSize = ((ulImageSize + 63) / 64) * 64;

        			// データ部分を引数のバッファにコピーします。
#if true // 2014.11.28 録画不具合
                    CopyMemory(pFrame, m_pUsbReadBuffer + 512, (int)(ulRcvSize-512));
#else
                    CopyMemory(pFrame, m_pUsbReadBuffer + 512, (int)ulRcvSize);
#endif // 2014.11.28 録画不具合
//                    CopyMemory(pFrame, m_pUsbReadBuffer + 128, 24);
#if false // RAW12orRAW10 8bit-in
                    m_monHeader.ulOrgSizeW /= 2;
                    m_monHeader.ulMonSizeW /= 2;
#endif
#if true // 2014.11.21 32bit-in,RAW8対応
                    m_monHeader.ulOrgSizeW *= 4;
                    m_monHeader.ulMonSizeW *= 4;
#endif

                    // フレーム取得フラグON
		        	bSet = true;
                    // 最初の512バイトはヘッダー情報なので、ヘッダー変数へコピーします
                    Marshal.StructureToPtr(m_monHeader, pHeader, false);
		        	break;
		        }

        	}

    	    // 1フレーム取得成功＆終了
	        if(bSet) return ulRcvSize;

	        // フレーム取得失敗＆終了
	        return 0;
        }

        /// <summary>
        /// FPGA内ユーザーロジックのレジスタを書き込む
        /// </summary>
        /// <param name="uiOffsetAddr"></param>
        /// <param name="uiRegByteWidth"></param>
        /// <param name="uiWriteValue"></param>
        /// <returns></returns>
        public static UInt32 UserRegWrite(UInt32 uiOffsetAddr, UInt32 uiRegByteWidth, UInt32 uiWriteValue)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;
            UInt32 ulHeader, ulOffset0, ulOffset1, ulOffset2, ulOffset3;

            UInt32 uiSubAddr = 0xc0000000;  // ユーザーロジック空間アドレス

            // ユーザーロジックレジスタを2クワッド分読み込む
            ulHeader = 0x40000000;
            ulOffset0 = 0x10000000 | (2 << 2);
            ulOffset1 = uiSubAddr + (uiOffsetAddr & 0xfffffffc);
            ulOffset2 = 0x0;
            ulOffset3 = 0x20000100;
            uint[] lpBuf = new uint[2];
            byte[] lbBuf = new byte[8];

            // SVI-06Qボードのレジスタを書き込む
            uiRet = NativeMethods.GetReg(ulHeader, ulOffset0, ulOffset1, ulOffset2, ulOffset3, 2, lpBuf);
            if (uiRet != NativeMethods.RET_NORMAL)
            {
                return uiRet;
            }
            // リードバイト数に応じて値をセットする
            Buffer.BlockCopy(lpBuf, 0, lbBuf, 0, 8);

            switch(uiRegByteWidth)
            {
                case 1: // 1バイト書込み
                    lbBuf[(uiOffsetAddr & 0x3)] = (byte)uiWriteValue;
                    break;
                case 2: // 2バイト書込み
                    lbBuf[(uiOffsetAddr & 0x3)+0] = (byte)(uiWriteValue >> 8);
                    lbBuf[(uiOffsetAddr & 0x3)+1] = (byte)(uiWriteValue >> 0);
                    break;
                case 3: // 3バイト書込み
                    lbBuf[(uiOffsetAddr & 0x3)+0] = (byte)(uiWriteValue >> 16);
                    lbBuf[(uiOffsetAddr & 0x3)+1] = (byte)(uiWriteValue >> 8);
                    lbBuf[(uiOffsetAddr & 0x3)+2] = (byte)(uiWriteValue >> 0);
                    break;
                case 4: // 4バイト書込み
                    lbBuf[(uiOffsetAddr & 0x3)+0] = (byte)(uiWriteValue >> 24);
                    lbBuf[(uiOffsetAddr & 0x3)+1] = (byte)(uiWriteValue >> 16);
                    lbBuf[(uiOffsetAddr & 0x3)+2] = (byte)(uiWriteValue >> 8);
                    lbBuf[(uiOffsetAddr & 0x3)+3] = (byte)(uiWriteValue >> 0);
                    break;
                default: // 1バイト書込み
                    lbBuf[(uiOffsetAddr & 0x3)] = (byte)uiWriteValue;
                    break;
            }
            
            // ユーザーロジックレジスタを2クワッド分書き込む
            ulHeader = 0x40010000;
            ulOffset0 = 0x10000000 | (2<<2);
            ulOffset1 = uiSubAddr + (uiOffsetAddr & 0xfffffffc);
            ulOffset2 = 0x0;
            ulOffset3 = 0x20000000;
            Buffer.BlockCopy(lbBuf, 0, lpBuf, 0, 8);

            // SVI-06Qボードのレジスタを書き込む
            uiRet = NativeMethods.PutReg(ulHeader, ulOffset0, ulOffset1, ulOffset2, ulOffset3, 2, lpBuf);
            if (uiRet != NativeMethods.RET_NORMAL)
            {
                return uiRet;
            }

            return uiRet;
        }

        /// <summary>
        /// FPGA内ユーザーロジックのレジスタを読み込む
        /// </summary>
        /// <param name="uiOffsetAddr"></param>
        /// <param name="uiRegByteWidth"></param>
        /// <param name="bBuf"></param>
        /// <returns></returns>
        public static UInt32 UserRegRead(UInt32 uiOffsetAddr, UInt32 uiRegByteWidth, byte[] bBuf)
        {
            UInt32 uiRet = NativeMethods.RET_NORMAL;
            UInt32 ulHeader, ulOffset0, ulOffset1, ulOffset2, ulOffset3;

            UInt32 uiSubAddr = 0xc0000000;  // ユーザーロジック空間アドレス

            // Critical section starts
            //CSingleLock obLock(&m_csSync, true);
            //m_csSync.Lock();

            //unsafe
            //{
                // ユーザーロジックレジスタを2クワッド分読み込む
                ulHeader = 0x40000000;
                ulOffset0 = 0x10000000 | (2 << 2);
                ulOffset1 = uiSubAddr + (uiOffsetAddr & 0xfffffffc);
                ulOffset2 = 0x0;
                ulOffset3 = 0x20000100;
                uint[] lpBuf = new uint[2];
                byte[] lbBuf = new byte[8];

                // SVI-06Qボードのレジスタを書き込む
                uiRet = NativeMethods.GetReg(ulHeader, ulOffset0, ulOffset1, ulOffset2, ulOffset3, 2, lpBuf);
                if (uiRet != NativeMethods.RET_NORMAL)
                {
                    return uiRet;
                }
                // リードバイト数に応じて値をセットする
                Buffer.BlockCopy(lpBuf, 0, lbBuf, 0, 8);
                Buffer.BlockCopy(lbBuf, (int)((uiOffsetAddr & 0x3)), bBuf, 0, (int)uiRegByteWidth);
            //}

            // Critical section ends
            //m_csSync.Unlock();

            return uiRet;
        }
#endif
    }
}

