﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SVUtil;

namespace SVImonCAN
{
    /// <summary>
    /// 設定
    /// </summary>
    public partial class RecOption : Form
    {
        /// <summary>
        /// 記録－設定
        /// </summary>
        public RecOption()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void RecOption_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);

            CheckRadioButtonByTag(grpFileName, Program.mEnv.mRecOption.mFileSetting.ToString());
            CheckRadioButtonByTag(groupVideoColor, Program.mEnv.mRecOption.mVideoColor.ToString());
            CheckRadioButtonByTag(groupVideoVSyncLevel, Program.mEnv.mRecOption.mVideoVSyncLevel.ToString());
            CheckRadioButtonByTag(groupVideoHSyncLevel, Program.mEnv.mRecOption.mVideoHSyncLevel.ToString());
            CheckRadioButtonByTag(groupVideoHSyncLevel, Program.mEnv.mRecOption.mVideoHSyncLevel.ToString());

            checkCanSet0.Checked = Program.mEnv.mRecOption.mVideoCAN0;
            checkCanSet1.Checked = Program.mEnv.mRecOption.mVideoCAN1;
            numericThreshold1.Value = Program.mEnv.mRecOption.mCanThreshold1;
            numericThreshold2.Value = Program.mEnv.mRecOption.mCanThreshold2;
            numericCANbps.Value = Program.mEnv.mRecOption.mCanBps;
            numericCAN_FDbps.Value = Program.mEnv.mRecOption.mCAN_FDbps;

            checkMaxTime.Checked = Program.mEnv.mRecOption.mMaxTime;
            numRecMaxTime.Value = Program.mEnv.mRecOption.mVideoRecoringTime;
            txtBaseFolder.Text = Program.mEnv.mRecOption.mDataFolder;
            textFileAnyName.Text = Program.mEnv.mRecOption.mAnyName;
            checkLsbPack.Checked = Program.mEnv.mRecOption.mDisplayLsbPack;
            checkReverse.Checked = Program.mEnv.mRecOption.mDisplayReverse;
            checkPreview.Checked = Program.mEnv.mRecOption.mPreview;

            this.checkPictureSizeAuto.Checked = Program.mEnv.mRecOption.mPictureAuto;
            this.numPictureWidth.Value = Program.mEnv.mRecOption.mPictureWidth;
            this.numPictureHeight.Value = Program.mEnv.mRecOption.mPictureHeight;
            this.numPictureFPS.Value = Program.mEnv.mRecOption.mPictureFPS;
            this.numTimeOut.Value = Program.mEnv.mRecOption.mTimeOut;

            // 配置
            this.Location = rc.Location;
        }

        /// <summary>
        /// 閉じる処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void RecOption_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            Program.mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }


        /// <summary>
        /// チェックされたラジオボタンの取得
        /// </summary>
        /// <param name="grpbox">グループボックス</param>
        /// <returns>番号</returns>
        int CheckRadioNo(GroupBox grpbox)
        {
            var radio_btn = grpbox.Controls.OfType<RadioButton>().SingleOrDefault(rb => rb.Checked == true);
            if (radio_btn == null)
            {
                return (-1);
            }
            return (Convert.ToInt32(radio_btn.Tag));
        }

        /// <summary>
        /// Tagを元にラジオボタンにチェックを入れる
        /// </summary>
        /// <param name="grpbox">グループボックス</param>
        /// <param name="tag">タグ名</param>
        void CheckRadioButtonByTag(GroupBox grpbox, object tag)
        {
            List<RadioButton> buttons = grpbox.Controls.OfType<RadioButton>().ToList();

            foreach (var radio in buttons)
            {
                if (radio.Tag.Equals(tag))
                {
                    radio.Checked = true;
                    break;
                }
            }
        }

        /// <summary>
        /// 適用
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (radioFileAny.Checked)
            {
                if (textFileAnyName.Text == string.Empty)
                {
                    Win32.SVMessageBox.Show(this, "Any File Name Empty", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // エラーメッセージ
                    DialogResult = DialogResult.None;
                    return;
                }
            }

            // 環境設定ファイルへ保存
            Program.mEnv.mRecOption.mDataFolder = txtBaseFolder.Text;
            Program.mEnv.mRecOption.mFileSetting = CheckRadioNo(grpFileName);
            Program.mEnv.mRecOption.mAnyName = textFileAnyName.Text;
            Program.mEnv.mRecOption.mVideoColor = CheckRadioNo(groupVideoColor);
            Program.mEnv.mRecOption.mVideoVSyncLevel = CheckRadioNo(groupVideoVSyncLevel);
            Program.mEnv.mRecOption.mVideoHSyncLevel = CheckRadioNo(groupVideoHSyncLevel);
            //Program.mEnv.mRecOption.mVideoHSyncLevel = CheckRadioNo(groupVideoHSyncLevel);

            Program.mEnv.mRecOption.mVideoCAN0 = checkCanSet0.Checked;
            Program.mEnv.mRecOption.mVideoCAN1 = checkCanSet1.Checked;
            Program.mEnv.mRecOption.mCanThreshold1 = (int)numericThreshold1.Value;
            Program.mEnv.mRecOption.mCanThreshold2 = (int)numericThreshold2.Value;
            Program.mEnv.mRecOption.mCanBps = (int)numericCANbps.Value;
            Program.mEnv.mRecOption.mCAN_FDbps = (int)numericCAN_FDbps.Value;

            Program.mEnv.mRecOption.mMaxTime = checkMaxTime.Checked;
            Program.mEnv.mRecOption.mVideoRecoringTime = (int)numRecMaxTime.Value;
            Program.mEnv.mRecOption.mDisplayLsbPack = checkLsbPack.Checked;
            Program.mEnv.mRecOption.mDisplayReverse = checkReverse.Checked;
            Program.mEnv.mRecOption.mPreview = checkPreview.Checked;

            Program.mEnv.mRecOption.mPictureAuto = this.checkPictureSizeAuto.Checked;
            Program.mEnv.mRecOption.mPictureWidth = (int)this.numPictureWidth.Value;
            Program.mEnv.mRecOption.mPictureHeight = (int)this.numPictureHeight.Value;
            Program.mEnv.mRecOption.mPictureFPS = (int)this.numPictureFPS.Value;
            Program.mEnv.mRecOption.mTimeOut = (int)this.numTimeOut.Value;
        }

        /// <summary>
        /// ファイル名Auto選択
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void radioFileAuto_CheckedChanged(object sender, EventArgs e)
        {
            // Autoが選ばれた場合は文字列入力不可とする
            textFileAnyName.Enabled = !(sender as RadioButton).Checked;
        }

        /// <summary>
        /// MAX Timeチェック有無
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void checkMaxTime_CheckedChanged(object sender, EventArgs e)
        {
            this.numRecMaxTime.Enabled = this.checkMaxTime.Checked;
        }

        /// <summary>
        /// チェック切り替え
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void checkPictureSizeAuto_CheckedChanged(object sender, EventArgs e)
        {
            this.numPictureWidth.Enabled =
            this.numPictureHeight.Enabled =
            this.numPictureFPS.Enabled = !this.checkPictureSizeAuto.Checked;
        }

        /// <summary>
        /// ドラッグオーバー（フォルダ確認）
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void txtBaseFolder_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (!System.IO.Directory.Exists(filenames[0]) || filenames.Length > 1)
                {
                    e.Effect = DragDropEffects.None;
                }
                else
                {
                    e.Effect = DragDropEffects.All;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// ドロップ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void txtBaseFolder_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop);
                this.txtBaseFolder.Text = filenames[0];
            }
        }

        /// <summary>
        /// フォルダ選択ダイアログ
        /// https://thenewsinpu.hatenablog.jp/entry/2018/04/21/230616
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void btnRefer_Click(object sender, EventArgs e)
        {
            var dlg = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog("Select Folder.");
            dlg.IsFolderPicker = true;
            dlg.DefaultFileName = this.txtBaseFolder.Text;
            if (dlg.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                this.txtBaseFolder.Text = dlg.FileName;
            }
        }
    }
}
