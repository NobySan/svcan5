﻿namespace SVImonCAN
{
    partial class RecOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonApply = new System.Windows.Forms.Button();
            this.groupVideoColor = new System.Windows.Forms.GroupBox();
            this.radioColorUYVY = new System.Windows.Forms.RadioButton();
            this.radioColorUYVY_Y = new System.Windows.Forms.RadioButton();
            this.groupVideoVSyncLevel = new System.Windows.Forms.GroupBox();
            this.radioVSyncHighLevel = new System.Windows.Forms.RadioButton();
            this.radioVSyncLowLevel = new System.Windows.Forms.RadioButton();
            this.groupVideoHSyncLevel = new System.Windows.Forms.GroupBox();
            this.radioHSyncHighLevel = new System.Windows.Forms.RadioButton();
            this.radioHSyncLowLevel = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numPictureFPS = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numPictureWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numPictureHeight = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.checkPictureSizeAuto = new System.Windows.Forms.CheckBox();
            this.groupFileSetting = new System.Windows.Forms.GroupBox();
            this.btnRefer = new System.Windows.Forms.Button();
            this.txtBaseFolder = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.grpFileName = new System.Windows.Forms.GroupBox();
            this.radioFileAuto = new System.Windows.Forms.RadioButton();
            this.textFileAnyName = new System.Windows.Forms.TextBox();
            this.radioFileAny = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericCANbps = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericCAN_FDbps = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericThreshold1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericThreshold2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.checkCanSet1 = new System.Windows.Forms.CheckBox();
            this.checkCanSet0 = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkMaxTime = new System.Windows.Forms.CheckBox();
            this.checkPreview = new System.Windows.Forms.CheckBox();
            this.numRecMaxTime = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkReverse = new System.Windows.Forms.CheckBox();
            this.checkLsbPack = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.numTimeOut = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.groupVideoColor.SuspendLayout();
            this.groupVideoVSyncLevel.SuspendLayout();
            this.groupVideoHSyncLevel.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureFPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureHeight)).BeginInit();
            this.groupFileSetting.SuspendLayout();
            this.grpFileName.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCANbps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCAN_FDbps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold2)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRecMaxTime)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeOut)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonApply.Location = new System.Drawing.Point(328, 549);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 6;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // groupVideoColor
            // 
            this.groupVideoColor.Controls.Add(this.radioColorUYVY);
            this.groupVideoColor.Controls.Add(this.radioColorUYVY_Y);
            this.groupVideoColor.Location = new System.Drawing.Point(15, 18);
            this.groupVideoColor.Name = "groupVideoColor";
            this.groupVideoColor.Size = new System.Drawing.Size(152, 74);
            this.groupVideoColor.TabIndex = 0;
            this.groupVideoColor.TabStop = false;
            this.groupVideoColor.Text = "Color";
            // 
            // radioColorUYVY
            // 
            this.radioColorUYVY.AutoSize = true;
            this.radioColorUYVY.Enabled = false;
            this.radioColorUYVY.Location = new System.Drawing.Point(14, 46);
            this.radioColorUYVY.Name = "radioColorUYVY";
            this.radioColorUYVY.Size = new System.Drawing.Size(76, 16);
            this.radioColorUYVY.TabIndex = 1;
            this.radioColorUYVY.TabStop = true;
            this.radioColorUYVY.Tag = "1";
            this.radioColorUYVY.Text = "UYVY 8bit";
            this.radioColorUYVY.UseVisualStyleBackColor = true;
            // 
            // radioColorUYVY_Y
            // 
            this.radioColorUYVY_Y.AutoSize = true;
            this.radioColorUYVY_Y.Location = new System.Drawing.Point(14, 24);
            this.radioColorUYVY_Y.Name = "radioColorUYVY_Y";
            this.radioColorUYVY_Y.Size = new System.Drawing.Size(89, 16);
            this.radioColorUYVY_Y.TabIndex = 0;
            this.radioColorUYVY_Y.TabStop = true;
            this.radioColorUYVY_Y.Tag = "0";
            this.radioColorUYVY_Y.Text = "UYVY-Y 8bit";
            this.radioColorUYVY_Y.UseVisualStyleBackColor = true;
            // 
            // groupVideoVSyncLevel
            // 
            this.groupVideoVSyncLevel.Controls.Add(this.radioVSyncHighLevel);
            this.groupVideoVSyncLevel.Controls.Add(this.radioVSyncLowLevel);
            this.groupVideoVSyncLevel.Location = new System.Drawing.Point(15, 102);
            this.groupVideoVSyncLevel.Name = "groupVideoVSyncLevel";
            this.groupVideoVSyncLevel.Size = new System.Drawing.Size(152, 74);
            this.groupVideoVSyncLevel.TabIndex = 1;
            this.groupVideoVSyncLevel.TabStop = false;
            this.groupVideoVSyncLevel.Text = "VSyncLevel";
            // 
            // radioVSyncHighLevel
            // 
            this.radioVSyncHighLevel.AutoSize = true;
            this.radioVSyncHighLevel.Location = new System.Drawing.Point(15, 43);
            this.radioVSyncHighLevel.Name = "radioVSyncHighLevel";
            this.radioVSyncHighLevel.Size = new System.Drawing.Size(74, 16);
            this.radioVSyncHighLevel.TabIndex = 1;
            this.radioVSyncHighLevel.TabStop = true;
            this.radioVSyncHighLevel.Tag = "1";
            this.radioVSyncHighLevel.Text = "High level";
            this.radioVSyncHighLevel.UseVisualStyleBackColor = true;
            // 
            // radioVSyncLowLevel
            // 
            this.radioVSyncLowLevel.AutoSize = true;
            this.radioVSyncLowLevel.Location = new System.Drawing.Point(15, 21);
            this.radioVSyncLowLevel.Name = "radioVSyncLowLevel";
            this.radioVSyncLowLevel.Size = new System.Drawing.Size(71, 16);
            this.radioVSyncLowLevel.TabIndex = 0;
            this.radioVSyncLowLevel.TabStop = true;
            this.radioVSyncLowLevel.Tag = "0";
            this.radioVSyncLowLevel.Text = "Low level";
            this.radioVSyncLowLevel.UseVisualStyleBackColor = true;
            // 
            // groupVideoHSyncLevel
            // 
            this.groupVideoHSyncLevel.Controls.Add(this.radioHSyncHighLevel);
            this.groupVideoHSyncLevel.Controls.Add(this.radioHSyncLowLevel);
            this.groupVideoHSyncLevel.Location = new System.Drawing.Point(15, 187);
            this.groupVideoHSyncLevel.Name = "groupVideoHSyncLevel";
            this.groupVideoHSyncLevel.Size = new System.Drawing.Size(152, 74);
            this.groupVideoHSyncLevel.TabIndex = 2;
            this.groupVideoHSyncLevel.TabStop = false;
            this.groupVideoHSyncLevel.Text = "HSyncLevel";
            // 
            // radioHSyncHighLevel
            // 
            this.radioHSyncHighLevel.AutoSize = true;
            this.radioHSyncHighLevel.Location = new System.Drawing.Point(16, 44);
            this.radioHSyncHighLevel.Name = "radioHSyncHighLevel";
            this.radioHSyncHighLevel.Size = new System.Drawing.Size(74, 16);
            this.radioHSyncHighLevel.TabIndex = 1;
            this.radioHSyncHighLevel.TabStop = true;
            this.radioHSyncHighLevel.Tag = "1";
            this.radioHSyncHighLevel.Text = "High level";
            this.radioHSyncHighLevel.UseVisualStyleBackColor = true;
            // 
            // radioHSyncLowLevel
            // 
            this.radioHSyncLowLevel.AutoSize = true;
            this.radioHSyncLowLevel.Location = new System.Drawing.Point(16, 22);
            this.radioHSyncLowLevel.Name = "radioHSyncLowLevel";
            this.radioHSyncLowLevel.Size = new System.Drawing.Size(71, 16);
            this.radioHSyncLowLevel.TabIndex = 0;
            this.radioHSyncLowLevel.TabStop = true;
            this.radioHSyncLowLevel.Tag = "0";
            this.radioHSyncLowLevel.Text = "Low level";
            this.radioHSyncLowLevel.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.groupVideoHSyncLevel);
            this.groupBox4.Controls.Add(this.groupVideoColor);
            this.groupBox4.Controls.Add(this.groupVideoVSyncLevel);
            this.groupBox4.Location = new System.Drawing.Point(12, 131);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(182, 406);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Video setting";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numPictureFPS);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.numPictureWidth);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.numPictureHeight);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.checkPictureSizeAuto);
            this.groupBox2.Location = new System.Drawing.Point(15, 270);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 119);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PictureSize && FPS";
            // 
            // numPictureFPS
            // 
            this.numPictureFPS.Location = new System.Drawing.Point(69, 88);
            this.numPictureFPS.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPictureFPS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPictureFPS.Name = "numPictureFPS";
            this.numPictureFPS.Size = new System.Drawing.Size(70, 19);
            this.numPictureFPS.TabIndex = 6;
            this.numPictureFPS.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 12);
            this.label8.TabIndex = 5;
            this.label8.Text = "FPS";
            // 
            // numPictureWidth
            // 
            this.numPictureWidth.Location = new System.Drawing.Point(69, 41);
            this.numPictureWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPictureWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPictureWidth.Name = "numPictureWidth";
            this.numPictureWidth.Size = new System.Drawing.Size(70, 19);
            this.numPictureWidth.TabIndex = 2;
            this.numPictureWidth.Value = new decimal(new int[] {
            1280,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "Width";
            // 
            // numPictureHeight
            // 
            this.numPictureHeight.Location = new System.Drawing.Point(69, 64);
            this.numPictureHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPictureHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPictureHeight.Name = "numPictureHeight";
            this.numPictureHeight.Size = new System.Drawing.Size(70, 19);
            this.numPictureHeight.TabIndex = 4;
            this.numPictureHeight.Value = new decimal(new int[] {
            960,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "Height";
            // 
            // checkPictureSizeAuto
            // 
            this.checkPictureSizeAuto.AutoSize = true;
            this.checkPictureSizeAuto.Location = new System.Drawing.Point(16, 20);
            this.checkPictureSizeAuto.Name = "checkPictureSizeAuto";
            this.checkPictureSizeAuto.Size = new System.Drawing.Size(48, 16);
            this.checkPictureSizeAuto.TabIndex = 0;
            this.checkPictureSizeAuto.Text = "Auto";
            this.checkPictureSizeAuto.UseVisualStyleBackColor = true;
            this.checkPictureSizeAuto.CheckedChanged += new System.EventHandler(this.checkPictureSizeAuto_CheckedChanged);
            // 
            // groupFileSetting
            // 
            this.groupFileSetting.Controls.Add(this.btnRefer);
            this.groupFileSetting.Controls.Add(this.txtBaseFolder);
            this.groupFileSetting.Controls.Add(this.label11);
            this.groupFileSetting.Controls.Add(this.grpFileName);
            this.groupFileSetting.Location = new System.Drawing.Point(12, 12);
            this.groupFileSetting.Name = "groupFileSetting";
            this.groupFileSetting.Size = new System.Drawing.Size(392, 113);
            this.groupFileSetting.TabIndex = 0;
            this.groupFileSetting.TabStop = false;
            this.groupFileSetting.Text = "File setting";
            // 
            // btnRefer
            // 
            this.btnRefer.Location = new System.Drawing.Point(326, 15);
            this.btnRefer.Name = "btnRefer";
            this.btnRefer.Size = new System.Drawing.Size(55, 23);
            this.btnRefer.TabIndex = 1;
            this.btnRefer.Text = "Refer";
            this.btnRefer.UseVisualStyleBackColor = true;
            this.btnRefer.Click += new System.EventHandler(this.btnRefer_Click);
            // 
            // txtBaseFolder
            // 
            this.txtBaseFolder.AllowDrop = true;
            this.txtBaseFolder.Location = new System.Drawing.Point(85, 17);
            this.txtBaseFolder.Name = "txtBaseFolder";
            this.txtBaseFolder.ReadOnly = true;
            this.txtBaseFolder.Size = new System.Drawing.Size(235, 19);
            this.txtBaseFolder.TabIndex = 0;
            this.txtBaseFolder.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtBaseFolder_DragDrop);
            this.txtBaseFolder.DragOver += new System.Windows.Forms.DragEventHandler(this.txtBaseFolder_DragOver);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 12);
            this.label11.TabIndex = 4;
            this.label11.Text = "Base Folder";
            // 
            // grpFileName
            // 
            this.grpFileName.Controls.Add(this.radioFileAuto);
            this.grpFileName.Controls.Add(this.textFileAnyName);
            this.grpFileName.Controls.Add(this.radioFileAny);
            this.grpFileName.Location = new System.Drawing.Point(7, 37);
            this.grpFileName.Name = "grpFileName";
            this.grpFileName.Size = new System.Drawing.Size(376, 69);
            this.grpFileName.TabIndex = 2;
            this.grpFileName.TabStop = false;
            this.grpFileName.Text = "File Name";
            // 
            // radioFileAuto
            // 
            this.radioFileAuto.AutoSize = true;
            this.radioFileAuto.Location = new System.Drawing.Point(12, 18);
            this.radioFileAuto.Name = "radioFileAuto";
            this.radioFileAuto.Size = new System.Drawing.Size(179, 16);
            this.radioFileAuto.TabIndex = 0;
            this.radioFileAuto.TabStop = true;
            this.radioFileAuto.Tag = "0";
            this.radioFileAuto.Text = "Auto - yyyyMMddHHmmss.dat";
            this.radioFileAuto.UseVisualStyleBackColor = true;
            this.radioFileAuto.CheckedChanged += new System.EventHandler(this.radioFileAuto_CheckedChanged);
            // 
            // textFileAnyName
            // 
            this.textFileAnyName.Location = new System.Drawing.Point(61, 40);
            this.textFileAnyName.Name = "textFileAnyName";
            this.textFileAnyName.Size = new System.Drawing.Size(130, 19);
            this.textFileAnyName.TabIndex = 2;
            // 
            // radioFileAny
            // 
            this.radioFileAny.AutoSize = true;
            this.radioFileAny.Location = new System.Drawing.Point(12, 41);
            this.radioFileAny.Name = "radioFileAny";
            this.radioFileAny.Size = new System.Drawing.Size(43, 16);
            this.radioFileAny.TabIndex = 1;
            this.radioFileAny.TabStop = true;
            this.radioFileAny.Tag = "1";
            this.radioFileAny.Text = "Any";
            this.radioFileAny.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericCANbps);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.numericCAN_FDbps);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.numericThreshold1);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.numericThreshold2);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.checkCanSet1);
            this.groupBox6.Controls.Add(this.checkCanSet0);
            this.groupBox6.Location = new System.Drawing.Point(208, 131);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(195, 176);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Can setting";
            // 
            // numericCANbps
            // 
            this.numericCANbps.Location = new System.Drawing.Point(118, 69);
            this.numericCANbps.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericCANbps.Name = "numericCANbps";
            this.numericCANbps.Size = new System.Drawing.Size(70, 19);
            this.numericCANbps.TabIndex = 16;
            this.numericCANbps.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "CAN Bitrate";
            // 
            // numericCAN_FDbps
            // 
            this.numericCAN_FDbps.Location = new System.Drawing.Point(118, 114);
            this.numericCAN_FDbps.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericCAN_FDbps.Name = "numericCAN_FDbps";
            this.numericCAN_FDbps.Size = new System.Drawing.Size(70, 19);
            this.numericCAN_FDbps.TabIndex = 17;
            this.numericCAN_FDbps.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "CAN-FD Bitrate";
            // 
            // numericThreshold1
            // 
            this.numericThreshold1.Location = new System.Drawing.Point(118, 137);
            this.numericThreshold1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericThreshold1.Name = "numericThreshold1";
            this.numericThreshold1.Size = new System.Drawing.Size(70, 19);
            this.numericThreshold1.TabIndex = 2;
            this.numericThreshold1.Value = new decimal(new int[] {
            68,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "BRS Sample Point";
            // 
            // numericThreshold2
            // 
            this.numericThreshold2.Location = new System.Drawing.Point(118, 91);
            this.numericThreshold2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericThreshold2.Name = "numericThreshold2";
            this.numericThreshold2.Size = new System.Drawing.Size(70, 19);
            this.numericThreshold2.TabIndex = 3;
            this.numericThreshold2.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "Bit Sample Point";
            // 
            // checkCanSet1
            // 
            this.checkCanSet1.AutoSize = true;
            this.checkCanSet1.Enabled = false;
            this.checkCanSet1.Location = new System.Drawing.Point(15, 47);
            this.checkCanSet1.Name = "checkCanSet1";
            this.checkCanSet1.Size = new System.Drawing.Size(105, 16);
            this.checkCanSet1.TabIndex = 1;
            this.checkCanSet1.Text = "CAN1 recording";
            this.checkCanSet1.UseVisualStyleBackColor = true;
            // 
            // checkCanSet0
            // 
            this.checkCanSet0.AutoSize = true;
            this.checkCanSet0.Enabled = false;
            this.checkCanSet0.Location = new System.Drawing.Point(15, 25);
            this.checkCanSet0.Name = "checkCanSet0";
            this.checkCanSet0.Size = new System.Drawing.Size(105, 16);
            this.checkCanSet0.TabIndex = 0;
            this.checkCanSet0.Text = "CAN0 recording";
            this.checkCanSet0.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.checkMaxTime);
            this.groupBox7.Controls.Add(this.checkPreview);
            this.groupBox7.Controls.Add(this.numRecMaxTime);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Location = new System.Drawing.Point(208, 318);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(196, 77);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Recording setting";
            // 
            // checkMaxTime
            // 
            this.checkMaxTime.AutoSize = true;
            this.checkMaxTime.Checked = true;
            this.checkMaxTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkMaxTime.Location = new System.Drawing.Point(14, 26);
            this.checkMaxTime.Name = "checkMaxTime";
            this.checkMaxTime.Size = new System.Drawing.Size(71, 16);
            this.checkMaxTime.TabIndex = 0;
            this.checkMaxTime.Text = "Max time";
            this.checkMaxTime.UseVisualStyleBackColor = true;
            this.checkMaxTime.CheckedChanged += new System.EventHandler(this.checkMaxTime_CheckedChanged);
            // 
            // checkPreview
            // 
            this.checkPreview.AutoSize = true;
            this.checkPreview.Checked = true;
            this.checkPreview.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkPreview.Location = new System.Drawing.Point(14, 49);
            this.checkPreview.Name = "checkPreview";
            this.checkPreview.Size = new System.Drawing.Size(64, 16);
            this.checkPreview.TabIndex = 3;
            this.checkPreview.Text = "Preview";
            this.checkPreview.UseVisualStyleBackColor = true;
            // 
            // numRecMaxTime
            // 
            this.numRecMaxTime.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRecMaxTime.Location = new System.Drawing.Point(96, 25);
            this.numRecMaxTime.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.numRecMaxTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRecMaxTime.Name = "numRecMaxTime";
            this.numRecMaxTime.Size = new System.Drawing.Size(63, 19);
            this.numRecMaxTime.TabIndex = 1;
            this.numRecMaxTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "sec";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkReverse);
            this.groupBox1.Controls.Add(this.checkLsbPack);
            this.groupBox1.Location = new System.Drawing.Point(208, 401);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 77);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Setting";
            // 
            // checkReverse
            // 
            this.checkReverse.AutoSize = true;
            this.checkReverse.Location = new System.Drawing.Point(14, 47);
            this.checkReverse.Name = "checkReverse";
            this.checkReverse.Size = new System.Drawing.Size(66, 16);
            this.checkReverse.TabIndex = 1;
            this.checkReverse.Text = "Reverse";
            this.checkReverse.UseVisualStyleBackColor = true;
            // 
            // checkLsbPack
            // 
            this.checkLsbPack.AutoSize = true;
            this.checkLsbPack.Location = new System.Drawing.Point(14, 25);
            this.checkLsbPack.Name = "checkLsbPack";
            this.checkLsbPack.Size = new System.Drawing.Size(74, 16);
            this.checkLsbPack.TabIndex = 0;
            this.checkLsbPack.Text = "LSB_Pack";
            this.checkLsbPack.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.numTimeOut);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(208, 484);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(196, 53);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Time Out";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "Time Out";
            // 
            // numTimeOut
            // 
            this.numTimeOut.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numTimeOut.Location = new System.Drawing.Point(96, 22);
            this.numTimeOut.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.numTimeOut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTimeOut.Name = "numTimeOut";
            this.numTimeOut.Size = new System.Drawing.Size(63, 19);
            this.numTimeOut.TabIndex = 1;
            this.numTimeOut.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(163, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "sec";
            // 
            // RecOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 582);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupFileSetting);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.groupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RecOption";
            this.Text = "Recording-Option";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RecOption_FormClosing);
            this.Load += new System.EventHandler(this.RecOption_Load);
            this.groupVideoColor.ResumeLayout(false);
            this.groupVideoColor.PerformLayout();
            this.groupVideoVSyncLevel.ResumeLayout(false);
            this.groupVideoVSyncLevel.PerformLayout();
            this.groupVideoHSyncLevel.ResumeLayout(false);
            this.groupVideoHSyncLevel.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureFPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPictureHeight)).EndInit();
            this.groupFileSetting.ResumeLayout(false);
            this.groupFileSetting.PerformLayout();
            this.grpFileName.ResumeLayout(false);
            this.grpFileName.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCANbps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCAN_FDbps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold2)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRecMaxTime)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeOut)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.GroupBox groupVideoColor;
        private System.Windows.Forms.RadioButton radioColorUYVY;
        private System.Windows.Forms.RadioButton radioColorUYVY_Y;
        private System.Windows.Forms.GroupBox groupVideoVSyncLevel;
        private System.Windows.Forms.RadioButton radioVSyncHighLevel;
        private System.Windows.Forms.RadioButton radioVSyncLowLevel;
        private System.Windows.Forms.GroupBox groupVideoHSyncLevel;
        private System.Windows.Forms.RadioButton radioHSyncHighLevel;
        private System.Windows.Forms.RadioButton radioHSyncLowLevel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupFileSetting;
        private System.Windows.Forms.TextBox textFileAnyName;
        private System.Windows.Forms.RadioButton radioFileAny;
        private System.Windows.Forms.RadioButton radioFileAuto;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkCanSet1;
        private System.Windows.Forms.CheckBox checkCanSet0;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numRecMaxTime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkLsbPack;
        private System.Windows.Forms.CheckBox checkReverse;
        private System.Windows.Forms.NumericUpDown numericThreshold2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkPreview;
        private System.Windows.Forms.NumericUpDown numericThreshold1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkMaxTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numPictureWidth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numPictureHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkPictureSizeAuto;
        private System.Windows.Forms.NumericUpDown numericCANbps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericCAN_FDbps;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numPictureFPS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numTimeOut;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnRefer;
        private System.Windows.Forms.TextBox txtBaseFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox grpFileName;
    }
}