﻿using SVUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyLogicAnalyzerWindow
{
    /// <summary>
    /// 波形表示処理
    /// </summary>
    public partial class EasyLogicAnalyzerWindow : Form
    {
        /// <summary>
        /// 環境保存用
        /// </summary>
        SVUtil.SVEnv mEnv;

        /// <summary>
        /// Hユニットサイズ
        /// </summary>
        const double WAVE_DRAWLINE_H_UNITSIZE = 4.0;
        /// <summary>
        /// ユニットサイズ
        /// </summary>
        const int WAVE_DRAWLINE_V_UNITSIZE = 16;

        const int WAVE_DRAW_MARGIN_Y = 1;
        const int WAVE_DRAW_SCALEMARGIN_Y = 25;
        const int WAVE_DRAW_BOUNDARY_Y = 3;
        const int WAVE_DRAW_SCALEINTERVAL = 100;
        const int WAVE_DRAW_SCALELINE_LENGTH = 10;
        const int WAVE_DRAWLINE_HDCK_UNITSIZE = 2;
        const int WAVE_DRAWLINE_MIN_UNITSIZE = WAVE_DRAWLINE_HDCK_UNITSIZE;
        const int WAVE_LOWBLANK_VAL = 0;
        const int WAVE_LOWACTIVE_VAL = 1;
        const int WAVE_HIGHBLANK_VAL = 1;
        const int WAVE_HIGHACTIVE_VAL = 0;
        double SYSTEM_CLOCK = 1e2;    // 100MHz

        const int LowActive = 0;
        const int LowEdge = 0;

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        public delegate void FramePosEventHandler(object sender, FramePosEventHandlerArgs e);

        /// <summary>
        /// イベントデリゲートの宣言
        /// </summary>
        public event FramePosEventHandler FramePosEvent;

        #region 内部変数

        /// <summary>
        /// スケール値
        /// </summary>
        double mZoom = 1.0;

        /// <summary>
        /// ラベル配列
        /// </summary>
        List<Label> mLabelCtrl = new List<Label>();
        /// <summary>
        /// 表示用グラフデータ(期間内の生データ)
        /// </summary>
        ushort[] mGraphData = null;
        /// <summary>
        /// SvCanData
        /// </summary>
        public SVCanData.SvCanData mSvCanDat = null;
        /// <summary>
        /// AdditionalLine位置
        /// </summary>
        Point mAdditionalLinePos = new Point(-1, 0);
        /// <summary>
        /// マウス移動モード
        /// </summary>
        enum MouseMode
        {
            /// <summary>
            /// 無し
            /// </summary>
            None = 0,
            /// <summary>
            /// ライン追加中
            /// </summary>
            Adding = 1
        }

        /// <summary>
        /// AdditionalLineの追加モード(0:無し 1:追加中)
        /// </summary>
        MouseMode mAdditionalLineMode = MouseMode.Adding;

        /// <summary>
        /// ロード有無
        /// </summary>
        bool IsLoaded = false;

        /// <summary>
        /// メニュー右クリック位置
        /// </summary>
        Point menuLocation = new Point();

        #endregion

        #region プロパティ

        double Zoom
        {
            set
            {
                // 波形エリアサイズ取得
                Rectangle rc = PictureClient;
                mZoom = value;

                textZoom.Text = mZoom.ToString("F6");

                // 描画Lineサイズと画面幅を比べて描画できないくらい拡大されないようにする
                // ZoomUpボタンが押されたときのLineサイズ取得
                int LineSize = (int)GetDrawLineSize(rc.Width) * 2;
                if (rc.Width < LineSize)
                {
                    btnZoomUp.Enabled = false;
                }
                else
                {
                    btnZoomUp.Enabled = true;
                }
            }
            get
            {
                return (mZoom);
            }
        }

        /// <summary>
        /// データ長さの取得
        /// </summary>
        long DataLength
        {
            get
            {
                if (mSvCanDat == null)
                {
                    return (0);
                }

                return (mSvCanDat.DataLength);
            }
        }

        /// <summary>
        /// ピクチャ領域
        /// </summary>
        Rectangle PictureClient
        {
            get
            {
                Rectangle rc = new Rectangle(picGraph.Location, picGraph.Size);
                return (rc);
            }
        }

        /// <summary>
        /// VSync Level (=0:Active)
        /// </summary>
        public int VideoVSyncLevel
        {
            get
            {
                if (mSvCanDat == null)
                {
                    return (0);
                }
                return (mSvCanDat.VSyncPorarity);
            }
        }
        /// <summary>
        /// HSync Level (=0:Active)
        /// </summary>
        public int VideoHSyncLevel
        {
            get
            {
                if (mSvCanDat == null)
                {
                    return (0);
                }
                return (mSvCanDat.HSyncPorarity);
            }
        }
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="env">環境変数</param>
        public EasyLogicAnalyzerWindow(SVUtil.SVEnv env)
        {
            mEnv = env;

            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void EasyLogicAnalyzerWindow_Load(object sender, EventArgs e)
        {
            this.MinimumSize = this.Size;
            this.MaximumSize = new Size(Screen.PrimaryScreen.Bounds.Width,this.Size.Height);

            IsLoaded = true;
            Rectangle rc = this.Bounds;
            mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;

            // ピクチャボックスと同じサイズのビットマップ作成
            picGraph.Image = new Bitmap(picGraph.Size.Width, picGraph.Size.Height);

            // グラフのセットアップ
            SetupGraph();

            // 拡大率初期値
            textZoom.Text = mZoom.ToString("F6");

            // 拡大無効
            this.btnZoomUp.Enabled = false;
        }

        /// <summary>
        /// グラフのセットアップ
        /// </summary>
        public void SetupGraph()
        {
            if (IsLoaded)
            {
                // フォームの初期化
                InitForm();

                // 仮表示
                MakeGraph();
            }
        }

        /// <summary>
        /// フォームの初期化処理
        /// </summary>
        private void InitForm()
        {
            // 先頭
            hscrollbar.ValueChanged -= hscrollbar_ValueChanged;
            hscrollbar.Value = 0;
            hscrollbar.ValueChanged += hscrollbar_ValueChanged;
            // スクロール幅設定
            SetScrollRange();
            // 波形表示の見出しラベルの位置設定
            SetWaveLabelInit();
            // 周波数スタティックテキストに値を設定する
            SetFrequencyVal(0.0);
        }

        /// <summary>
        /// グラフの作成
        /// </summary>
        private void MakeGraph()
        {
            using (Graphics g = Graphics.FromImage(picGraph.Image))
            {
                g.SmoothingMode = SmoothingMode.None;

                g.FillRectangle(Brushes.Black, picGraph.ClientRectangle);

                // グラフデータの作成＆取得
                SetGraphData();
                // 目盛りをメモリデバイスコンテキストに描画
                DrawScaleLine(g);
                // メモリの数字をメモリデバイスコンテキストに描画
                DrawScaleText(g);
                // 区分け線描画
                DrawDivideLine(g);
                // VSyncをメモリデバイスコンテキストに描画
                // DrawVSyncWave(g);
                // HSyncをメモリデバイスコンテキストに描画
                // DrawHSyncWave(g);
                // DCKをメモリデバイスコンテキストに描画
                DrawDCKWave(g);

                // データ描画
                for (int i = 15; i >= 0; i--)
                {
                    DrawCDWave(g, i);
                }

                mGraphData = null;
            }

            picGraph.Refresh();
        }

        /// <summary>
        /// 波形表示用のスクロールバーにレンジ幅を設定
        /// </summary>
        void SetScrollRange()
        {
            // 波形エリアサイズ取得
            Rectangle rc = PictureClient;
#if true
            long totalHScrollSize = (long)((DataLength / 2) * Zoom * WAVE_DRAWLINE_H_UNITSIZE);
            totalHScrollSize -= rc.Width;
            if (totalHScrollSize < 0)
                totalHScrollSize = 0;

            // スクロール範囲の設定
            hscrollbar.Minimum = 0;
            hscrollbar.Maximum = totalHScrollSize;
#else
            int picWidth = rc.Width;
            double drawDataSize;

            // 描画Lineサイズ取得
            double drawWidth = GetDrawLineSize((int)WAVE_DRAWLINE_H_UNITSIZE);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // 描画データ領域サイズ取得(波形描画Line単位で割ったものが描画に必要なデータ数)
                drawDataSize = picWidth / (int)(drawWidth);
                // （データ取得とデータ描画のFor文との兼ね合いで２です）
                drawDataSize += 2;
            }
            else
            {
                // 縮小時
                // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                int skipDataSize = GetSikpDataSize((int)WAVE_DRAWLINE_H_UNITSIZE);
                // 描画データ領域サイズ取得(縮小時はデータを読み飛ばしてみるため、(通常描画時に必要なデータ数×読み飛ばし数)のデータを取得する)
                drawWidth = 1.0 / (double)skipDataSize;
                drawDataSize = picWidth * skipDataSize;    // 範囲内に描画できるデータ数
            }

            hscrollbar.Minimum = 0;
            hscrollbar.Maximum = (long)(((double)(DataLength / 2.0) - drawDataSize) * drawWidth); // - (int)((double)picWidth * drawWidth);
#endif
        }

        /// <summary>
        /// 波形表示の見出しラベルの位置設定を行います
        /// </summary>
        void SetWaveLabelInit()
        {
            int drawUnitSizeY;              // 単位描画LineサイズY
            int drawStartY;

            //mLabelCtrl.Add(labelVSync);
            mLabelCtrl.Add(labelHSync);
            mLabelCtrl.Add(labelDCK);
            mLabelCtrl.Add(labelD15);
            mLabelCtrl.Add(labelD14);
            mLabelCtrl.Add(labelD13);
            mLabelCtrl.Add(labelD12);
            mLabelCtrl.Add(labelD11);
            mLabelCtrl.Add(labelD10);
            mLabelCtrl.Add(labelD9);
            mLabelCtrl.Add(labelD8);
            mLabelCtrl.Add(labelD7);
            mLabelCtrl.Add(labelD6);
            mLabelCtrl.Add(labelD5);
            mLabelCtrl.Add(labelD4);
            mLabelCtrl.Add(labelD3);
            mLabelCtrl.Add(labelD2);
            mLabelCtrl.Add(labelD1);
            mLabelCtrl.Add(labelD0);

            // 波形エリアサイズ取得
            Rectangle rc = PictureClient;

            // 描画データ１つ当たりのLineのY方向Pixelサイズを求める
            drawUnitSizeY = WAVE_DRAWLINE_V_UNITSIZE - 4;

            Point location = new Point();
            foreach (var lbl in mLabelCtrl)
            {
                // 描画開始高さ計算
                GetDrawStartYPos(lbl, out drawStartY);
                // 変更後の位置算出
                // topは波形描画開始位置から上にdrawUnitSizeYに設定
                location.Y = rc.Top + drawStartY - drawUnitSizeY;
                // 波形描画エリア左 - Staticボックスの現在の幅
                location.X = picGraph.Location.X - lbl.Width;

                // 描画位置設定
                lbl.Location = location;
            }
        }

        /// <summary>
        /// グラフ表示用データの取得(画面分)
        /// </summary>
        void SetGraphData()
        {
            // ピクチャボックスのサイズ取得
            Rectangle picSize = PictureClient;
            int picWidth = picSize.Width;

#if true
            // データ読み込みサイズ
            long size = (long)Math.Round((double)picWidth / (Zoom * WAVE_DRAWLINE_H_UNITSIZE));
            // グラフデータ
            this.mGraphData = new ushort[size];

            // 位置算出(データ読込位置は２倍)
            long  pos = (long)((double)hscrollbar.Value / (Zoom * WAVE_DRAWLINE_H_UNITSIZE)) * 2;

            // データ取得
            this.mSvCanDat.GetData(pos, mGraphData);

#else
            int drawDataSize;

            // 描画Lineサイズ取得
            double drawWidth = GetDrawLineSize((int)WAVE_DRAWLINE_H_UNITSIZE);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // 描画データ領域サイズ取得(波形描画Line単位で割ったものが描画に必要なデータ数)
                drawDataSize = picWidth / (int)(drawWidth);
                // （データ取得とデータ描画のFor文との兼ね合いで２です）
                drawDataSize += 2;
            }
            else
            {
                // 縮小時
                // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                int skipDataSize = GetSikpDataSize((int)WAVE_DRAWLINE_H_UNITSIZE);
                // 描画データ領域サイズ取得(縮小時はデータを読み飛ばしてみるため、(通常描画時に必要なデータ数×読み飛ばし数)のデータを取得する)
                drawDataSize = picWidth * skipDataSize;
                drawWidth = 1.0 / (double)skipDataSize;
            }

            // グラフデータ
            mGraphData = new ushort[drawDataSize];
            mSvCanDat.GetData((long)((double)hscrollbar.Value / drawWidth) * 2, mGraphData);
#endif
        }

        /// <summary>
        /// 波形描画エリアの下端の位置を返却します
        /// </summary>
        /// <param name="label">該当する見出しラベル</param>
        /// <param name="drawStartY">見出しに対応した描画開始座標Yを返します。また引数に該当するIDがない場合には-1を返します</param>
        /// <returns>trueなら成功、それ以外は失敗</returns>
        bool GetDrawStartYPos(Label label, out int drawStartY)
        {
            int labelIndex = mLabelCtrl.IndexOf(label);
            if (labelIndex == -1)
            {
                drawStartY = -1;
                return (false);
            }

            // 描画エリアのY座標計算
            // 波形表示ピクチャボックスの高さから始めの波形表示位置まで（波形描画サイズ＋上マージン＋目盛り描画マージン＋区切り線描画マージン）
            int drawY = WAVE_DRAWLINE_V_UNITSIZE + WAVE_DRAW_MARGIN_Y + WAVE_DRAW_SCALEMARGIN_Y + WAVE_DRAW_BOUNDARY_Y;
            // 2つめ以降の波形描画位置計算(波形描画サイズ + (マージン × 2 ) + 境界線描画サイズ ) × 波形位置(0～)
            drawY += (WAVE_DRAWLINE_V_UNITSIZE + (WAVE_DRAW_MARGIN_Y * 2) + WAVE_DRAW_BOUNDARY_Y) * labelIndex;
            // 引数に値設定
            drawStartY = drawY;

            return (true);
        }

        /// <summary>
        /// １データで描画するLineサイズ（倍率を考慮）
        /// </summary>
        /// <param name="drawUnitSizeX">drawUnitSizeX vSyncDataの1Line描画Xサイズ</param>
        /// <returns>１データで描画するLineサイズ</returns>
        double GetDrawLineSize(int drawUnitSizeX)
        {
            return Zoom * drawUnitSizeX;
        }

        /// <summary>
        /// スクロールバーに指定した位置を設定
        /// </summary>
        /// <param name="pos">スクロールバーの現在位置</param>
        void SetScrollPos(long pos)
        {
            if (pos < 0) pos = 0;
            hscrollbar.Value = pos;
        }

        /// <summary>
        /// スクロールバーの現在位置を取得
        /// </summary>
        /// <returns>スクロールバーの現在位置</returns>
        long GetScrollPos()
        {
            return ((long)(hscrollbar.Value));
        }

        /// <summary>
        /// 波形の目盛り線を描画します
        /// </summary>
        /// <param name="g">グラフィックス</param>
        void DrawScaleLine(Graphics g)
        {
            Rectangle picSize;              // ピクチャーボックスサイズ取得
            int picWidth;                   // ピクチャボックス幅
            int i;                          // カウンタ
            Point startPos = new Point();   // 現在位置取得（描画の始点）
            Point endPos = new Point();     // 描画の終点
            int drawLinePosX;               // 描画開始位置X

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;

            // ペンの色を設定
            // ペンを変更
            using (Pen pen = new Pen(Color.Wheat, 1))
            {

                // 決められたサイズごとに縦線を引きます
                for (i = 0; i <= picWidth; i += WAVE_DRAW_SCALEINTERVAL)
                {
                    drawLinePosX = i;               // Line描画開始位置X
                    if (i == 0)
                    {
                        // 0ではLineを描画できないので１にする
                        drawLinePosX += 1;
                    }
                    else if (i == picWidth)
                    {
                        // 幅と同一の位置にはLine描画できないので２減らす
                        drawLinePosX -= 2;
                    }
                    // 描画開始位置Line設定
                    startPos.X = drawLinePosX;
                    startPos.Y = WAVE_DRAW_SCALEMARGIN_Y;
                    // 描画終了位置Line設定
                    endPos.X = drawLinePosX;
                    endPos.Y = WAVE_DRAW_SCALEMARGIN_Y - WAVE_DRAW_SCALELINE_LENGTH;

                    // 描画
                    g.DrawLine(pen, startPos, endPos);
                }
            }
        }

        /// <summary>
        /// 波形の目盛りの数字を描画します
        /// </summary>
        /// <param name="g">グラフィックス</param>
        void DrawScaleText(Graphics g)
        {
            double zoomMag;             // 描画Line単位サイズ
            Rectangle picSize;          // ピクチャーボックスサイズ取得
            int picWidth;               // ピクチャボックス幅
            int i;                      // カウンタ
            Color textColor;            // 描画textカラー
            int drawLinePosX;           // 描画開始位置X
            string scaleLable;         // 目盛りの数字格納
            double scaleLableNum;       // 目盛りの数字
            Rectangle drawTextRect = new Rectangle();         // 目盛り描画サイズ設定
            long scrollPos;              // スクロールバー現在値
            int textMargin;             // メモリ表示ボックスの余裕分を指定する
            double drawWidth;           // X方向描画サイズ

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;

            // 倍率取得
            zoomMag = Zoom;

            // ペンの色を設定
            textColor = Color.White;

            // スクロールバーの位置取得
            scrollPos = GetScrollPos();
            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            int drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;
            // 描画Lineサイズ取得
            drawWidth = GetDrawLineSize(drawUnitSizeX);

            // メモリの数字をふります
            for (i = 0; i <= picWidth; i += WAVE_DRAW_SCALEINTERVAL * 2)
            {
                drawLinePosX = i;               // Line描画開始位置X
                                                // 周波数を求める
                scaleLableNum = ConvertScrollPosToFrequency(i);
                // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
                if ((int)drawWidth >= WAVE_DRAWLINE_MIN_UNITSIZE)
                {
                    // 等倍、拡大ならば少数点以下表示する
                    scaleLable = scaleLableNum.ToString("F2");
                }
                else
                {
                    // 縮小ならば少数点以下表示しない
                    scaleLable = scaleLableNum.ToString("F0");
                }
                // メモリに単位ラベルを付けます
                if (i == 0)
                {
                    // スケールラベルに単位を付ける
                    scaleLable += "us";
                }

                // 目盛り数字描画に必要な幅と高さ計算(DT_CALCRECTフラグを付けるとCRect構造体に幅、高さが設定されて返される)
                SizeF size = g.MeasureString(scaleLable, Font);
                // 描画位置設定
                drawTextRect.Y = 0;
                drawTextRect.Height = (int)size.Height;
                textMargin = 4;
                if (i == 0)
                {
                    // 0ではLineを描画できないので１にする
                    drawTextRect.X = 1;
                    drawTextRect.Width = (int)(size.Width + 1);
                }
                else if (i == picWidth)
                {
                    // 幅と同一の位置にはLine描画できないので２減らす
                    drawTextRect.X = (i - 2) - (int)size.Width - textMargin;
                    drawTextRect.Width = (int)(size.Width + 1);
                }
                else
                {
                    drawTextRect.X = i - (int)((size.Width + textMargin) / 2);
                    drawTextRect.Width = i + (int)(size.Width + 1);
                }
                // 目盛り数字描画に必要な幅と高さ計算
                g.DrawString(scaleLable, Font, Brushes.White, drawTextRect.Location);
            }
        }

        /// <summary>
        /// 波形表示ダイアログのスクロールポジションを周波数に変換する
        /// </summary>
        /// <param name="pointX">波形表示画面上のマウスのX位置。座標開始位置(始点)は波形表示エリアの左上を(0,0)とする</param>
        /// <returns>変換した周波数</returns>
        double ConvertScrollPosToFrequency(int pointX)
        {
            double scaleLableNum;       // 目盛りの数字
#if true
            // 位置算出
            long pos = (long)Math.Round((double)(hscrollbar.Value + pointX) / (Zoom * WAVE_DRAWLINE_H_UNITSIZE));
            scaleLableNum = pos / SYSTEM_CLOCK;
#else
            long scrollPos = 0;              // スクロールバーの位置
            double sysClock;            // システムクロック
            double drawWidth;           // X方向描画サイズ

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            int drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;
            // スクロールバーの位置取得
            scrollPos = GetScrollPos();
            // 描画Lineサイズ取得
            drawWidth = GetDrawLineSize(drawUnitSizeX);
            // ボードのSystemClock取得
            sysClock = 0;
            if (sysClock == 0)
            {
                sysClock = SYSTEM_CLOCK;
            }

            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 目盛り計算((画面右端の位置+目盛り描画位置) / (描画Lineサイズ) *(周期[μ秒])
                // フレームデータ位置= (経過)波長 = (画面右端の位置+目盛り描画位置) / (描画Lineサイズ)
                // フレームデータ位置の時間[[μs] = フレームデータ位置 * 周期[μ秒](=1.0/sysClock*1000)
                scaleLableNum = ((scrollPos + (pointX/ drawWidth)) * (1.0 / sysClock) * 1000.0);
            }
            else
            {
                // 目盛り計算((画面右端の位置+目盛り描画位置)*読み飛ばし数 / (描画Lineサイズ) *(周期[μ秒])
                // フレームデータ位置= (経過)波長 = (画面右端の位置+目盛り描画位置)*読み飛ばし数 / (描画Lineサイズ)
                // フレームデータ位置の時間[[μs] = フレームデータ位置 * 周期[μ秒](=1.0/sysClock*1000)
                int skipDataSize = GetSikpDataSize(drawUnitSizeX);
                // 目盛り計算
                scaleLableNum = (((scrollPos + pointX) * skipDataSize) * (1.0 / sysClock) * 1000.0);
            }
#endif
            return scaleLableNum;
        }

        /// <summary>
        /// 縮小時のデータ読み飛ばしサイズを求める
        /// </summary>
        /// <param name="drawUnitSizeX">vSyncDataの1Line描画Xサイズ</param>
        /// <returns>データ読み飛ばしサイズ</returns>
        int GetSikpDataSize(int drawUnitSizeX)
        {
            int skipDataSize;           // 縮小時のデータ読み飛ばしサイズ
            // 現在の設定倍率取得
#if true
            skipDataSize = (int)Math.Round(1.0 / (Zoom * drawUnitSizeX));
#else
            skipDataSize = (int)Math.Round((double)WAVE_DRAWLINE_MIN_UNITSIZE / (Zoom * drawUnitSizeX));
#endif
            return skipDataSize;
        }

        /// <summary>
        /// 波形ごとの区切り線を描画する
        /// </summary>
        /// <param name="g">グラフィックス</param>
        void DrawDivideLine(Graphics g)
        {
            Rectangle picSize;              // ピクチャーボックスサイズ取得
            int picWidth;                   // ピクチャボックス幅
            int i;                          // カウンタ
            Point startPos = new Point();   // 現在位置取得（描画の始点）
            Point endPos = new Point();     // 描画の終点
            Color lineColor;                // 描画Lineカラー
            int drawLinePosY;               // 描画開始位置Y
            int divideArea;                 // 区分け領域の数

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;

            // ペンの色を設定
            lineColor = Color.FromArgb(20, 130, 20);
            // ペンを変更
            using (Pen pen = new Pen(lineColor, 1))
            {
                // 区分け領域の数設定
                divideArea = 19;

                startPos.X = 0;
                endPos.X = picWidth;

                // 決められたサイズごとに縦線を引きます
                for (i = 0; i <= divideArea; i++)
                {
                    if (i == 0)
                    {
                        // メモリと波形領域の区分け線描画位置Yを計算
                        drawLinePosY = WAVE_DRAW_SCALEMARGIN_Y + WAVE_DRAW_MARGIN_Y + (WAVE_DRAW_BOUNDARY_Y - 2);
                    }
                    else
                    {
                        // はじめの区分け線以降のLine描画
                        // メモリ領域 + (境界線領域 * i) + (((上下マージン) + 波形描画幅) * i) + (境界線 - 1)
                        drawLinePosY = WAVE_DRAW_SCALEMARGIN_Y + (WAVE_DRAW_BOUNDARY_Y * i);
                        drawLinePosY += ((WAVE_DRAW_MARGIN_Y * 2) + WAVE_DRAWLINE_V_UNITSIZE) * i;
                        drawLinePosY += (WAVE_DRAW_BOUNDARY_Y - 1);
                    }
                    // 描画開始位置Line設定
                    startPos.Y = drawLinePosY;
                    // 描画終了位置Line設定
                    endPos.Y = drawLinePosY;

                    // 描画
                    g.DrawLine(pen, startPos, endPos);
                }
            }
        }

#if false
        /// <summary>
        /// Vsync波形データ描画
        /// </summary>
        /// <param name="g">描画領域</param>
        /// <returns></returns>
        void DrawVSyncWave(Graphics g)
        {
            Rectangle picSize;          // ピクチャーボックスサイズ取得
            int picWidth;               // ピクチャボックス幅
            int picHeight;              // ピクチャーボックスの高さ
            int drawStartHight;         // 描画開始位置Yを設定する
            int drawUnitSizeX;          // x方向描画Line単位サイズ
            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;
            picHeight = picSize.Height;

            // x方向描画単位サイズ計算
            // DCK波形をHSyncとVSyncにあせるために2倍します
            // パルス波形(|~|_)を表示するのにX方向に2Line単位必要なため、
            drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;

            // VSync擬似データ作成
            byte[] data = GetVSyncData(drawUnitSizeX);
            // 描画開始位置Yを設定
            GetDrawStartYPos(labelVSync, out drawStartHight);

            // ペンを変更
            using (Pen pen = new Pen(Color.White, 1))
            {
                // Line描画
                DrawWave(g, pen, data, drawStartHight, drawUnitSizeX);
            }
        }

        /// <summary>
        /// Hsync波形データ描画
        /// </summary>
        /// <param name="g">グラフィック</param>
        void DrawHSyncWave(Graphics g)
        {
            long scrollPos;              // スクロールバー現在値
            Rectangle picSize;              // ピクチャーボックスサイズ取得
            int picWidth;               // ピクチャボックス幅
            int picHeight;              // ピクチャーボックスの高さ
            int drawUnitSizeX;          // x方向描画Line単位サイズ
            int drawStartHight;         // 描画開始位置Yを設定する

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;
            picHeight = picSize.Height;

            // スクロールバーの位置取得
            scrollPos = GetScrollPos();

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;

            // HSync擬似データ作成
            byte[] data = GetHSyncData(drawUnitSizeX);
            // 描画開始位置Yを設定
            GetDrawStartYPos(labelHSync, out drawStartHight);

            // ペンを変更
            using (Pen pen = new Pen(Color.White, 1))
            {
                // Line描画
                DrawWave(g, pen, data, drawStartHight, drawUnitSizeX);
            }
        }
#endif

        /// <summary>
        /// アクティブ・ブランキングそれぞれのデータ設定値を取得する
        /// SVOSettingのSyncModeの設定を取得し、設定に合ったActive/Blanking時の値を返却します。
        /// SyncModeがLowActiveの時にはactive = 1、blanking = 0を、
        /// HIGHActiveの時にはactive = 0、blanking = 1を返却します。
        /// </summary>
        /// <param name="level">SyncMode level</param>
        /// <param name="active">Active時の値</param>
        /// <param name="blanking">Blanking時の値</param>
        void GetActiveAndBlankDataVal(int level, out int active, out int blanking)
        {
            if (level == LowActive)
            {
                // LowActive
                blanking = WAVE_LOWBLANK_VAL;
                active = WAVE_LOWACTIVE_VAL;
            }
            else
            {
                // HighActive
                blanking = WAVE_HIGHBLANK_VAL;
                active = WAVE_HIGHACTIVE_VAL;
            }
        }

        /// <summary>
        /// DCK波形データ描画
        /// </summary>
        /// <param name="g">グラフィック</param>
        void DrawDCKWave(Graphics g)
        {
            long scrollPos;              // スクロールバー現在値
            Rectangle picSize;          // ピクチャーボックスサイズ取得
            int picWidth;               // ピクチャボックス幅
            int picHeight;              // ピクチャーボックスの高さ
            int drawStartHight;         // 描画開始位置Yを設定する
            int drawUnitSizeX;          // x方向描画Line単位サイズ
            int drawDataSize;			// 描画データの配列サイズ
            double drawWidth;

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;
            picHeight = picSize.Height;

            // スクロールバーの位置取得
            scrollPos = GetScrollPos();

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            drawUnitSizeX = WAVE_DRAWLINE_HDCK_UNITSIZE;

            // 描画開始位置Yを設定
            GetDrawStartYPos(labelDCK, out drawStartHight);

            // 描画Lineサイズ取得
            drawWidth = GetDrawLineSize(drawUnitSizeX);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
#if false
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // 描画データ領域サイズ取得(波形描画Line単位で割ったものが描画に必要なデータ数)
                drawDataSize = picWidth / (int)drawWidth;
                // （データ取得とデータ描画のFor文との兼ね合いで２です）
                drawDataSize += 2;

                // DCK擬似データ作成
                var data = new ByteBitData(GetDCKData(drawDataSize, drawUnitSizeX));

                // ペンを変更
                using (Pen pen = new Pen(Color.White, 1))
                {
                    // Line描画
                    DrawWave(g, pen, data, drawStartHight, drawUnitSizeX);
                }
            }
            else
            {
                // DCKは矩形で描画を行う
                int drawUnitSizeY = WAVE_DRAWLINE_V_UNITSIZE;
                var drawRectSize = new Rectangle(0, drawStartHight - drawUnitSizeY, this.picGraph.Width, drawUnitSizeY + 1);
                using (HatchBrush brushRect = new HatchBrush(HatchStyle.BackwardDiagonal, Color.FromArgb(255, 0, 0), Color.White))
                {
                    DrawRect(g, brushRect, drawRectSize);
                }
            }
#else
            int drawUnitSizeY = WAVE_DRAWLINE_V_UNITSIZE;
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                int step = (int)(drawWidth);
                int sw = (((scrollPos / step) % 2) == 1)? 1: 0;
                int ix;

                if (step > 1)
                {
                    ix = -(int)(scrollPos % step);
                }
                else
                {
                    ix = 0;
                }

                // 開始位置へ移動
                this.MoveTo(g, new Point(ix, drawStartHight - (drawUnitSizeY * sw)));

                // ペンを変更
                using (Pen pen = new Pen(Color.White, 1))
                {
                    // 波形を描画
                    for( int i=ix+step; i <= this.picGraph.Width; i+= step)
                    {
                        this.LineTo(g, pen, new Point(i, drawStartHight - (drawUnitSizeY * sw)));
                        sw = (sw + 1) & 0x01;
                        this.LineTo(g, pen, new Point(i, drawStartHight - (drawUnitSizeY * sw)));
                    }
                }
            }
            else
            {
                // DCKは矩形で描画を行う
                var drawRectSize = new Rectangle(0, drawStartHight - drawUnitSizeY, this.picGraph.Width, drawUnitSizeY + 1);
                using (HatchBrush brushRect = new HatchBrush(HatchStyle.BackwardDiagonal, Color.FromArgb(255, 0, 0), Color.White))
                {
                    DrawRect(g, brushRect, drawRectSize);
                }
            }
#endif

        }

        /// <summary>
        /// 画像データ波形描画
        /// </summary>
        /// <param name="g">グラフィック</param>
        /// <param name="drawBit">描画ビットデータ</param>
        /// <returns></returns>
        void DrawCDWave(Graphics g, int drawBit)
        {
            long scrollPos;              // スクロールバー現在値
            Rectangle picSize;              // ピクチャーボックスサイズ取得
            int picWidth;               // ピクチャボックス幅
            int picHeight;              // ピクチャーボックスの高さ
            int drawUnitSizeX;          // x方向描画Line単位サイズ
            int drawStartHight;         // 描画開始位置Yを設定する
            double zoomMag;             // ズーム倍率

            // ピクチャボックスのサイズ取得
            picSize = PictureClient;
            // ピクチャーボックスの幅と高さ取得
            picWidth = picSize.Width;
            picHeight = picSize.Height;

            // スクロールバーの位置取得
            scrollPos = GetScrollPos();

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;

            // 倍率取得
            zoomMag = Zoom;

            // 画像データ取得
            var drawData = new WordBitData(this.mGraphData, drawBit);

            // 描画開始位置Yを設定
            GetDrawStartYPos(mLabelCtrl[14 - drawBit + 3], out drawStartHight);

            // ペンの色を設定
            // ペンを変更
            using (Pen pen = new Pen(Color.White, 1))
            {
                // Line描画
                DrawWave(g, pen, drawData, drawStartHight, drawUnitSizeX);
            }
        }

        /// <summary>
        /// datファイルのアクティブ・ブランキングそれぞれの判定ビットを取得する
        /// </summary>
        /// <param name="wordActiveV">wordActiveV VSyncActive時の判定ビット値</param>
        /// <param name="wordActiveH">wordActiveH HSyncActive時の判定ビット値</param>
        void GetActiveAndBlankBit(out ushort wordActiveV, out ushort wordActiveH)
        {
            if (VideoVSyncLevel == LowActive)
            {
                wordActiveV = SignalBit.SYNCACTIVEMASK_V;
            }
            else
            {
                wordActiveV = 0;
            }
            if (VideoHSyncLevel == LowActive)
            {
                wordActiveH = SignalBit.SYNCACTIVEMASK_H;
            }
            else
            {
                wordActiveH = 0;
            }
        }

        /// <summary>
        /// Vsyncデータ作成
        /// VSyncデータを作成します。
        /// drawDataSize分のデータを一度に作成します。
        /// また作成開始位置はスクロールバーの位置を取得して、その位置が何フレーム目かを
        /// 判断します。その後そのフレーム番号に合ったブランク位置、アクティブ位置を
        /// 算出し、データを作成します。
        /// 作成されるデータはActive状態の間はアクティブ状態を示す値が、
        /// Blank状態の間はブランク状態を示す値をもつデータになります。
        /// </summary>
        /// <param name="drawUnitSizeX">vSyncDataの1Line描画Xサイズ</param>
        /// <returns></returns>
        byte[] GetVSyncData(int drawUnitSizeX)
        {
            int vBlankVal;              // vBlankの時の信号値
            int vActiveVal;             // vActiveの時の信号値
            int i;                      // カウンタ
                                        // データサイズ取得

            byte[] vSyncData = new byte[mGraphData.Length];

            // Active値とBlank値判定
            GetActiveAndBlankDataVal(VideoVSyncLevel, out vActiveVal, out vBlankVal);

            //制御データ値調整
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            // datファイルのSyncビット値取得
            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);

            for (i = 0; i < mGraphData.Length; i++)
            {
                if ((mGraphData[i] & SignalBit.SYNCACTIVEMASK_V) == wordSyncActive_V)
                {
                    // VSyncActive
                    vSyncData[i] = (byte)vActiveVal;
                }
                else
                {
                    // VSyncBlanking
                    vSyncData[i] = (byte)vBlankVal;
                }
                //else
                //{
                //    // datデータ範囲を超えた場合にはブランキングを出力する
                //    // vBlanking
                //    *(vSyncData + i) = vBlankVal;
                //}
            }
            return (vSyncData);
        }

        /// <summary>
        /// Hsyncデータ作成
        /// HSyncデータを作成します。
        /// drawDataSize分のデータを一度に作成します。
        /// また作成開始位置はスクロールバーの位置を取得して、その位置が何フレーム目かを
        /// 判断します。その後そのフレーム番号に合ったブランク位置、アクティブ位置を
        /// 算出し、データを作成します。
        /// 作成されるデータはActive状態の間はアクティブ状態を示す値が、
        /// Blank状態の間はブランク状態を示す値をもつデータになります。
        /// </summary>
        /// <param name="drawUnitSizeX">vSyncDataの1Line描画Xサイズ</param>
        /// <returns></returns>
        byte[] GetHSyncData(int drawUnitSizeX)
        {
            int hBlankVal;              // vBlankの時の信号値
            int hActiveVal;             // vActiveの時の信号値
            int i;                      // カウンタ
                                        // データサイズ取得

            byte[] hSyncData = new byte[mGraphData.Length];

            // Active値とBlank値判定
            GetActiveAndBlankDataVal(VideoHSyncLevel, out hActiveVal, out hBlankVal);

            //制御データ値調整
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            // datファイルのSyncビット値取得
            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);

            for (i = 0; i < mGraphData.Length; i++)
            {
                if ((mGraphData[i] & SignalBit.SYNCACTIVEMASK_H) == wordSyncActive_H)
                {
                    // hSyncActive
                    hSyncData[i] = (byte)hActiveVal;
                }
                else
                {
                    // hSyncBlanking
                    hSyncData[i] = (byte)hBlankVal;
                }
                hSyncData[i] = (byte)hActiveVal;
                //{
                //    // datデータ範囲を超えた場合にはブランキングを出力する
                //    // hSyncBlanking
                //    hSyncData[i] = hBlankVal;
                //}
            }
            return (hSyncData);
        }

        /// <summary>
        /// DCKデータ作成
        /// DCKデータを作成します。
        /// drawDataSize分のデータを一度に作成します。
        /// また作成開始位置はスクロールバーの位置を取得して、その位置が何フレーム目かを
        /// 判断します。その後そのフレーム番号に合ったブランク位置、アクティブ位置を
        /// 算出し、データを作成します。
        /// 作成されるデータはActive状態の間はアクティブ状態を示す値が、
        /// Blank状態の間はブランク状態を示す値をもつデータになります。
        /// </summary>
        /// <param name="drawDataSize">描画サイズ</param>
        /// <param name="drawUnitSizeX">DCK1lineの描画サイズ</param>
        /// <returns></returns>
        byte[] GetDCKData(int drawDataSize, int drawUnitSizeX)
        {
            int i;                      // カウンタ
            int drawCurrPos = 0;        // 現在のHSync描画位置
            int dck = 0;                // 初期DCK信号
            int pixelPol = LowEdge;     // DataInputTimeing設定値(仮)
            int drawUnitSize;           // 描画Line単位サイズ

            byte[] dckData = new byte[drawDataSize];

            // InputTiming取得
            // 描画データ１つ当たりのLineのPixelサイズを求める
            drawUnitSize = 1;

            drawCurrPos = (int)ConvertScrollPosToFramePos(drawUnitSizeX);

            // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
            for (i = 0; i < dckData.Length; i++)
            {
                // dck信号値取得
                dck = drawCurrPos % 2;
                // LowEdgeならば波形反転させる
                if (pixelPol == LowEdge)
                {
                    if (dck == 0)
                    {
                        dck = 1;
                    }
                    else
                    {
                        dck = 0;
                    }
                }
                dckData[i] = (byte)dck;
                // drawCurrentPosを移動
                drawCurrPos += drawUnitSize;
            }
            return (dckData);
        }

        /// <summary>
        /// 現在のマウスポジション(スクロールバー位置)からフレームデータの位置に変換する
        /// </summary>
        /// <param name="drawUnitSizeX">vSyncDataの1Line描画Xサイズ</param>
        /// <param name="mousePos">マウス位置</param>
        /// <returns>フレームデータ位置</returns>
        long ConvertScrollPosToFramePos(int drawUnitSizeX, int mousePos = 0)
        {
            long drawCurrPos = 0;       // 現在のHSync描画位置
            double zoomMag;             // 倍率取得用
            long scrollPos;              // スクロールバー現在値

            // 現在の設定倍率取得
            zoomMag = Zoom;

            // スクロールバーの位置取得
            scrollPos = GetScrollPos();

            // 描画Lineサイズ取得
            int drawWidth = (int)GetDrawLineSize(drawUnitSizeX);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
                drawCurrPos = scrollPos / drawWidth;
                drawCurrPos += mousePos / drawWidth;
            }
            else
            {
                // 縮小時
                // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                int skipDataSize = GetSikpDataSize(drawUnitSizeX);
                // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
                drawCurrPos = scrollPos * skipDataSize;//  / WAVE_DRAWLINE_MIN_UNITSIZE;
                // マウス位置
                drawCurrPos += mousePos * skipDataSize; //  / WAVE_DRAWLINE_MIN_UNITSIZE;

                // 補正した値が奇数の場合表示がおかしくなるので、奇数の場合偶数にします
                drawCurrPos = drawCurrPos - (drawCurrPos % 2);
            }

            return drawCurrPos;
        }

#region Win32 GUI

        Point mOldPos = new Point();

        /// <summary>
        /// ポイント移動
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pos">移動座標</param>
        void MoveTo(Graphics g, Point pos)
        {
            mOldPos = pos;
        }

        /// <summary>
        /// 直線描画
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pen">描画色</param>
        /// <param name="pos">移動座標</param>
        void LineTo(Graphics g, Pen pen, Point pos)
        {
            g.DrawLine(pen, mOldPos, pos);
            mOldPos = pos;
        }
#endregion

        /// <summary>
        /// 立上がり波形描画
        /// 立上がり波形を描画します。
        /// まず始めに縦に描画を行います。次に水平線を描画することにより、立ち上がり線を描画します。
        /// Windowsの描画は画面の左上を原点として下方向が＋となるため、立ち上がり線はマイナス方向への
        /// 縦線の描画で表現されます。
        /// また、縦線と、横線の１データで描画されるLineの単位幅については、横線は倍率設定値分、
        /// 縦線がWAVE_DRAWLINE_V_UNITSIZEで設定できます。
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pen">描画色</param>
        /// <param name="drawWidth">描画幅</param>
        void DrawUpLine(Graphics g, Pen pen, int drawWidth)
        {
            Point startPos;            // 現在位置取得（描画の始点）
            Point endPos;              // 描画の終点
            int drawHeight;             // 描画LineサイズY

            // 描画データ１つ当たりのLineのPixelサイズを求める
            drawHeight = WAVE_DRAWLINE_V_UNITSIZE;

            // 現在位置取得（描画の始点）
            startPos = mOldPos;
            // 縦線描画位置設定
            endPos = new Point(startPos.X, startPos.Y - drawHeight);
            // 立ち上がり線（縦線）を描画する
            LineTo(g, pen, endPos);
            // 終点位置を設定
            endPos.X = startPos.X + drawWidth;
            endPos.Y = startPos.Y - drawHeight;
            // 終点へ水平線を描画する
            LineTo(g, pen, endPos);
        }

        /// <summary>
        /// 立下がり波形描画
        /// 立下がり波形を描画します。
        /// まず始めに縦に描画を行います。次に水平線を描画することにより、立下がり線を描画します。
        /// Windowsの描画は画面の左上を原点として下方向が＋となるため、立下がり線はプラス方向への
        /// 縦線の描画で表現されます。
        /// また、縦線と、横線の１データで描画されるLineの単位幅については、横線は倍率設定値分、
        /// 縦線がWAVE_DRAWLINE_V_UNITSIZEで設定できます。
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pen">描画色</param>
        /// <param name="drawWidth">描画幅</param>
        void DrawDownLine(Graphics g, Pen pen, int drawWidth)
        {
            Point startPos;            // 現在位置取得（描画の始点）
            Point endPos;              // 描画の終点
            int drawHeight;             // 描画LineサイズY

            // 描画データ１つ当たりのLineのPixelサイズを求める
            drawHeight = WAVE_DRAWLINE_V_UNITSIZE;

            // 現在位置取得（描画の始点）
            startPos = mOldPos;
            // 縦線描画位置設定
            endPos = new Point(startPos.X, startPos.Y + drawHeight);
            // 立ち下がり線（縦線）を描画する
            LineTo(g, pen, endPos);
            // 終点位置を設定
            endPos.X = startPos.X + drawWidth;
            endPos.Y = startPos.Y + drawHeight;
            // 終点へ水平線を描画する
            LineTo(g, pen, endPos);
        }

        /// <summary>
        /// 水平波形描画
        /// 水平波形を描画します。
        /// 次に水平線を描画することにより、水平線を描画します。
        /// また、横線の１データで描画されるLineの単位幅については、横線は倍率設定値で設定します
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pen">描画色</param>
        /// <param name="drawWidth">描画幅</param>
        void DrawHorizontalLine(Graphics g, Pen pen, int drawWidth)
        {
            Point startPos;            // 現在位置取得（描画の始点）
            Point endPos;              // 描画の終点

            // 現在位置取得（描画の始点）
            startPos = mOldPos;
            // 終点位置を設定
            endPos = new Point(startPos.X + drawWidth, startPos.Y);

            // 前点から現点へ線を描画する
            LineTo(g, pen, endPos);
        }

        /// <summary>
        /// 四角形描画
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="brush">ブラシ</param>
        /// <param name="drawRect"></param>
        void DrawRect(Graphics g, HatchBrush brush, Rectangle drawRect)
        {
            Point startPos;            // 現在位置取得（描画の始点）

            // 現在位置取得（描画の始点）
            startPos = mOldPos;

            // 四角形描画
            g.FillRectangle(brush, drawRect);

            // 描画位置を描画した四角形まで移動する
            mOldPos = new Point(startPos.X + drawRect.Width, startPos.Y);
        }

        /// <summary>
        /// 波形データ描画
        /// </summary>
        /// <param name="g">グラフィックス</param>
        /// <param name="pen">ペン情報</param>
        /// <param name="drawData">描画データ値</param>
        /// <param name="drawStartHight">描画開始高さ位置</param>
        /// <param name="drawUnitSizeX">描画データ１つ当たりのLineのX方向Pixelサイズ</param>
        void DrawWave(Graphics g, Pen pen, BitData drawData, int drawStartHight, int drawUnitSizeX)
        {
            ushort beforeData = 0;             // 一つ前に描画したデータ
            ushort currentData = 0;            // 現在描画しようとしてるデータ
            int i;                          // カウンタ
            int j;                          // カウンタ
            long scrollPos = 0;              // スクロールバーの位置
            int drawUnitSizeY;              // 単位描画LineサイズY
            double zoomMag;                 // 設定倍率
            double drawWidth = 1.0;         // X方向描画サイズ
            int drawWidthInt = 1;           // X方向描画サイズ(int型)
            int firstDrawWidth = 0;         // 最初のデータをX方向へ描画するサイズ
            int skipDataSize;               // 縮小時のデータ読み飛ばしサイズ
            Rectangle drawRectSize;         // 縮小時の描画省略四角形を描画するサイズ
            int incompDataNum;              // データ不一致の数
            bool drawRectFlag = false;      // drawRectSizeの内容で四角形を描画するかしないかを判別するフラグ(TRUE;描画する、FALSE;描画しない)
            bool recDrawRectFlag = false;   // drawRectSizeを記録しているかどうかを判定する(TRUE;記録中、FALSE;記録してない)
            int compBeforeData = 0;         // 四角描画判定用
            int compCurrentData = 0;        // 四角描画判定用

            // 描画データ１つ当たりのLineのPixelサイズを求める
            drawUnitSizeY = WAVE_DRAWLINE_V_UNITSIZE;

            // ブラシの自動破棄
            using (HatchBrush brushRect = new HatchBrush(HatchStyle.BackwardDiagonal, Color.FromArgb(255, 0, 0), Color.White))
            {
                // 倍率を取得する
                zoomMag = Zoom;

                // 初期位置を始めのデータとする
                beforeData = drawData.GetBitData(0);

                // 初期描画位置に移動
                MoveTo(g, new Point(0, drawStartHight - (beforeData * drawUnitSizeY)));
                // スクロールバーの位置取得
                scrollPos = GetScrollPos();

                // 描画Lineサイズ計算
                drawWidth = GetDrawLineSize(drawUnitSizeX);
                // 描画LineサイズのInt型変数取得
                drawWidthInt = (int)(drawWidth);
                // 描画データサイズを取得
                // 等倍以上であれば通常処理を実施。無ければ縮小処理実施
                if (drawWidthInt > 1)
                {
                    // 等倍と拡大時の描画Lineサイズ
                    // 現在のスクロールバーの位置から最初のデータの描画Line数を取得
                    //(最初のデータはスクロール位置により全ライン描画できないため)
                    firstDrawWidth = drawWidthInt - (int)(scrollPos % drawWidthInt);
                    // 1番目のデータ描画
                    // 水平線描画
                    DrawHorizontalLine(g, pen, firstDrawWidth);
                    // 2番目以降のデータを描画
                    for (i = 1; i < drawData.Length; i++)
                    {
                        // 描画データ取得
                        currentData = drawData.GetBitData(i);

                        // 描画波形判定
                        if (beforeData == 1 && currentData == 0)
                        {
                            // 立下り波形描画
                            DrawDownLine(g, pen, drawWidthInt);
                        }
                        else if (beforeData == 0 && currentData == 1)
                        {
                            // 立ち上がり波形描画
                            DrawUpLine(g, pen, drawWidthInt);
                        }
                        else
                        {
                            // 水平線描画
                            DrawHorizontalLine(g, pen, drawWidthInt);
                        }

                        // 描画終了データを保存
                        beforeData = currentData;
                    }
                }
                else
                {
                    // 縮小時
                    // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                    skipDataSize = GetSikpDataSize(WAVE_DRAWLINE_HDCK_UNITSIZE);

                    // 初期化
                    drawRectSize = new Rectangle(0, drawStartHight - drawUnitSizeY, 0, drawUnitSizeY + 1);
                    for (i = 0; i < (drawData.Length- skipDataSize); i += skipDataSize)
                    {
                        // 描画データ取得
                        currentData = drawData.GetBitData(i);

                        // 初期化
                        incompDataNum = 0;
                        compBeforeData = currentData;
                        // 読み飛ばすデータをチェックし各データが同一の値かどうか確かめる
                        for (j = 0; j < skipDataSize; j++)
                        {
                            // データ取得
                            compCurrentData = drawData.GetBitData(i + j);
                            // データ比較
                            if (compCurrentData != compBeforeData)
                            {
                                // データが異なるのでフラグをTRUEにする
                                incompDataNum++;
                            }
                            compBeforeData = compCurrentData;
                        }
                        // データチェックの判定
                        if (incompDataNum >= 1)
                        {
                            // 2つ以上異なるデータがあった
                            if (recDrawRectFlag == false)
                            {
                                // 描画開始位置未記録状態なので記録
                                drawRectSize.X = (i / skipDataSize) * WAVE_DRAWLINE_MIN_UNITSIZE;
                                // 四角形描画位置を開始したのでフラグ変更
                                recDrawRectFlag = true;
                            }
                        }
                        else
                        {
                            // 異なるデータなし
                            if (recDrawRectFlag == true)
                            {
                                // 記録中ならばそこまでで一度四角形を描画する
                                // 描画サイズを設定する
                                drawRectSize.Width = (i / skipDataSize) * WAVE_DRAWLINE_MIN_UNITSIZE - drawRectSize.Left;
                                // 四角描画フラグON
                                drawRectFlag = true;
                                // 記録フラグOFF
                                recDrawRectFlag = false;
                            }
                        }

                        // 描画波形判定
                        if (drawRectFlag == true)
                        {
                            // 四角形描画
                            DrawRect(g, brushRect, drawRectSize);
                            // 四角形を描画したので位置情報初期化
                            drawRectSize = new Rectangle(0, drawStartHight - drawUnitSizeY, 0, drawUnitSizeY + 1);
                            // 描画開始フラグ初期化
                            drawRectFlag = false;
                        }
                        if (recDrawRectFlag == false)
                        {
                            // 記録もしておらず、四角形描画指示も無いので通常のライン描画をおこなう
                            if (beforeData == 1 && currentData == 0)
                            {
                                // 立下り波形描画
                                DrawDownLine(g, pen, WAVE_DRAWLINE_MIN_UNITSIZE);
                            }
                            else if (beforeData == 0 && currentData == 1)
                            {
                                // 立ち上がり波形描画
                                DrawUpLine(g, pen, WAVE_DRAWLINE_MIN_UNITSIZE);
                            }
                            else
                            {
                                // 水平線描画
                                DrawHorizontalLine(g, pen, WAVE_DRAWLINE_MIN_UNITSIZE);
                            }
                        }
                        else
                        {
                            // 記録中の処理
                            // 描画位置変更
                            Point startPos;            // 現在位置取得（描画の始点）
                                                       // 現在位置取得（描画の始点）
                            startPos = mOldPos;
                            // 四角形描画記録中なのでLine描画位置のY座標だけをずらす（DrawRectは描画後座標位置を描画位置に変更しないため）
                            if (beforeData == 1 && currentData == 0)
                            {
                                // Y位置をDrawDownLineの位置に移動
                                MoveTo(g, new Point(startPos.X, startPos.Y + WAVE_DRAWLINE_V_UNITSIZE));
                            }
                            else if (beforeData == 0 && currentData == 1)
                            {
                                // Y位置をDrawUpLineの位置に移動
                                MoveTo(g, new Point(startPos.X, startPos.Y - WAVE_DRAWLINE_V_UNITSIZE));
                            }

                        }
                        // 描画終了データを保存
                        beforeData = currentData;
                    }
                    // 最後まで四角形を描画していない場合はここで描画する
                    if (recDrawRectFlag == true)
                    {
                        // 位置設定
                        drawRectSize.Width = (drawData.Length / skipDataSize) * WAVE_DRAWLINE_MIN_UNITSIZE - drawRectSize.Left;
                        // 四角形描画
                        DrawRect(g, brushRect, drawRectSize);
                        // 四角形を描画したので位置情報初期化
                        drawRectSize = new Rectangle(0, drawStartHight - drawUnitSizeY, 0, drawUnitSizeY + 1);
                        // 描画開始フラグ初期化
                        recDrawRectFlag = false;
                    }
                }
            }
        }

        /// <summary>
        /// マウス位置に補助線を描画します
        /// </summary>
        /// <param name="g">メモリデバイスコンテキスト</param>
        void DrawAdditionalLine(Graphics g, Point pos)
        {
            Rectangle picRect;          // ピクチャボックスの矩形領域取得
            int drawLinePosY;           // 描画開始位置Y

            // ピクチャボックスのサイズ取得
            picRect = PictureClient;

            // 補助線描画チェックボックスの値取得
            // 補助線描画状態確認
            if (chkAddLine.Checked)
            {
                // 補助線を描画する
                // ペンの色を設定
                using (Pen pen = new Pen(Color.Red, 1))
                {
                    pen.DashStyle = DashStyle.Dot;
                    drawLinePosY = WAVE_DRAW_SCALEMARGIN_Y + WAVE_DRAW_MARGIN_Y + (WAVE_DRAW_BOUNDARY_Y - 2);
                    g.DrawLine(pen,
                        pos.X, drawLinePosY,
                        pos.X, picRect.Bottom);
                }
            }
        }

        /// <summary>
        /// 周波数設定スタティックボックスに値を描画します
        /// </summary>
        /// <param name="freq">周波数</param>
        void SetFrequencyVal(double freq)
        {
            string freqStr;            // 周波数文字列

            // マウス位置を文字列に変換
            freqStr = freq.ToString("F0");
            // μHzを付加
            freqStr += "us";

            labelFrequency.Text = freqStr;
        }


        /// <summary>
        /// Sync検索フラグ
        /// </summary>
        enum WAVE_SEARCH
        {
            NEXTVSYNCACT = 0,   //  次のVSyncActive検索
            PREVVSYNCACT,       //  前のVSyncActive検索
            NEXTHSYNCACT,       //  次のHSyncActive検索
            PREVHSYNCACT,       //  前のVSyncActive検索
        }

        /// <summary>
        /// フラグで指定された波形描画位置を検索しスクロールバーを再設定する
        /// </summary>
        /// <param name="searchFlag"検索フラグ</param>
        void SearchActivePos(WAVE_SEARCH searchFlag)
        {
            ushort wordSyncActive_V;      // VSYNC用ビットマスク
            ushort wordSyncActive_H;      // HSYNC用ビットマスク

            Point pt = this.menuLocation;

            // datファイルのSyncビット値取得
            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);

            // 検索フラグにより各値を設定する
            if (searchFlag == WAVE_SEARCH.NEXTVSYNCACT)
            {
                // 次のVSynkを探して再設定
                SearchNextActivePos(SignalBit.SYNCACTIVEMASK_V, wordSyncActive_V, pt.X);
            }
            else if (searchFlag == WAVE_SEARCH.PREVVSYNCACT)
            {
                // 前のVSynkを探して再設定
                SearchPrevActivePos(SignalBit.SYNCACTIVEMASK_V, wordSyncActive_V, pt.X);
            }
            else if (searchFlag == WAVE_SEARCH.NEXTHSYNCACT)
            {
                // 次のHSynkを探して再設定
                SearchNextActivePos(SignalBit.SYNCACTIVEMASK_H, wordSyncActive_H, pt.X);
            }
            else if (searchFlag == WAVE_SEARCH.PREVHSYNCACT)
            {
                // 前のHSynkを探して再設定
                SearchPrevActivePos(SignalBit.SYNCACTIVEMASK_H, wordSyncActive_H, pt.X);
            }
        }

        /// <summary>
        /// 現在位置から次のActive波形描画位置を検索しスクロールバーを再設定する
        /// </summary>
        /// <param name="wordSyncMask">SYNC用判定マスクビット</param>
        /// <param name="wordSyncActive">SYNCActive判定用ビット</param>
        /// <param name="mouseXpos">縮小時のデータ読み飛ばしサイズ</param>
        void SearchNextActivePos(ushort wordSyncMask, ushort wordSyncActive, int mouseXpos)
        {
            long drawCurrPos = 0;           // 現在のVSync描画位置
            long vSyncActivePos;            // vSyncActiveの位置
            long scrollPos;                  // スクロール位置
            int drawUnitSizeX;              // Line描画サイズ
            int frame_no;

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;
            // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
            drawCurrPos = ConvertScrollPosToFramePos(drawUnitSizeX, mouseXpos);
            // VSync位置記録用変数に現在地設定
            vSyncActivePos = mSvCanDat.GetNextSyncData(drawCurrPos, wordSyncMask, wordSyncActive, out frame_no);
            if (vSyncActivePos >= 0)
            {
                scrollPos = ConvertFramePosToScrollPos(drawUnitSizeX, vSyncActivePos);
                SetScrollPos(scrollPos);
                OnFramePosEvent(frame_no);
            }
        }

        /// <summary>
        /// 現在位置から前のActive波形描画位置を検索しスクロールバーを再設定する
        /// </summary>
        /// <param name="wordSyncMask">SYNC用判定マスクビット</param>
        /// <param name="wordSyncActive">SYNCActive判定用ビット</param>
        /// <param name="skipDataSize">縮小時のデータ読み飛ばしサイズ</param>
        void SearchPrevActivePos(ushort wordSyncMask, ushort wordSyncActive, int mouseXpos)
        {
            long drawCurrPos = 0;       // 現在のVSync描画位置
            long vSyncActivePos;        // vSyncActiveの位置
            long scrollPos;              // スクロール位置
            int drawUnitSizeX;          // Line描画サイズ
            int frame_no;

            // x方向描画単位サイズ計算（詳細はDrawVSyncWaveのdrawUnitSizeX部分に記述あり）
            drawUnitSizeX = (int)WAVE_DRAWLINE_H_UNITSIZE;
            // データサイズ取得
            // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
            drawCurrPos = ConvertScrollPosToFramePos(drawUnitSizeX, mouseXpos);
            // VSync位置記録用変数に現在地設定
            vSyncActivePos = mSvCanDat.GetPrevSyncData(drawCurrPos, wordSyncMask, wordSyncActive, out frame_no);
            if (vSyncActivePos >= 0)
            {
                scrollPos = ConvertFramePosToScrollPos(drawUnitSizeX, vSyncActivePos);
                SetScrollPos(scrollPos);
                OnFramePosEvent(frame_no);
            }
        }

        /// <summary>
        /// 現在のフレームデータの位置からスクロールバー位置に変換する
        /// </summary>
        /// <param name="drawUnitSizeX">vSyncDataの1Line描画Xサイズ</param>
        /// <param name="framePos">フレームデータ位置</param>
        /// <returns></returns>
        long ConvertFramePosToScrollPos(int drawUnitSizeX, long framePos)
        {
            long drawCurrPos = 0;        // 現在のHSync描画位置
#if false
            // 描画Lineサイズ取得
            int drawWidth = (int)GetDrawLineSize(drawUnitSizeX);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
                drawCurrPos = framePos * drawWidth;
            }
            else
            {
                // 縮小時
                // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                int skipDataSize = GetSikpDataSize(drawUnitSizeX);
                // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
                drawCurrPos = framePos * WAVE_DRAWLINE_MIN_UNITSIZE / skipDataSize;
                // 補正した値が奇数の場合表示がおかしくなるので、奇数の場合偶数にします
                drawCurrPos = drawCurrPos - (drawCurrPos % 2);
            }
#endif
            int drawWidth = (int)GetDrawLineSize(drawUnitSizeX);
            // 最低描画Line以上か判定(最低描画Line = 等倍、最低描画Line以上 = 拡大、最低描画Line未満 = 縮小)
            if ((int)(drawWidth) >= WAVE_DRAWLINE_MIN_UNITSIZE)
            {
                // 等倍、拡大時
                // drawCurrPos開始位置設定(倍率とLine描画単位サイズを考慮したサイズを取得する)
                drawCurrPos = framePos * drawWidth;
            }
            else
            {
                // 縮小時
                // 描画に必要なデータ数を計算する(データサイズ = 最低描画Lineサイズ / 倍率を考慮した描画Lineサイズ)
                int skipDataSize = GetSikpDataSize(drawUnitSizeX);
                drawCurrPos = framePos / skipDataSize;
                // 補正した値が奇数の場合表示がおかしくなるので、奇数の場合偶数にします
                drawCurrPos = drawCurrPos - (drawCurrPos % 2);
            }
            return drawCurrPos;
        }


        /// <summary>
        /// 縮小
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void btnZoomDown_Click(object sender, EventArgs e)
        {
            long scrollPos;          // スクロールバーの位置取得

            // スクロール位置の現在地取得
            scrollPos = GetScrollPos();
            // 倍率変更前の位置算出(現在位置/2)
            scrollPos /= 2;
            // 現在の設定倍率取得
            // 値を再設定する
            // 1/2倍する
            Zoom = Zoom / 2;
            // スクロール幅設定
            SetScrollRange();
            // 倍率変更前の位置移動
            SetScrollPos(scrollPos);
            // 波形表示ピクチャボックス再描画指示
            MakeGraph();

            //  縮小を廃止
            if (Zoom <= Math.Pow(2.0,-20.0))
            {
                this.btnZoomDown.Enabled = false;
            }
        }

        /// <summary>
        /// 拡大
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void btnZoomUp_Click(object sender, EventArgs e)
        {
            long scrollPos;          // スクロールバーの位置取得
            // スクロール位置の現在地取得
            scrollPos = GetScrollPos();
            // 倍率変更前の位置算出(現在位置*2)
            scrollPos *= 2;
            // 現在の設定倍率取得
            // 値を再設定する
            Zoom = Zoom * 2;
            // スクロール幅設定
            SetScrollRange();
            // 倍率変更前の位置移動
            SetScrollPos(scrollPos);
            // 波形表示ピクチャボックス再描画指示
            MakeGraph();

            // 無条件に縮小処理ボタンを有効にする
            this.btnZoomDown.Enabled = true;
        }

        /// <summary>
        /// スクロール処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void hscrollbar_ValueChanged(object sender, EventArgs e)
        {
            // 波形表示ピクチャボックス再描画指示
            MakeGraph();
        }

        /// <summary>
        /// 線描画
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void picGraph_Paint(object sender, PaintEventArgs e)
        {
            if (mAdditionalLineMode == MouseMode.None)
            {
                DrawAdditionalLine(e.Graphics, mAdditionalLinePos);
            }
            else
            {

            }
        }

        /// <summary>
        /// Additional Lineチェック処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void chkAddLine_CheckedChanged(object sender, EventArgs e)
        {
            picGraph.Refresh();
        }

        /// <summary>
        /// マウスクリック
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void picGraph_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (chkAddLine.Checked)
                {
                    if (mAdditionalLineMode == MouseMode.None)
                    {
                        // ライン移動開始
                        mAdditionalLineMode = MouseMode.Adding;
                    }
                    else
                    {
                        // 確定
                        mAdditionalLineMode = MouseMode.None;
                        mAdditionalLinePos = e.Location;
                        picGraph.Refresh();
                    }
                }
            }
        }

        /// <summary>
        /// マウス移動
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void picGraph_MouseMove(object sender, MouseEventArgs e)
        {
            if (chkAddLine.Checked)
            {
                if (mAdditionalLineMode == MouseMode.Adding)
                {
                    picGraph.Refresh();

                    using (var g = picGraph.CreateGraphics())
                    {
                        DrawAdditionalLine(g, e.Location);
                    }
                }
            }

            // フレーム情報表示
            SetFrequencyVal(ConvertScrollPosToFrequency(e.X));
        }

        /// <summary>
        /// VSync左側検索
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuVSyncLeft_Click(object sender, EventArgs e)
        {
            SearchActivePos(WAVE_SEARCH.PREVVSYNCACT);
        }

        /// <summary>
        /// VSync右側検索
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuVSyncRight_Click(object sender, EventArgs e)
        {
            SearchActivePos(WAVE_SEARCH.NEXTVSYNCACT);
        }

        /// <summary>
        /// HSync左側検索
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuHSyncLeft_Click(object sender, EventArgs e)
        {
            SearchActivePos(WAVE_SEARCH.PREVHSYNCACT);
        }

        /// <summary>
        /// HSync右側検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuHSyncRight_Click(object sender, EventArgs e)
        {
            SearchActivePos(WAVE_SEARCH.NEXTHSYNCACT);
        }

        /// <summary>
        /// フォーム終了処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void EasyLogicAnalyzerWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// フレーム番号のデータを設定する
        /// </summary>
        /// <param name="frame_no">フレーム番号</param>
        public void SetFrame(int frame_no)
        {
            // フレームポジションはバイト位置なので/2(WORD)とする
            hscrollbar.Value = ConvertFramePosToScrollPos((int)WAVE_DRAWLINE_H_UNITSIZE, mSvCanDat.GetFrameFirstPos(frame_no)/2);
        }


        /// <summary>
        /// フレームポジションの指定
        /// </summary>
        /// <param name="frame_no">フレーム番号(0～)</param>
        /// <param name="pt">座標データ</param>
        /// <param name="dump_pos">ダンプ位置(フレーム内)</param>
        public void SetFrameXY(int frame_no, Point pt, out int dump_pos)
        {
            dump_pos = 0;
            // フレーム範囲チェック
            if (frame_no < 0 || frame_no >= mSvCanDat.FrameMax)
            {
                return;
            }
            hscrollbar.Value = ConvertFramePosToScrollPos((int)WAVE_DRAWLINE_H_UNITSIZE, mSvCanDat.GetFrameDataPos(frame_no, pt, out dump_pos)/2);
        }

        /// <summary>
        /// 位置移動
        /// </summary>
        /// <param name="pos">位置</param>
        public void MovePosition(long pos)
        {
            hscrollbar.Value = ConvertFramePosToScrollPos((int)WAVE_DRAWLINE_H_UNITSIZE, pos/2);
        }

        /// <summary>
        /// フレームイベント
        /// </summary>
        /// <param name="frame_no">フレーム番号</param>
        void OnFramePosEvent(int frame_no)
        {
            if (FramePosEvent != null && frame_no >=0)
            {
                FramePosEventHandlerArgs e = new FramePosEventHandlerArgs();
                e.mFrameNo = frame_no;
                FramePosEvent(this, e);
            }
        }

        /// <summary>
        /// サイズの変更処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void EasyLogicAnalyzerWindow_Resize(object sender, EventArgs e)
        {
            // 初期以外にサイズ変更
            // 最小化された場合picGraph.Sizeが(0,0)になる対応を追加
            if (this.mLabelCtrl.Count != 0 && picGraph.Size.Width != 0 && picGraph.Size.Height != 0)
            {
                // ピクチャボックスと同じサイズのビットマップ作成
                picGraph.Image = new Bitmap(picGraph.Size.Width, picGraph.Size.Height);
                this.SetScrollRange();
                this.SetGraphData();
                this.MakeGraph();
            }
        }

        /// <summary>
        /// コンテキストメニューのマウス位置設定
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            var menu = sender as ContextMenuStrip;
            //マウスカーソルの位置を画面座標で取得
            Point mp = Control.MousePosition;
            //ContextMenuStripを表示しているコントロールのクライアント座標に変換
            this.menuLocation = menu.SourceControl.PointToClient(mp);
        }


        /// <summary>
        /// bitデータ取得
        /// </summary>
        class BitData
        {
            /// <summary>
            /// データ長さ
            /// </summary>
            public virtual int Length { get; }
            /// <summary>
            /// データ値取得
            /// </summary>
            /// <param name="index">インデックス値</param>
            /// <returns>データ値(0 or 1)</returns>
            public virtual byte GetBitData(long index) { return 0; }
        }

        /// <summary>
        /// Byte Bit データ
        /// </summary>
        private class ByteBitData : BitData
        {
            private byte[] buf;

            // 長さ
            public override int Length
            {
                get
                {
                    return buf.Length;
                }
            }

            /// <summary>
            /// Bit Dataの取得
            /// </summary>
            /// <param name="index">インデックス値</param>
            /// <returns>結果</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public override byte GetBitData(long index)
            {
                return this.buf[index];
            }

            /// <summary>
            /// データ設定
            /// </summary>
            /// <param name="data">バッファ</param>
            public ByteBitData(byte [] data)
            {
                this.buf = data;
            }
        }

        /// <summary>
        /// Word Bit データ
        /// </summary>
        private class WordBitData : BitData
        {
            private ushort[] buf;
            private int shift_bit;

            // 長さ
            public override int Length
            {
                get
                {
                    return buf.Length;
                }
            }

            /// <summary>
            /// Bit Dataの取得
            /// </summary>
            /// <param name="index">インデックス値</param>
            /// <returns>結果</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public override byte GetBitData(long index)
            {
                return (byte)((this.buf[index] >> shift_bit) & 0x01);
            }

            /// <summary>
            /// データ設定
            /// </summary>
            /// <param name="data">バッファ</param>
            /// <param name="sbit">シフトbit</param>
            public WordBitData(ushort[] data, int sbit)
            {
                this.buf = data;
                this.shift_bit = sbit;
            }
        }
    }

    /// <summary>
    /// イベント情報
    /// </summary>
    public class FramePosEventHandlerArgs : EventArgs
    {
        /// <summary>
        /// フレーム番号
        /// </summary>
        public int mFrameNo;
    }
}
