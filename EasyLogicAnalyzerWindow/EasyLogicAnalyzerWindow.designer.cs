﻿namespace EasyLogicAnalyzerWindow
{
    partial class EasyLogicAnalyzerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picGraph = new System.Windows.Forms.PictureBox();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuVSyncLeft = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVSyncRight = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHSyncLeft = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHSyncRight = new System.Windows.Forms.ToolStripMenuItem();
            this.chkAddLine = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelFrequency = new System.Windows.Forms.Label();
            this.btnZoomDown = new System.Windows.Forms.Button();
            this.btnZoomUp = new System.Windows.Forms.Button();
            this.labelD0 = new System.Windows.Forms.Label();
            this.labelD1 = new System.Windows.Forms.Label();
            this.labelD2 = new System.Windows.Forms.Label();
            this.labelD3 = new System.Windows.Forms.Label();
            this.labelD4 = new System.Windows.Forms.Label();
            this.labelD5 = new System.Windows.Forms.Label();
            this.labelD6 = new System.Windows.Forms.Label();
            this.labelD7 = new System.Windows.Forms.Label();
            this.labelD8 = new System.Windows.Forms.Label();
            this.labelD9 = new System.Windows.Forms.Label();
            this.labelD10 = new System.Windows.Forms.Label();
            this.labelD11 = new System.Windows.Forms.Label();
            this.labelD12 = new System.Windows.Forms.Label();
            this.labelD13 = new System.Windows.Forms.Label();
            this.labelD14 = new System.Windows.Forms.Label();
            this.labelD15 = new System.Windows.Forms.Label();
            this.labelDCK = new System.Windows.Forms.Label();
            this.labelHSync = new System.Windows.Forms.Label();
            this.textZoom = new System.Windows.Forms.TextBox();
            this.hscrollbar = new SVUtil.ScrollBarEnhanced();
            ((System.ComponentModel.ISupportInitialize)(this.picGraph)).BeginInit();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // picGraph
            // 
            this.picGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picGraph.ContextMenuStrip = this.contextMenu;
            this.picGraph.Location = new System.Drawing.Point(66, 9);
            this.picGraph.Name = "picGraph";
            this.picGraph.Size = new System.Drawing.Size(600, 427);
            this.picGraph.TabIndex = 0;
            this.picGraph.TabStop = false;
            this.picGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.picGraph_Paint);
            this.picGraph.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picGraph_MouseClick);
            this.picGraph.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picGraph_MouseMove);
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuVSyncLeft,
            this.menuVSyncRight,
            this.menuHSyncLeft,
            this.menuHSyncRight});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(181, 114);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // menuVSyncLeft
            // 
            this.menuVSyncLeft.Name = "menuVSyncLeft";
            this.menuVSyncLeft.Size = new System.Drawing.Size(180, 22);
            this.menuVSyncLeft.Text = "←VSync";
            this.menuVSyncLeft.Click += new System.EventHandler(this.menuVSyncLeft_Click);
            // 
            // menuVSyncRight
            // 
            this.menuVSyncRight.Name = "menuVSyncRight";
            this.menuVSyncRight.Size = new System.Drawing.Size(180, 22);
            this.menuVSyncRight.Text = "→VSync";
            this.menuVSyncRight.Click += new System.EventHandler(this.menuVSyncRight_Click);
            // 
            // menuHSyncLeft
            // 
            this.menuHSyncLeft.Name = "menuHSyncLeft";
            this.menuHSyncLeft.Size = new System.Drawing.Size(180, 22);
            this.menuHSyncLeft.Text = "←HSync";
            this.menuHSyncLeft.Click += new System.EventHandler(this.menuHSyncLeft_Click);
            // 
            // menuHSyncRight
            // 
            this.menuHSyncRight.Name = "menuHSyncRight";
            this.menuHSyncRight.Size = new System.Drawing.Size(180, 22);
            this.menuHSyncRight.Text = "→HSync";
            this.menuHSyncRight.Click += new System.EventHandler(this.menuHSyncRight_Click);
            // 
            // chkAddLine
            // 
            this.chkAddLine.AutoSize = true;
            this.chkAddLine.Location = new System.Drawing.Point(66, 467);
            this.chkAddLine.Name = "chkAddLine";
            this.chkAddLine.Size = new System.Drawing.Size(100, 16);
            this.chkAddLine.TabIndex = 2;
            this.chkAddLine.Text = "Additional Line";
            this.chkAddLine.UseVisualStyleBackColor = true;
            this.chkAddLine.CheckedChanged += new System.EventHandler(this.chkAddLine_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(187, 468);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Frequency";
            // 
            // labelFrequency
            // 
            this.labelFrequency.Location = new System.Drawing.Point(267, 468);
            this.labelFrequency.Name = "labelFrequency";
            this.labelFrequency.Size = new System.Drawing.Size(100, 15);
            this.labelFrequency.TabIndex = 4;
            this.labelFrequency.Text = "00000000.00us";
            // 
            // btnZoomDown
            // 
            this.btnZoomDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZoomDown.Location = new System.Drawing.Point(487, 463);
            this.btnZoomDown.Name = "btnZoomDown";
            this.btnZoomDown.Size = new System.Drawing.Size(80, 23);
            this.btnZoomDown.TabIndex = 6;
            this.btnZoomDown.Text = "ZoomDown";
            this.btnZoomDown.UseVisualStyleBackColor = true;
            this.btnZoomDown.Click += new System.EventHandler(this.btnZoomDown_Click);
            // 
            // btnZoomUp
            // 
            this.btnZoomUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZoomUp.Location = new System.Drawing.Point(581, 463);
            this.btnZoomUp.Name = "btnZoomUp";
            this.btnZoomUp.Size = new System.Drawing.Size(80, 23);
            this.btnZoomUp.TabIndex = 6;
            this.btnZoomUp.Text = "ZoomUp";
            this.btnZoomUp.UseVisualStyleBackColor = true;
            this.btnZoomUp.Click += new System.EventHandler(this.btnZoomUp_Click);
            // 
            // labelD0
            // 
            this.labelD0.AutoSize = true;
            this.labelD0.Location = new System.Drawing.Point(22, 406);
            this.labelD0.Name = "labelD0";
            this.labelD0.Size = new System.Drawing.Size(27, 12);
            this.labelD0.TabIndex = 7;
            this.labelD0.Text = "D[0]";
            // 
            // labelD1
            // 
            this.labelD1.AutoSize = true;
            this.labelD1.Location = new System.Drawing.Point(22, 385);
            this.labelD1.Name = "labelD1";
            this.labelD1.Size = new System.Drawing.Size(27, 12);
            this.labelD1.TabIndex = 7;
            this.labelD1.Text = "D[1]";
            // 
            // labelD2
            // 
            this.labelD2.AutoSize = true;
            this.labelD2.Location = new System.Drawing.Point(22, 364);
            this.labelD2.Name = "labelD2";
            this.labelD2.Size = new System.Drawing.Size(27, 12);
            this.labelD2.TabIndex = 7;
            this.labelD2.Text = "D[2]";
            // 
            // labelD3
            // 
            this.labelD3.AutoSize = true;
            this.labelD3.Location = new System.Drawing.Point(22, 343);
            this.labelD3.Name = "labelD3";
            this.labelD3.Size = new System.Drawing.Size(27, 12);
            this.labelD3.TabIndex = 7;
            this.labelD3.Text = "D[3]";
            // 
            // labelD4
            // 
            this.labelD4.AutoSize = true;
            this.labelD4.Location = new System.Drawing.Point(22, 322);
            this.labelD4.Name = "labelD4";
            this.labelD4.Size = new System.Drawing.Size(27, 12);
            this.labelD4.TabIndex = 7;
            this.labelD4.Text = "D[4]";
            // 
            // labelD5
            // 
            this.labelD5.AutoSize = true;
            this.labelD5.Location = new System.Drawing.Point(22, 301);
            this.labelD5.Name = "labelD5";
            this.labelD5.Size = new System.Drawing.Size(27, 12);
            this.labelD5.TabIndex = 7;
            this.labelD5.Text = "D[5]";
            // 
            // labelD6
            // 
            this.labelD6.AutoSize = true;
            this.labelD6.Location = new System.Drawing.Point(22, 280);
            this.labelD6.Name = "labelD6";
            this.labelD6.Size = new System.Drawing.Size(27, 12);
            this.labelD6.TabIndex = 7;
            this.labelD6.Text = "D[6]";
            // 
            // labelD7
            // 
            this.labelD7.AutoSize = true;
            this.labelD7.Location = new System.Drawing.Point(22, 259);
            this.labelD7.Name = "labelD7";
            this.labelD7.Size = new System.Drawing.Size(27, 12);
            this.labelD7.TabIndex = 7;
            this.labelD7.Text = "D[7]";
            // 
            // labelD8
            // 
            this.labelD8.AutoSize = true;
            this.labelD8.Location = new System.Drawing.Point(22, 238);
            this.labelD8.Name = "labelD8";
            this.labelD8.Size = new System.Drawing.Size(27, 12);
            this.labelD8.TabIndex = 7;
            this.labelD8.Text = "D[8]";
            // 
            // labelD9
            // 
            this.labelD9.AutoSize = true;
            this.labelD9.Location = new System.Drawing.Point(22, 217);
            this.labelD9.Name = "labelD9";
            this.labelD9.Size = new System.Drawing.Size(27, 12);
            this.labelD9.TabIndex = 7;
            this.labelD9.Text = "D[9]";
            // 
            // labelD10
            // 
            this.labelD10.AutoSize = true;
            this.labelD10.Location = new System.Drawing.Point(22, 196);
            this.labelD10.Name = "labelD10";
            this.labelD10.Size = new System.Drawing.Size(33, 12);
            this.labelD10.TabIndex = 7;
            this.labelD10.Text = "D[10]";
            // 
            // labelD11
            // 
            this.labelD11.AutoSize = true;
            this.labelD11.Location = new System.Drawing.Point(22, 175);
            this.labelD11.Name = "labelD11";
            this.labelD11.Size = new System.Drawing.Size(33, 12);
            this.labelD11.TabIndex = 7;
            this.labelD11.Text = "D[11]";
            // 
            // labelD12
            // 
            this.labelD12.AutoSize = true;
            this.labelD12.Location = new System.Drawing.Point(22, 154);
            this.labelD12.Name = "labelD12";
            this.labelD12.Size = new System.Drawing.Size(38, 12);
            this.labelD12.TabIndex = 7;
            this.labelD12.Text = "VSync";
            // 
            // labelD13
            // 
            this.labelD13.AutoSize = true;
            this.labelD13.Location = new System.Drawing.Point(22, 133);
            this.labelD13.Name = "labelD13";
            this.labelD13.Size = new System.Drawing.Size(38, 12);
            this.labelD13.TabIndex = 7;
            this.labelD13.Text = "HSync";
            // 
            // labelD14
            // 
            this.labelD14.AutoSize = true;
            this.labelD14.Location = new System.Drawing.Point(22, 112);
            this.labelD14.Name = "labelD14";
            this.labelD14.Size = new System.Drawing.Size(35, 12);
            this.labelD14.TabIndex = 7;
            this.labelD14.Text = "CAN0";
            // 
            // labelD15
            // 
            this.labelD15.AutoSize = true;
            this.labelD15.Location = new System.Drawing.Point(22, 91);
            this.labelD15.Name = "labelD15";
            this.labelD15.Size = new System.Drawing.Size(35, 12);
            this.labelD15.TabIndex = 7;
            this.labelD15.Text = "CAN1";
            // 
            // labelDCK
            // 
            this.labelDCK.AutoSize = true;
            this.labelDCK.Location = new System.Drawing.Point(22, 70);
            this.labelDCK.Name = "labelDCK";
            this.labelDCK.Size = new System.Drawing.Size(28, 12);
            this.labelDCK.TabIndex = 7;
            this.labelDCK.Text = "DCK";
            // 
            // labelHSync
            // 
            this.labelHSync.AutoSize = true;
            this.labelHSync.Location = new System.Drawing.Point(22, 49);
            this.labelHSync.Name = "labelHSync";
            this.labelHSync.Size = new System.Drawing.Size(38, 12);
            this.labelHSync.TabIndex = 7;
            this.labelHSync.Text = "LiDAR";
            // 
            // textZoom
            // 
            this.textZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textZoom.Location = new System.Drawing.Point(373, 465);
            this.textZoom.Name = "textZoom";
            this.textZoom.ReadOnly = true;
            this.textZoom.Size = new System.Drawing.Size(100, 19);
            this.textZoom.TabIndex = 9;
            // 
            // hscrollbar
            // 
            this.hscrollbar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hscrollbar.BookmarksOnTop = false;
            this.hscrollbar.LargeChange = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.hscrollbar.Location = new System.Drawing.Point(66, 442);
            this.hscrollbar.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.hscrollbar.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.hscrollbar.MinimumSize = new System.Drawing.Size(51, 0);
            this.hscrollbar.Name = "hscrollbar";
            this.hscrollbar.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.hscrollbar.QuickBookmarkNavigation = false;
            this.hscrollbar.Size = new System.Drawing.Size(600, 15);
            this.hscrollbar.SmallChange = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.hscrollbar.TabIndex = 10;
            this.hscrollbar.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.hscrollbar.ValueChanged += new System.EventHandler(this.hscrollbar_ValueChanged);
            // 
            // EasyLogicAnalyzerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 492);
            this.Controls.Add(this.hscrollbar);
            this.Controls.Add(this.textZoom);
            this.Controls.Add(this.labelDCK);
            this.Controls.Add(this.labelD15);
            this.Controls.Add(this.labelD14);
            this.Controls.Add(this.labelHSync);
            this.Controls.Add(this.labelD13);
            this.Controls.Add(this.labelD12);
            this.Controls.Add(this.labelD11);
            this.Controls.Add(this.labelD10);
            this.Controls.Add(this.labelD9);
            this.Controls.Add(this.labelD8);
            this.Controls.Add(this.labelD7);
            this.Controls.Add(this.labelD6);
            this.Controls.Add(this.labelD5);
            this.Controls.Add(this.labelD4);
            this.Controls.Add(this.labelD3);
            this.Controls.Add(this.labelD2);
            this.Controls.Add(this.labelD1);
            this.Controls.Add(this.labelD0);
            this.Controls.Add(this.btnZoomUp);
            this.Controls.Add(this.btnZoomDown);
            this.Controls.Add(this.labelFrequency);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkAddLine);
            this.Controls.Add(this.picGraph);
            this.MaximizeBox = false;
            this.Name = "EasyLogicAnalyzerWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EasyLogic Analyzer Window";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EasyLogicAnalyzerWindow_FormClosing);
            this.Load += new System.EventHandler(this.EasyLogicAnalyzerWindow_Load);
            this.Resize += new System.EventHandler(this.EasyLogicAnalyzerWindow_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.picGraph)).EndInit();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picGraph;
        private System.Windows.Forms.CheckBox chkAddLine;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFrequency;
        private System.Windows.Forms.Button btnZoomDown;
        private System.Windows.Forms.Button btnZoomUp;
        private System.Windows.Forms.Label labelD0;
        private System.Windows.Forms.Label labelD1;
        private System.Windows.Forms.Label labelD2;
        private System.Windows.Forms.Label labelD3;
        private System.Windows.Forms.Label labelD4;
        private System.Windows.Forms.Label labelD5;
        private System.Windows.Forms.Label labelD6;
        private System.Windows.Forms.Label labelD7;
        private System.Windows.Forms.Label labelD8;
        private System.Windows.Forms.Label labelD9;
        private System.Windows.Forms.Label labelD10;
        private System.Windows.Forms.Label labelD11;
        private System.Windows.Forms.Label labelD12;
        private System.Windows.Forms.Label labelD13;
        private System.Windows.Forms.Label labelD14;
        private System.Windows.Forms.Label labelD15;
        private System.Windows.Forms.Label labelDCK;
        private System.Windows.Forms.Label labelHSync;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem menuVSyncLeft;
        private System.Windows.Forms.ToolStripMenuItem menuVSyncRight;
        private System.Windows.Forms.ToolStripMenuItem menuHSyncLeft;
        private System.Windows.Forms.ToolStripMenuItem menuHSyncRight;
        private System.Windows.Forms.TextBox textZoom;
        private SVUtil.ScrollBarEnhanced hscrollbar;
    }
}