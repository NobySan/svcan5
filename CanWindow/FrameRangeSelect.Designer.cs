﻿namespace CanWindow
{
    partial class FrameRangeSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonApply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numFrameStart = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numFrameEnd = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameEnd)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(195, 67);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 2;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Frame No.";
            // 
            // numFrameStart
            // 
            this.numFrameStart.Location = new System.Drawing.Point(75, 30);
            this.numFrameStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameStart.Name = "numFrameStart";
            this.numFrameStart.Size = new System.Drawing.Size(70, 19);
            this.numFrameStart.TabIndex = 0;
            this.numFrameStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(151, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "-";
            // 
            // numFrameEnd
            // 
            this.numFrameEnd.Location = new System.Drawing.Point(168, 30);
            this.numFrameEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameEnd.Name = "numFrameEnd";
            this.numFrameEnd.Size = new System.Drawing.Size(70, 19);
            this.numFrameEnd.TabIndex = 1;
            this.numFrameEnd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // FrameRangeSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 93);
            this.Controls.Add(this.numFrameEnd);
            this.Controls.Add(this.numFrameStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonApply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrameRangeSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Any time frame range select";
            ((System.ComponentModel.ISupportInitialize)(this.numFrameStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameEnd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numFrameStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numFrameEnd;
    }
}