﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanWindow
{
    /// <summary>
    /// CAN 表示ウィンドウ
    /// </summary>
    public partial class CanWindow : Form
    {
        /// <summary>
        /// 環境保存用
        /// </summary>
        SVUtil.SVEnv mEnv;

        /// <summary>
        /// CANレコード情報
        /// </summary>
        List<SVCanData.SvCanRecord> mCanRecords = new List<SVCanData.SvCanRecord>();    // ダミー

        /// <summary>
        /// CAN番号
        /// </summary>
        int mCanNo;

        /// <summary>
        /// 最大フレーム数
        /// </summary>
        public int mFrameMax = 1;

        /// <summary>
        /// タイトル保存域
        /// </summary>
        public string mTitle;

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        public delegate void CanCSVEventHandler(object sender, CanCSVEventArgs e);

        /// <summary>
        /// イベントデリゲートの宣言
        /// </summary>
        public event CanCSVEventHandler CanCSVEvent;


        /// <summary>
        /// リストインデックスの選択
        /// </summary>
        public int SelectIndex
        {
            set
            {
                if ( value < this.dgvView.Rows.Count)
                {
                    this.dgvView.Rows[value].Selected = true;
                    this.dgvView.FirstDisplayedScrollingRowIndex = value;
                }
            }
        }

        /// <summary>
        /// 検索ID
        /// </summary>
        private int findID = -1;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CanWindow(SVUtil.SVEnv env, int can_no)
        {
            // 環境変数保存
            mEnv = env;
            mCanNo = can_no;

            InitializeComponent();

            this.mTitle = string.Format("CAN ch{0} window", can_no);

            this.Text = mTitle;
        }

        /// <summary>
        /// タイトルへ付与する
        /// </summary>
        /// <param name="text">追加するタイトル</param>
        public void AppendTitle(string text)
        {
            this.Text = mTitle + text;
        }

        /// <summary>
        /// 読込処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void CanWindow_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            mEnv.GetWindowRect(this.Name + mCanNo, ref rc);

            // 配置
            this.Bounds = rc;
        }

        /// <summary>
        /// フォーム終了処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void CanWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            mEnv.SetWindowRect(this.Name + mCanNo, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// CANレコードの設定
        /// </summary>
        /// <param name="rec">登録レコード</param>
        public void SetCanRecord(List<SVCanData.SvCanRecord> rec)
        {
            mCanRecords = rec;

            dgvView.RowCount = rec.Count();
            if (dgvView.Visible)
            {
                // メニューチェック時はフィルタ処理
                if (this.menuIDFilter.Checked)
                {
                    this.SetFilter(this.findID);
                }

                dgvView.Invalidate(true);
            }
        }

        /// <summary>
        /// セル値の取得
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void dgvView_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex >= 0 && (mCanRecords != null && (e.RowIndex <= (mCanRecords.Count-1))))
            {
                // IDLE状態か
                if (mCanRecords[e.RowIndex].frame_type == SVCanData.SvCanFrameType.IDLE)
                {
                    // 名称以外はNULL
                    if (e.ColumnIndex != 1)
                    {
                        e.Value = string.Empty;
                        return;
                    }
                }

                switch (e.ColumnIndex)
                {
                    case 0:
                        e.Value = mCanRecords[e.RowIndex].time;
                        break;
                    case 1:
                        e.Value = mCanRecords[e.RowIndex].frame_type.ToString();
                        break;
                    case 2:
                        e.Value = mCanRecords[e.RowIndex].id;
                        break;
                    case 3:
                        e.Value = mCanRecords[e.RowIndex].dlc;
                        break;
                    case 4:
                        e.Value = mCanRecords[e.RowIndex].dump_data;
                        break;
                    case 5:
                        e.Value = mCanRecords[e.RowIndex].crc.ToString("X4");
                        break;
                }
            }
        }

        /// <summary>
        /// 全フレーム出力
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuAllTimeCSV_Click(object sender, EventArgs e)
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                dlg.DefaultExt = ".csv";
                dlg.Filter = "SVI documents (.csv)|*.csv";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    CanCSVEventArgs arg = new CanCSVEventArgs();
                    arg.mode = CanCSVEventArgs.CanCSVMode.AllTime;
                    arg.file_name = dlg.FileName;
                    arg.can_no = mCanNo;
                    OnCanCSVEvent(arg);
                }
            }
        }

        /// <summary>
        /// 任意フレーム出力開始
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuAnyTimeCSV_Click(object sender, EventArgs e)
        {
            using (var dlg2 = new FrameRangeSelect())
            {
                dlg2.FrameMax = mFrameMax;
                if (dlg2.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                // 範囲設定

                using (var dlg = new SaveFileDialog())
                {
                    dlg.FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                    dlg.DefaultExt = ".csv";
                    dlg.Filter = "SVI documents (.csv)|*.csv";
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        CanCSVEventArgs arg = new CanCSVEventArgs();
                        arg.mode = CanCSVEventArgs.CanCSVMode.AnyTime;
                        arg.file_name = dlg.FileName;
                        arg.frame_start = dlg2.FrameStart;
                        arg.frame_end = dlg2.FrameEnd;
                        arg.can_no = mCanNo;
                        OnCanCSVEvent(arg);
                    }
                }
            }
        }

        /// <summary>
        /// カレントフレーム出力開始
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuCurrentFrameCSV_Click(object sender, EventArgs e)
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                dlg.DefaultExt = ".csv";
                dlg.Filter = "SVI documents (.csv)|*.csv";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    CanCSVEventArgs arg = new CanCSVEventArgs();
                    arg.mode = CanCSVEventArgs.CanCSVMode.Current;
                    arg.file_name = dlg.FileName;
                    arg.can_no = mCanNo;
                    OnCanCSVEvent(arg);
                }
            }

        }

        /// <summary>
        /// イベントハンドラの実行
        /// </summary>
        /// <param name="e">イベント情報</param>
        protected virtual void OnCanCSVEvent(CanCSVEventArgs e)
        {
            CanCSVEvent?.Invoke(this, e);
        }

        /// <summary>
        /// メニューの設定
        /// </summary>
        public void SetContentMenu()
        {
            dgvView.ContextMenuStrip = contextMenuStrip;
        }

        /// <summary>
        /// 「ID Filter」の選択
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuIDFilter_Click(object sender, EventArgs e)
        {
            if (this.menuIDFilter.Checked)
            {
                this.menuIDFilter.Checked = false;
                for( int i=0; i < this.dgvView.Rows.Count; i++)
                {
                    if (!this.dgvView.Rows[i].Visible)
                    {
                        this.dgvView.Rows[i].Visible = true;
                    }
                }
            }
            else
            {
                using (IDFilterDialog dlg = new IDFilterDialog())
                {
                    if ( dlg.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                    this.findID = dlg.FindID;
                }

                this.menuIDFilter.Checked = true;
                this.SetFilter(this.findID);
            }
        }

        /// <summary>
        /// フィルターの設定
        /// </summary>
        /// <param name="id">フィルタID</param>
        void SetFilter(int id)
        {
            for( int i=0; i < this.mCanRecords.Count; i++)
            {
                if (mCanRecords[i].id == id)
                {
                    this.dgvView.Rows[i].Visible = true;
                }
                else
                {
                    this.dgvView.Rows[i].Visible = false;
                }
            }
        }
    }

    /// <summary>
    /// CAN CSV イベント
    /// </summary>
    public class CanCSVEventArgs : EventArgs
    {
        /// <summary>
        /// CSVモード
        /// </summary>
        public enum CanCSVMode
        {
            AllTime=0,
            AnyTime,
            Current
        }
        /// <summary>
        /// モード
        /// </summary>
        public CanCSVMode mode = CanCSVMode.AllTime;
        /// <summary>
        /// 開始フレーム
        /// </summary>
        public int frame_start;
        /// <summary>
        /// 終了フレーム
        /// </summary>
        public int frame_end;
        /// <summary>
        /// ファイル名
        /// </summary>
        public string file_name;
        /// <summary>
        /// CAN番号
        /// </summary>
        public int can_no;
    }

}
