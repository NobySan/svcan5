﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanWindow
{
    /// <summary>
    /// フレーム範囲選択
    /// </summary>
    public partial class FrameRangeSelect : Form
    {
        /// <summary>
        /// 最大フレーム数
        /// </summary>
        public int FrameMax
        {
            set
            {
                numFrameStart.Maximum = value;
                numFrameEnd.Maximum = value;
            }
        }

        /// <summary>
        /// 開始フレーム
        /// </summary>
        public int FrameStart
        {
            get
            {
                return ((int)numFrameStart.Value);
            }
        }
        /// <summary>
        /// 終了フレーム
        /// </summary>
        public int FrameEnd
        {
            get
            {
                return ((int)numFrameEnd.Value);
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FrameRangeSelect()
        {
            InitializeComponent();
        }

        /// <summary>
        /// APPLY処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (numFrameStart.Value > numFrameEnd.Value)
            {
                Win32.SVMessageBox.Show(this, "The start and end frames are inverted.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DialogResult = DialogResult.OK;
        }
    }
}
