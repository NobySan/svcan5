﻿namespace CanWindow
{
    partial class CanWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuAllTimeCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAnyTimeCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCurrentFrameCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.menuIDFilter = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvView)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvView
            // 
            this.dgvView.AllowUserToAddRows = false;
            this.dgvView.AllowUserToDeleteRows = false;
            this.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dgvView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvView.Location = new System.Drawing.Point(0, 0);
            this.dgvView.MultiSelect = false;
            this.dgvView.Name = "dgvView";
            this.dgvView.RowHeadersVisible = false;
            this.dgvView.RowTemplate.Height = 21;
            this.dgvView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvView.Size = new System.Drawing.Size(875, 147);
            this.dgvView.TabIndex = 0;
            this.dgvView.VirtualMode = true;
            this.dgvView.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgvView_CellValueNeeded);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Time";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Frame Type";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "ID";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "DLC";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Data";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 400;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "CRC(h)";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAllTimeCSV,
            this.menuAnyTimeCSV,
            this.menuCurrentFrameCSV,
            this.menuIDFilter});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(184, 114);
            // 
            // menuAllTimeCSV
            // 
            this.menuAllTimeCSV.Name = "menuAllTimeCSV";
            this.menuAllTimeCSV.Size = new System.Drawing.Size(183, 22);
            this.menuAllTimeCSV.Text = "All time to CSV";
            this.menuAllTimeCSV.Click += new System.EventHandler(this.menuAllTimeCSV_Click);
            // 
            // menuAnyTimeCSV
            // 
            this.menuAnyTimeCSV.Name = "menuAnyTimeCSV";
            this.menuAnyTimeCSV.Size = new System.Drawing.Size(183, 22);
            this.menuAnyTimeCSV.Text = "Any time to CSV";
            this.menuAnyTimeCSV.Click += new System.EventHandler(this.menuAnyTimeCSV_Click);
            // 
            // menuCurrentFrameCSV
            // 
            this.menuCurrentFrameCSV.Name = "menuCurrentFrameCSV";
            this.menuCurrentFrameCSV.Size = new System.Drawing.Size(183, 22);
            this.menuCurrentFrameCSV.Text = "Current frame to CSV";
            this.menuCurrentFrameCSV.Click += new System.EventHandler(this.menuCurrentFrameCSV_Click);
            // 
            // menuIDFilter
            // 
            this.menuIDFilter.Name = "menuIDFilter";
            this.menuIDFilter.Size = new System.Drawing.Size(183, 22);
            this.menuIDFilter.Text = "ID Filter";
            this.menuIDFilter.Click += new System.EventHandler(this.menuIDFilter_Click);
            // 
            // CanWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 147);
            this.Controls.Add(this.dgvView);
            this.Location = new System.Drawing.Point(0, 538);
            this.Name = "CanWindow";
            this.RightToLeftLayout = true;
            this.Text = "CanInfomationWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CanWindow_FormClosing);
            this.Load += new System.EventHandler(this.CanWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvView)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuAllTimeCSV;
        private System.Windows.Forms.ToolStripMenuItem menuAnyTimeCSV;
        private System.Windows.Forms.ToolStripMenuItem menuCurrentFrameCSV;
        private System.Windows.Forms.ToolStripMenuItem menuIDFilter;
    }
}