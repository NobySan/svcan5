﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanWindow
{
    /// <summary>
    /// ID Filter設定画面
    /// </summary>
    public partial class IDFilterDialog : Form
    {
        /// <summary>
        /// 検索ID
        /// </summary>
        public int FindID = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public IDFilterDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 「OK」クリック
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            // 入力数値チェック
            if (!int.TryParse(this.txtID.Text, out this.FindID))
            {
                this.DialogResult = DialogResult.None;
                MessageBox.Show("You cannot enter anything other than numbers.", "Exclamation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

        }
    }
}
