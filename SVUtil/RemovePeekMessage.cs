﻿namespace SVUtil
{
    using Win32;
    using System.Runtime.InteropServices;
  
    
    /// <summary>
    /// 保留されているウィンドウメッセージを削除
    /// </summary>
    public static class RemovePeekMessage
    {

        #region メソッド
        /// <summary>
        /// 保留されているウィンドウメッセージを削除します。
        /// </summary>
        public static void Invoke()
        {
            User32.MSG wm;
            while (User32.PeekMessage(out wm, 0, 0, 0, User32.EPeekMessageOption.PM_REMOVE))
            {
                switch (wm.msg)
                {
                    case User32.WM_LBUTTONUP:
                    case User32.WM_MOUSELEAVE:
                    case User32.WM_PAINT:
                        User32.DispatchMessage(out wm);
                        break;
                }
            }
        }
        #endregion
    }
}
