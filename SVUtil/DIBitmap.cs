﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Win32;

namespace SVUtil
{
    /// <summary>
    /// DIBビットマップ
    /// </summary>
    public class DIBitmap : IDisposable
    {
        /// <summary>
        /// ビットマップハンドル
        /// </summary>
        IntPtr m_bitmap = IntPtr.Zero;
        /// <summary>
        /// ビットマップヘッダ
        /// </summary>
        Gdi32.BITMAPINFOHEADER256 m_binfo = new Gdi32.BITMAPINFOHEADER256();
        /// <summary>
        /// ビットマップ先頭アドレス
        /// </summary>
        IntPtr m_mem = IntPtr.Zero;
        /// <summary>
        /// 4バイトバウンダリ
        /// </summary>
        int m_bytealign_pitch = 0;

        /// <summary>
        /// Bitmapハンドル
        /// </summary>
        public IntPtr Handle
        {
            get
            {
                return (m_bitmap);
            }
        }

        /// <summary>
        /// DIB Bitmap
        /// </summary>
        public DIBitmap()
        {
            m_binfo.Init();
        }

        /// <summary>
        /// サイズ
        /// </summary>
        /// <param name="width">幅</param>
        /// <param name="height">高さ</param>
        /// <param name="bit">１ピクセルのビット数</param>
        /// <returns>ビットマップハンドル</returns>
        public IntPtr CreateDIB(int width, int height, int bit)
        {
            if (m_bitmap != IntPtr.Zero)
            {
                Gdi32.DeleteObject(m_bitmap);
                m_bitmap = IntPtr.Zero;
//                Marshal.FreeHGlobal(m_mem);
            }

            m_binfo.header.biBitCount = (ushort)bit;
            switch (bit)
            {
                case 8:
                    m_bytealign_pitch = (width + 3) / 4;
                    m_bytealign_pitch = m_bytealign_pitch * 4;
                    break;
                case 16:
                    m_bytealign_pitch = (width * 2 + 3) / 4;
                    m_bytealign_pitch = m_bytealign_pitch * 4;
                    m_binfo.header.biClrUsed = 0;
                    break;
                case 24:
                    m_bytealign_pitch = (width * 3 + 3) / 4;
                    m_bytealign_pitch = m_bytealign_pitch * 4;
                    m_binfo.header.biClrUsed = 0;
                    break;
            }

            m_binfo.header.biWidth = width;
            m_binfo.header.biHeight = height;

            IntPtr hdc = User32.GetDC(IntPtr.Zero);
            m_bitmap = Gdi32.CreateDIBSection(hdc, ref m_binfo, 0, out m_mem, IntPtr.Zero, 0);
            User32.ReleaseDC(IntPtr.Zero, hdc);


            return (m_bitmap);
        }

        /// <summary>
        /// イメージポインタアドレスの取得
        /// </summary>
        /// <returns>イメージポインタ</returns>
        public IntPtr GetImagePtr()
        {
            return m_mem;
        }

        /// <summary>
        /// イメージデータの設定
        /// </summary>
        /// <param name="data">イメージバッファ</param>
        public void SetImage( byte[] data )
        {
            int pitch = m_binfo.header.biBitCount / 8;
            unsafe
            {
                fixed (byte* src = data)
                {
                    byte* src2 = src;
                    byte* dst = (byte*)m_mem;
                    for (int ih = 0; ih < Math.Abs(m_binfo.header.biHeight); ih++)
                    {
                        for (int iw = 0; iw < m_binfo.header.biWidth* pitch; iw++)
                        {
                            // メモリ不足?
                            if ((int)(ih * m_binfo.header.biWidth * pitch + iw) >= data.Length) break;

                            dst[iw] = src2[iw];
                        }
                        src2 += m_binfo.header.biWidth * pitch;
                        dst += m_bytealign_pitch;
                    }
                }
            }
        }

        /// <summary>
        /// 開放
        /// </summary>
        public void FreeBitmap()
        {
            if (m_bitmap != IntPtr.Zero)
            {
                Win32.Gdi32.DeleteObject(m_bitmap);
                m_bitmap = IntPtr.Zero;
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (m_bitmap != IntPtr.Zero)
                    {
                        Win32.Gdi32.DeleteObject(m_bitmap);
                        m_bitmap = IntPtr.Zero;
                    }
                }
                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~DIBitmap256() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        void IDisposable.Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
