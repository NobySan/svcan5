﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SVUtil
{
    /// <summary>
    /// SV共通環境変数クラス
    /// </summary>
    [DataContract]
    public class SVEnv
    {
        /// <summary>
        /// ウィンドウ配置情報
        /// </summary>
        [DataMember(Name = "Window")]
        Dictionary<string, Rectangle> mWindow = new Dictionary<string, Rectangle>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SVEnv()
        {
        }

        /// <summary>
        /// ウィンドウの情報を設定
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="rc">表示位置</param>
        public void SetWindowRect(string key, Rectangle rc)
        {
            mWindow[key] = rc;
        }

        /// <summary>
        /// ウィンドウ値の取得
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="rc">表示位置</param>
        public void GetWindowRect(string key, ref Rectangle rc)
        {
            if (mWindow.ContainsKey(key))
            {
                Rectangle orc = mWindow[key];

                // 表示位置がウィンドウ内におさまる場合はそのウィンドウ領域を返却
                foreach (var screen in System.Windows.Forms.Screen.AllScreens)
                {
                    if (screen.Bounds.Contains(orc.Location))
                    {
                        rc = orc;
                        return;
                    }
                }
            }
            // 無い場合は登録
            mWindow[key] = rc;
        }
    }
}
