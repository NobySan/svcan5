﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVUtil
{
    /// <summary>
    /// 進捗ウィンドウ
    /// </summary>
    public partial class ProgressWindow : Form
    {
        /// <summary>
        /// 中止フラグ
        /// </summary>
        private bool _abort = false;
        public bool IsAbort
        {
            get
            {
                return (_abort);
            }
            set
            {
                _abort = value;
            }
        }

        int m_progress_value;


        /// <summary>
        /// 進捗ウィンドウ
        /// </summary>
        public ProgressWindow()
        {
            InitializeComponent();

            progressBar.Minimum = 0;
            progressBar.Maximum = 100;
            m_progress_value = 0;
            timer_event.Enabled = true;
        }

        /// <summary>
        /// 中止クリック
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonAbort_Click(object sender, EventArgs e)
        {
            _abort = true;
        }

        /// <summary>
        /// 進捗設定
        /// </summary>
        /// <param name="value">進捗値</param>
        public void SetProgress(int value)
        {
            m_progress_value = value;
        }

        /// <summary>
        /// 最後の処理
        /// </summary>
        public void Finish()
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                timer_event.Enabled = false;
                if ( _abort)
                {
                    DialogResult = DialogResult.Cancel;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            });
        }

        /// <summary>
        /// タイマイベント（進捗表示用)
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void timer_event_Tick(object sender, EventArgs e)
        {
            progressBar.Value = m_progress_value;
        }
    }
}
