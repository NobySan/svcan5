﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVUtil
{
    /// <summary>
    /// 各信号線ビット
    /// </summary>
    static public class SignalBit
    {
        /// <summary>
        /// VSync信号ビット
        /// </summary>
        static public ushort SYNCACTIVEMASK_V = 0x1000;
        /// <summary>
        /// HSync信号ビット
        /// </summary>
        static public ushort SYNCACTIVEMASK_H = 0x2000;
        /// <summary>
        /// VHのマスク
        /// </summary>
        static public ushort SYNCACTIVEMASK_VH = 0x3000;
        /// <summary>
        /// CAN0のマスク
        /// </summary>
        static public ushort MASK_CAN0 = 0x4000;
        /// <summary>
        /// CAN1のマスク
        /// </summary>
        static public ushort MASK_CAN1 = 0x8000;
    }
}
