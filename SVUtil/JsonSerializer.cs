﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace SVUtil
{
    /// <summary>
    /// JsonSerializer
    /// </summary>
    public class JsonSerializer
    {
        /// <summary>
        /// 通常用
        /// </summary>
        /// <typeparam name="TYpe">任意の型</typeparam>
        /// <returns></returns>
        public static DataContractJsonSerializer Serializer<TYpe>() => new DataContractJsonSerializer(typeof(TYpe));

        /// <summary>
        /// Listオブジェクト用
        /// </summary>
        /// <typeparam name="TYpe">任意の型</typeparam>
        /// <returns></returns>
        public static DataContractJsonSerializer SerializerList<TYpe>() => new DataContractJsonSerializer(typeof(List<TYpe>));

        /// <summary>
        /// Dictionaryオブジェクト用
        /// </summary>
        /// <typeparam name="TYpe1">任意の型</typeparam>
        /// <typeparam name="TYpe2">任意の型</typeparam>
        /// <returns></returns>
        public static DataContractJsonSerializer SerializerDictionary<TYpe1, TYpe2>() => new DataContractJsonSerializer(typeof(Dictionary<TYpe1, TYpe2>));
    }
}
