﻿namespace SVUtil
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 拡張チェックボックス
    /// </summary>
    public class CheckBoxEx : System.Windows.Forms.CheckBox
    {
        #region メンバ
        private bool _AllowDoubleClick;
        #endregion

        #region コンストラクタ
        public CheckBoxEx()
        {
            this._AllowDoubleClick = false;
        }
        #endregion

        #region プロパティ
        [Category("動作")]
        [DefaultValue(false)]
        [Description("２度押しを許可するか否かを取得または設定します。")]
        public bool AllowDoubleClick
        {
            get { return this._AllowDoubleClick; }
            set { this._AllowDoubleClick = value; }
        }
        #endregion

        #region オーバーライド
        /// <summary>
        /// OnClick
        /// </summary>
        /// <param name="e">イベントデータ</param>
        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);

            if (!this._AllowDoubleClick)
            {
                //保留されているメッセージを削除し、２度押しを許可しない
                RemovePeekMessage.Invoke();
            }
        }
        #endregion
    }
}
