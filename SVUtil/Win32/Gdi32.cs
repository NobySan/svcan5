﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Win32
{
    /// <summary>
    /// Gdi32.dll
    /// </summary>
    static public class Gdi32
    {
        [DllImport("gdi32.dll")]
        public static extern bool StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int nHeightDest,
                                             IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight,
           IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern void DeleteObject(IntPtr hgdiobj);

        [DllImport("gdi32.dll", EntryPoint = "CreateDIBSection")]
        public static extern IntPtr CreateDIBSection(IntPtr hdc, ref BITMAPINFOHEADER256 pbmi,
                             uint iUsage, out IntPtr ppvBits, IntPtr hSection, uint dwOffset);

        [DllImport("gdi32.dll", EntryPoint = "SetStretchBltMode")]
        public static extern bool SetStretchBltMode(IntPtr hdc, StrechModeFlags flag);

        /// <summary>
        /// ストレッチモード
        /// </summary>
        public enum StrechModeFlags: uint
        {
            BLACKONWHITE = 1,
            WHITEONBLACK = 2,
            COLORONCOLOR = 3,
            HALFTONE = 4,
            MAXSTRETCHBLTMODE = 4
        }

        /// <summary>
        /// BIT情報
        /// </summary>
        public enum BitmapCompressionMode : uint
        {
            BI_RGB = 0,
            BI_RLE8 = 1,
            BI_RLE4 = 2,
            BI_BITFIELDS = 3,
            BI_JPEG = 4,
            BI_PNG = 5
        }

        /// <summary>
        /// ビットマップ操作
        /// </summary>
        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020,
            SRCPAINT = 0x00EE0086,
            SRCAND = 0x008800C6,
            SRCINVERT = 0x00660046,
            SRCERASE = 0x00440328,
            NOTSRCCOPY = 0x00330008,
            NOTSRCERASE = 0x001100A6,
            MERGECOPY = 0x00C000CA,
            MERGEPAINT = 0x00BB0226,
            PATCOPY = 0x00F00021,
            PATPAINT = 0x00FB0A09,
            PATINVERT = 0x005A0049,
            DSTINVERT = 0x00550009,
            BLACKNESS = 0x00000042,
            WHITENESS = 0x00FF0062,
            CAPTUREBLT = 0x40000000
        }

        /// <summary>
        /// ビットマップヘッダ
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct BITMAPINFOHEADER
        {
            /// <summary>
            /// サイズ
            /// </summary>
            public uint biSize;
            /// <summary>
            /// 幅
            /// </summary>
            public int biWidth;
            /// <summary>
            /// 高さ
            /// </summary>
            public int biHeight;
            /// <summary>
            /// プレーン数
            /// </summary>
            public ushort biPlanes;
            /// <summary>
            /// ビット幅
            /// </summary>
            public ushort biBitCount;
            /// <summary>
            /// 圧縮方式
            /// </summary>
            public BitmapCompressionMode biCompression;
            /// <summary>
            /// イメージサイズ
            /// </summary>
            public uint biSizeImage;
            /// <summary>
            /// X pixel meter
            /// </summary>
            public int biXPelsPerMeter;
            /// <summary>
            /// Y pixel meter
            /// </summary>
            public int biYPelsPerMeter;
            /// <summary>
            /// カラーパレットの使用
            /// </summary>
            public uint biClrUsed;
            /// <summary>
            /// カラーパレット
            /// </summary>
            public uint biClrImportant;

            /// <summary>
            /// 初期化
            /// </summary>
            public void Init()
            {
                biSize = (uint)Marshal.SizeOf(this);
                biCompression = BitmapCompressionMode.BI_RGB;
                biPlanes = 1;
                biBitCount = 8;
                biXPelsPerMeter = 0;
                biYPelsPerMeter = 0;
                biClrUsed = 0;
                biClrImportant = 0;
                biSizeImage = 0;
            }
        }

        /// <summary>
        /// 256ビットマップ
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct BITMAPINFOHEADER256
        {
            /// <summary>
            /// ビットマップヘッダ
            /// </summary>
            public BITMAPINFOHEADER header;

            /// <summary>
            /// RGBパレット
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
            public RGBQUAD[] rgbgray;

            /// <summary>
            /// 初期化
            /// </summary>
            public void Init()
            {
                rgbgray = new RGBQUAD[256];

                header.Init();
                for (int i = 0; i < rgbgray.Length; i++)
                {
                    rgbgray[i].rgbRed = (byte)i;
                    rgbgray[i].rgbGreen = (byte)i;
                    rgbgray[i].rgbBlue = (byte)i;
                    rgbgray[i].rgbReserved = 0xff;
                }
            }
        }

        /// <summary>
        /// RGBパレット
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RGBQUAD
        {
            public byte rgbBlue;
            public byte rgbGreen;
            public byte rgbRed;
            public byte rgbReserved;
        }
    }
}
