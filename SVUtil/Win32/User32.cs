﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Win32
{
    /// <summary>
    /// user32.dll 
    /// </summary>
    static public class User32
    {
        [Flags]
        public enum FileAccess : uint
        {
            //
            // Standart Section
            //

            AccessSystemSecurity = 0x1000000,   // AccessSystemAcl access type
            MaximumAllowed = 0x2000000,     // MaximumAllowed access type

            Delete = 0x10000,
            ReadControl = 0x20000,
            WriteDAC = 0x40000,
            WriteOwner = 0x80000,
            Synchronize = 0x100000,

            StandardRightsRequired = 0xF0000,
            StandardRightsRead = ReadControl,
            StandardRightsWrite = ReadControl,
            StandardRightsExecute = ReadControl,
            StandardRightsAll = 0x1F0000,
            SpecificRightsAll = 0xFFFF,

            FILE_READ_DATA = 0x0001,        // file & pipe
            FILE_LIST_DIRECTORY = 0x0001,       // directory
            FILE_WRITE_DATA = 0x0002,       // file & pipe
            FILE_ADD_FILE = 0x0002,         // directory
            FILE_APPEND_DATA = 0x0004,      // file
            FILE_ADD_SUBDIRECTORY = 0x0004,     // directory
            FILE_CREATE_PIPE_INSTANCE = 0x0004, // named pipe
            FILE_READ_EA = 0x0008,          // file & directory
            FILE_WRITE_EA = 0x0010,         // file & directory
            FILE_EXECUTE = 0x0020,          // file
            FILE_TRAVERSE = 0x0020,         // directory
            FILE_DELETE_CHILD = 0x0040,     // directory
            FILE_READ_ATTRIBUTES = 0x0080,      // all
            FILE_WRITE_ATTRIBUTES = 0x0100,     // all

            //
            // Generic Section
            //

            GenericRead = 0x80000000,
            GenericWrite = 0x40000000,
            GenericExecute = 0x20000000,
            GenericAll = 0x10000000,

            SPECIFIC_RIGHTS_ALL = 0x00FFFF,
            FILE_ALL_ACCESS =
            StandardRightsRequired |
            Synchronize |
            0x1FF,

            FILE_GENERIC_READ =
            StandardRightsRead |
            FILE_READ_DATA |
            FILE_READ_ATTRIBUTES |
            FILE_READ_EA |
            Synchronize,

            FILE_GENERIC_WRITE =
            StandardRightsWrite |
            FILE_WRITE_DATA |
            FILE_WRITE_ATTRIBUTES |
            FILE_WRITE_EA |
            FILE_APPEND_DATA |
            Synchronize,

            FILE_GENERIC_EXECUTE =
            StandardRightsExecute |
              FILE_READ_ATTRIBUTES |
              FILE_EXECUTE |
              Synchronize
        }

        [Flags]
        public enum FileShare : uint
        {
            /// <summary>
            /// 
            /// </summary>
            None = 0x00000000,
            /// <summary>
            /// Enables subsequent open operations on an object to request read access. 
            /// Otherwise, other processes cannot open the object if they request read access. 
            /// If this flag is not specified, but the object has been opened for read access, the function fails.
            /// </summary>
            Read = 0x00000001,
            /// <summary>
            /// Enables subsequent open operations on an object to request write access. 
            /// Otherwise, other processes cannot open the object if they request write access. 
            /// If this flag is not specified, but the object has been opened for write access, the function fails.
            /// </summary>
            Write = 0x00000002,
            /// <summary>
            /// Enables subsequent open operations on an object to request delete access. 
            /// Otherwise, other processes cannot open the object if they request delete access.
            /// If this flag is not specified, but the object has been opened for delete access, the function fails.
            /// </summary>
            Delete = 0x00000004
        }

        public enum FileMode : uint
        {
            /// <summary>
            /// Creates a new file. The function fails if a specified file exists.
            /// </summary>
            New = 1,
            /// <summary>
            /// Creates a new file, always. 
            /// If a file exists, the function overwrites the file, clears the existing attributes, combines the specified file attributes, 
            /// and flags with FILE_ATTRIBUTE_ARCHIVE, but does not set the security descriptor that the SECURITY_ATTRIBUTES structure specifies.
            /// </summary>
            CreateAlways = 2,
            /// <summary>
            /// Opens a file. The function fails if the file does not exist. 
            /// </summary>
            OpenExisting = 3,
            /// <summary>
            /// Opens a file, always. 
            /// If a file does not exist, the function creates a file as if dwCreationDisposition is CREATE_NEW.
            /// </summary>
            OpenAlways = 4,
            /// <summary>
            /// Opens a file and truncates it so that its size is 0 (zero) bytes. The function fails if the file does not exist.
            /// The calling process must open the file with the GENERIC_WRITE access right. 
            /// </summary>
            TruncateExisting = 5
        }

        [Flags]
        public enum FileAttributes : uint
        {
            Readonly = 0x00000001,
            Hidden = 0x00000002,
            System = 0x00000004,
            Directory = 0x00000010,
            Archive = 0x00000020,
            Device = 0x00000040,
            Normal = 0x00000080,
            Temporary = 0x00000100,
            SparseFile = 0x00000200,
            ReparsePoint = 0x00000400,
            Compressed = 0x00000800,
            Offline = 0x00001000,
            NotContentIndexed = 0x00002000,
            Encrypted = 0x00004000,
            Write_Through = 0x80000000,
            Overlapped = 0x40000000,
            NoBuffering = 0x20000000,
            RandomAccess = 0x10000000,
            SequentialScan = 0x08000000,
            DeleteOnClose = 0x04000000,
            BackupSemantics = 0x02000000,
            PosixSemantics = 0x01000000,
            OpenReparsePoint = 0x00200000,
            OpenNoRecall = 0x00100000,
            FirstPipeInstance = 0x00080000
        }

        [Flags]
        public enum MoveMethod : uint
        {
            FILE_BEGIN = 0,
            FILE_CURRENT = 1,
            FILE_END = 2
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern IntPtr ReleaseDC(IntPtr hwnd, IntPtr hdc);
        #region GetWindowLong
        [DllImport("user32.dll")]
        static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);
        const int GWL_HINSTANCE = -6;

        public static IntPtr GetWindowHInstance(IntPtr hWnd) => GetWindowLong(hWnd, GWL_HINSTANCE);
        #endregion

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetCurrentThreadId();

        #region WindowsHookEx
        [DllImport("user32.dll")]
        static extern IntPtr SetWindowsHookEx(int idHook, HOOKPROC lpfn, IntPtr hInstance, IntPtr threadId);
        const int WH_CBT = 5;
        public static IntPtr SetWindowsHookEx(HOOKPROC lpfn, IntPtr hInstance, IntPtr threadId) => SetWindowsHookEx(WH_CBT, lpfn, hInstance, threadId);

        [DllImport("user32.dll")]
        public static extern bool UnhookWindowsHookEx(IntPtr hHook);
        [DllImport("user32.dll")]
        public static extern IntPtr CallNextHookEx(IntPtr hHook, int nCode, IntPtr wParam, IntPtr lParam);

        public delegate IntPtr HOOKPROC(int nCode, IntPtr wParam, IntPtr lParam);
        public const int HCBT_ACTIVATE = 5;
        #endregion

        #region File I/O
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr CreateFile(
             [MarshalAs(UnmanagedType.LPTStr)] string filename,
             [MarshalAs(UnmanagedType.U4)] FileAccess access,
             [MarshalAs(UnmanagedType.U4)] FileShare share,
             IntPtr securityAttributes, // optional SECURITY_ATTRIBUTES struct or IntPtr.Zero
             [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
             [MarshalAs(UnmanagedType.U4)] FileAttributes flagsAndAttributes,
             IntPtr templateFile);
        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        public static extern unsafe int SetFilePointer(
                System.IntPtr hFile,        // handle to file
                int lDistanceToMov,         // low
                int *lpDistanceToMoveHigh,  // high
                MoveMethod dwMoveMethod     // 状態
            );

        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        public static extern unsafe bool ReadFile
            (
                System.IntPtr hFile,      // handle to file
                void* pBuffer,            // data buffer
                int NumberOfBytesToRead,  // number of bytes to read
                int* pNumberOfBytesRead,  // number of bytes read
                int Overlapped            // overlapped buffer
            );
        [System.Runtime.InteropServices.DllImport("kernel32", SetLastError = true)]
        public static extern unsafe bool CloseHandle
         (
             System.IntPtr hObject // handle to object
         );
        #endregion



        #region GetWindowRect
        [DllImport("user32.dll")]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        public struct RECT
        {
            public int Left, Top, Right, Bottom;
            public int Width => this.Right - this.Left;
            public int Height => this.Bottom - this.Top;
        }

        public static RECT GetWindowRect(IntPtr hWnd)
        {
            RECT rc;
            GetWindowRect(hWnd, out rc);
            return rc;
        }
        #endregion

        #region SetWindowPos
        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);
        const uint SWP_NOSIZE = 0x0001;
        const uint SWP_NOMOVE = 0x0002;
        const uint SWP_NOZORDER = 0x0004;
        const uint SWP_NOACTIVATE = 0x0010;
        static IntPtr HWND_TOP = IntPtr.Zero;
        static IntPtr HWND_TOPMOST = new IntPtr(-1);
        static IntPtr HWND_NOTOPMOST = new IntPtr(-2);

        /// <summary>
        /// ウィンドウ移動(座標移動)
        /// </summary>
        /// <param name="hWnd">ウィンドウハンドル</param>
        /// <param name="x">X座標</param>
        /// <param name="y">Y座標</param>
        /// <returns></returns>
        public static bool SetWindowPos(IntPtr hWnd, int x, int y)
        {
            var flags = SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE;
            return SetWindowPos(hWnd, IntPtr.Zero, x, y, 0, 0, flags);
        }

        /// <summary>
        /// ウィンドウ移動処理(位置移動、幅高さ変更)
        /// </summary>
        /// <param name="hWnd">ウィンドウハンドル</param>
        /// <param name="x">X座標</param>
        /// <param name="y">Y座標</param>
        /// <param name="w">幅</param>
        /// <param name="h">高</param>
        /// <returns></returns>
        public static bool SetWindowPos(IntPtr hWnd, int x, int y, int w, int h)
        {
            var flags =  SWP_NOZORDER | SWP_NOACTIVATE;
            return SetWindowPos(hWnd, IntPtr.Zero, x, y, w, h, flags);
        }

        /// <summary>
        /// 前面へ表示
        /// </summary>
        /// <param name="hWnd">ウィンドウハンドル</param>
        public static void SetTopWindow(IntPtr hWnd)
        {
            var flags = SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE;
            SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, flags);
            SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, flags);
        }

        #endregion

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr PostMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern int SendMessageTimeout(IntPtr hWnd, uint msg, uint wParam, IntPtr lParam, int fuFlags, uint uTimeout, out UIntPtr lpdwResult);
        public const int SMTO_NORMAL = 0x0000;
        public const int SMTO_BLOCK = 0x0001;
        public const int SMTO_ABORTIFHUNG = 0x0002;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// WM_COMMAND
        /// </summary>
        public const uint WM_COMMAND = 0x0111;

        /// <summary>
        /// WM_COPYDATA
        /// </summary>
        public const uint WM_COPYDATA = 0x004A;

        /// <summary>
        /// WM_CLOSE
        /// </summary>
        public const uint WM_CLOSE = 0x0010;

        /// <summary>
        /// WM_COPYDATA構造体
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct COPYDATASTRUCT
        {
            /// <summary>
            /// 任意の値
            /// </summary>
            public IntPtr dwData;
            /// <summary>
            /// lpDataのバイト数
            /// </summary>
            public int cbData;
            /// <summary>
            /// メッセージメモリアドレス
            /// </summary>
            public IntPtr lpData;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int EnumWindows(EnumWindowsDelegate lpEnumFunc, IntPtr lparam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hWnd, string lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hwnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        public delegate bool EnumWindowsDelegate(IntPtr hWnd, IntPtr lparam);

        /// <summary>
        /// 指定された構造体等のポインタ化
        /// </summary>
        /// <typeparam name="T">構造体等</typeparam>
        /// <param name="param">パラメータ</param>
        /// <returns>ポインタ</returns>
        public static IntPtr IntPtrAlloc<T>(T param)
        {
            IntPtr retval = Marshal.AllocHGlobal(Marshal.SizeOf(param));
            Marshal.StructureToPtr(param, retval, false);
            return retval;
        }

        // Free a pointer to an arbitrary structure from the global heap.
        /// <summary>
        /// IntPtrAllocにて取得したポインターの解放
        /// </summary>
        /// <param name="preAllocated">メモリポインタ</param>
        public static void IntPtrFree(ref IntPtr preAllocated)
        {
            if (IntPtr.Zero == preAllocated)
                throw (new NullReferenceException("Go Home"));
            Marshal.FreeHGlobal(preAllocated);
            preAllocated = IntPtr.Zero;
        }


        #region 定数
        /// <summary>
        /// 該当するウィンドウに再描画が必要とOSが判断したときにポストされるメッセージ
        /// </summary>
        public const int WM_PAINT = 0xF;

        /// <summary>
        ///  ウィンドウのクライアント領域でユーザーがマウスの移動をしたときにポストされるメッセージ
        /// </summary>
        public const int WM_MOUSEMOVE = 0x200;

        /// <summary>
        /// ウィンドウのクライアント領域でユーザーがマウスの左ボタンを押したときにポストされるメッセージ
        /// </summary>
        public const int WM_LBUTTONDOWN = 0x201;

        /// <summary>
        /// ウィンドウのクライアント領域でユーザーがマウスの左ボタンを離したときにポストされるメッセージ
        /// </summary>
        public const int WM_LBUTTONUP = 0x202;

        /// <summary>
        /// マウスポインタが領域外に出た時にポストされるメッセージ
        /// </summary>
        public const int WM_MOUSELEAVE = 0x2A3;

        #endregion

        #region 構造体
        /// <summary>
        /// MSG 構造体型
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct MSG
        {
            /// <summary>
            /// ウィンドウハンドル
            /// </summary>
            public IntPtr hWnd;
            /// <summary>
            /// MessageID
            /// </summary>
            public int msg;
            /// <summary>
            /// WParamフィールド
            /// </summary>
            public IntPtr wParam;
            /// <summary>
            /// LParamフィールド
            /// </summary>
            public IntPtr lParam;
            /// <summary>
            /// 時間
            /// </summary>
            public int time;
            /// <summary>
            /// カーソル位置（スクリーン座標）
            /// </summary>
            public POINTAPI pt;
        }

        /// <summary>
        /// POINTAPI構造体
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINTAPI
        {
            /// <summary>
            /// X座標
            /// </summary>
            public int x;
            /// <summary>
            /// Y座標
            /// </summary>
            public int y;
        }
        #endregion

        #region 列挙型
        /// <summary>
        /// wRemoveMsgで利用可能なオプション
        /// </summary>
        public enum EPeekMessageOption
        {
            /// <summary>
            /// 処理後メッセージをキューから削除しない
            /// </summary>
            PM_NOREMOVE = 0,
            /// <summary>
            /// 処理後メッセージをキューから削除する
            /// </summary>
            PM_REMOVE
        }
        #endregion

        #region DllImport
        /// <summary>
        /// スレッドのメッセージキューにメッセージがあるかどうかをチェックし
        /// あれば、指定された構造体にそのメッセージを格納します
        /// </summary>
        /// <param name="lpMsg"> メッセージ情報を格納する、MSG 構造体型変数のポインタを指定します</param>
        /// <param name="hWnd">メッセージを取得するウィンドウのハンドルを指定します。全ての場合は NULL</param>
        /// <param name="wMsgFilterMin">メッセージの最小値を指定し、フィルタリングします。しない場合は0</param>
        /// <param name="wMsgFilterMax">メッセージの最大値を指定し、フィルタリングします。しない場合は0</param>
        /// <param name="wRemoveMsg">処理後メッセージをキューを削除するか否か</param>
        /// <returns>メッセージを取得したときは 0 以外、取得しなかったときは 0</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool PeekMessage(
            out MSG lpMsg,
            int hWnd,
            int wMsgFilterMin,
            int wMsgFilterMax,
            EPeekMessageOption wRemoveMsg
        );

        /// <summary>
        /// 指定された MSG 構造体型変数にメッセージを格納します
        /// </summary>
        /// <param name="lpMsg">メッセージ情報を格納する、MSG 構造体型変数のポインタを指定します</param>
        /// <param name="hWnd">メッセージを取得するウィンドウのハンドルを指定します。全ての場合は NULL</param>
        /// <param name="wMsgFilterMin">メッセージの最小値を指定し、フィルタリングします。しない場合は0</param>
        /// <param name="wMsgFilterMax">メッセージの最大値を指定し、フィルタリングします。しない場合は0</param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetMessage(
            out MSG lpMsg,
            int hWnd,
            int wMsgFilterMin,
            int wMsgFilterMax
        );

        /// <summary>
        /// 指定されたウィンドウメッセージをウィンドウプロシージャにディスパッチします
        /// </summary>
        /// <param name="lpMsg">ディスパッチするメッセージを格納した MSG 構造体変数のポインタを指定します</param>
        /// <returns>ウィンドウプロシージャの戻り値</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr DispatchMessage(
             out MSG lpMsg
        );
        #endregion
    }

    /// <summary>
    /// メッセージボックス
    /// </summary>
    static public class SVMessageBox
    {

        /// <summary>
        /// ダイアログメッセージボックス
        /// </summary>
        /// <param name="owner">親ウィンドウ</param>
        /// <param name="text">文字列</param>
        /// <param name="caption">キャプション</param>
        /// <param name="buttons">表示ボタン</param>
        /// <param name="icon">アイコン</param>
        /// <returns>ダイアログ戻り値</returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons= MessageBoxButtons.OK, MessageBoxIcon icon= MessageBoxIcon.None)
        {
            hOwner = owner.Handle;
            var hInstance = User32.GetWindowHInstance(owner.Handle);
            var threadId = User32.GetCurrentThreadId();
            hHook = User32.SetWindowsHookEx(new User32.HOOKPROC(HookProc), hInstance, threadId);
            return MessageBox.Show(owner, text, caption, buttons, icon);
        }


        // 内部保持用
        private static IntPtr hOwner = (IntPtr)0;
        private static IntPtr hHook = (IntPtr)0;

        /// <summary>
        /// フック処理
        /// </summary>
        private static IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode == User32.HCBT_ACTIVATE)
            {
                // オーナーウィンドウとメッセージボックスの領域を取得
                var rcOwner = User32.GetWindowRect(hOwner);
                var rcMsgBox = User32.GetWindowRect(wParam);

                // メッセージボックスをオーナーウィンドウの中央位置に移動
                var x = rcOwner.Left + (rcOwner.Width - rcMsgBox.Width) / 2;
                var y = rcOwner.Top + (rcOwner.Height - rcMsgBox.Height) / 2;
                User32.SetWindowPos(wParam, x, y);

                // フックを解除
                User32.UnhookWindowsHookEx(hHook);
                hHook = (IntPtr)0;
            }
            return User32.CallNextHookEx(hHook, nCode, wParam, lParam);
        }
    }

}
