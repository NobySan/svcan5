﻿namespace NVdat2VSYNC
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.groupBox_BitSelector = new System.Windows.Forms.GroupBox();
            this.radioButton_8bit = new System.Windows.Forms.RadioButton();
            this.radioButton_16bit = new System.Windows.Forms.RadioButton();
            this.btnCheckGo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDatFolder = new System.Windows.Forms.Button();
            this.tBoxDatFilename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox_BitSelector.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_BitSelector
            // 
            this.groupBox_BitSelector.Controls.Add(this.radioButton_8bit);
            this.groupBox_BitSelector.Controls.Add(this.radioButton_16bit);
            this.groupBox_BitSelector.Location = new System.Drawing.Point(11, 11);
            this.groupBox_BitSelector.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox_BitSelector.Name = "groupBox_BitSelector";
            this.groupBox_BitSelector.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox_BitSelector.Size = new System.Drawing.Size(124, 54);
            this.groupBox_BitSelector.TabIndex = 0;
            this.groupBox_BitSelector.TabStop = false;
            this.groupBox_BitSelector.Text = "Dat Format";
            // 
            // radioButton_8bit
            // 
            this.radioButton_8bit.AutoSize = true;
            this.radioButton_8bit.Checked = true;
            this.radioButton_8bit.Location = new System.Drawing.Point(71, 23);
            this.radioButton_8bit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButton_8bit.Name = "radioButton_8bit";
            this.radioButton_8bit.Size = new System.Drawing.Size(42, 16);
            this.radioButton_8bit.TabIndex = 1;
            this.radioButton_8bit.TabStop = true;
            this.radioButton_8bit.Text = "8bit";
            this.radioButton_8bit.UseVisualStyleBackColor = true;
            // 
            // radioButton_16bit
            // 
            this.radioButton_16bit.AutoSize = true;
            this.radioButton_16bit.Enabled = false;
            this.radioButton_16bit.Location = new System.Drawing.Point(10, 23);
            this.radioButton_16bit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButton_16bit.Name = "radioButton_16bit";
            this.radioButton_16bit.Size = new System.Drawing.Size(48, 16);
            this.radioButton_16bit.TabIndex = 0;
            this.radioButton_16bit.Text = "16bit";
            this.radioButton_16bit.UseVisualStyleBackColor = true;
            // 
            // btnCheckGo
            // 
            this.btnCheckGo.Location = new System.Drawing.Point(390, 70);
            this.btnCheckGo.Name = "btnCheckGo";
            this.btnCheckGo.Size = new System.Drawing.Size(135, 23);
            this.btnCheckGo.TabIndex = 4;
            this.btnCheckGo.Text = "Check Go !";
            this.btnCheckGo.UseVisualStyleBackColor = true;
            this.btnCheckGo.Click += new System.EventHandler(this.btnAllConvert_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDatFolder);
            this.groupBox1.Controls.Add(this.tBoxDatFilename);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(139, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(386, 54);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Setting";
            // 
            // btnDatFolder
            // 
            this.btnDatFolder.Location = new System.Drawing.Point(320, 14);
            this.btnDatFolder.Name = "btnDatFolder";
            this.btnDatFolder.Size = new System.Drawing.Size(55, 23);
            this.btnDatFolder.TabIndex = 7;
            this.btnDatFolder.Text = "Refer";
            this.btnDatFolder.UseVisualStyleBackColor = true;
            this.btnDatFolder.Click += new System.EventHandler(this.btnDatFolder_Click);
            // 
            // tBoxDatFilename
            // 
            this.tBoxDatFilename.AllowDrop = true;
            this.tBoxDatFilename.Location = new System.Drawing.Point(76, 18);
            this.tBoxDatFilename.Name = "tBoxDatFilename";
            this.tBoxDatFilename.ReadOnly = true;
            this.tBoxDatFilename.Size = new System.Drawing.Size(235, 19);
            this.tBoxDatFilename.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "DAT File";
            // 
            // FormMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 100);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCheckGo);
            this.Controls.Add(this.groupBox_BitSelector);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.Text = "NVdatchk";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox_BitSelector.ResumeLayout(false);
            this.groupBox_BitSelector.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_BitSelector;
        private System.Windows.Forms.RadioButton radioButton_8bit;
        private System.Windows.Forms.RadioButton radioButton_16bit;
        private System.Windows.Forms.Button btnCheckGo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDatFolder;
        private System.Windows.Forms.TextBox tBoxDatFilename;
        private System.Windows.Forms.Label label1;
    }
}

