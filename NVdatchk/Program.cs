﻿namespace NVdat2VSYNC
{
    using SVUtil;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;

    
    /// <summary>
    /// メイン
    /// </summary>
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        public static extern bool AttachConsole(uint dwProcessId);

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        public static extern bool FreeConsole();

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        /// <param name="args">起動引数</param>
        /// <returns>正常なら0,異常時は1を返却する</returns>
        [STAThread]
        static int Main(string[] args)
        {
            int ret = 0;

            bool attach = AttachConsole(uint.MaxValue);

            // コマンドプロセス実行
            try
            {
                ret = Exec(args);
            }
            finally
            {
                if (attach)
                {
                    FreeConsole();
                }
            }
            return ret;
        }

        /// <summary>
        /// 実行処理
        /// </summary>
        /// <param name="args">引数</param>
        /// <returns>処理結果</returns>
        static int Exec(string[] args)
        {
            // ダミー改行
            Console.WriteLine();
            int ret=0;

            // 変換処理クラス
            Dat2CSV dat2CSV = new Dat2CSV();
            if (args.Length == 0)
            {
                ret = 1; // 引数なしなのでファイルエラーとする
            }
            else if (args.Length == 1)
            {
                ret = dat2CSV.Execute(args[0], "10", "500"); // mode指定なしであればDAT全チェックを行う
            }
            else if (args.Length == 2)
            {
                ret = dat2CSV.Execute(args[0], args[1], "500"); // mode指定なしであればDAT全チェックを行う
            }
            else
            {
                ret = dat2CSV.Execute(args[0], args[1], args[2]);
            }
            Console.WriteLine("...NVdatchk end fnum={0} retcode={1}\n", (ret >> 8), (ret & 0xff));
            return ret;
        }

    }
}
