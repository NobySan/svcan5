﻿namespace NVdat2VSYNC
{
    using System.Runtime.Serialization;

    /// <summary>
    /// CANパラメータ
    /// </summary>
    [DataContract]
    public class CanParameter
    {
        #region フィールド
        #endregion

        #region プロパティ

        /// <summary>
        /// データサイズ
        /// </summary>
        [DataMember(Name = "DataSize")]
        public Define.DataSize DataSize { get; set; } = Define.DataSize._8bit;

        /// <summary>
        /// AVI ch1をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertAVI1")]
        public bool AlertAVI1 { get; set; } = true;

        /// <summary>
        /// AVI ch2をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertAVI2")]
        public bool AlertAVI2 { get; set; } = true;

        /// <summary>
        /// AVI ch3をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertAVI3")]
        public bool AlertAVI3 { get; set; } = true;

        /// <summary>
        /// AVI ch4をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertAVI4")]
        public bool AlertAVI4 { get; set; } = true;

        /// <summary>
        /// CAN ch0をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertCAN0")]
        public bool AlertCAN0 { get; set; } = true;

        /// <summary>
        /// CAN ch1をチェックするかのフラグ
        /// </summary>
        [DataMember(Name = "AlertCAN1")]
        public bool AlertCAN1 { get; set; } = true;

        #endregion

        #region コンストラクタ
        #endregion

        /// <summary>
        /// デシリアライズ
        /// </summary>
        /// <param name="sc">コンテキスト</param>
        [OnDeserializing]
        internal void OnDeserializing(StreamingContext sc)
        {
        }

    }
}
