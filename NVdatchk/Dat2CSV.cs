﻿namespace NVdat2VSYNC
{
    using candec;
    using SVCanData;
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Globalization;
    using System.Runtime.InteropServices;

    /// <summary>
    /// 変換処理のサンプルです（内容は16bit⇒8bit変換）
    /// </summary>
    public class Dat2CSV
    {
        #region ▼ Field
        #endregion

        #region ▼ Property
        #endregion

        #region ▼ Constructor
        /// <summary>
        /// バイナリファイルの書き込み
        /// </summary>
        BinaryReader mBinaryReader = null;
        /// <summary>
        /// バイナリファイルの書き込み
        /// </summary>
        BinaryWriter mBinaryWriter = null;
        /// <summary>
        /// エラーファイル名
        /// </summary>
        private const string ERROR_FILE_NAME = "err.txt";
        #endregion

        #region ▼ Event
        #endregion

        #region ▼ Private Method
        /// <summary>
        /// ファイル名に付与する日時文字列を取得します。
        /// </summary>
        /// <returns>ファイル名に付与する日時文字列（_YYYYMMDD_hhmmssxxx）</returns>
        private string GetFileNameSuffix()
        {
            // 現在時刻
            DateTime nowTime = DateTime.Now;

            return "_" + nowTime.Year.ToString("D4") + nowTime.Month.ToString("D2") + nowTime.Day.ToString("D2")
                    + "_" + nowTime.Hour.ToString("D2") + nowTime.Minute.ToString("D2") + nowTime.Second.ToString("D2") + nowTime.Millisecond.ToString("D3");
        }

        /// <summary>
        /// バイトをキロバイトに変換します。
        /// </summary>
        /// <param name="targetSize">対象のサイズ（Byte）</param>
        /// <returns>変換後のサイズ（KB）</returns>
        private int ConvertByteToKiloByte(long targetSize)
        {
            return (int)(targetSize / 1024);
        }

        /// <summary>
        /// データ部分を変換します
        /// </summary>
        /// <param name="dataArray">処理対象の配列</param>
        /// <remarks>
        /// 与えられた配列を、先頭から順に1バイト捨て1バイト残す、という処理を繰り返します。
        /// </remarks>
        /// <returns>処理後の配列</returns>
        private async Task<byte[][]> ConvertDataPart(byte[] dataArray)
        {
            // 配列長
            int dataArrayLength = dataArray.Length;

            // 各スレッドで作成する配列のサイズ
            int arraySize = dataArrayLength;
            if (arraySize % 4 == 0)
            {
                // ※2スレッドで処理するので1/2、1バイト捨て1バイト残すので1/2、(1/2)*(1/2)=1/4。
                arraySize /= 4;
            }

            // 処理開始位置
            int startIndex = 1;
            // 処理終了位置
            int endIndex = 0;

            // ２スレッドで処理する場合
            if (dataArrayLength % 4 == 0)
            {
                endIndex = dataArrayLength / 2;
            }
            // １スレッドで処理する場合
            else
            {
                endIndex = dataArrayLength;
            }

            // １スレッド目
            var task1 = Task<byte[]>.Run(() =>
            {
                // 戻り値
                byte[] resultArray = new byte[arraySize];
                // 配列のインデックス
                int index = 0;

                for (int i = startIndex; i <= endIndex; i++)
                {
                    // 現在のループ回数が偶数の場合、Listにプールする
                    // ※データ2byte(16bit)単位で、前1byte（8bit）を捨てて後1byteを取るので。
                    if (i % 2 == 0)
                    {
                        // ※-1しているのはiの初期値（startIndex）を1としているため
                        resultArray[index++] = dataArray[i - 1];
                    }
                }

                return resultArray;
            });

            // ２スレッド目
            var task2 = Task<byte[]>.Run(() =>
            {
                // 戻り値
                byte[] resultArray = null;

                // ２スレッドで処理する場合
                if (dataArrayLength % 4 == 0)
                {
                    resultArray = new byte[arraySize];
                    // 配列のインデックス
                    int index = 0;

                    for (int i = startIndex; i <= endIndex; i++)
                    {
                        // 現在のループ回数が偶数の場合、Listにプールする
                        // ※データ2byte(16bit)単位で、前1byte（8bit）を捨てて後1byteを取るので。
                        if (i % 2 == 0)
                        {
                            // ※-1しているのはiの初期値（startIndex）を1としているため
                            resultArray[index++] = dataArray[i - 1 + endIndex];
                        }
                    }
                }

                return resultArray;
            });

            return await Task.WhenAll(task1, task2);
        }
        /// <summary>
        /// CANパケットよりビットカウントを算出する
        /// </summary>
        /// <param name="can_packet">CANパケット配列</param>
        /// <returns>ビットカウント</returns>
        private int get_bitcount(long[] can_packet)
        {
            int b = 2;
            int c = 0;
            int l = 0;
            int s = 0;
            int i = 0;

            // データなしなら0を返して抜ける
            if (can_packet[0] == -1) return 0;

            // CANデコーダーから帰ってきたビット数をカウント
            for(i++; ; i++)
            {
                if (can_packet[i] == -1) break;
                c = (int)can_packet[i];
                if (c == b)
                {
                    l++;
                    if (l == 5)
                    {
                        s++;
                        l = 0;
                    }
                }
                else
                {
                    l = 1;
                }
                b = c;
            }

            return (i + s - 2); // 純粋なビットカウントにスタッフカウントを足してACLデリミタとEOFとITMで発生したスタッフbitt2つを引く
        }

        /// <summary>
        /// byte配列から構造体へ変換
        /// </summary>
        /// <typeparam name="Type">型</typeparam>
        /// <param name="buffer">バイト配列</param>
        /// <returns>変換した型</returns>
        Type Byte2Struct<Type>(byte[] buffer)
        {
            GCHandle gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            Type ret = (Type)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Type));
            gch.Free();
            return (ret);
        }

        /// <summary>
        /// チェック結果を出力します。
        /// </summary>
        /// <param name="datFileName">datファイルパス</param>
        /// <param name="frm_cnt">フレームカウント</param>
        private void WriteCheckResult(string datFileName, int frm_cnt, long vsyncHighPosition)
        {
            // ファイル保存先のフルパス
            string savePath = Path.Combine(Path.GetDirectoryName(datFileName), Path.GetFileNameWithoutExtension(datFileName) + ".txt");

            // ※同名のファイルがあれば上書き
            using (StreamWriter streamWriter = new StreamWriter(savePath, false, Encoding.GetEncoding("shift_jis")))
            {
                streamWriter.WriteLine(frm_cnt.ToString());
                streamWriter.WriteLine(vsyncHighPosition.ToString());
                streamWriter.Close();
            }
        }
        #endregion

        #region ▼ Public Method
        /// <summary>
        /// 変換処理を実行します。
        /// </summary>
        /// <param name="datFilename">変換対象のdatファイル名（フルパス）</param>
        /// <param name="mode">チェック種類</param>
        /// <param name="canBps">CAN BPS(単位K)</param>
        /// <returns>成功なら0,エラーならそれ以外を返却</returns>
        /// エラー情報 bit07-00 : 0:正常, 1:ファイルエラー, 2:ファイル容量エラー, 3:VsyncHighエラー, 4:不連続エラー, 5:ブランクエラー, 6:CANエラー
        /// フレーム数 bit31-08 : フレーム数（24bit範囲：155時間分）
        public int Execute(string datFilename, string mode, string canBps)
        {
            int rec_width = 2; // DAT種類（1:8bit, 2:16bit）
            int chk_mode = 0;
            int frm_cnt = 0;
            long clockStartVsyncPos = -1;
            long clockEndVsyncPos = -1;
            int canIdleCheckClockCount = 100000 / int.Parse(canBps); // 100nsでのクロック数
            long clockStartCcan0Pos = -1;
            long clockStartCcan1Pos = -1;
            long fraction_cnt = 0;

            if (mode == "0") chk_mode = 0; // フレーム数のみカウント
            else if (mode == "1") chk_mode = 1; // 不連続チェックのみ
            else if (mode == "2") chk_mode = 2; // ブランクチェックのみ
            else if (mode == "3") chk_mode = 3; // 不連続チェック+ブランクチェック
            else if (mode == "4") chk_mode = 4; // サイズチェックのみ
            else if (mode == "5") chk_mode = 5; // 不連続チェック+サイズチェック
            else if (mode == "6") chk_mode = 6; // ブランクチェック+サイズチェック
            else if (mode == "7") chk_mode = 7; // 全部検査
            else if (mode == "10") chk_mode = 10; // ソニー対応16bit dat対応

            var file = new System.IO.FileStream(datFilename, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite);
            mBinaryReader = new System.IO.BinaryReader(file);
            mBinaryWriter = new System.IO.BinaryWriter(file);

            // 入力ファイルのストリーム情報
            Stream baseStreamR;
            // 出力ファイルのストリーム情報
            Stream baseStreamW;

            try
            {
                ////////////////////////////////////////////////////////////////////////
                /// DATファイルチェック
                ////////////////////////////////////////////////////////////////////////
                {
                    // 入力ファイルのストリーム情報
                    baseStreamR = mBinaryReader.BaseStream;
                    // 変換元のファイルサイズ
                    long fileLength = baseStreamR.Length;
                    // VSYNC立ち上がり情報出力のためのフラグ -1:初期状態 0:ブランク中 1:有効期間中
                    int vsyncFlag = -1;
                    // VSYNC立ち上がり情報出力のための現在のクロックポジション
                    long clockPos = 0;
                    long clockPosLowStart = 0; // VSYNCがHighからLowに立ち下った時の最初のLowクロックポジション
                    long errorVsync = -1; // VSYNCエラーチェックカウンタ
                    long bufferSize = 32 * 1024 * 1024; // 32MB
                    long idleCan0ClockCount = -1; // CAN0のアイドルカウンタ
                    long idleCan1ClockCount = -1; // CAN1のアイドルカウンタ

                    // ヘッダーを読み込む
                    byte[] head = mBinaryReader.ReadBytes(Marshal.SizeOf(typeof(SvCanHeader)));
                    if (head.Length != Marshal.SizeOf(typeof(SvCanHeader)))
                    {
                        Console.Write("Dat header size error");
                        return ((frm_cnt << 8) | 0x2);
                    }

                    // ヘッダ読込
                    SvCanHeader header = this.Byte2Struct<SvCanHeader>(head);
                    if (header.rec_data_width != rec_width)
                    {
                        Console.Write("Dat bit size error");
                        return ((frm_cnt << 8) | 0x3);
                    }

                    // メモリにプールするサイズ（byte）
                    int poolSize = (int)bufferSize;

                    // データ（終端に達するまで）
                    while (baseStreamR.Position != fileLength)
                    {
                        // １度に読み込むサイズ
                        byte[] poolDataArray = null;

                        // 未読み込み部分がプールサイズ未満の場合
                        if ((fileLength - baseStreamR.Position) < poolSize)
                        {
                            // 残り全部読む
                            var remainingSize = (int)(fileLength - baseStreamR.Position);
                            // データを読み込む
                            poolDataArray = mBinaryReader.ReadBytes(remainingSize);
                            // 32MBバウンダリーにするための端数を取得
                            fraction_cnt = poolSize - remainingSize;
                        }
                        else
                        {
                            // プールサイズ分だけ読む
                            poolDataArray = mBinaryReader.ReadBytes(poolSize);
                        }

                        // データを変換
                        int i = 0;
                        int add = 0;
                        int step = 1;
                        if (rec_width == 2) // 16bit-datだったら
                        {
                            add++;
                            step = 2;
                        }

                        for (i = 0; i < poolDataArray.Length; i += step)
                        {
                            // ブランクチェック
                            byte clockVal = poolDataArray[i + add];

                            // VSYNC立ち上がり情報を出力 (vsyncFlag: -1:初期状態 0:ブランク中 1:有効期間中)
                            if (vsyncFlag == -1)
                            {
                                if ((clockVal & 0x10) == 0)
                                {
                                    vsyncFlag = 0; // ブランク中
                                    clockPosLowStart = clockPos;
                                }
                                else
                                {
                                    // ファイルの先頭がVSYNC-Highで始まっているのでエラーとする
                                    Console.Write("VSYNC high error");
                                    return ((frm_cnt << 8) | 0x4);
                                }
                            }
                            else if (vsyncFlag == 0)
                            {
                                if ((clockVal & 0x10) == 0x10)
                                {
                                    vsyncFlag = 1; // 有効期間中
                                    errorVsync = 1; // V有効期間カウンタアップ
                                    if (clockStartVsyncPos == -1)
                                    {
                                        // 最初の有効フレームの先頭クロックポジションを保存
                                        clockStartVsyncPos = clockPos;
                                    }
                                }
                            }
                            else if (vsyncFlag == 1)
                            {
                                if ((clockVal & 0x10) == 0x0)
                                {
                                    if(errorVsync < 0x100)
                                    {
                                        // VSYNC信号が不正なのでエラーとする
                                        Console.Write("VSYNC invalid error");
                                        return ((frm_cnt << 8) | 0x5);
                                    }
                                    frm_cnt++; // 1Vが終わったらフレーム数カウンタをアップする
                                    vsyncFlag = 0; // ブランク中
                                    errorVsync = 0; // V有効期間カウンタアップ
                                    clockEndVsyncPos = clockPos; // 有効フレームの最後のクロックポジションを保存
                                }
                                else
                                {
                                    errorVsync++; // V有効期間カウンタアップ
                                }
                            }

                            // CANデータ確認
                            if (clockStartVsyncPos != -1)
                            {
                                // 最初の有効フレームが始まって後、最初のCANのIDLE期間を見つける
                                if(clockStartCcan0Pos == -1)
                                {
                                    if((clockVal & 0x40) == 0x40)
                                    {
                                        idleCan0ClockCount++;
                                        if(idleCan0ClockCount > (canIdleCheckClockCount*11))
                                        {
                                            clockStartCcan0Pos = clockPos;
                                        }
                                    }
                                    else
                                    {
                                        idleCan0ClockCount = 0;
                                    }
                                }
                                if (clockStartCcan1Pos == -1)
                                {
                                    if ((clockVal & 0x80) == 0x80)
                                    {
                                        idleCan1ClockCount++;
                                        if (idleCan1ClockCount > (canIdleCheckClockCount * 11))
                                        {
                                            clockStartCcan1Pos = clockPos;
                                        }
                                    }
                                    else
                                    {
                                        idleCan1ClockCount = 0;
                                    }
                                }
                            }

                            clockPos += step;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("File open error");
                return ((frm_cnt << 8) | 0x1);
            }
            finally
            {
                long clockLoopEndPos = 0;

                // ファイルのストリーム情報
                baseStreamR = mBinaryReader.BaseStream;
                baseStreamW = mBinaryWriter.BaseStream;

                // 先頭のCAN0とCAN1とVSをアイドル化する
                if(clockStartCcan1Pos > clockStartCcan0Pos)
                {
                    clockLoopEndPos = clockStartCcan1Pos;
                }
                else
                {
                    clockLoopEndPos = clockStartCcan0Pos;
                }
                byte[] idleData = { 0x0, 0x0 };
                for (long i = 0;i < clockLoopEndPos; i+=2)
                {
                    baseStreamR.Position = i + 256; // DATデータ部の先頭を設定
                    idleData = mBinaryReader.ReadBytes(2);

                    if (i < clockStartVsyncPos)
                    {
                        idleData[0] = 0x0;
                        idleData[1] = 0x0;
                    }
                    if (i < clockStartCcan0Pos)
                    {
                        idleData[1] |= 0x40;
                    }
                    if (i < clockStartCcan1Pos)
                    {
                        idleData[1] |= 0x80;
                    }
                    baseStreamW.Position = i + 256; // DATデータ部の先頭を設定
                    mBinaryWriter.Write(idleData);
                }

                clockEndVsyncPos += 256;
                baseStreamW.Position = clockEndVsyncPos;
                long fileLength = baseStreamW.Length;
                long sabun = fileLength - clockEndVsyncPos;
                idleData[0] = 0x0;
                idleData[1] = 0xc0;
                for (;sabun > 0;sabun-=2)
                {
                    mBinaryWriter.Write(idleData);
                }
                // データ部を32MBバウンダリーにする
                if(fraction_cnt != 0)
                {
                    long dataLength = fileLength - 256 + fraction_cnt;
                    for (; fraction_cnt > 0; fraction_cnt -= 2)
                    {
                        mBinaryWriter.Write(idleData);
                    }
                    if ((clockEndVsyncPos + (300000 << 1)) > baseStreamW.Length)
                    {
                        for (fraction_cnt = (32*1024*1024); fraction_cnt > 0; fraction_cnt -= 2)
                        {
                            mBinaryWriter.Write(idleData);
                        }
                        dataLength += (32 * 1024 * 1024);
                    }
                    baseStreamW.Position = 0x30; // ヘッダー内DATデータ部のサイズ格納エリア（64bit）
                    mBinaryWriter.Write(dataLength);
                    clockEndVsyncPos += (300000 << 1);
                }
                else
                {
                    clockEndVsyncPos += (300000 << 1);
                    if ((clockEndVsyncPos & 0x1ffffff) != 0)
                    {
                        clockEndVsyncPos = fileLength;
                    }
                }
                mBinaryWriter.Close();
                mBinaryReader.Close();
                Console.Write("DAT file scan end");
            }

            this.WriteCheckResult(datFilename, frm_cnt, clockEndVsyncPos);
            return ((frm_cnt << 8) | 0x0);
        }
        #endregion

        #region ▼ Override Method
        #endregion

        #region ▼ Inner class
        #endregion
    }
}
