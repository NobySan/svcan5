﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NVdat2VSYNC
{
    /// <summary>
    /// 進捗情報
    /// </summary>
    public class Progress
    {
        #region ▼ Field
        #endregion

        #region ▼ Property
        /// <summary>
        /// 進捗ダイアログが持つBackgroundWorkerを取得または設定します。
        /// </summary>
        public BackgroundWorker BackgroundWorker
        {
            get; set;
        }

        public DoWorkEventArgs DoWorkEventArgs
        {
            get; set;
        }

        /// <summary>
        /// 処理対象のファイル数を取得または設定します。
        /// </summary>
        public int NumberOfTargetFiles
        {
            get; set;
        }

        /// <summary>
        /// 着手済みのファイル数を取得または設定します。
        /// </summary>
        public int NumberOfProcessedFiles
        {
            get; set;
        }

        /// <summary>
        /// 処理対象のファイルサイズを取得または設定します。
        /// </summary>
        /// <remarks>
        /// これに設定した値が、プログレスバーのMaximumに設定されます。
        /// </remarks>
        public int TargetFileSize
        {
            get; set;
        }
        #endregion

        #region ▼ Constructor
        #endregion

        #region ▼ Event
        #endregion

        #region ▼ Private Method
        #endregion

        #region ▼ Public Method
        #endregion

        #region ▼ Override Method
        #endregion
    }
}
