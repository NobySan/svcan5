﻿namespace NVdat2VSYNC
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    /// <summary>
    /// 進捗ダイアログ
    /// </summary>
    public partial class FormProgressDialog : Form
    {
        #region ▼ Field
        /// <summary>
        /// バックグラウンドで実行するメソッドの引数
        /// </summary>
        private object argument;
        /// <summary>
        /// エラー
        /// </summary>
        private Exception error;
        /// <summary>
        /// 開始時間
        /// </summary>
        private DateTime startTime;
        /// <summary>
        /// guiモード
        /// </summary>
        private bool gui = true;
        /// <summary>
        /// ポジション
        /// </summary>
        private System.Drawing.Point dialogPosition;
        /// <summary>
        /// ポジション
        /// </summary>
        private int datchk_error;
        #endregion

        #region ▼ Property
        /// <summary>
        /// 処理中に発生したエラーを取得します。
        /// </summary>
        public Exception Error
        {
            get
            {
                return this.error;
            }
        }
        /// <summary>
        /// 完了メッセージ
        /// </summary>
        public string CompleteMessage { get; set; } = "The conversion is complete.";

        #endregion

        #region ▼ Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="doWork">バックグラウンドで実行するメソッド</param>
        /// <param name="argument">バックグラウンドで実行するメソッドの引数</param>
        /// <param name="gui">guiモード時はtrue</param>
        public FormProgressDialog(DoWorkEventHandler doWork = null, object argument = null, bool gui = true)
        {
            // デザイナコンポーネントを初期化
            this.InitializeComponent();

            this.Text = "NVdatchk";

            // バックグラウンド処理するメソッドの引数
            this.argument = argument;

            this.backgroundWorker_Convert.WorkerReportsProgress = true;
            this.backgroundWorker_Convert.WorkerSupportsCancellation = true;
            this.backgroundWorker_Convert.DoWork += doWork;

            #region タイマー
            // 開始時間を設定
            this.startTime = DateTime.Now;

            // 経過時間用タイマー
            this.timer_Elapsed.Interval = 1;
            this.timer_Elapsed.Start();
            #endregion

            this.dialogPosition = this.Location;
            this.gui = gui;
            if (!gui)
            {
                int right = 0;

                // 場外に表示させる
                foreach (var screen in System.Windows.Forms.Screen.AllScreens)
                {
                    if (screen.WorkingArea.Right > right)
                    {
                        right = screen.WorkingArea.Right;
                    }
                }
                this.dialogPosition = new System.Drawing.Point(right+32,0);
            }
        }
        #endregion

        #region ▼ Event
        /// <summary>
        /// ダイアログが最初に表示されたときの処理
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void FormProgressDialog_Shown(object sender, EventArgs e)
        {
            this.Location = this.dialogPosition;

            // バックグラウンド処理を実行
            this.backgroundWorker_Convert.RunWorkerAsync(this.argument);
        }

        /// <summary>
        /// ダイアログが閉じられるときの処理
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void FormProgressDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.timer_Elapsed.Stop();
        }

        /// <summary>
        /// キャンセルボタンのクリックイベント
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            // キャンセルボタンを無効化
            this.button_Cancel.Enabled = false;
            // バックグラウンド処理をキャンセル
            this.backgroundWorker_Convert.CancelAsync();
        }

        /// <summary>
        /// タイマーイベント
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void Timer_Elapsed_Tick(object sender, EventArgs e)
        {
            this.label_ElapsedTime.Text = DateTime.Now.Subtract(this.startTime).ToString(@"hh\:mm\:ss");
        }

        /// <summary>
        /// 進捗通知時の処理
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void BackgroundWorker_Convert_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // 進捗情報
            Progress progress = e.UserState as Progress;

            if (progress != null)
            {
                this.progressBar_Convert.Maximum = progress.TargetFileSize;
                this.label_Progress.Text = string.Format("{0}／{1}", progress.NumberOfProcessedFiles, progress.NumberOfTargetFiles);
            }

            // 進捗度が0以上の場合
            if ((e.ProgressPercentage >= this.progressBar_Convert.Minimum) && (e.ProgressPercentage <= this.progressBar_Convert.Maximum))
            {
                this.progressBar_Convert.Value = e.ProgressPercentage;
                if( !this.gui )
                {
                    double width = this.progressBar_Convert.Maximum - this.progressBar_Convert.Minimum;
                    double prg = ((e.ProgressPercentage - this.progressBar_Convert.Minimum)) / width;

                    Console.Write("\r{0:P2}", prg);
                    Console.Out.Flush();
                }
            }
        }

        /// <summary>
        /// バックグラウンド処理が終了したときの処理
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void BackgroundWorker_Convert_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // タイマー停止
            this.timer_Elapsed.Stop();

            // エラーが発生した場合
            if (e.Error != null)
            {
                if (this.gui)
                {
                    // エラーメッセージを表示
                    Win32.SVMessageBox.Show(this, e.Error.Message, this.Text + " - Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Console.WriteLine("NG");
                    Console.WriteLine("{0}:{1}", this.Text + " - Error", this, e.Error.Message);
                }
                this.error = e.Error;
                this.DialogResult = DialogResult.Abort;
            }
            // キャンセルされた場合
            else if (e.Cancelled)
            {
                this.DialogResult = DialogResult.Cancel;
                if (!this.gui)
                {
                    Console.WriteLine("CANCEL");
                }
            }
            // エラーがあった場合
            else if(e.Result != null)
            {
                if (this.gui)
                {
                    // エラーメッセージを表示
                    Win32.SVMessageBox.Show(this, e.Result.ToString(), this.Text + " - Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Console.WriteLine("NG");
                    Console.WriteLine("{0}:{1}", this.Text + " - Error", this, e.Result.ToString());
                }
            }
            else
            {
                if (this.gui)
                {
                    Win32.SVMessageBox.Show(this, this.CompleteMessage, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Console.WriteLine("OK");
                }
                this.DialogResult = DialogResult.OK;
            }

            // ダイアログを閉じる
            this.Close();
        }
        #endregion

        #region ▼ Private Method
        #endregion

        #region ▼ Public Method
        #endregion

        #region ▼ Override Method
        #endregion
    }
}
