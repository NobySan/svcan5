﻿namespace NVdat2VSYNC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    /// <summary>
    /// 汎用メソッドの定義
    /// </summary>
    public static class Utility
    {
        #region メソッド

        /// <summary>
        /// 子フォルダの取得及び設定
        /// </summary>
        public class ChildFileItem
        {
            /// <summary>
            /// 親フォルダ
            /// </summary>
            public string ParentFolder
            {
                get; set;
            }

            /// <summary>
            /// 子階層化のファイル名(*.dat)
            /// </summary>
            public List<string> ChildFiles
            {
                get; set;
            } = new List<string>();

            /// <summary>
            /// ファイル数チェック
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return this.ChildFiles.Count == 0;
                }
            }

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="parentFolder">親フォルダまたはdatファイル</param>
            /// <param name="searchOption">配下検索オプション</param>
            public ChildFileItem(string parentFolder, System.IO.SearchOption searchOption = System.IO.SearchOption.AllDirectories)
            {
                // 親フォルダのチェック
                if (!System.IO.Directory.Exists(parentFolder))
                {
                    this.ParentFolder = System.IO.Path.GetDirectoryName(parentFolder);

                    // datファイル単体
                    var ext = System.IO.Path.GetExtension(parentFolder);
                    if (ext.ToUpper() == ".DAT")
                    {
                        // 単一ファイル
                        this.ChildFiles.Add(parentFolder);
                    }
                    return;
                }

                this.ParentFolder = parentFolder;

                // フォルダ内のファイルリスト（サブフォルダは無視）
                string[] files = System.IO.Directory.GetFiles(parentFolder, "*.dat", searchOption);
                long size = files.Length;
                if (searchOption == System.IO.SearchOption.TopDirectoryOnly)
                {
                    size = Math.Min(size, 1);
                }
                for( int i = 0; i < size; i++ )
                {
                    // 先頭ファイルのみ追加
                    this.ChildFiles.Add(files[i]);
                }
            }
        }

        /// <summary>
        /// 指定された親フォルダ配下の構成を取得する(無制限)
        /// </summary>
        /// <param name="parentFolder">親フォルダ</param>
        /// <returns>取得した子階層</returns>
        public static List<ChildFileItem> GetChildFiles(string parentFolder)
        {
            List<ChildFileItem> childFileItems = new List<ChildFileItem>();

            if (!System.IO.Directory.Exists(parentFolder))
            {
                // datファイルのみ
                var item = new ChildFileItem(parentFolder);
                if (item.ChildFiles.Count != 0)
                {
                    childFileItems.Add(item);
                }
            }
            else
            {
                // 配下に.datファイルが存在する場合は単体フォルダとして扱う
                string[] files = System.IO.Directory.GetFiles(parentFolder, "*.dat", System.IO.SearchOption.TopDirectoryOnly);

                // 階層がない場合はdatファイルのみを取得
                if (files.Length != 0)
                {
                    var item = new ChildFileItem(parentFolder, System.IO.SearchOption.TopDirectoryOnly);
                    if (item.ChildFiles.Count != 0)
                    {
                        childFileItems.Add(item);
                    }
                    return childFileItems;
                }

                string[] folders = System.IO.Directory.GetDirectories(parentFolder, "*", System.IO.SearchOption.TopDirectoryOnly);

                // 親フォルダ無制限
                for (int i = 0; i < folders.Length; i++)
                {
                    var item = new ChildFileItem(folders[i]);
                    if (!item.IsEmpty)
                    {
                        childFileItems.Add(item);
                    }
                }
            }

            return childFileItems;
        }

        /// <summary>
        /// ファイルの総数を取得する
        /// </summary>
        /// <param name="childFileItems">フォルダ構成</param>
        /// <returns>ファイル数</returns>
        public static int GetFileCounts(List<ChildFileItem> childFileItems)
        {
            int counts = 0;
            for (int i = 0; i < childFileItems.Count; i++)
            {
                counts += childFileItems[i].ChildFiles.Count;
            }

            return counts;
        }

        #endregion
    }
}
