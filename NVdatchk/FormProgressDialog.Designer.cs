﻿namespace NVdat2VSYNC
{
    partial class FormProgressDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.progressBar_Convert = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker_Convert = new System.ComponentModel.BackgroundWorker();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.label_Progress = new System.Windows.Forms.Label();
            this.timer_Elapsed = new System.Windows.Forms.Timer(this.components);
            this.label_ElapsedTime = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar_Convert
            // 
            this.progressBar_Convert.Location = new System.Drawing.Point(25, 74);
            this.progressBar_Convert.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar_Convert.Name = "progressBar_Convert";
            this.progressBar_Convert.Size = new System.Drawing.Size(389, 25);
            this.progressBar_Convert.TabIndex = 0;
            // 
            // backgroundWorker_Convert
            // 
            this.backgroundWorker_Convert.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker_Convert_ProgressChanged);
            this.backgroundWorker_Convert.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker_Convert_RunWorkerCompleted);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(341, 122);
            this.button_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(73, 21);
            this.button_Cancel.TabIndex = 1;
            this.button_Cancel.Text = "キャンセル(&C)";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // label_Progress
            // 
            this.label_Progress.AutoSize = true;
            this.label_Progress.Location = new System.Drawing.Point(23, 33);
            this.label_Progress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Progress.Name = "label_Progress";
            this.label_Progress.Size = new System.Drawing.Size(35, 12);
            this.label_Progress.TabIndex = 2;
            this.label_Progress.Text = "label1";
            // 
            // timer_Elapsed
            // 
            this.timer_Elapsed.Tick += new System.EventHandler(this.Timer_Elapsed_Tick);
            // 
            // label_ElapsedTime
            // 
            this.label_ElapsedTime.AutoSize = true;
            this.label_ElapsedTime.Location = new System.Drawing.Point(23, 122);
            this.label_ElapsedTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_ElapsedTime.Name = "label_ElapsedTime";
            this.label_ElapsedTime.Size = new System.Drawing.Size(35, 12);
            this.label_ElapsedTime.TabIndex = 3;
            this.label_ElapsedTime.Text = "label1";
            // 
            // FormProgressDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 163);
            this.Controls.Add(this.label_ElapsedTime);
            this.Controls.Add(this.label_Progress);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.progressBar_Convert);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProgressDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Progress...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormProgressDialog_FormClosing);
            this.Shown += new System.EventHandler(this.FormProgressDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar_Convert;
        private System.ComponentModel.BackgroundWorker backgroundWorker_Convert;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Label label_Progress;
        private System.Windows.Forms.Timer timer_Elapsed;
        private System.Windows.Forms.Label label_ElapsedTime;
    }
}