﻿namespace NVdat2VSYNC
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    using System;
    using SVUtil;
    using System.Reflection;
    using System.Diagnostics;

    /// <summary>
    /// メインウィンドウ
    /// </summary>
    public partial class FormMain : Form
    {
        #region 定数
        #endregion

        #region ▼ Field
        /// <summary>
        /// バックグラウンドで実行するメソッドの引数
        /// </summary>
        private int datchk_error;
        #endregion

        #region プロパティ
        #endregion

        #region コンストラクタ

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FormMain()
        {
            this.InitializeComponent();
        }
        #endregion

        #region メソッド(公開)

        #endregion

        #region メソッド(非公開)

        /// <summary>
        /// 各値を初期化
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            this.radioButton_8bit.Checked = true;
        }

        #region ▼ Event

        /// <summary>
        /// バックグラウンド実行する処理
        /// </summary>
        /// <param name="sender">発生元</param>
        /// <param name="e">添付情報</param>
        private void ProgressDialog_DoWork(object sender, DoWorkEventArgs e)
        {
            // 進捗管理クラス
            Progress progress = new Progress()
            {
                BackgroundWorker = sender as BackgroundWorker,
                DoWorkEventArgs = e
            };

            Dictionary<string, object> args = e.Argument as Dictionary<string, object>;
            Dictionary<string, object> checkFolder = new Dictionary<string, object>();

            // CANパラメータ
            CanParameter canParameter = args["param"] as CanParameter;
            // 変換処理クラス
            Dat2CSV dat2CSV = new Dat2CSV();

            // 着手数を更新
            progress.NumberOfProcessedFiles++;
            // 進捗を通知
            progress.BackgroundWorker.ReportProgress(-1, progress);

            // コンソール進捗
            Console.WriteLine("Progress...");

            // 変換実行
            //int ret = dat2CSV.Execute(tBoxDatFilename.Text, canParameter, progress);

            // 進捗ダイアログでキャンセルされた場合
            if (progress.BackgroundWorker.CancellationPending == true)
            {
                // 処理を終了する
                progress.DoWorkEventArgs.Cancel = true;
            }

            //if (ret == 0)
            //{
            //    // 正常
            //    progress.DoWorkEventArgs.Result = 0;
            //    this.datchk_error = 0;
            //}
            //else
            //{
            //    // 失敗
            //    progress.DoWorkEventArgs.Result = ret;
            //    this.datchk_error = ret;
            //}

            // 改行
            Console.WriteLine();
        }
        #endregion

        /// <summary>
        /// パラメータを取得する
        /// </summary>
        /// <param name="param">設定パラメータ</param>
        /// <param name="message">メッセージ有無</param>
        /// <returns>取得できた場合はtrue,それ以外はfalse</returns>
        private bool GetParameter(CanParameter param, bool message=true)
        {
            try
            {
                if (this.radioButton_8bit.Checked)
                {
                    param.DataSize = Define.DataSize._8bit;
                }
                else
                {
                    param.DataSize = Define.DataSize._16bit;
                }
            }
            catch (Exception)
            {
                if (message)
                {
                    Win32.SVMessageBox.Show(this, "A non-numeric value has been entered.\r\nPlease enter only numbers.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// 変換処理を実行します。
        /// </summary>
        /// <param name="param">CANパラメータ</param>
        /// <param name="gui">GUIモード</param>
        /// <param>エラー終了ならfalse</param>
        public bool ExecuteConvert(CanParameter param, bool gui)
        {
            try
            {
                this.AllowDrop = false;

                Dictionary<string, object> args = new Dictionary<string, object>()
                {
                    { "param", param },
                };

                using (FormProgressDialog progressDialog = new FormProgressDialog(this.ProgressDialog_DoWork, args, gui))
                {
                    // 進捗ダイアログを表示・処理を実行
                    DialogResult result = progressDialog.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        return true;
                    }
                }
            }
            catch(Exception)
            {
            }
            finally
            {
                this.AllowDrop = true;
            }

            return false;
        }


        /// <summary>
        /// 変換処理を実行します。
        /// </summary>
        /// <param name="param">CANパラメータ</param>
        /// <param name="gui">GUIモード</param>
        /// <param>エラー終了ならfalse</param>
        public int ExecuteConvert2(string[] args)
        {
            try
            {
                this.AllowDrop = false;

                using (FormProgressDialog progressDialog = new FormProgressDialog(this.ProgressDialog_DoWork, args, false))
                {
                    // 進捗ダイアログを表示・処理を実行
                    DialogResult result = progressDialog.ShowDialog();
                    return this.datchk_error;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                this.AllowDrop = true;
            }

            return 0;
        }

        /// <summary>
        /// 実行処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void btnAllConvert_Click(object sender, EventArgs e)
        {
            // 変換処理を実行
            //this.ExecuteConvert(Program.mEnv, true);
        }

        /// <summary>
        /// フォームを閉じる処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 正常設定ならパラメータ保存
            CanParameter canParameter = new CanParameter();
            if (this.GetParameter(canParameter, false))
            {
                //Program.mEnv = canParameter;
            }
        }

        #endregion

        /// <summary>
        /// フォルダ選択ダイアログ
        /// https://thenewsinpu.hatenablog.jp/entry/2018/04/21/230616
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void btnDatFolder_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            TextBox tbx = null;

            tbx = this.tBoxDatFilename;

            var dlg = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog("Select DAT File");
            dlg.IsFolderPicker = false;
            dlg.DefaultFileName = tbx.Text;
            if (dlg.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                tbx.Text = dlg.FileName;
            }
        }
    }
}
