﻿namespace NVdat2VSYNC
{
    using SVCanData;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// CANデータCSV出力
    /// </summary>
    public class CAN2Csv
    {
        #region フィールド

        /// <summary>
        /// 書き込みストリーム
        /// </summary>
        private System.IO.StreamWriter streamWriter = null;

        /// <summary>
        /// 出力行数
        /// </summary>
        private long writeCounts = 0;

        #endregion

        #region プロパティ

        /// <summary>
        /// エラー有無(内部的な)
        /// </summary>
        public bool IsError { get; private set; } = false;


        #endregion

        #region コンストラクタ

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CAN2Csv()
        {

        }
        #endregion

        #region メソッド   

        /// <summary>
        /// 書き込みファイルオープン
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        /// <param name="msg">オープンエラーメッセージ</param>
        /// <returns>正常ならtrue,失敗ならfalseを返却</returns>
        public bool Open(string fileName, out string msg)
        {
            msg = string.Empty;

            try
            {
                // 書き込みオープン
                this.streamWriter = new System.IO.StreamWriter(fileName, false, System.Text.Encoding.GetEncoding("shift_jis"));

                // ヘッダ書き込み
                this.streamWriter.WriteLine("Time,Type,Ch,ID(h),DLC,Data");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }

            return true;
        }


        /// <summary>
        /// １レコードの書き込み(CAN CSVより取得)
        /// </summary>
        /// <param name="ch">CAN CH番号</param>
        /// <param name="canRecord">CANレコード</param>
        /// <param name="msg">オープンエラーメッセージ</param>
        /// <returns>正常ならtrue,失敗ならfalseを返却</returns>
        public bool Write(int ch, SVCanData.SvCanRecord canRecord, out string msg)
        {
            msg = string.Empty;

            try
            {
                //if (canRecord.frame_type == SvCanFrameType.DecodeError)
                //{
                //    this.IsError = true;
                //}

                this.streamWriter.WriteLine("{0},{1},{2},{3:X3},{4},{5}",
                    canRecord.time,
                    canRecord.frame_type,
                    ch+1,
                    canRecord.id,
                    canRecord.dlc,
                    canRecord.dump_data
                );

                this.writeCounts++;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// １レコードの書き込み(VSYNC情報)
        /// </summary>
        /// <param name="ch">VSYNC CH番号</param>
        /// <param name="pos">VSYNC立ち上がりのクロック位置(100MHz)</param>
        /// <param name="msg">オープンエラーメッセージ</param>
        /// <returns>正常ならtrue,失敗ならfalseを返却</returns>
        public bool WriteVsync(int ch, long pos, out string msg)
        {
            msg = string.Empty;
            string time = "";

            try
            {
                //if (canRecord.frame_type == SvCanFrameType.DecodeError)
                //{
                //    this.IsError = true;
                //}
                time = string.Format("{0:F1}", ((double)(pos / 1) / 100)); // xx.x usec
                this.streamWriter.WriteLine("{0},{1},{2}",
                    time,
                    "VSYNC",
                    ch
                );

                this.writeCounts++;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// ファイルを閉じる
        /// </summary>
        public void Close()
        {
            if (this.streamWriter != null)
            {
                // 内部エラーが無ければ
                if (this.IsError == false)
                {
                    // 全部idleなら
                    if (this.writeCounts == 0)
                    {
                        this.IsError = true;
                    }
                }

                try
                {
                    this.streamWriter.Close();
                    this.streamWriter.Dispose();
                    this.streamWriter = null;
                }
                catch
                {
                }
            }
        }

        #endregion
    }
}
