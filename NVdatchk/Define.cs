﻿namespace NVdat2VSYNC
{
    /// <summary>
    /// 初期値情報
    /// </summary>
    public static class Define
    {
        /// <summary>
        /// データサイズ
        /// </summary>
        public enum DataSize
        {
            _16bit = 0,
            _8bit = 1
        }
    }
}
