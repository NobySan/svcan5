﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SVCanData
{
    /// <summary>
    /// SvCanヘッダ
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class SvCanHeader : ICloneable
    {
        /// <summary>
        /// ヘッダー長(256固定)
        /// </summary>
        public Int32 header_length = 256;
        /// <summary>
        /// アプリーケーション名 バージョン "SVImonCAN 0000"
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte [] appl_name = new byte[16];
        /// <summary>
        /// ファームウェアのバージョン番号(100 を掛けた値) 1 00 の場合 100
        /// </summary>
        public Int32 version_of_firmware = 100;
        /// <summary>
        /// ハードウェアのバージョン番号(100 を掛けた値) 1 21 の場合 121
        /// </summary>
        public Int32 version_of_hardware = 100;
        /// <summary>
        /// 画像幅
        /// </summary>
        public Int32 picture_width;
        /// <summary>
        /// 画像高さ
        /// </summary>
        public Int32 picture_height = 1;
        /// <summary>
        /// VSync 信号の極性(0 Low, 1 High)
        /// </summary>
        public Int32 vsync_porarity;
        /// <summary>
        /// HSync 信号の極性(0 Low, 1 High)
        /// </summary>
        public Int32 hsync_porarity = 1;
        /// <summary>
        /// 画像フォーマット(2:RAW-8 bit, 7:RAW-12bit)
        /// </summary>
        public Int32 picture_type = 7;
        /// <summary>
        /// データサイズ(ヘッダーを含まない)バイト数
        /// </summary>
        public UInt64 num_of_scan = 0;
        /// <summary>
        /// 10秒間にVSync信号が立ち上がった回数 、/10で1秒間のFPS
        /// </summary>
        public Int32 frame_rate = 0;
        /// <summary>
        /// 1クロック当たりの記録バイト数 0x0002 固定
        /// </summary>
        public Int32 rec_data_width = 0x0002;
        /// <summary>
        /// 録画開始日(yyyymmdd)
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] start_date = new byte[8];
        /// <summary>
        /// 録画開始時間(hhmmssmm)
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] start_time = new byte[8];
        /// <summary>
        /// ロストフレーム
        /// </summary>
        public Int32 lost_frame_cnt = 0x0002;
        /// <summary>
        /// 予約領域
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 172)]
        public byte[] reserve = new byte[176];

        /// <summary>
        /// V Sync Active Mask
        /// </summary>
        public ushort VSyncActiveMask
        {
            get
            {
                if (vsync_porarity == 0)
                {
                    return(SvCanBitMask.SYNCACTIVEMASK_V);
                }
                else
                {
                    return( 0 );
                }
            }
        }

        /// <summary>
        /// H Sync Active Mask
        /// </summary>
        public ushort HSyncActiveMask
        {
            get
            {
                if (hsync_porarity == 0)
                {
                    return (SvCanBitMask.SYNCACTIVEMASK_H);
                }
                else
                {
                    return (0);
                }
            }
        }

        /// <summary>
        /// バージョン情報
        /// </summary>
        public SvCanHeader()
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes("SVImonCAN 1070");
            Array.Copy(data, appl_name, data.Length);
        }

        /// <summary>
        /// 開始日時を打刻
        /// </summary>
        public void SetStartDateTime()
        {
            DateTime now = DateTime.Now;
            byte[] data = System.Text.Encoding.ASCII.GetBytes(now.ToString("yyyyMMdd"));
            Array.Copy(data, start_date, 8);

            data = System.Text.Encoding.ASCII.GetBytes(now.ToString("HHmmssff"));
            Array.Copy(data, start_time, 8);
        }

        /// <summary>
        /// クローン
        /// </summary>
        /// <returns>クローン値</returns>
        public Object Clone()
        {
            return MemberwiseClone();
        }
    }
}
