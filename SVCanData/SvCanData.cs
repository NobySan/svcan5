﻿using candec;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVCanData
{
    /// <summary>
    /// SvCanDatクラス
    /// </summary>
    public class SvCanData : IDisposable
    {
        /// <summary>
        /// フレームバッファ管理データ
        /// </summary>
        class Frame
        {
            /// <summary>
            /// フレーム開始アドレス
            /// </summary>
            long startAdress;
            /// <summary>
            /// フレーム終了アドレス
            /// </summary>
            long endAdress;
            /// <summary>
            /// 有効ピクセル数
            /// </summary>
            int pixcelCount;
            /// <summary>
            /// 有効Line数
            /// </summary>
            int lineCount;
            /// <summary>
            /// フレーム番号
            /// </summary>
            int frameNo;
            /// <summary>
            /// VSync
            /// </summary>
            int vBlanking;
            /// <summary>
            /// HSync
            /// </summary>
            int hBlanking;

            /// <summary>
            /// 幅
            /// </summary>
            int _width;
            public int Width
            {
                get
                {
                    return _width;
                }
            }
            /// <summary>
            /// 高さ
            /// </summary>
            int _height;
            public int Height
            {
                get
                {
                    return _height;
                }
            }

            /// <summary>
            /// CAN0レコード
            /// </summary>
            public List<SvCanRecord> mCan0Record;
            /// <summary>
            /// CAN1レコード
            /// </summary>
            public List<SvCanRecord> mCan1Record;

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="sAdress">フレーム開始アドレス</param>
            /// <param name="eAdress">フレーム終了アドレス</param>
            /// <param name="pixCount">有効ピクセル数</param>
            /// <param name="linCount">有効ライン数</param>
            /// <param name="fNo">フレーム番号</param>
            /// <param name="vB">VBlanking</param>
            /// <param name="hB">HBlanking</param>
            public Frame(long sAdress, long eAdress, int pixCount, int linCount, int fNo, int vB, int hB, List<SvCanRecord> can0, List<SvCanRecord> can1)
            {
                startAdress = sAdress;          // フレーム開始アドレス
                endAdress = eAdress;            // フレーム終了アドレス
                pixcelCount = pixCount;         // 有効ピクセル数
                lineCount = linCount;           // 有効Line数
                frameNo = fNo;                  // フレーム番号
                vBlanking = vB;                 // VBlanking
                hBlanking = hB;					// HBlanking
                if (linCount > 1) _width = pixCount / linCount;
                else _width = 0;
                _height = linCount;

                mCan0Record = can0;
                mCan1Record = can1;
            }

            /// <summary>
            /// 開始位置
            /// </summary>
            public long SPos
            {
                get
                {
                    return (startAdress);
                }
            }
            /// <summary>
            /// 終了位置
            /// </summary>
            public long EPos
            {
                get
                {
                    return (endAdress);
                }
                set
                {
                    endAdress = value;
                }
            }

        }

        /// <summary>
        /// ファイルストリーム
        /// </summary>
        Win32File m_file = null;
        /// <summary>
        /// ファイルオフセット
        /// </summary>
        long FILE_OFFSET = Marshal.SizeOf(typeof(SvCanHeader));
        /// <summary>
        /// ヘッダ情報
        /// </summary>
        SvCanHeader m_header = null;
        /// <summary>
        /// フレームバッファ
        /// </summary>
        List<Frame> m_frames = new List<Frame>();
        /// <summary>
        /// 読込サイズ位置(バイト単位)
        /// </summary>
        int mReadCounts = 0;
        /// <summary>
        /// 読込サイズ(バイト単位)
        /// </summary>
        int mReadSize = 0;
        /// <summary>
        /// 読込キャッシュ
        /// </summary>
        byte[] mReadCache = new byte[READ_CAHCHE_SIZE];
        /// <summary>
        /// 読込キャッシュサイズ(2の倍数)
        /// </summary>
        const int READ_CAHCHE_SIZE = 1024 * 1024 * 16;
        /// <summary>
        /// 読込終了フラグ
        /// </summary>
        bool mReadEOF = false;

        /// <summary>
        /// CAN0レコード
        /// </summary>
        public List<SvCanRecord> AllCan0Record = new List<SvCanRecord>();
        /// <summary>
        /// CAN1レコード
        /// </summary>
        public List<SvCanRecord> AllCan1Record = new List<SvCanRecord>();


        /// <summary>
        /// フレームサイズ
        /// </summary>
        public Size FrameSize
        {
            get
            {
                if (m_frames.Count == 0)
                {
                    return (new Size(0, 0));
                }
                return (new Size(m_frames[0].Width, m_frames[0].Height));
            }
        }

        /// <summary>
        /// フレームサイズ(空データ読み込み時)
        /// </summary>
        public int FrameDataSize { get; set; } = 1;

        /// <summary>
        /// OpenNone時のフレーム数
        /// </summary>
        public int FrameCounts { get; set; } = 0;

        /// <summary>
        /// 最大フレーム
        /// </summary>
        public int FrameMax
        {
            get
            {
                // 0の場合は疑似フレーム数の返却
                if ( this.m_frames.Count == 0)
                {
                    return this.FrameCounts;
                }

                return (m_frames.Count);
            }

        }

        /// <summary>
        /// V Sync Level
        /// </summary>
        public int VSyncPorarity
        {
            get
            {
                return (m_header.vsync_porarity);
            }

        }

        /// <summary>
        /// H Sync Level
        /// </summary>
        public int HSyncPorarity
        {
            get
            {
                return (m_header.hsync_porarity);
            }

        }

        /// <summary>
        /// ビットマップ用ハンドル
        /// </summary>
        SVUtil.DIBitmap mBitmap = null;

        /// <summary>
        /// ファイル名
        /// </summary>
        string mFileName;

        /// <summary>
        /// CAN0レコード(1フレーム)
        /// </summary>
        public List<SvCanRecord> mCan0Record;
        /// <summary>
        /// CAN1レコード(1フレーム)
        /// </summary>
        public List<SvCanRecord> mCan1Record;

        /// <summary>
        /// ヘッダ情報
        /// </summary>
        public SvCanHeader Header
        {
            get
            {
                return this.m_header;
            }
        }

        /// <summary>
        /// OpenNoneで開いた場合の処理
        /// </summary>
        public bool IsOpenNone
        {
            get
            {
                if (this.svCanListData != null)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// エラーメッセージ
        /// </summary>
        public string ErrMessage = string.Empty;

        /// <summary>
        /// リスト.dat群
        /// </summary>
        private SVCanData.SvCanListData svCanListData = null;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SvCanData()
        {
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~SvCanDat() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion

        /// <summary>
        /// byte配列から構造体へ変換
        /// </summary>
        /// <typeparam name="Type">型</typeparam>
        /// <param name="buffer">バイト配列</param>
        /// <returns>変換した型</returns>
        static Type Byte2Struct<Type>(byte[] buffer)
        {
            GCHandle gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            Type ret = (Type)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Type));
            gch.Free();
            return (ret);
        }

        /// <summary>
        /// データ長さ(バイト)
        /// </summary>
        public long DataLength
        {
            get
            {
                if (this.svCanListData != null)
                {
                    return this.svCanListData.TotalBytes;
                }

                if (m_header == null)
                {
                    return (0);
                }
                return ((long)m_header.num_of_scan);
            }
        }

        /// <summary>
        /// FPS値
        /// </summary>
        public double FPS
        {
            get
            {
                if (m_header == null)
                {
                    return (0.0);
                }
                return ((double)m_header.frame_rate / 100.0);
            }
        }

        /// <summary>
        /// ビットマップ反転
        /// </summary>
        bool mBitmapReverse = false;
        /// <summary>
        /// LSB Pack シフトビット数
        /// </summary>
        ushort mLsbBackShift = 0;

        /// <summary>
        /// ビットマップモードの変更
        /// </summary>
        /// <param name="reverse">リバース</param>
        /// <param name="lsb_pack">パック</param>
        public void SetBitmapMode(bool reverse, bool lsb_pack)
        {
            mLsbBackShift = (ushort)((lsb_pack) ? 0 : 4);

            if (m_file != null && mBitmap != null)
            {
                // 書き換え
                if (reverse != mBitmapReverse)
                {
                    if (reverse)
                    {
                        mBitmap.CreateDIB(m_frames[0].Width, -m_frames[0].Height, 8);
                    }
                    else
                    {
                        mBitmap.CreateDIB(m_frames[0].Width, m_frames[0].Height, 8);
                    }
                }
            }
            mBitmapReverse = reverse;
        }

        /// <summary>
        /// データのリセット
        /// </summary>
        void Reset()
        {
            this.ErrMessage = string.Empty;

            if (this.svCanListData != null)
            {
                this.svCanListData.Close();
                this.svCanListData = null;
            }
        }

        /// <summary>
        /// ファイルを開く
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <param name="threshold">CAN解析用のthreshold1,threshold2</param>
        /// <param name="bps">CAN解析用のBPS,FD-BPS</param>
        /// <returns>ファイルが正常に開けた場合はtrue,それ以外はfalseを返却</returns>
        public bool Open(string file_name, int []threshold, int [] bps)
        {
            Reset();

            try
            {
                m_file = new Win32File();

                // ファイルオープン
                if (!m_file.Open(file_name))
                {
                    return (false);
                }

                mFileName = file_name;

                int size = Marshal.SizeOf(typeof(SvCanHeader));
                byte[] buf = new byte[size];
                size = m_file.Read(buf, size);

                // バッファ不足
                if (buf.Length != size)
                {
                    m_file.Close();
                    this.ErrMessage = "Header information is empty.";
                    return (false);
                }

                // ヘッダ読込
                m_header = Byte2Struct<SvCanHeader>(buf);

                // フレーム作成進捗表示
                using (SVUtil.ProgressWindow dlg = new SVUtil.ProgressWindow())
                {
                    // フレームリストの作成
                    System.Threading.Tasks.Task.Run(() =>
                        {
                            while (true)
                            {
                                // ダイアログが有効化されたら実行
                                if (dlg.InvokeRequired)
                                {
                                    try
                                    {
                                        MakeFrameList(dlg, threshold, bps);
                                        break;
                                    }
                                    catch(Exception ex)
                                    {
                                        dlg.IsAbort = true;

                                        dlg.Invoke((MethodInvoker)delegate ()
                                        {
                                            Win32.SVMessageBox.Show(dlg, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        });
                                        dlg.Finish();

                                        return;
                                    }

                                }
                                System.Threading.Thread.Sleep(100);
                            }
                        }
                    );

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                    {
                        this.ErrMessage = "Aborted.";
                        return (false);
                    }

                    if (m_frames.Count == 0)
                    {
                        this.ErrMessage = "Frame data is empty.";
                        return(false);
                    }

                    mBitmap = new SVUtil.DIBitmap();
                    if (mBitmapReverse)
                    {
                        mBitmap.CreateDIB(m_frames[0].Width, -m_frames[0].Height, 8);
                    }
                    else
                    {
                        mBitmap.CreateDIB(m_frames[0].Width, m_frames[0].Height, 8);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SvCanData.Open Exception = {0}", ex.Message);
                this.ErrMessage = string.Format("Exception Error.\r\n{0}", ex.Message );
                return (false);
            }

            return (true);
        }

        /// <summary>
        /// ファイルを開く(解析無し)
        /// </summary>
        /// <param name="fileNames">ファイル群</param>
        /// <returns>ファイルが正常に開けた場合はtrue,それ以外はfalseを返却</returns>
        public bool OpenNone(List<string> fileNames)
        {
            Reset();
            try
            {
                this.svCanListData = new SvCanListData();
                if (this.svCanListData.SetFiles(fileNames) != SvCanListData.STATUS.OK)
                {
                    this.ErrMessage = this.svCanListData.ErrorMsg;
                    this.svCanListData = null;
                    return false;
                }

                // ヘッダ読込
                m_header = this.svCanListData.Header;

                if (m_header.rec_data_width != 2)
                {
                    this.ErrMessage = "Error : There is no data of 2byte mode.";
                    this.svCanListData = null;
                    return (false);
                }

                mBitmap = new SVUtil.DIBitmap();
                mBitmap.CreateDIB(m_header.picture_width, m_header.picture_height, 8);

                // 最終フレーム情報の設定
                this.FrameCounts = this.svCanListData.TotalFrames;
                this.FrameDataSize = (int)(this.svCanListData.TotalBytes / (long)this.FrameCounts);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SvCanData.OpenNone Exception = {0}", ex.Message);
                this.ErrMessage = string.Format("SvCanData.OpenNone Exception Error.\r\n{0}", ex.Message);
                return (false);
            }

            return (true);
        }
        
        /// <summary>
        /// フレーム情報の設定
        /// </summary>
        void FrameDataInfoSet()
        {
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            int lineCount = 0;
            bool enableV = false;
            long size = (long)(m_header.num_of_scan);

            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            // データ先頭位置へ
            DataSeek(0);

            long offset = 0, start = 0, last_vs_active = 0, last_vs_non_active = 0;
            long i;
            for (i = 0; i < size; i++, offset++)
            {
                UInt16 wordCurrent = DataRead();

                // VSyncの立ち上がりチェック
                if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V)
                {
                    if (!enableV)
                    {
                        enableV = true;
                        if (lineCount == 1)
                        {
                            this.FrameDataSize = (int)(offset - start - 1);
                            break;
                        }
                        lineCount++;
                        start = offset;
                    }
                }
                else if (enableV)
                {
                    enableV = false;
                }
            }

            // 最後のVSync立ち下がり位置をチェック
            offset = size;
            for (i = offset; i > 0 && last_vs_non_active == 0;)
            {
                int read_size;
                if (i < READ_CAHCHE_SIZE)
                {
                    read_size = (int)i;
                }
                else
                {
                    read_size = READ_CAHCHE_SIZE;
                }
                offset -= read_size;

                m_file.Seek(offset + FILE_OFFSET);
                mReadSize = m_file.Read(this.mReadCache, read_size);

                for (int j = mReadSize - 1; j >= 0; j--)
                {
                    if (last_vs_active == 0)
                    {
                        if ((this.mReadCache[j] & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V)
                        {
                            last_vs_active = offset + j;
                        }
                    }
                    else
                    {
                        if ((this.mReadCache[j] & SvCanBitMask.SYNCACTIVEMASK_V) != wordSyncActive_V)
                        {
                            last_vs_non_active = offset + j + 1;
                            break;
                        }
                    }
                }

                i -= read_size;
            }

            double dd = ((double)(last_vs_non_active - start) / (double)this.FrameDataSize) + 0.5;
            FrameCounts = (int)dd;
            //FrameCounts = (int)((last_vs_non_active - start) / this.FrameDataSize);
            if ((last_vs_non_active - last_vs_active) > (1280*960)) FrameCounts++;

            //最後の終了位置を書き換える
            m_header.num_of_scan = (ulong)size;
        }


        /// <summary>
        /// ファイルを閉じる
        /// </summary>
        public void Close()
        {
            if (this.svCanListData != null)
            {
                this.svCanListData.Close();
            }
            else
            {
                m_file.Close();
            }
        }

        /// <summary>
        /// ブランクビットマスクの取得
        /// </summary>
        /// <param name="wordActiveV">アクティブV</param>
        /// <param name="wordActiveH">アクティブH</param>
        void GetActiveAndBlankBit(out ushort wordActiveV, out ushort wordActiveH)
        {
            if (m_header.vsync_porarity == 0)
            {
                wordActiveV = SvCanBitMask.SYNCACTIVEMASK_V;
            }
            else
            {
                wordActiveV = 0;
            }
            if (m_header.hsync_porarity == 0)
            {
                wordActiveH = SvCanBitMask.SYNCACTIVEMASK_H;
            }
            else
            {
                wordActiveH = 0;
            }
            // SVOgenCANではVHsyncの極性は無視して、BlankはLowActive固定とする
            wordActiveV = SvCanBitMask.SYNCACTIVEMASK_V;
            wordActiveH = SvCanBitMask.SYNCACTIVEMASK_H;
        }

        /// <summary>
        /// フレームリストの作成
        /// </summary>
        /// <param name="dlg">進捗ダイアログ</param>
        /// <param name="threshold">CANデータthreshold</param>
        /// <param name="bps">CANデータBPS</param>
        private void MakeFrameList(SVUtil.ProgressWindow dlg, int[] threshold, int [] bps)
        {
            m_frames.Clear();

            Candec can_codec = new Candec();

            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            bool enableFrame = false;
            bool enableLine = false;
            int pixcelCount = 0;
            int lineCount = 0;
            int frameCount = 0;                         // フレームカウンター
            int vertical = 0;                           // Vブランキング
            int horizontal = 0;                         // Hブランキング
            bool enableVBlank = false;					// Vブランキング期間判定
            long sAdress = 0, eAdress;

            List<SvCanRecord> can0_rec = new List<SvCanRecord>();
            List<SvCanRecord> can1_rec = new List<SvCanRecord>();

            long[] can0_packet = new long[32767];
            long[] can1_packet = new long[32767];

            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            // データ先頭位置へ
            DataSeek(0);

            // CANデコード開始
            can_codec.Can_Start(threshold[0], threshold[1]);
            can_codec.Can_set_bitrate(0, bps[0], bps[1]);
            can_codec.Can_set_bitrate(1, bps[0], bps[1]);

            this.FrameDataSize = 0;

            long size = (long)(m_header.num_of_scan / 2);  //   １データは２バイト
            long offset = 0;
            for (long i = 0; i < size; i++, offset += 2)
            {
                UInt16 wordCurrent = DataRead();
                if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                {
                    if (enableFrame == false)
                    {
                        //有効フレーム開始
                        enableFrame = true;
                        //有効フレーム開始位置保存
                        sAdress = offset;
                        //画素数カウント初期化
                        pixcelCount = 0;
                        //有効ラインカウント初期化
                        lineCount = 0;
                        // フレームカウンタインクリメント
                        frameCount++;
                    }

                    if (enableLine == false)
                    {
                        //ライン有効
                        enableLine = true;
                    }

                    //画素数カウント
                    pixcelCount++;
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V) //VSYNCのみ Active
                {
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        enableLine = false;
                        //有効ラインカウントアップ
                        lineCount++;
                        if(lineCount > 957 && frameCount == 291)
                        {
                            lineCount = lineCount;
                        }
                    }
                    // 無効期間
                    horizontal++;
                }
                else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_H) == wordSyncActive_H) //HSYNCのみ Active
                {
                    //Vブランキング期間
                    enableFrame = false;
                    // Vブランキング期間カウント判定
                    if (enableVBlank == true)
                    {
                        // カウント停止
                        enableVBlank = false;
                        // カウントUp
                        vertical++;
                    }
                }
                else
                {
                    //VHブランキング期間

                    // 2005.11.2 V3.31 VHハイ状態からいきなりVHロー状態になった時の対処
                    //Hブランキング期間(VSYNC Active)
                    if (enableLine == true)
                    {
                        //有効ラインカウントアップ
                        lineCount++;
                    }

                    // Vブランキング区間カウント
                    if (enableVBlank == false)
                    {
                        // カウント開始フラグOn
                        enableVBlank = true;
                    }

                    // フレーム終了判定
                    if (enableFrame == true)
                    {
                        eAdress = offset;
                        if (frameCount != 0)
                        {
                            //一つ前のフレーム情報を保存します
                            if (lineCount == 0)
                            {
                                //データ異常
                                return;
                            }

                            if ((m_header.picture_width == (pixcelCount / lineCount) && m_header.picture_height == lineCount))
                            {
                                // IDLE判定
                                if (can0_rec.Count == 0)
                                {
                                    can0_rec.Add(new SvCanRecord { frame_type = SvCanFrameType.IDLE });
                                }
                                if (can1_rec.Count == 0)
                                {
                                    can1_rec.Add(new SvCanRecord { frame_type = SvCanFrameType.IDLE });
                                }

                                // 解析情報格納
                                m_frames.Add(new Frame(sAdress, eAdress, pixcelCount, lineCount, frameCount, vertical, horizontal / lineCount, can0_rec, can1_rec));

                                // リスト再作成
                                can0_rec = new List<SvCanRecord>();
                                can1_rec = new List<SvCanRecord>();

                                if (this.FrameDataSize == 0)
                                {
                                    FrameDataSize = (int)(eAdress - sAdress);
                                }
                            }
                            else
                            {
                                Console.Out.WriteLine("Error Width={0}, Height={1}", (pixcelCount / lineCount), lineCount);
                                //Console.Write("Error Width={0}, Height={1}", (pixcelCount / lineCount), lineCount);
                            }
                            //Hブランキング期間の長さをリセット
                            horizontal = 0;
                            //Vブランキング期間の長さをリセット
                            vertical = 0;
                        }
                    }
                    enableFrame = false;
                }

                // CANデータの作成
                {
                    SvCanRecord rec;

                    int ret = can_codec.Can_serch_data(0,wordCurrent,ref can0_packet,ref can1_packet);
                    if (ret == 0)
                    {

                    }
                    else if (ret == 1)
                    {
                        // データ未取得
                    }
                    else
                    {

                        if (can0_packet[0] != -1)
                        {
                            rec = SvCapPacket.PacketConvert(can0_packet);
                            if (rec != null)
                            {
                                can0_rec.Add(rec);

                                this.AllCan0Record.Add(rec);
                            }
                        }
                        if (can1_packet[0] != -1)
                        {
                            rec = SvCapPacket.PacketConvert(can1_packet);
                            if (rec != null)
                            {
                                can1_rec.Add(rec);

                                this.AllCan1Record.Add(rec);
                            }
                        }
                    }
                }

                if (dlg.IsAbort) break;
                dlg.SetProgress((int)(((double)i / size) * 100));
            }

            if (m_frames.Count > 1)
            {
                // 開始～終了の終了を調整
                for (int i = 0; i < m_frames.Count - 1; i++)
                {
                    m_frames[i].EPos = m_frames[i + 1].SPos - 2;
                }
                m_frames[m_frames.Count - 1].EPos = (long)(m_header.num_of_scan - 2);
            }

            // 終了
            can_codec.Can_End();

            // ダイアログを閉じる
            dlg.Finish();
        }

        /// <summary>
        /// フレームデータの設定
        /// </summary>
        /// <param name="frame_no">フレームデータ</param>
        public IntPtr GetFrameData(int frame_no)
        {
            // データ読込無しの場合はそのまま返却
            if ( this.m_frames.Count() == 0)
            {
                return (mBitmap.Handle);
            }

            lock (m_frames)
            {
                ushort wordSyncActive_V;
                ushort wordSyncActive_H;

                bool enableFrame = false;
                bool enableLine = false;
                int pixcelCount = 0;
                int lineCount = 0;
                int frameCount = 0;                         // フレームカウンター
                int vertical = 0;                           // Vブランキング
                int horizontal = 0;                         // Hブランキング
                bool enableVBlank = false;                  // Vブランキング期間判定

                GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
                ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

                // データ先頭位置へ
                DataSeek(m_frames[frame_no].SPos);

                byte[] pixels = new byte[m_frames[frame_no].Width * m_frames[frame_no].Height];

                //SvCanFrameInfo can0FrameInfo = new SvCanFrameInfo();
                //SvCanFrameInfo can1FrameInfo = new SvCanFrameInfo();
    
                // CANレコード
                mCan0Record = m_frames[frame_no].mCan0Record;
                mCan1Record = m_frames[frame_no].mCan1Record;

                // 解析
                for (long i = m_frames[frame_no].SPos; i <= m_frames[frame_no].EPos; i += 2)
                {
                    UInt16 wordCurrent = DataRead();
                    if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                    {
                        if (enableFrame == false)
                        {
                            //有効フレーム開始
                            enableFrame = true;
                            //画素数カウント初期化
                            pixcelCount = 0;
                            //有効ラインカウント初期化
                            lineCount = 0;
                            // フレームカウンタインクリメント
                            frameCount++;
                        }

                        if (enableLine == false)
                        {
                            //ライン有効
                            enableLine = true;
                        }

                        //画素数カウント
                        if (pixcelCount < pixels.Length)
                        {
                            pixels[pixcelCount] = (byte)(wordCurrent >> mLsbBackShift);
                            pixcelCount++;
                        }
                    }
                    else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V) //VSYNCのみ Active
                    {
                        //Hブランキング期間(VSYNC Active)
                        if (enableLine == true)
                        {
                            enableLine = false;
                            //有効ラインカウントアップ

                            lineCount++;
                            // ピクセル配列のインデックスを調整
                            pixcelCount = m_frames[frame_no].Width * lineCount;
                        }
                        // 無効期間
                        horizontal++;
                    }
                    else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_H) == wordSyncActive_H) //HSYNCのみ Active
                    {
                        //Vブランキング期間
                        enableFrame = false;
                        // Vブランキング期間カウント判定
                        if (enableVBlank == true)
                        {
                            // カウント停止
                            enableVBlank = false;
                            // カウントUp
                            vertical++;
                        }
                    }
                    else
                    {
                        //VHブランキング期間

                        // 2005.11.2 V3.31 VHハイ状態からいきなりVHロー状態になった時の対処
                        //Hブランキング期間(VSYNC Active)
                        if (enableLine == true)
                        {
                            //有効ラインカウントアップ
                            lineCount++;
                        }

                        // Vブランキング区間カウント
                        if (enableVBlank == false)
                        {
                            // カウント開始フラグOn
                            enableVBlank = true;
                        }

                        // フレーム終了判定
                        if (enableFrame == true)
                        {
                            mBitmap.SetImage(pixels);
                            break;
                        }
                        enableFrame = false;
                    }
                }
            }
            return (mBitmap.Handle);
        }

        /// <summary>
        /// CANデータの取得
        /// </summary>
        /// <param name="frame_no">フレームデータ</param>
        /// <param name="can_no">フレームデータ</param>
        /// <param name="can_record">CANレコード</param>
        public bool GetCanData(int frame_no, int can_no, List<SvCanRecord> can_record)
        {
            can_record.Clear();
            if (frame_no >= 0 && frame_no <= (m_frames.Count-1))
            {
                if (can_no == 0)
                {
                    can_record.AddRange(m_frames[frame_no].mCan0Record);
                }
                else if (can_no == 1)
                {
                    can_record.AddRange(m_frames[frame_no].mCan1Record);
                }
            }
            return (true);
        }

        /// <summary>
        /// 指定されたデータを取得する
        /// </summary>
        /// <param name="pos">Seek位置(2バイト単位)</param>
        /// <param name="buf">取得データ</param>
        public void GetData(long pos, ushort[] buf)
        {
            try
            {
                lock (m_frames)
                {
                    // ヘッダポジション移動
                    DataSeek(pos);

                    for (int i = 0; i < buf.Length; i++)
                    {
                        buf[i] = DataRead();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// ダンプデータの取得（バイト)
        /// </summary>
        /// <param name="frame_no">フレーム番号(0～N)</param>
        /// <returns>バッファ</returns>
        public byte[] GetDumpData(int frame_no)
        {
            int size = (int)(m_frames[frame_no].EPos - m_frames[frame_no].SPos);
            byte[] buf = new byte[size];
            m_file.Seek(m_frames[frame_no].SPos + FILE_OFFSET);
            m_file.Read(buf, size);

            return (buf);
        }

        /// <summary>
        /// 指定された位置のフレーム番号を検索する
        /// </summary>
        /// <param name="pos">フレーム位置</param>
        /// <returns>フレーム番号</returns>
        int GetPos2FrameNo(long pos)
        {
            int i;
            for (i = 0; i < m_frames.Count; i++)
            {
                if (pos >= m_frames[i].SPos) break;
            }
            for (; i < m_frames.Count; i++)
            {
                if (pos < m_frames[i].EPos)
                {
                    return i+1;
                }
            }
            return (0);
        }



        /// <summary>
        /// 次SyncDataの検索
        /// </summary>
        /// <param name="pos">Seek位置(2バイト単位)</param>
        /// <param name="mask">マスク</param>
        /// <param name="active"アクティブ></param>
        /// <returns></returns>
        public long GetNextSyncData(long pos, ushort mask, ushort active, out int frame_no)
        {
            long ret = -1;
            bool blankingFlag = false;      // Blanking区間が発見されたかどうか
            frame_no = -1;

            try
            {
                pos *= 2;
                DataSeek(pos);
                for (long i = pos; i < (long)m_header.num_of_scan; i += 2)
                {
                    ushort data = DataRead();
                    if ((data & mask) == active)
                    {
                        if (blankingFlag)
                        {
                            frame_no = GetPos2FrameNo(i);
                            return (i/2);
                        }
                    }
                    else
                    {
                        // 最初からブランキングの可能性もある(2020/09/25)
                        // if (i != pos)
                        {
                            blankingFlag = true;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return (ret);
        }

        /// <summary>
        /// 前のSyncDataの検索
        /// </summary>
        /// <param name="pos">Seek位置(2バイト単位)</param>
        /// <param name="mask">マスク</param>
        /// <param name="active"アクティブ></param>
        /// <returns></returns>
        public long GetPrevSyncData(long pos, ushort mask, ushort active, out int frame_no)
        {
            long ret = -1;
            bool blankingFlag = false;      // Blanking区間が発見されたかどうか
            ushort data;
            frame_no = -1;

            try
            {
                pos *= 2;
                for (long i = pos - 2; i >= 0; i -= 2)
                {
                    if (m_file.ReadShort(i + FILE_OFFSET, out data) == 0)
                    {
                        break;
                    }
                    if ((data & mask) == active)
                    {
                        if (blankingFlag)
                        {
                            frame_no = GetPos2FrameNo(i);
                            return (i/2);
                        }
                    }
                    else
                    {
                        // 最初からブランキングの可能性もある(2020/09/25)
                        // if (i != pos)
                        {
                            blankingFlag = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return (ret);
        }


        /// <summary>
        /// ファイルポインタ移動
        /// </summary>
        /// <param name="offset">バイト位置</param>
        void DataSeek(long offset)
        {
            mReadCounts = 0;
            mReadEOF = false;
            m_file.Seek(offset + FILE_OFFSET);
            mReadSize = m_file.Read(mReadCache, READ_CAHCHE_SIZE);
            if (mReadSize < READ_CAHCHE_SIZE)
            {
                mReadEOF = true;
            }
        }

        /// <summary>
        /// データ読み込み
        /// </summary>
        /// <returns>値</returns>
        UInt16 DataRead()
        {
            // 
            if (mReadCounts >= mReadSize)
            {
                // データ無しは0返却
                if (mReadEOF)
                {
                    return (0);
                }

                mReadCounts = 0;
                mReadSize = m_file.Read(mReadCache, READ_CAHCHE_SIZE);
                if (mReadSize < READ_CAHCHE_SIZE)
                {
                    mReadEOF = true;
                }
                if (mReadSize == 0)
                {
                    return (0);
                }
            }
            UInt16 ret = (UInt16)((mReadCache[mReadCounts + 1] << 8) | mReadCache[mReadCounts]);
            mReadCounts += 2;
            return (ret);
        }

        /// <summary>
        /// 進捗用コールバック
        /// </summary>
        /// <param name="pos">進捗</param>
        public delegate void ProgressPos(int pos);

        /// <summary>
        /// ファイルのコピー
        /// </summary>
        /// <param name="file_name">書込みファイル名</param>
        /// <param name="start_frame">開始フレーム</param>
        /// <param name="end_frame">終了フレーム</param>
        /// <param name="func">関数</param>
        /// <param name="error_msg">エラーメッセージ</param>
        /// <returns></returns>
        public bool Clip2File(string file_name, int start_frame, int end_frame, ProgressPos func, out string error_msg)
        {
            error_msg = string.Empty;
            try
            {
                long total_size = m_frames[end_frame].EPos - m_frames[start_frame].SPos + 4;

                // ファイル位置移動
                m_file.Seek(m_frames[start_frame].SPos + FILE_OFFSET);

                using (System.IO.BinaryWriter binaryWriter = new System.IO.BinaryWriter(new System.IO.FileStream(file_name, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None)))
                {

                    SvCanHeader head = m_header.Clone() as SvCanHeader;

                    head.num_of_scan = (ulong)total_size;
                    head.SetStartDateTime();

                    // ヘッダ書き込み
                    {
                        Int32 size = Marshal.SizeOf(typeof(SvCanHeader));
                        byte[] by = new byte[size];
                        IntPtr ptr = Marshal.AllocHGlobal(size);

                        //マネージオブジェクトからアンマネージメモリにデータをマーシャリング
                        Marshal.StructureToPtr(head, ptr, false);

                        //アンマネージデータをマネージのbyte[]にコピーする
                        Marshal.Copy(ptr, by, 0, size);
                        Marshal.FreeHGlobal(ptr);

                        binaryWriter.Write(by);

                        // 先頭のVSYNC/HSYNCをLowにする(2020/09/25)
                        by = new byte[2];
                        by[0] = 0x00;
                        by[1] = 0xc0;
                        binaryWriter.Write(by);
                    }

                    int block_size = 1024 * 1024;
                    int block_count = (int)((total_size - 1) / block_size + 1);

                    for (int i = 0; i < block_count; i++)
                    {
                        // どちらか小さい容量を読み込み
                        int size = (int)Math.Min(total_size, block_size);
                        if (size == 0)
                        {
                            break;
                        }

                        byte[] buf = new byte[size];
                        m_file.Read(buf, size);

                        binaryWriter.Write(buf);

                        // 更新
                        if (func != null)
                        {
                            func((i * 100) / block_count);
                        }

                        total_size -= block_size;
                    }
                    if (func != null)
                    {
                        func(100);
                    }

                    // 最後のデータを書き直し 
                    using (System.IO.BinaryReader read = new System.IO.BinaryReader(binaryWriter.BaseStream))
                    {

                        long pos;
                        int size;
                        if (binaryWriter.BaseStream.Length > (long)( 256 + 64 * 1024 * 1024))
                        {
                            size = 64 * 1024 * 1024;
                        }
                        else
                        {
                            size = (int)(binaryWriter.BaseStream.Length - 256);
                        }
                        pos = binaryWriter.BaseStream.Length - size;
                        read.BaseStream.Position = pos;
                        byte [] by = read.ReadBytes(size);
                        by = SetLastVsync(by);
                        binaryWriter.BaseStream.Position = pos;
                        binaryWriter.Write(by);
                    }
                }

            }
            catch (Exception ex)
            {
                error_msg = ex.Message;
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// 最後のVSYNCを残す
        /// </summary>
        /// <param name="buf"></param>
        /// <returns></returns>
        byte[] SetLastVsync(byte[] buf)
        {
            ushort wordSyncActive_V = m_header.VSyncActiveMask;
            ushort wordSyncActive_H = m_header.HSyncActiveMask;
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);
            // 後ろから有効フレームの検出
            int i;
            for (i = buf.Length; i >= 2; i -= 2)
            {
                ushort data = (ushort)((buf[i - 1] << 8) | buf[i - 2]);

                if ((data & SvCanBitMask.SYNCACTIVEMASK_VH) == 0)
                {
                    break;
                }
            }
            //　ブロックすべてならスキップ
            if (i <= 0)
            {
                return (null);
            }

            // 後方を全て0xC000で埋める
            for (; i < buf.Length; i += 2)
            {
                buf[i] = 0x00;
                buf[i + 1] = 0xc0;
            }
            return (buf);
        }

        /// <summary>
        /// データクローン(ファイル再オープン等)
        /// </summary>
        /// <returns>クローンデータ</returns>
        public SvCanData Clone()
        {
            SvCanData data = new SvCanData();
            /// ファイルストリーム
            data.mFileName = mFileName;
            data.m_file = new Win32File();
            data.m_file.Open(mFileName);
            /// ヘッダ情報
            data.m_header = m_header;
            /// フレームバッファ
            data.m_frames = m_frames;
            data.mBitmapReverse = mBitmapReverse;

            return (data);
        }

        /// <summary>
        /// フレームの最初のポジションを取得
        /// </summary>
        /// <param name="frame_no">フレーム番号(0～)</param>
        /// <returns>オフセット位置</returns>
        public long GetFrameFirstPos(int frame_no)
        {
            return (m_frames[frame_no].SPos);
        }

        /// <summary>
        /// ヘッダ以降のトップへファイルポインタを戻す
        /// </summary>
        public void SeekToTop()
        {
            if (this.svCanListData != null)
            {
                this.svCanListData.Open();
            }
            else
            { 
                m_file.Seek(FILE_OFFSET);
            }
        }

        /// <summary>
        /// ブロックファイルの読込
        /// </summary>
        /// <param name="buffer">送信先</param>
        /// <param name="count">読込バイト数</param>
        /// <returns>読み込んだバイト数</returns>
        public int ReadBlock(byte[] buffer, int count)
        {
            int ret;
            if (this.svCanListData != null)
            {
                // リストファイルからの読み込み(リピート無し)
                SvCanListData.READSTATUS sts = this.svCanListData.ReadBlock(buffer, count, false);
                if (sts == SvCanListData.READSTATUS.EOF)
                {
                    // 終了
                    ret = 0;
                }
                if (sts == SvCanListData.READSTATUS.ERROR)
                {
                    // I/Oエラー
                    ret = -1;
                }
                else
                {
                    ret = count;
                }
            }
            else
            {
                ret = m_file.Read(buffer, count);
            }
            return (ret);
        }


        /// <summary>
        /// 指定されたフレーム・座標位置のポジションを取得する
        /// </summary>
        /// <param name="frame_no">フレームデータ</param>
        /// <param name="pt">座標データ</param>
        /// <param name="dump_pos">ダンプ位置</param>
        /// <returns>シーク位置</returns>
        public long GetFrameDataPos(int frame_no, Point pt, out int dump_pos)
        {
            lock (m_frames)
            {
                ushort wordSyncActive_V;
                ushort wordSyncActive_H;

                bool enableFrame = false;
                bool enableLine = false;
                int pixcelCount = 0;
                int lineCount = 0;
                int frameCount = 0;                         // フレームカウンター
                int vertical = 0;                           // Vブランキング
                int horizontal = 0;                         // Hブランキング
                bool enableVBlank = false;                  // Vブランキング期間判定

                GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
                ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);


                dump_pos = 0;

                // 反転OFF
                if (mBitmapReverse == false)
                {
                    pt.Y = m_frames[frame_no].Height - pt.Y - 1;
                }

                // データ先頭位置へ
                DataSeek(m_frames[frame_no].SPos);

                // 解析
                for (long i = m_frames[frame_no].SPos; i <= m_frames[frame_no].EPos; i += 2)
                {
                    UInt16 wordCurrent = DataRead();
                    if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                    {
                        if (enableFrame == false)
                        {
                            //有効フレーム開始
                            enableFrame = true;
                            //画素数カウント初期化
                            pixcelCount = 0;
                            //有効ラインカウント初期化
                            lineCount = 0;
                            // フレームカウンタインクリメント
                            frameCount++;
                        }

                        if (enableLine == false)
                        {
                            //ライン有効
                            enableLine = true;
                        }

                        // 座標チェック
                        if ((pixcelCount% m_frames[frame_no].Width) == pt.X)
                        {
                            if (lineCount == pt.Y)
                            {
                                dump_pos = (int)(i - m_frames[frame_no].SPos);
                                return (i);
                            }
                        }
                        pixcelCount++;
                    }
                    else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_V) == wordSyncActive_V) //VSYNCのみ Active
                    {
                        //Hブランキング期間(VSYNC Active)
                        if (enableLine == true)
                        {
                            enableLine = false;
                            //有効ラインカウントアップ

                            lineCount++;
                            // ピクセル配列のインデックスを調整
                            pixcelCount = m_frames[frame_no].Width * lineCount;
                        }
                        // 無効期間
                        horizontal++;
                    }
                    else if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_H) == wordSyncActive_H) //HSYNCのみ Active
                    {
                        //Vブランキング期間
                        enableFrame = false;
                        // Vブランキング期間カウント判定
                        if (enableVBlank == true)
                        {
                            // カウント停止
                            enableVBlank = false;
                            // カウントUp
                            vertical++;
                        }
                    }
                    else
                    {
                        //VHブランキング期間

                        // 2005.11.2 V3.31 VHハイ状態からいきなりVHロー状態になった時の対処
                        //Hブランキング期間(VSYNC Active)
                        if (enableLine == true)
                        {
                            //有効ラインカウントアップ
                            lineCount++;
                        }

                        // Vブランキング区間カウント
                        if (enableVBlank == false)
                        {
                            // カウント開始フラグOn
                            enableVBlank = true;
                        }
                    }
                }
            }
            return (0);
        }
    }

    /// <summary>
    /// win32 ファイルI/O
    /// </summary>
    class Win32File
    {
        /// <summary>
        /// ハンドル
        /// </summary>
        IntPtr handle = IntPtr.Zero;

        /// <summary>
        /// ファイルを開く(共有モード)
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns>ファイルオープン有無(true:成功 false:失敗)</returns>
        public bool Open(string file_name)
        {
            if (handle != IntPtr.Zero)
            {
                Win32.User32.CloseHandle(handle);
                handle = IntPtr.Zero;
            }

            handle = Win32.User32.CreateFile(file_name,
                                                Win32.User32.FileAccess.GenericRead,
                                                Win32.User32.FileShare.Read,
                                                IntPtr.Zero,
                                                Win32.User32.FileMode.OpenExisting,
                                                Win32.User32.FileAttributes.Normal|Win32.User32.FileAttributes.RandomAccess,
                                                IntPtr.Zero);
            if ( (Int64)handle == -1)
            {
                handle = IntPtr.Zero;
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// ファイルポインタ移動(先頭より)
        /// </summary>
        /// <param name="seek">シーク位置(バイト)</param>
        /// <returns></returns>
        public void Seek(long seek)
        {
            int high = (int)(seek >> 32);
            int low  = (int)(seek & 0xffffffff);
            unsafe
            {
                Win32.User32.SetFilePointer(handle, low, &high, Win32.User32.MoveMethod.FILE_BEGIN);
            }
        }

        /// <summary>
        /// ファイル読込
        /// </summary>
        /// <param name="buffer">読込バッファ</param>
        /// <param name="count">読込サイズ</param>
        /// <returns>カウンタ</returns>
        public unsafe int Read(byte[] buffer, int count)
        {
            int n = 0;
            fixed (byte* p = buffer)
            {
                if (!Win32.User32.ReadFile(handle, p, count, &n, 0))
                {
                    return 0;
                }
            }
            return n;
        }

        /// <summary>
        /// 指定された位置のデータを読み込む(Prev用)
        /// </summary>
        /// <param name="offset">バイトオフセット</param>
        /// <param name="data">読込データ</param>
        /// <returns>0:エラー N:読み込んだバイト数</returns>
        public unsafe int ReadShort(long offset, out ushort data)
        {
            Seek(offset);
            int n = 0;
            fixed ( ushort *p = &data )
            {
                if (!Win32.User32.ReadFile(handle, p, 2, &n, 0))
                {
                    return 0;
                }
            }
            return (n);
        }

        /// <summary>
        /// クローズ処理
        /// </summary>
        public void Close()
        {
            if (handle != IntPtr.Zero)
            {
                Win32.User32.CloseHandle(handle);
                handle = IntPtr.Zero;
            }
        }
    }

}
