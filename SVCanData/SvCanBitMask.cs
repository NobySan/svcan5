﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVCanData
{
    /// <summary>
    /// BitMask
    /// </summary>
    static public class SvCanBitMask
    {
        /// <summary>
        /// VSyncビットマスク
        /// </summary>
        static public ushort SYNCACTIVEMASK_V = 0x1000;
        /// <summary>
        /// HSyncビットマスク
        /// </summary>
        static public ushort SYNCACTIVEMASK_H = 0x2000;
        /// <summary>
        /// VHSyncビットマスク
        /// </summary>
        static public ushort SYNCACTIVEMASK_VH = (ushort)(SYNCACTIVEMASK_V | SYNCACTIVEMASK_H);
        /// <summary>
        /// CAN0 ビットマスク
        /// </summary>
        static public ushort CAN0_MASK = 0x4000;
        /// <summary>
        /// CAN1 ビットマスク
        /// </summary>
        static public ushort CAN1_MASK = 0x8000;
        /// <summary>
        /// EOFビット(ACK(1)+EOF(7)+ITM(3)
        /// </summary>
        static public ushort CAN_EOF_BITS = 11;
    }
}
