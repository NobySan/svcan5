﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using candec;

namespace SVCanData
{
    /// <summary>
    /// 1フレームデータサイズ
    /// </summary>
    public class SVCanFrameData : EventArgs
    {
        public int width;
        public int height;
        public List<byte> buffer;
    }

    /// <summary>
    /// SVCanレコーディングデータ
    /// </summary>
    public class SVCanRecData
    {
        /// <summary>
        /// ヘッダ情報
        /// </summary>
        SvCanHeader mSvCanHeader = new SvCanHeader();
        /// <summary>
        /// バイナリファイルの書き込み
        /// </summary>
        System.IO.BinaryWriter mBinaryWriter = null;
        /// <summary>
        /// キューバッファを作成する
        /// </summary>
        BlockingCollection<byte[]> mQueBuffer = new BlockingCollection<byte[]>();

        /// <summary>
        /// 書込み終了フラグのチェック
        /// </summary>
        volatile bool mWriteEnd = false;
        /// <summary>
        /// ロック処理
        /// </summary>
        object mWriteLock = new object();

        /// <summary>
        /// CANの閾値
        /// </summary>
        private int[] CanThreshold { get; set; } = new int[2] { 1, 1};

        /// <summary>
        /// CAN BPS
        /// </summary>
        private int[] Canbps { get; set; } = new int[2] { 500, 2000 };

        /// <summary>
        /// ブロックサイズ
        /// </summary>
        private const int BLOCK_SIZE = (64 * 1024 * 1024);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SVCanRecData()
        {
        }

        /// <summary>
        /// レコーディング用のファイルを開き、ヘッダを仮書込みする
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <param name="threshold">CAN解析用のthreshold1,threshold2</param>
        /// <param name="bps">CAN解析用のBPS,FD-BPS</param>
        /// <returns></returns>
        public bool Open(string file_name, int[] threshold, int[] bps)
        {
            bool ret = false;
            try
            {
                // ファイルを閉じる
                Close();

                mBinaryWriter = new System.IO.BinaryWriter(new System.IO.FileStream(file_name, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite));
                if (mBinaryWriter == null)
                {
                    return (ret);
                }

                Array.Copy(threshold, this.CanThreshold, 2);
                Array.Copy(bps, this.Canbps, 2);

                // ヘッダ書き込み
                mSvCanHeader.SetStartDateTime();

                Int32 size = Marshal.SizeOf(typeof(SVCanData.SvCanHeader));
                byte[] by = new byte[size];
                IntPtr ptr = Marshal.AllocHGlobal(size);

                //マネージオブジェクトからアンマネージメモリにデータをマーシャリング
                Marshal.StructureToPtr(mSvCanHeader, ptr, false);

                //アンマネージデータをマネージのbyte[]にコピーする
                Marshal.Copy(ptr, by, 0, size);
                Marshal.FreeHGlobal(ptr);

                mBinaryWriter.Write(by);

                mWriteEnd = false;

                // 書込み用スレッドの起動
                System.Threading.Tasks.Task.Run(() =>
                {
#if STOPW
                    var sw = new System.Diagnostics.Stopwatch();
#endif
                    // ロック処理
                    lock(mWriteLock)
                    {
                        try
                        {
                            do
                            {
                                byte[] buf;
                                // バッファが空の場合はWait
                                if ( !mQueBuffer.TryTake(out buf, 1 ))
                                {
                                    if (mWriteEnd)
                                    {
                                        return;
                                    }
                                    continue;
                                }

#if STOPW
                                sw.Reset();
                                sw.Start();
#endif
                                mBinaryWriter.Write(buf);

                                // バッファサイズ更新
                                mSvCanHeader.num_of_scan += (ulong)buf.Length;
#if STOPW
                                sw.Stop();
                                Console.WriteLine("mBinaryWriter.Write[{0}] {1} msec", buf.Length, sw.ElapsedMilliseconds);
#endif
                                buf = null;

                            } while (true);
                        }
                        catch
                        {
                        }
                    }

                });

                ret = true;
            }
            catch (Exception)
            {

            }
            return (ret);
        }


        /// <summary>
        /// ヘッダファイル更新
        /// </summary>
        public void UpdateHeader()
        {
            if (mBinaryWriter != null)
            {
                try
                {
                    // 全バッファの書込み待ち
                    mWriteEnd = true;

                    // 修了待ち(WriteLockが外れるのを待つ)
                    lock (mWriteLock)
                    {

                        // 先頭位置へ
                        mBinaryWriter.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);

                        // ヘッダのみ再書き込み
                        Int32 size = Marshal.SizeOf(typeof(SVCanData.SvCanHeader));
                        byte[] by = new byte[size];
                        IntPtr ptr = Marshal.AllocHGlobal(size);

                        //マネージオブジェクトからアンマネージメモリにデータをマーシャリング
                        Marshal.StructureToPtr(mSvCanHeader, ptr, false);

                        //アンマネージデータをマネージのbyte[]にコピーする
                        Marshal.Copy(ptr, by, 0, size);
                        Marshal.FreeHGlobal(ptr);

                        mBinaryWriter.Write(by);

                        // 最後のデータを書き直し 
                        using (System.IO.BinaryReader read = new System.IO.BinaryReader(mBinaryWriter.BaseStream))
                        {
                            long pos = mBinaryWriter.BaseStream.Length - BLOCK_SIZE;
                            read.BaseStream.Position = pos;
                            by = read.ReadBytes(BLOCK_SIZE);
                            by = SetLastVsync(by);
                            mBinaryWriter.BaseStream.Position = pos;
                            mBinaryWriter.Write(by);

                            // CANの調整
                            // ヘッダをスキップ
                            read.BaseStream.Position = size;
                            by = read.ReadBytes(BLOCK_SIZE);
                            // 先頭データへCAN IDLEを挿入
                            by = this.SetIDLECanData(by);
                            // ヘッダをスキップ
                            mBinaryWriter.BaseStream.Position = size;
                            mBinaryWriter.Write(by);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        /// <summary>
        /// ファイル書き込み
        /// </summary>
        /// <param name="buffer">バッファ</param>
        /// <param name="pos">書込み位置</param>
        /// <param name="size"></param>
        public void Write(byte[] buffer)
        {
            // QUEへ書き込み
            mQueBuffer.TryAdd(buffer);
        }

        /// <summary>
        /// ファイルを閉じる
        /// </summary>
        public void Close()
        {
            if (mBinaryWriter != null)
            {
                mBinaryWriter.Close();
                mBinaryWriter.Dispose();
                mBinaryWriter = null;
            }
        }

        /// <summary>
        /// FPS値の設定
        /// </summary>
        public int FPS
        {
            set
            {
                mSvCanHeader.frame_rate = value;
            }
        }

        /// <summary>
        /// widthの設定
        /// </summary>
        public int Width
        {
            set
            {
                mSvCanHeader.picture_width= value;
            }
        }

        /// <summary>
        /// heightの設定
        /// </summary>
        public int Height
        {
            set
            {
                mSvCanHeader.picture_height= value;
            }
        }

        /// <summary>
        /// ロストフレームカウント
        /// </summary>
        public int LostFrameCnt
        {
            set
            {
                mSvCanHeader.lost_frame_cnt = value;
            }
        }

        /// <summary>
        /// VSync 信号の極性(0 Low, 1 High)
        /// </summary>
        public int VsyncPorarity
        {
            set
            {
                mSvCanHeader.vsync_porarity = value;
            }
        }

        /// <summary>
        /// HSync 信号の極性(0 Low, 1 High)
        /// </summary>
        public int HsyncPorarity
        {
            set
            {
                mSvCanHeader.hsync_porarity = value;
            }
        }

        /// <summary>
        /// ファームウェアのバージョン番号(100 を掛けた値) 1 00 の場合 100
        /// </summary>
        public int VersionOfFirmware
        {
            set
            {
                mSvCanHeader.version_of_firmware = value;
            }
        }

        /// <summary>
        /// ハードウェアのバージョン番号(100 を掛けた値) 1 21 の場合 121
        /// </summary>
        public int VersionOfHardware
        {
            set
            {
                mSvCanHeader.version_of_hardware = value;
            }
        }

        /// <summary>
        /// 最後のVSYNCを残す
        /// </summary>
        /// <param name="buf">バッファ</param>
        /// <returns>間引いたバッファ</returns>
        byte [] SetLastVsync(byte [] buf)
        {
            List<long> can0 = new List<long>();
            List<long> can1 = new List<long>();

            long[] can0_packet = new long[32767];
            long[] can1_packet = new long[32767];
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            Candec can_codec = new Candec();

            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            can_codec.Can_Start(this.CanThreshold[0], this.CanThreshold[1]);
            can_codec.Can_set_bitrate(0, this.Canbps[0], this.Canbps[1]);
            can_codec.Can_set_bitrate(1, this.Canbps[0], this.Canbps[1]);

            // 取得ループ
            int vsync_pos = -1;
            ushort wordCurrent;


            // 後方からVSYNCの位置を検索
            for (int i = buf.Length - 2; i >= 0; i -= 2)
            {
                wordCurrent = (ushort)((buf[i + 1] << 8) | buf[i]);

                // 無効画素
                if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == 0)
                {
                    for (; i >= 0; i -= 2)
                    {
                        wordCurrent = (ushort)((buf[i + 1] << 8) | buf[i]);
                        if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                        {
                            vsync_pos = i;
                            break;
                        }
                    }
                    break;
                }
            }

            for (int i = 0; i < buf.Length; i += 2)
            {
                wordCurrent = (ushort)((buf[i + 1] << 8) | buf[i]);

                int ret = can_codec.Can_serch_data(0, wordCurrent, ref can0_packet, ref can1_packet);
                if (ret == 0)
                {
                    // 未処理
                }
                else if (ret == 1)
                {
                    // データ未取得
                }
                else
                {
                    // CAN0データの取得
                    if (can0_packet[0] != -1)
                    {
                        // 先頭のレコード値取得
                        can0.Add(can0_packet[0]);
                    }
                    if (can1_packet[0] != -1)
                    {
                        // 先頭のレコード値取得
                        can1.Add(can1_packet[0]);
                    }
                }
            }



            do
            {
                // VSYNCが見つからない場合は全てIDLE
                if (vsync_pos == -1)
                {
                    for (int i = 0; i < buf.Length; i += 2)
                    {
                        buf[i + 1] |= (0x80 | 0x40);
                    }
                    break;
                }

                int first_pos = vsync_pos + 2;
                for (int i = 0; i < can0.Count; i++)
                {
                    if (can0[i] > vsync_pos)
                    {
                        if (i != 0)
                        {
                            first_pos = (int)(can0[i - 1] - 2);
                        }
                        else
                        {
                            first_pos = 0;
                        }
                        break;
                    }
                }

                // CAN0のIDLE出力
                for (int i = first_pos; i < buf.Length; i += 2)
                {
                    buf[i + 1] |= 0x40;
                }

                first_pos = vsync_pos + 2;
                for (int i = 0; i < can1.Count; i++)
                {
                    if (can1[i] > vsync_pos)
                    {
                        if (i != 0)
                        {
                            first_pos = (int)(can1[i - 1] - 2);
                        }
                        else
                        {
                            first_pos = 0;
                        }
                        break;
                    }
                }

                // CAN1のIDLE出力
                for (int i = first_pos; i < buf.Length; i += 2)
                {
                    buf[i + 1] |= 0x80;
                }

            } while (false);

            return buf;
        }


        /// <summary>
        /// 指定されたバッファの先頭からみた中途半端なCANデータを除外する
        /// </summary>
        /// <param name="buf">バッファ</param>
        /// <returns></returns>
        private byte [] SetIDLECanData(byte[] buf)
        {
            List<long> can0 = new List<long>();
            List<long> can1 = new List<long>();

            long[] can0_packet = new long[32767];
            long[] can1_packet = new long[32767];
            ushort wordSyncActive_V;
            ushort wordSyncActive_H;

            Candec can_codec = new Candec();

            GetActiveAndBlankBit(out wordSyncActive_V, out wordSyncActive_H);
            ushort wordSyncActive_VH = (ushort)(wordSyncActive_V | wordSyncActive_H);

            can_codec.Can_Start(this.CanThreshold[0], this.CanThreshold[1]);
            can_codec.Can_set_bitrate(0, this.Canbps[0], this.Canbps[1]);
            can_codec.Can_set_bitrate(1, this.Canbps[0], this.Canbps[1]);

            // 取得ループ
            int vsync_pos = -1;
            for( int i=0; i < buf.Length; i += 2)
            {
                ushort wordCurrent = (ushort)((buf[i+1] << 8) | buf[i]);

                // 最初のVSYNC位置を検索
                if (vsync_pos == -1)
                {
                    if ((wordCurrent & SvCanBitMask.SYNCACTIVEMASK_VH) == wordSyncActive_VH) //有効画素
                    {
                        vsync_pos = i;
                    }
                }
                int ret = can_codec.Can_serch_data(0, wordCurrent, ref can0_packet, ref can1_packet);
                if (ret == 0)
                {

                }
                else if (ret == 1)
                {
                    // データ未取得
                }
                else
                {
                    // CAN0データの取得
                    if (can0_packet[0] != -1)
                    {
                        // 先頭のレコード値取得
                        can0.Add(can0_packet[0]);
                    }
                    if (can1_packet[0] != -1)
                    {
                        // 先頭のレコード値取得
                        can1.Add(can1_packet[0]);
                    }
                }
            }

            do
            {
                // VSYNCが検索できないのでCANデータをIDLEにして返却
                if (vsync_pos == -1)
                {
                    for (int i = 0; i < buf.Length; i += 2)
                    {
                        buf[i + 1] |= 0xC0;
                    }
                    break;
                }

                int last_pos = buf.Length;
                for ( int i=0; i < can0.Count; i++)
                {
                    if (can0[i] > vsync_pos)
                    {
                        last_pos = (int)(can0[i] - 2);
                        break;
                    }
                }

                // CAN0のIDLE出力
                for (int i = 0; i < last_pos; i += 2)
                {
                    buf[i + 1] |= 0x40;
                }

                last_pos = buf.Length;
                for (int i = 0; i < can1.Count; i++)
                {
                    if (can1[i] > vsync_pos)
                    {
                        last_pos = (int)(can1[i] - 2);
                        break;
                    }
                }

                // CAN1のIDLE出力
                for (int i = 0; i < last_pos; i += 2)
                {
                    buf[i + 1] |= 0x80;
                }

            } while (false);

            return buf;
        }

        /// <summary>
        /// ブランクビットマスクの取得
        /// </summary>
        /// <param name="wordActiveV">アクティブV</param>
        /// <param name="wordActiveH">アクティブH</param>
        private void GetActiveAndBlankBit(out ushort wordActiveV, out ushort wordActiveH)
        {
            if (this.mSvCanHeader.vsync_porarity == 0)
            {
                wordActiveV = SvCanBitMask.SYNCACTIVEMASK_V;
            }
            else
            {
                wordActiveV = 0;
            }
            if (this.mSvCanHeader.hsync_porarity == 0)
            {
                wordActiveH = SvCanBitMask.SYNCACTIVEMASK_H;
            }
            else
            {
                wordActiveH = 0;
            }
        }
    }
}
