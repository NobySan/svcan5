﻿namespace SVCanData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Listで指定された.datファイルを確認する
    /// </summary>
    public class SvCanListData
    {
        /// <summary>
        /// .datファイル
        /// </summary>
        private List<string> datFiles = new List<string>();

        /// <summary>
        /// テキスト情報
        /// </summary>
        private List<TextInfo> textInfos = new List<TextInfo>();

        /// <summary>
        /// ヘッダ情報
        /// </summary>
        private SvCanHeader svCanHeader = new SvCanHeader();

        /// <summary>
        /// 読込ファイル
        /// </summary>
        private System.IO.BinaryReader readFile = null;

        /// <summary>
        /// ファイル読込位置
        /// </summary>
        private int filePos = 0;

        /// <summary>
        /// ブロックサイズ(32MB)
        /// </summary>
        private const int BLOCK_SIZE = 32 * 1024 * 1024;

        /// <summary>
        /// .txt情報
        /// </summary>
        struct TextInfo
        {
            /// <summary>
            /// フレーム数
            /// </summary>
            public int frames;
            /// <summary>
            /// .datファイルからのオフセット(終端)
            /// </summary>
            public Int64 offset;
        }

        /// <summary>
        /// エラー情報
        /// </summary>
        public enum STATUS : int
        { 
            /// <summary>
            /// 処理結果エラー無し
            /// </summary>
            OK=0,
            /// <summary>
            /// listが空
            /// </summary>
            LIST_EMPTY,
            /// <summary>
            /// .datファイルオープンエラー
            /// </summary>
            DAT_FILE_OPEN_ERROR,
            /// <summary>
            /// .txtファイルオープンエラー
            /// </summary>
            TEXT_FILE_OPEN_ERROR,
            /// <summary>
            /// .txtファイル解析エラー
            /// </summary>
            TEXT_FILE_ANALYZE_ERROR,
            /// <summary>
            /// プログラムエラー
            /// </summary>
            PROGRAM_ERROR,
        }

        /// <summary>
        /// 読込ステータス
        /// </summary>
        public enum READSTATUS
        {
            /// <summary>
            /// 次レコードあり
            /// </summary>
            OK = 0,
            /// <summary>
            /// エラー
            /// </summary>
            ERROR,
            /// <summary>
            /// EOF
            /// </summary>
            EOF,
        }

        /// <summary>
        /// エラーメッセージ
        /// </summary>
        public string ErrorMsg;

        /// <summary>
        /// 合計フレーム
        /// </summary>
        public int TotalFrames = 0;

        /// <summary>
        /// 合計サイズ
        /// </summary>
        public Int64 TotalBytes = 0;

        /// <summary>
        /// 画素幅
        /// </summary>
        public int Width
        {
            get
            {
                return this.svCanHeader.picture_width;
            }
        }

        /// <summary>
        /// 画素高さ
        /// </summary>
        public int Height
        {
            get
            {
                return this.svCanHeader.picture_height;
            }
        }

        /// <summary>
        /// フレームレート(300) -> (30.0)
        /// </summary>
        public int Fps
        {
            get
            {
                return this.svCanHeader.frame_rate;
            }
        }

        /// <summary>
        /// ヘッダ情報の取得
        /// </summary>
        public SvCanHeader Header
        {
            get
            {
                return this.svCanHeader;
            }
        }

        /// <summary>
        /// 指定された.datファイルに関連するファイルを設定する
        /// </summary>
        /// <param name="datFiles">.datファイルリスト</param>
        /// <returns>処理結果</returns>
        public STATUS SetFiles(List<string> datFiles)
        {
            this.TotalFrames = 0;
            this.TotalBytes = 0;

            if (datFiles.Count == 0)
            {
                this.ErrorMsg = "list data empty.";
                return STATUS.LIST_EMPTY;
            }

            try
            {
                // コピー
                this.datFiles = new List<string>(datFiles);

                // チェック
                for (int i=0; i < this.datFiles.Count; i++)
                {
                    // .datファイル
                    if (!System.IO.File.Exists(this.datFiles[i]))
                    {
                        this.ErrorMsg = string.Format("The specified file [{0}] does not exist.", this.datFiles[i]);
                        return STATUS.DAT_FILE_OPEN_ERROR;
                    }

                    // .txtファイル
                    string textFile = System.IO.Path.ChangeExtension(this.datFiles[i], ".txt");
                    TextInfo textInfo = new TextInfo();

                    if (!System.IO.File.Exists(textFile))
                    {
                        // フレーム解析する場合はこの処理を復活させる
#if false
                        SvCanDataFrame svCanDataFrame = new SvCanDataFrame();

                        if (!svCanDataFrame.Analyze(this.datFiles[i], out SvCanDataFrame.Info info))
                        {
                            this.ErrorMsg = string.Format("The specified file [{0}] error.\r\n{1}", this.datFiles[i], svCanDataFrame.ErrMessage);
                            return STATUS.TEXT_FILE_OPEN_ERROR;
                        }
                        textInfo.frames = info.FrameCounts;
                        textInfo.offset = info.EndPosition;
#else
                        this.ErrorMsg = string.Format("The specified file [{0}] error.", textFile);
                        return STATUS.TEXT_FILE_OPEN_ERROR;
#endif
                    }
                    else
                    {
                        // .dat -> .txt読込
                        STATUS sts = this.GetTextInfo(textFile, ref textInfo);
                        if (sts != STATUS.OK)
                        {
                            return sts;
                        }
                    }

                    // ファイル位置が超えた場合
                    var fileInfo = new System.IO.FileInfo(this.datFiles[i]);
                    if (textInfo.offset > fileInfo.Length)
                    {
                        this.ErrorMsg = string.Format("txt file offset error.\r\nfile:[{0}]", textFile);
                        return STATUS.TEXT_FILE_ANALYZE_ERROR;
                    }

                    // リストへ情報追加
                    this.textInfos.Add(textInfo);

                    // 合計フレーム数
                    this.TotalFrames += textInfo.frames;

                    // 合計バイト数
                    this.TotalBytes += (fileInfo.Length - Marshal.SizeOf(typeof(SvCanHeader)));
                }

                // ヘッダ情報読込
                // 先頭のファイルのみ取得
                using (var reader = new System.IO.BinaryReader(new System.IO.FileStream(this.datFiles[0], System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read)))
                {
                    byte[] buf = reader.ReadBytes(Marshal.SizeOf(typeof(SvCanHeader)));
                    this.svCanHeader = Byte2Struct<SvCanHeader>(buf);
                }
            }
            catch (Exception ex)
            {
                this.ErrorMsg = string.Format("Program Error : {0}", ex.Message);
                return STATUS.PROGRAM_ERROR;
            }

            return STATUS.OK;
        }

        /// <summary>
        /// byte配列から構造体へ変換
        /// </summary>
        /// <typeparam name="Type">型</typeparam>
        /// <param name="buffer">バイト配列</param>
        /// <returns>変換した型</returns>
        static Type Byte2Struct<Type>(byte[] buffer)
        {
            GCHandle gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            Type ret = (Type)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Type));
            gch.Free();
            return (ret);
        }

        /// <summary>
        /// テキスト情報の取得
        /// 1行目:フレーム数 2行目:オフセット
        /// </summary>
        /// <param name="textFile">.txtファイル</param>
        /// <param name="info">取得情報</param>
        /// <returns>処理結果</returns>
        private STATUS GetTextInfo(string textFile, ref TextInfo info)
        {
            try
            {
                // シフトJISで読む
                string[] texts = System.IO.File.ReadAllLines(textFile, System.Text.Encoding.GetEncoding("shift_jis"));

                if (texts.Length < 2)
                {
                    this.ErrorMsg = string.Format("There are no two rows of data.\r\nfile:[{0}]", textFile);
                    return STATUS.TEXT_FILE_ANALYZE_ERROR;
                }

                // フレーム数取得
                if (!int.TryParse(texts[0],out info.frames))
                {
                    this.ErrorMsg = string.Format("The number of frames is non-numeric data.\r\nfile:[{0}]", textFile);
                    return STATUS.TEXT_FILE_ANALYZE_ERROR;
                }

                // オフセット取得
                if (!Int64.TryParse(texts[1], out info.offset))
                {
                    this.ErrorMsg = string.Format("The number of offset is non-numeric data.\r\nfile:[{0}]", textFile);
                    return STATUS.TEXT_FILE_ANALYZE_ERROR;
                }

            }
            catch(Exception ex)
            {
                this.ErrorMsg = string.Format("An error occurred in the following file.\r\nfile:[{0}]\r\n{1}", textFile, ex.Message);
                return STATUS.TEXT_FILE_OPEN_ERROR;
            }
            return STATUS.OK;
        }

        /// <summary>
        /// バイナリファイルのオープン
        /// </summary>
        /// <param name="fileName">バイナリファイル名</param>
        /// <returns>バイナリリーダ</returns>
        private System.IO.BinaryReader OpenBinary(string fileName)
        {
            System.IO.BinaryReader binaryReader = null;

            try
            {
                binaryReader = new System.IO.BinaryReader(new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read));

                // ヘッダ分を読み飛ばし
                binaryReader.BaseStream.Position = Marshal.SizeOf(typeof(SvCanHeader));
            }
            catch (Exception)
            {
                return null;
            }
            return binaryReader;
        }

        /// <summary>
        /// ファイルを開く
        /// </summary>
        /// <returns>正常ならtrue、失敗ならfalse</returns>
        public bool Open()
        {
            try
            {
                // 開いているファイルを開く
                if (this.readFile != null)
                {
                    this.readFile.Close();
                    this.readFile = null;
                }

                // 先頭のファイルを開く
                this.filePos = 0;
                this.readFile = OpenBinary(this.datFiles[this.filePos]);
                if (this.readFile == null)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// クローズ
        /// </summary>
        public void Close()
        {
            if (this.readFile != null)
            {
                this.readFile.Close();
                this.readFile = null;
            }
        }

        /// <summary>
        /// ファイル読込
        /// </summary>
        /// <param name="buf">バッファ</param>
        /// <param name="readBlockSize">読込サイズ</param>
        /// <param name="repeate">リピート有無</param>
        /// <returns>処理結果</returns>
        public READSTATUS ReadBlock(byte[] buf, int readBlockSize, bool repeate)
        {
            try
            {
                long readSize = this.textInfos[this.filePos].offset - this.readFile.BaseStream.Position;
                if (!repeate)
                {
                    // 連続再生ではない場合、最後のサイズを調整しない
                    if ((this.filePos + 1) >= this.datFiles.Count)
                    {
                        readSize = this.readFile.BaseStream.Length - this.readFile.BaseStream.Position;
                    }
                }

                // ファイルが読込サイズより大きい場合
                if (readSize > readBlockSize)
                {
                    // 1ブロックダイレクト読込
                    this.readFile.Read(buf, 0, readBlockSize);
                }
                else
                {
                    // 読込データが存在しない場合
                    if (readSize <= 0)
                    {
                        return READSTATUS.EOF;
                    }
                    if (this.datFiles.Count != 1)
                    {
                        if ((this.filePos + 1) >= this.datFiles.Count)
                        {
                            for (int i = 0; i < readBlockSize; i += 2)
                            {
                                buf[i + 0] = 0x0;
                                buf[i + 1] = 0xc0;
                            }
                        }
                    }
                    this.readFile.Read(buf, 0, (int)readSize);

                    // ファイル数が１個の場合
                    if (this.datFiles.Count == 1)
                    {
                        if (repeate)
                        {
                            // ヘッダ分スキップ
                            this.readFile.BaseStream.Position = Marshal.SizeOf(typeof(SvCanHeader));
                        }
                        else
                        {
//                            Array.Resize(ref buf, (int)readSize);
                            return READSTATUS.OK;
                        }
                    }
                    else
                    {
                        // ファイル数が複数の場合
                        // ファイル位置が最後になった場合はポジションを0へ
                        if ((this.filePos+1) >= this.datFiles.Count)
                        {
                            if (repeate)
                            {
                                this.filePos = 0;
                            }
                            else
                            {
//                                Array.Resize(ref buf, (int)readSize);
                                return READSTATUS.OK;
                            }
                        }
                        else
                        {
                            this.filePos++;
                        }

                        this.Close();

                        // 次ファイルを開く
                        this.readFile = OpenBinary(this.datFiles[this.filePos]);
                    }

                    //　各ファイルは32MB以上のデータが存在すること
                    long remainSize = readBlockSize - readSize;
                    if (remainSize == 0) remainSize = readSize;
                    this.readFile.Read(buf, (int)readSize, (int)remainSize);
                }
            }
            catch(Exception ex)
            {
                this.ErrorMsg = string.Format("ReadBlock Program Error : {0}", ex.Message);
                return READSTATUS.ERROR;
            }

            return READSTATUS.OK;
        }
    }
}
