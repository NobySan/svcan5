﻿namespace SVCanData
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    /// <summary>
    /// HSyncデータI/O
    /// </summary>
    public class SvCanRecHSync : SVCanFileIO
    {
        #region フィールド

        /// <summary>
        /// 拡張子
        /// </summary>
        private const string ext = ".hsync";

        /// <summary>
        /// サイズ
        /// </summary>
        private int recordSize = Marshal.SizeOf(typeof(Record));

        /// <summary>
        /// レコード最大
        /// </summary>
        private Int32 recordMax;

        /// <summary>
        /// レコードデータ
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public class Record
        {
            /// <summary>
            /// 開始レコード
            /// </summary>
            public Int64 Start;
            /// <summary>
            /// 終了レコード
            /// </summary>
            public Int64 End;
            /// <summary>
            /// フレーム番号
            /// </summary>
            public Int32 FrameNo;
            /// <summary>
            /// ピクセル数
            /// </summary>
            public Int32 Pixel;
        }

        #endregion

        /// <summary>
        /// ファイル名を取得する
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static public string GetFileName(string file_name)
        {
            return System.IO.Path.ChangeExtension(file_name, ext);
        }

        #region 書き込み

        /// <summary>
        /// フレームファイルの書込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public override bool WriteOpen(string file_name)
        {
            // 書き込みファイルオープン
            if (!base.WriteOpen(GetFileName(file_name)))
            {
                return false;
            }
            this.recordMax = 0;
            return true;
        }

        #endregion


        #region 読込
        /// <summary>
        /// フレームファイルの読込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public override bool ReadOpen(string file_name)
        {
            // 書き込みファイルオープン
            if (!base.ReadOpen(GetFileName(file_name)))
            {
                return false;
            }

            // 総フレーム数の取得
            this.recordMax = (int)(this.binaryReader.BaseStream.Length / recordSize);

            return true;
        }

        /// <summary>
        /// 指定されたフレーム番号のレコードを取得する
        /// </summary>
        /// <param name="frame_no">フレーム番号</param>
        /// <returns>取得したレコード</returns>
        public List<Record> GetRecords(Int64 start, Int64 end)
        {
            Int64 low = 0;
            Int64 high = this.recordMax - 1;

            List<Record> ret = new List<Record>();

            while (low <= high)
            {
                // 中心位置
                Int64 im = (high + low) / 2;

                this.binaryReader.BaseStream.Position = (im * this.recordSize);
                Record rec = this.ReadFrom<Record>();

                // startが検索できたらendの範囲になるまで検索
                if (start >= rec.Start && start <= rec.End)
                {
                    ret.Add(rec);
                    im++;
                    for (; im < this.recordMax; im++)
                    {
                        rec = this.ReadFrom<Record>();
                        if (rec.Start > end)
                        {
                            break;
                        }
                        ret.Add(rec);
                    }
                    return ret;
                }

                if (rec.Start > start)
                {
                    high = im - 1;
                }
                else
                {
                    low = im + 1;
                }
            }
            return null;
        }

        #endregion
    }
}
