﻿
namespace SVCanData
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    /// <summary>
    /// SVCANファイルI/O
    /// </summary>
    public class SVCanFileIO
    {
        /// <summary>
        /// バイナリファイル書込
        /// </summary>
        protected BinaryWriter binaryWriter = null;

        /// <summary>
        /// バイナリファイル読込
        /// </summary>
        protected BinaryReader binaryReader = null;

        /// <summary>
        /// 指定されたファイルを書き込みオープンする
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public virtual bool WriteOpen(string file_name)
        {
            if (this.binaryWriter != null)
            {
                this.binaryWriter.Close();
                this.binaryWriter = null;
            }

            try
            {
                this.binaryWriter = new BinaryWriter(File.OpenWrite(file_name));
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 書き込みファイルを閉じる
        /// </summary>
        public void WriteClose()
        {
            if(this.binaryWriter != null)
            {
                this.binaryWriter.Close();
                this.binaryWriter = null;
            }
        }

        /// <summary>
        /// 指定されたファイルを読込オープンする
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public virtual bool ReadOpen(string file_name)
        {
            if (this.binaryReader != null)
            {
                this.binaryReader.Close();
                this.binaryReader = null;
            }

            try
            {
                this.binaryReader = new BinaryReader(File.OpenRead(file_name));
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 書き込みファイルを閉じる
        /// </summary>
        public void ReadClose()
        {
            if (this.binaryReader != null)
            {
                this.binaryReader.Close();
                this.binaryReader = null;
            }
        }

        /// <summary>
        /// アドレス変換
        /// </summary>
        /// <typeparam name="T">テンプレート</typeparam>
        /// <param name="obj">オブジェクト</param>
        /// <returns>アドレス</returns>
        static IntPtr ToPtr<T>(T obj)
        {
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(T)));
            Marshal.StructureToPtr(obj, ptr, false);
            return ptr;
        }

        /// <summary>
        /// 構造体へキャストする
        /// </summary>
        /// <typeparam name="T">テンプレート</typeparam>
        /// <param name="obj">オブジェクト</param>
        /// <returns>構造体</returns>
        static T ToStruct<T>(byte[] obj)
        {
            GCHandle gch = GCHandle.Alloc(obj, GCHandleType.Pinned);
            T ret = (T)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(T));
            gch.Free();
            return ret;
        }

        /// <summary>
        /// 構造体をファイルへ書き出す
        /// </summary>
        /// <typeparam name="T">構造体</typeparam>
        /// <param name="s">書き込む構造体</param>
        unsafe public void WriteTo<T>(T s)
        {
            var buffer = new byte[Marshal.SizeOf(typeof(T))];

            IntPtr src = ToPtr<T>(s);
            Marshal.Copy(src, buffer, 0, buffer.Length);
            Marshal.FreeHGlobal(src);

            this.binaryWriter.Write(buffer);
        }

        /// <summary>
        /// 構造体を読み込む
        /// </summary>
        /// <typeparam name="T">読み込む構造体</typeparam>
        /// <returns>読み込んだ構造体</returns>
        unsafe public T ReadFrom<T>()
        {
            var buffer = this.binaryReader.ReadBytes(Marshal.SizeOf(typeof(T)));
            return (ToStruct<T>(buffer));
        }
    }
}
