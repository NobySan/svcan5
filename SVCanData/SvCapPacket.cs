﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVCanData
{
    /// <summary>
    /// CAN packet 作成クラス
    /// </summary>
    public static class SvCapPacket
    {
        /// <summary>
        /// DLC->byte変換テーブル
        /// </summary>
        static Dictionary<int, int> mDlcBytes = new Dictionary<int, int>
        {
            { 9, 12 }, { 10, 16}, { 11, 20}, { 12, 24}, { 13, 32}, { 14, 48}, { 15, 64}
        };

        /// <summary>
        /// 指定されたcan_packetを解析し返却する
        /// </summary>
        /// <param name="can_packet">CAN パケット</param>
        /// <remarks>
        /// int can_packet[0] = 0; // 1:10ns
        /// int can_packet[1] = 0; // bit0:SOF
        /// int can_packet[2] = 0; // bit1:ID(bit1_0)
        /// int can_packet[3] = 1; // bit2:ID(bit1_1)
        /// </remarks>
        /// <returns>レコード</returns>
        public static SvCanRecord PacketConvert(long [] can_packet)
        {

            bool brs = false;
            int i = 2; // SOFは無視
            SvCanRecord rec = new SvCanRecord();

            // CANのレコード数
            int can_length = Array.IndexOf(can_packet, -1);
            if (can_length <= 2)
            {
                return (null);
            }

            // 初期化
            rec.id = -1;
            rec.time = string.Format("{0:F1}", ((double)(can_packet[0]/1) / 100)); // xx.x usec
            try
            {
                do
                {
                    // ID (11bit)
                    rec.id = 0;
                    for (int j = 0; j < 11; j++, i++)
                    {
                        rec.id = (rec.id << 1) | (int)can_packet[i];
                    }

                    // RTRが標準フォーマットか?
                    if (can_packet[i] == 0)
                    {
                        // 
                        if (can_packet[i + 2] == 0x00)
                        {
                            i += 3; // RTR,IDE,R
                            rec.frame_type = SvCanFrameType.Standard;
                        }
                        else
                        {
                            // RRS, IDE, FDF, res, BRS, ESI
                            // BRS判定
                            if (can_packet[i + 4] == 0x01)
                            {
                                // 速度変化あり
                                brs = true;
                                rec.frame_type = SvCanFrameType.Standard_FD2;
                            }
                            else
                            {
                                rec.frame_type = SvCanFrameType.Standard_FD;
                            }
                            i += 6; // RRS, IDE, FDF, res, BRS, ESI
                        }
                    }
                    else
                    {
                        i++; // SRR or RTR
                             // IDE=1なら拡張フォーマット
                        if (can_packet[i] == 1)
                        {
                            rec.frame_type = SvCanFrameType.Expansion;

                            i++;   // IDE
                                   // 18bit 拡張ID
                            for (int j = 0; j < 18; j++, i++)
                            {
                                rec.id = (rec.id << 1) | (int)can_packet[i];
                            }

                            i += 3; // RTR,R1,R0

                        }
                        else
                        {
                            rec.frame_type = SvCanFrameType.Remote;

                            i += 3; // RTR,IDE,R
                        }
                    }

                    // 4bit バイト数
                    rec.dlc = 0;
                    for (int j = 0; j < 4; j++, i++)
                    {
                        rec.dlc = ((rec.dlc << 1) | (int)can_packet[i]);
                    }

                    //if(rec.id == 0x163)
                    //{
                    //    rec.dlc = rec.dlc;
                    //}

                    // データフィールドが存在するか？
                    if (rec.dlc != 0)
                    {
                        int length = rec.dlc;

                        // CAN-FDの判断
                        if (rec.frame_type == SvCanFrameType.Standard_FD || rec.frame_type == SvCanFrameType.Standard_FD2)
                        {
                            if (mDlcBytes.ContainsKey(length))
                            {
                                length = mDlcBytes[length];
                                rec.dlc = length;
                            }
                        }
                        else
                        {
                            if (length > 8)
                            {
                                length = 8;
                            }
                        }

                        rec.data = new byte[length];
                        for (int j = 0; j < length; j++)
                        {
                            for (int k = 0; k < 8; k++, i++)
                            {
                                rec.data[j] = (byte)((rec.data[j] << 1) | (byte)can_packet[i]);
                            }
                        }
                    }

                    // CAN-FDの場合はStaff-Count
                    if (rec.frame_type == SvCanFrameType.Standard_FD || rec.frame_type == SvCanFrameType.Standard_FD2)
                    {
                        // 現状4bit読み飛ばし
                        i += 4; 
                    }

                    // CRC
                    rec.crc = 0;
                    for (int j = 0; j < 15; j++, i++)
                    {
                        rec.crc = (ushort)((rec.crc << 1) | (ushort)can_packet[i]);
                    }

                    ///デリミタスロットデリミタもアイドル期間としたため以下のカウントを無効にする
                    //i++;    // CRC(デリミタ)
                    //i++;    // ACK(スロット)
                    //i++;    // ACK(デリミタ)
                    //i += 7; // EOF
                    // i += 3; // ITM  
                }
                while (false);
            }
            catch(Exception)
            {
                rec.id = -1;
            }

            // 解析長さを超えたものはエラー
            if ( i > can_length)
            {
                return (null);
            }


            // 解結果返却
            return (rec);
        }

    }
}
