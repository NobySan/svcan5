﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace candec
{
    public class Candec
    {
        private int can0_phase = 0; // CAN 0ch信号解析フェーズ 0:初期, 1:SOF_Lowサーチ中, 2:SOF_Highサーチ中, 3:Highサーチ中, 4:Lowサーチ中
        private int can1_phase = 0; // CAN 1ch信号解析フェーズ 0:初期, 1:SOF_Lowサーチ中, 2:SOF_Highサーチ中, 3:Highサーチ中, 4:Lowサーチ中
        private long can0_cnt = 0; // 現在のCAN 0ch信号のカウンタ
        private long can1_cnt = 0; // 現在のCAN 1ch信号のカウンタ
        private int can0_bit1_cnt = 0; // CAN 0ch信号の1bit期間のクロック数
        private int can1_bit1_cnt = 0; // CAN 1ch信号の1bit期間のクロック数
        private int can0_idle_cnt = 0; // CAN 0ch信号のアイドル期間のクロック数
        private int can1_idle_cnt = 0; // CAN 1ch信号のアイドル期間のクロック数
        private int can_theshold = 50; // エラーとする最小ビット期間
        private int can_gosa = 5; // ビット期間の誤差
        private int[] can_0_arry; // CAN 0ch デコード後のビット配列を格納
        private int[] can_1_arry; // CAN 1ch デコード後のビット配列を格納
        private int arry0_cnt = 0; // CAN 0ch ビット配列の格納数
        private int arry1_cnt = 0; // CAN 1ch ビット配列の格納数
        private int can0_flag = 0; // CAN 0ch 動作モード 0:Bitrate検出中, 1:パケット検出中
        private int can1_flag = 0; // CAN 1ch 動作モード 0:Bitrate検出中, 1:パケット検出中
        private long sof0_pos = 0; // CAN 0chのSOFファイルポジションを記憶しておく
        private long sof1_pos = 0; // CAN 1chのSOFファイルポジションを記憶しておく
        private long eof0_pos = 0; // CAN 0chのEOFファイルポジションを記憶しておく
        private long eof1_pos = 0; // CAN 1chのEOFファイルポジションを記憶しておく
        private int can0_sts = 0; // CAN 0chの現在のビット極性
        private int can1_sts = 0; // CAN 1chの現在のビット極性
        private int can0_bef = -1; // CAN 0chの前のビット極性
        private int can1_bef = -1; // CAN 1chの前のビット極性
        private int can0_bef2 = -1; // CAN 0chの前のビット極性(スタッフビットチェック用)
        private int can1_bef2 = -1; // CAN 1chの前のビット極性(スタッフビットチェック用)
        private int can0_dec_flag = 0; // CAN 0chの検索フラグ
        private int can1_dec_flag = 0; // CAN 1chの検索フラグ
        private int sof0_arry_no = 0; // CAN ch0 SOFが見つかった配列番号
        private int sof1_arry_no = 0; // CAN ch1 SOFが見つかった配列番号
        private int eof0_arry_no = 0; // CAN ch0 EOFが見つかった配列番号
        private int eof1_arry_no = 0; // CAN ch1 EOFが見つかった配列番号
                                      
        private int can0_bps = 500; // CAN ch0ビットレート
        private int canfd0_bps = 2000; // CAN-FD ch0ビットレート
        private int can1_bps = 500; // CAN ch1ビットレート
        private int canfd1_bps = 2000; // CAN-F1 ch0ビットレート
        private int canfd0_bit1_cnt = 0; // CAN-FD 0ch信号の1bit期間のクロック数
        private int canfd1_bit1_cnt = 0; // CAN-FD 1ch信号の1bit期間のクロック数

        private int can_bit_sample_point = (200 * 70) / 100; // CAN期間のBit Sample Pointを500K換算したもの
        private int canfd_bit_sample_point = (50 * 70) / 100; // CANFD期間のBit Sample Pointを2M換算したもの

        private int brs_threshold = 68; // CAN-FD時のBRSのサンプルポイントの比率
        private int can_brs_cnt; // CAN-FD時のCAN側BRSのサンプルカウントの設定
        private int canfd_brs_cnt; // CAN-FD時のCANFD側のBRSのサンプルカウントの設定

        private long domi0_pos = 0; // CAN ch0 ドミナント位置
        private long domi1_pos = 0; // CAN ch1 ドミナント位置
        private int can0_sample_cnt = 0; // CAN ch0 CAN時の1bit期間のサンプル数
        private int can1_sample_cnt = 0; // CAN ch1 CAN時の1bit期間のサンプル数
        private int fdf0_flag = 0; // CAN ch0 0:CAN, 1:CAN-FF
        private int fdf1_flag = 0; // CAN ch1 0:CAN, 1:CAN-FF
        private int brs0_flag = 0; // CAN ch0 0:速度変更なし, 1:速度変更あり
        private int brs1_flag = 0; // CAN ch1 0:速度変更なし, 1:速度変更あり
        private int can0_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
        private int can1_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
        private int brs0_cnt = 0; // CAN ch0 BRS期間のビットカウンタ
        private int brs1_cnt = 0; // CAN ch1 BRS期間のビットカウンタ
        private int brs0_dlc = 0; // CAN ch0 BRS期間のDLC+StusCount+CRC+CRCデリミタ
        private int brs1_dlc = 0; // CAN ch1 BRS期間のDLC+StusCount+CRC+CRCデリミタ
        private int brs0_dlc2 = 0; // CAN ch0 BRS期間のDLC分のカウント
        private int brs1_dlc2 = 0; // CAN ch1 BRS期間のDLC分のカウント
        private int brs0_dlc_cnt = 0; // CAN ch0 BRS期間のデータビットカウンタ
        private int brs1_dlc_cnt = 0; // CAN ch1 BRS期間のデータビットカウンタ
        private int can0_stf_cnt = 0; // CAN ch0 スタッフビットカウンタ
        private int can1_stf_cnt = 0; // CAN ch1 スタッフビットカウンタ
        private int can0_no_stuff_chk = 0; // CAN ch0 0:スタッフビット行う 1:スタッフビット行わない
        private int can1_no_stuff_chk = 0; // CAN ch1 0:スタッフビット行う 1:スタッフビット行わない
        private int std0_dlc = 0; // CAN ch0 低速期間のデータビットカウンタ
        private int std1_dlc = 0; // CAN ch1 低速期間のデータビットカウンタ

        private int can0_ren_cnt = 0; // for debug
        private int can0_ren_cnt_max = 0; // for debug
        private int can0_bef3 = 0; // for debug
        private int can0_fderr = 0; // CAN-FD期間のスタッフィングエラーフラグ
        private int can1_fderr = 0; // CAN-FD期間のスタッフィングエラーフラグ

        private long[] can_0_mon_pkt; // CAN 0ch モニタリング用デコード後のビット配列
        private long[] can_1_mon_pkt; // CAN 1ch モニタリング用デコード後のビット配列



        /// <summary>
        /// CANデコーダーに対して最初に1回だけコールする
        /// </summary>
        /// <param name="iThreshold1"></param>
        /// <param name="iThreshold2"></param>
        /// <returns></returns>
        public int Can_Start(int iThreshold1, int iThreshold2)
        {
            can_0_arry = new int[1024]; // デコード後のビット配列を格納
            can_1_arry = new int[1024]; // デコード後のビット配列を格納
            can_0_mon_pkt = new long[1024]; // CAN 0ch モニタリング用デコード後のビット配列
            can_1_mon_pkt = new long[1024]; // CAN 1ch モニタリング用デコード後のビット配列
            arry0_cnt = 0; // CAN 0ch ビット配列の格納数
            arry1_cnt = 0; // CAN 1ch ビット配列の格納数
            can0_dec_flag = 0; // CAN 0chの検索フラグ 0:最初
            can1_dec_flag = 0; // CAN 1chの検索フラグ 0:最初
            can0_cnt = 0; // 現在のCAN 0ch信号のカウンタ
            can1_cnt = 0; // 現在のCAN 1ch信号のカウンタ
            can0_bit1_cnt = 200; // CAN 0ch信号の1bit期間のクロック数
            can1_bit1_cnt = 200; // CAN 1ch信号の1bit期間のクロック数
            can0_idle_cnt = 50; // CAN 0ch信号のアイドル期間のクロック数
            can1_idle_cnt = 50; // CAN 1ch信号のアイドル期間のクロック数
            if (iThreshold2 == 24) iThreshold2 = 70;
            can_gosa = iThreshold2; // Bit Sample Point (%)
            brs_threshold = iThreshold1; // CAN-FD時のBRSのサンプルポイントの比率
            sof0_pos = -1; // CAN 0ch信号のSOFポジション
            sof1_pos = -1; // CAN 0ch信号のSOFポジション
            eof0_pos = -1; // CAN 0ch信号のEOFポジション
            eof1_pos = -1; // CAN 0ch信号のEOFポジション

            can0_phase = 0;
            can1_phase = 0;

            // CAN信号の各パラメータ設定
            Can_set_bitrate(0, can0_bps, canfd0_bps);
            Can_set_bitrate(1, can0_bps, canfd0_bps);

            domi0_pos = 0; // CAN ch0 ドミナント位置
            domi1_pos = 0; // CAN ch1 ドミナント位置
            can0_sample_cnt = 0; // CAN ch0 CAN時の1bit期間のサンプル数
            can1_sample_cnt = 0; // CAN ch1 CAN時の1bit期間のサンプル数
            can0_sts = -1; // CAN 0chの現在のビット極性（0:ドミナント, 1:レセッシブ）
            can1_sts = -1; // CAN 1chの現在のビット極性（0:ドミナント, 1:レセッシブ）
            can0_bef = -1; // CAN 0chの前のビット極性
            can1_bef = -1; // CAN 1chの前のビット極性
            can0_bef2 = -1; // CAN 0chの前のビット極性
            can1_bef2 = -1; // CAN 1chの前のビット極性
            fdf0_flag = 0; // CAN ch0 0:CAN, 1:CAN-FF
            fdf1_flag = 0; // CAN ch1 0:CAN, 1:CAN-FF
            brs0_flag = 0; // CAN ch0 0:速度変更なし, 1:速度変更あり
            brs1_flag = 0; // CAN ch1 0:速度変更なし, 1:速度変更あり
            can0_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
            can1_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
            brs0_cnt = 0; // CAN ch0 BRS期間のビットカウンタ
            brs1_cnt = 0; // CAN ch1 BRS期間のビットカウンタ
            brs0_dlc = 0; // CAN ch0 BRS期間のDLC
            brs1_dlc = 0; // CAN ch0 BRS期間のDLC
            brs0_dlc_cnt = 0; // CAN ch0 BRS期間のデータビットカウンタ
            brs1_dlc_cnt = 0; // CAN ch0 BRS期間のデータビットカウンタ
            can0_stf_cnt = 0; // CAN ch0 スタッフビットカウンタ
            can1_stf_cnt = 0; // CAN ch1 スタッフビットカウンタ

            // 2020.05.27 bug modify start
            can0_no_stuff_chk = 0; // CAN ch0 0:スタッフビット行う 1:スタッフビット行わない
            can1_no_stuff_chk = 0; // CAN ch1 0:スタッフビット行う 1:スタッフビット行わない
            std0_dlc = 0; // CAN ch0 低速期間のデータビットカウンタ
            std1_dlc = 0; // CAN ch1 低速期間のデータビットカウンタ
            // 2020.05.27 bug modify end

            can_bit_sample_point = (can0_bit1_cnt * can_gosa) / 100; // CAN期間のBit Sample PointをCAN_bps換算したもの
            canfd_bit_sample_point = (canfd0_bit1_cnt * can_gosa) / 100; // CANFD期間のBit Sample PointをCANFD_bps換算したもの

            return 1;
        }
        /// <summary>
        /// CANデコーダーに対して2回目以降に毎回コールする
        /// </summary>
        /// <param name="iThreshold1"></param>
        /// <param name="iThreshold2"></param>
        /// <returns></returns>
        public int Can_Start_next(int iThreshold1, int iThreshold2)
        {
            arry0_cnt = 0; // CAN 0ch ビット配列の格納数
            arry1_cnt = 0; // CAN 1ch ビット配列の格納数
            can0_dec_flag = 0; // CAN 0chの検索フラグ 0:最初
            can1_dec_flag = 0; // CAN 1chの検索フラグ 0:最初
            can0_cnt = 0; // 現在のCAN 0ch信号のカウンタ
            can1_cnt = 0; // 現在のCAN 1ch信号のカウンタ
            can0_bit1_cnt = 200; // CAN 0ch信号の1bit期間のクロック数
            can1_bit1_cnt = 200; // CAN 1ch信号の1bit期間のクロック数
            can0_idle_cnt = 50; // CAN 0ch信号のアイドル期間のクロック数
            can1_idle_cnt = 50; // CAN 1ch信号のアイドル期間のクロック数
            if (iThreshold2 == 24) iThreshold2 = 70;
            can_gosa = iThreshold2; // Bit Sample Point
            brs_threshold = iThreshold1; // CAN-FD時のBRSのサンプルポイントの比率
            sof0_pos = -1; // CAN 0ch信号のSOFポジション
            sof1_pos = -1; // CAN 0ch信号のSOFポジション
            eof0_pos = -1; // CAN 0ch信号のEOFポジション
            eof1_pos = -1; // CAN 0ch信号のEOFポジション

            can0_phase = 0;
            can1_phase = 0;

            // CAN信号の各パラメータ設定
            Can_set_bitrate(0, can0_bps, canfd0_bps);
            Can_set_bitrate(1, can0_bps, canfd0_bps);

            domi0_pos = 0; // CAN ch0 ドミナント位置
            domi1_pos = 0; // CAN ch1 ドミナント位置
            can0_sample_cnt = 0; // CAN ch0 CAN時の1bit期間のサンプル数
            can1_sample_cnt = 0; // CAN ch1 CAN時の1bit期間のサンプル数
            can0_sts = -1; // CAN 0chの現在のビット極性（0:ドミナント, 1:レセッシブ）
            can1_sts = -1; // CAN 1chの現在のビット極性（0:ドミナント, 1:レセッシブ）
            can0_bef = -1; // CAN 0chの前のビット極性
            can1_bef = -1; // CAN 1chの前のビット極性
            can0_bef2 = -1; // CAN 0chの前のビット極性
            can1_bef2 = -1; // CAN 1chの前のビット極性
            fdf0_flag = 0; // CAN ch0 0:CAN, 1:CAN-FF
            fdf1_flag = 0; // CAN ch1 0:CAN, 1:CAN-FF
            brs0_flag = 0; // CAN ch0 0:速度変更なし, 1:速度変更あり
            brs1_flag = 0; // CAN ch1 0:速度変更なし, 1:速度変更あり
            can0_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
            can1_end_chk = 0; // パケットエンドを検知するためのレセッシブカウンタ
            brs0_cnt = 0; // CAN ch0 BRS期間のビットカウンタ
            brs1_cnt = 0; // CAN ch1 BRS期間のビットカウンタ
            brs0_dlc = 0; // CAN ch0 BRS期間のDLC
            brs1_dlc = 0; // CAN ch0 BRS期間のDLC
            brs0_dlc_cnt = 0; // CAN ch0 BRS期間のデータビットカウンタ
            brs1_dlc_cnt = 0; // CAN ch0 BRS期間のデータビットカウンタ
            can0_stf_cnt = 0; // CAN ch0 スタッフビットカウンタ
            can1_stf_cnt = 0; // CAN ch1 スタッフビットカウンタ

            // 2020.05.27 bug modify start
            can0_no_stuff_chk = 0; // CAN ch0 0:スタッフビット行う 1:スタッフビット行わない
            can1_no_stuff_chk = 0; // CAN ch1 0:スタッフビット行う 1:スタッフビット行わない
            std0_dlc = 0; // CAN ch0 低速期間のデータビットカウンタ
            std1_dlc = 0; // CAN ch1 低速期間のデータビットカウンタ
            // 2020.05.27 bug modify end

            can_bit_sample_point = (can0_bit1_cnt * can_gosa) / 100; // CAN期間のBit Sample PointをCAN_bps換算したもの
            canfd_bit_sample_point = (canfd0_bit1_cnt * can_gosa) / 100; // CANFD期間のBit Sample PointをCANFD_bps換算したもの

            return 1;
        }
        public int Can_End()
        {
            // delete[] can_0_arry
            // delete[] can_1_arry
            return 1;
        }
        /// <summary>
        /// CANのbpsを取得
        /// </summary>
        /// <remarks>
        /// チャンネルごとにコールして設定すること
        /// </remarks>
        public int Can_get_bitrate(int ch)
        {
            int bit_rate = 0;

            if(ch == 0)
            {
                // CANビットレート算出
                bit_rate = can0_bps;
            }
            else
            {
                // CANビットレート算出
                bit_rate = can1_bps;
            }
            return bit_rate;
        }
        /// <summary>
        /// CAN-FDのbpsを取得
        /// </summary>
        /// <remarks>
        /// チャンネルごとにコールして設定すること
        /// </remarks>
        public int Canfd_get_bitrate(int ch)
        {
            int bit_rate = 0;

            if (ch == 0)
            {
                // CANビットレート算出
                bit_rate = canfd0_bps;
            }
            else
            {
                // CANビットレート算出
                bit_rate = canfd1_bps;
            }
            return bit_rate;
        }
        /// <summary>
        /// CAN, CAN-FDのbpsを通知
        /// </summary>
        /// <remarks>
        /// チャンネルごとにコールして設定すること
        /// </remarks>
        public void Can_set_bitrate(int ch, int Can_bps, int Canfd_bps)
        {
            if (ch == 0)
            {
                // CANビットレート保存
                can0_bps = Can_bps;
                // CAN-FDビットレート保存
                canfd0_bps = Canfd_bps;
                // CAN 0ch信号の1bit期間のクロック数設定
                can0_bit1_cnt = 1000000 / Can_bps / 10;
                // CAN 0ch信号のACKデリミタ(1)+EOF(7)+ITM(1)のクロック数設定
                can0_idle_cnt = (can0_bit1_cnt * 11) - can_gosa;
                // CAN-FD 0ch信号の1bit期間のクロック数設定
                canfd0_bit1_cnt = 1000000 / Canfd_bps / 10;
            }
            else
            {
                // CANビットレート保存
                can1_bps = Can_bps;
                // CAN-FDビットレート保存
                canfd1_bps = Canfd_bps;
                // CAN 1ch信号の1bit期間のクロック数設定
                can1_bit1_cnt = 1000000 / Can_bps / 10; ;
                // CAN 1ch信号のACKデリミタ(1)+EOF(7)+ITM(1)のクロック数設定
                can1_idle_cnt = (can1_bit1_cnt * 11) - can_gosa;
                // CAN-FD 1ch信号の1bit期間のクロック数設定
                canfd1_bit1_cnt = 1000000 / Canfd_bps / 10;
            }

            // CAN-FD時のBRSのサンプルカウントの設定
            // 100M / bps * brs_threshold / 100
            can_brs_cnt = (100000 / Can_bps) * brs_threshold / 100;
            canfd_brs_cnt = (100000 / Canfd_bps) * brs_threshold / 100;

            // Bit Sample Pointの適用
            can_bit_sample_point = (can0_bit1_cnt * can_gosa) / 100; // CAN期間のBit Sample PointをCAN_bps換算したもの
            canfd_bit_sample_point = (canfd0_bit1_cnt * can_gosa) / 100; // CANFD期間のBit Sample PointをCANFD_bps換算したもの
            return;
        }
        /// <summary>
        /// サンプル数より偏差をなくしたサンプル数を返す
        /// 例えば500Kbpsの時は計算では1bit期間のサンプル数は200であるが
        /// ターゲットによっては206と194とか220とかになるので、これを200に
        /// 丸める
        /// </summary>
        /// <param name="icanval"></param>
        /// <param name="dec_flg"></param>
        /// <param name="ch"></param>
        /// <param name="icancnt"></param>
        /// <returns></returns>
        public int can_get_cnt(int icanval, int dec_flg, int ch, ref int icancnt)
        {
            int can_val = 0;
            int can_cnt = 0;
            int can_cnt1 = 0;
            int can_cnt2 = 0;
            can_val = icanval;
            if (dec_flg == 0)
            {
                // CAN速度
                if (ch == 0)
                {
                    can_cnt1 = (icanval + can_gosa) / can0_bit1_cnt;
                    can_cnt = can_cnt1 * can0_bit1_cnt;
                    can_cnt2 = can0_bit1_cnt * can_cnt1;
                    if ((icanval > (can_cnt - can_gosa)) && (icanval < (can_cnt + can_gosa))) can_val = can_cnt;
                }
                else
                {
                    can_cnt1 = (icanval + can_gosa) / can1_bit1_cnt;
                    can_cnt = can_cnt1 * can1_bit1_cnt;
                    can_cnt2 = can1_bit1_cnt * can_cnt1;
                    if ((icanval > (can_cnt - can_gosa)) && (icanval < (can_cnt + can_gosa))) can_val = can_cnt;
                }
            }
            else
            {
                // CAN-FD速度
                if (ch == 0)
                {
                    can_cnt1 = (icanval + can_gosa) / canfd0_bit1_cnt;
                    can_cnt = can_cnt1 * canfd0_bit1_cnt;
                    can_cnt2 = canfd0_bit1_cnt * can_cnt1;
                    if ((icanval > (can_cnt - can_gosa)) && (icanval < (can_cnt + can_gosa))) can_val = can_cnt;
                }
                else
                {
                    can_cnt1 = (icanval + can_gosa) / canfd1_bit1_cnt;
                    can_cnt = can_cnt1 * canfd1_bit1_cnt;
                    can_cnt2 = canfd1_bit1_cnt * can_cnt1;
                    if ((icanval > (can_cnt - can_gosa)) && (icanval < (can_cnt + can_gosa))) can_val = can_cnt;
                }
            }
            icancnt = can_cnt1;
            return can_val;
        }
        /// <summary>
        /// レコーディングデータのCAN, CAN-FDの解析を行う
        /// </summary>
        /// <remarks>
        /// データは1クロックごとに受信する。先頭のアイドルを見つけて、それ以降を対象とする
        /// CANパケットの構成　FDFがONでCAN-FDパケット、BRSがONでハイビットレートとなる
        /// CAN   (SOF,ID,RTR,IDE,res,DLC,DATA,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
        /// CAN-FD(SOF,ID,RRS,IDE,FDF,res,BRS,ESI,DLC,DATA,StuffCount,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
        /// </remarks>
        public int Can_serch_data(int flag, UInt16 dataVal, ref long[] can0_packet, ref long[] can1_packet)
        {
            int can0_val = 0;
            int can1_val = 0;
            int iRet = 1;
            int iRet2 = 1;

            // パケットステータス初期化
            can0_packet[0] = -1;
            can1_packet[0] = -1;

            // チャンネル単位に分解
            can0_val = (int)(dataVal >> 14) & 0x1;
            can1_val = (int)(dataVal >> 15) & 0x1;
            // サンプル時間を更新
            can0_cnt++;
            can1_cnt++;
            // CAN信号状態を保存
            can0_bef = can0_sts;
            can1_bef = can1_sts;
            can0_sts = can0_val;
            can1_sts = can1_val;
            // ドミナントになったら同期処理を行う
            if (can0_bef == 1 && can0_sts == 0)
            {
                if (brs0_flag == 0)
                {
                    //can0_sample_cnt = (can0_bit1_cnt >> 1);
                    can0_sample_cnt = can_bit_sample_point; // CAN期間のBit Sample Pointを500K換算したもの
                }
                else
                {
                    //can0_sample_cnt = (canfd0_bit1_cnt >> 1);
                    can0_sample_cnt = canfd_bit_sample_point; // CANFD期間のBit Sample Pointを2M換算したもの
                }
                can0_flag = 1; // CAN信号がレセッシブからドミナントになったことのフラグ
            }
            if (can1_bef == 1 && can1_sts == 0)
            {
                if (brs1_flag == 0)
                {
                    //can1_sample_cnt = (can1_bit1_cnt >> 1);
                    can1_sample_cnt = can_bit_sample_point; // CAN期間のBit Sample Pointを500K換算したもの
                }
                else
                {
                    //can1_sample_cnt = (canfd1_bit1_cnt >> 1);
                    can1_sample_cnt = canfd_bit_sample_point; // CANFD期間のBit Sample Pointを2M換算したもの
                }
                can1_flag = 1; // CAN信号がレセッシブからドミナントになったことのフラグ
            }

            //////////////////////////////////////////////////////////////////////////////
            /// CH0の処理
            //////////////////////////////////////////////////////////////////////////////

            // 最初のドミナントを探す、最初からドミナントであれば、次のドミナントを探す
            if (can0_phase == 0)
            {
                if (can0_val == 0)
                {
                }
                else
                {
                    // 最初の信号がレセッシブなので、次のドミナントを探す
                    can0_phase = 1;
                }
                // 2020.06.08 CAN-FD時のスタッフィングエラー対応
                can0_fderr = 0;
            }
            // 最初の信号がレセッシブなので、次のドミナントを探す
            else if (can0_phase == 1)
            {
                if (can0_val == 0)
                {
                    // ドミナント開始位置を保存
                    domi0_pos = can0_cnt - 1;
                    // サンプリング開始
                    can0_phase = 2;
                    //can0_sample_cnt = (can0_bit1_cnt >> 1) - 1; // 最初は半周期分
                    sof0_pos = can0_cnt - 1;
                    // for debug
                    if(sof0_pos > 1511336500)
                    {
                        sof0_pos = sof0_pos;
                    }
                }
                else
                {
                    // 次のドミナントを探す
                }
            }
            // サンプリング開始、最初のSOFを探す
            else if (can0_phase == 2)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can0_sample_cnt--;
                if (can0_sample_cnt == 0)
                {
                    // CAN信号状態を保存
                    can0_bef2 = can0_val;
                    // 最初のビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_0_arry[arry0_cnt++] = (int)can0_val;
                    // サンプル数を1bit期間とする
                    can0_sample_cnt = can0_bit1_cnt;
                    can0_stf_cnt = 1;
                    can0_phase = 3;
                }
            }
            // サンプリング継続、最初のSOFを探す
            else if (can0_phase == 3)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can0_sample_cnt--;
                if (can0_sample_cnt == 0)
                {
                    // スタッフビット排除処理
                    if (can0_bef2 == can0_val)
                    {
                        can0_stf_cnt++;
                        if (can0_stf_cnt > (11+23))
                        {
                            // 異常時または長いアイドル？
                            can0_phase = 5;
                            can0_bef2 = can0_val;
                            goto CAN1_PHASE;
                        }
                    }
                    else
                    {
                        if (can0_stf_cnt == 5 && can0_no_stuff_chk == 0)
                        {
                            // サンプル数を1bit期間とする
                            can0_sample_cnt = can0_bit1_cnt;
                            can0_stf_cnt = 1;
                            can0_bef2 = can0_val;
                            goto CAN1_PHASE;
                        }
                        can0_stf_cnt = 1;
                        can0_bef2 = can0_val;
                    }
                    // ビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_0_arry[arry0_cnt++] = (int)can0_val;
                    // サンプル数を1bit期間とする
                    can0_sample_cnt = can0_bit1_cnt;
                    //  CAN   (SOF,ID,RTR,IDE,res,DLC,DATA,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
                    //         0   1  12  13  14  15  19
                    //  CAN-FD(SOF,ID,RRS,IDE,FDF,res,BRS,ESI,DLC,DATA,StuffCount,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
                    //         0   1  12  13  14  15  16  17  18
                    // FDFかチェック
                    if (arry0_cnt == 15)
                    {

                        // 2020.05.23 bug リモートフレームの対応をする
                        if (can_0_arry[arry0_cnt - 1] == 0 || can_0_arry[12] == 1) // CAN or Remote Frameか？
                        // 2020.05.23 bug リモートフレームの対応をする
                        //if (can_0_arry[arry0_cnt - 1] == 0) // CANか？
                        {
                            fdf0_flag = 0;
                        }
                        else // CAN-FD
                        {
                            fdf0_flag = 1;
                        }
                    }
                    // DLCを取得
                    if (arry0_cnt >= 16 && arry0_cnt <= 19 && fdf0_flag == 0)
                    {
                        std0_dlc |= can0_val;
                        if (arry0_cnt == 19)
                        {
                            //std0_dlc = (std0_dlc << 3);
                            // 2020.05.23 bug リモートフレームの対応をする
                            std0_dlc = (std0_dlc << 3) + 15;
                            if (can_0_arry[12] == 1) // Remote Frameか？
                            {
                                std0_dlc = 15;
                            }
                            // 2020.05.23 bug リモートフレームの対応をする
                        }
                        else
                        {
                            std0_dlc <<= 1;
                        }
                    }
                    // CAN-FD時BRS ONかチェック
                    if (fdf0_flag == 1)
                    {
                        if (arry0_cnt == 17)
                        {
                            if (can_0_arry[arry0_cnt - 1] == 0) // 速度変更なしか？
                            {
                                brs0_flag = 0;
                                can0_sample_cnt = can0_bit1_cnt;
                            }
                            else // 速度変更あり
                            {
                                brs0_flag = 1;
                                // サンプリングポイントを設定
                                //can0_sample_cnt = (can_brs_cnt + canfd_brs_cnt) - (can0_bit1_cnt >> 1) + (canfd0_bit1_cnt >> 1);
                                can0_sample_cnt = (can_brs_cnt + canfd_brs_cnt) - (can_bit_sample_point) + (canfd_bit_sample_point);
                            }
                            brs0_cnt = 0;
                            brs0_dlc = 0;
                            can0_phase = 4;
                        }
                    }
                    // CAN時パケット終了を探す
                    else
                    {
                        if (fdf0_flag == 0 && arry0_cnt > 19)
                        {
                            if (std0_dlc > 0)
                            {
                                std0_dlc--;
                                // 2020.05.19 bug modify start
                                if (std0_dlc == 0)
                                {
                                    // DATA領域の後ろからはスタッフィングチェックを行わない
                                    can0_no_stuff_chk = 1;
                                }
                                // 2020.05.19 bug modify end
                            }
                        }
                        if (std0_dlc == 0)
                        {
                            if (can0_sts == 1)
                            {
                                can0_end_chk++;
                                if (can0_end_chk == 11)
                                {
                                    // 2020.05.23 bug リモートフレームの対応をする
                                    if (can_0_arry[12] == 1) // Remote Frameか？
                                    {
                                        // 異常時または長いアイドル？
                                        can0_phase = 5;
                                        can0_bef2 = can0_val;
                                        goto CAN1_PHASE;
                                    }
                                    // 2020.06.09 add CAN-FD期間にエラーが立っていたらデコードエラーとする
                                    if (can0_fderr != 0)
                                    {
                                        // 異常時または長いアイドル？
                                        can0_phase = 5;
                                        can0_bef2 = can0_val;
                                        goto CAN1_PHASE;
                                    }
                                    // 2020.05.23 bug リモートフレームの対応をする
                                    // 最後のビット位置を保存
                                    eof0_pos = can0_cnt - 1;
                                    // 上位へCAN情報を返すためにスタッフビットを削除しCAN情報を生成
                                    // CAN   標準フォーマットの最大ビット数　1+11+1+6+64+15+1+1+1+7+3=111
                                    // CAN   拡張フォーマットの最大ビット数　1+29+1+6+64+15+1+1+1+7+3=129
                                    // CAN-FD標準フォーマットの最大ビット数　1+11+1+9+512+4+21+1+1+1+7+3=572
                                    // CAN-FD拡張フォーマットの最大ビット数　1+29+1+9+512+4+21+1+1+1+7+3=590
                                    int i = 0;
                                    int j = 0;
                                    can0_packet[j++] = sof0_pos;
                                    for (i = 0; i < arry0_cnt; i++)
                                    {
                                        can0_packet[j++] = can_0_arry[i];
                                    }
                                    can0_packet[j++] = -1;
                                    // 2020.05.25 NVcalfil対応でEOFポジションを格納するようにする
                                    can0_packet[j++] = eof0_pos;
                                    // 2020.05.25 NVcalfil対応でEOFポジションを格納するようにする
                                    can0_phase = 0;
                                    arry0_cnt = 0;
                                    fdf0_flag = 0;
                                    brs0_flag = 0;
                                    can0_no_stuff_chk = 0;
                                    std0_dlc = 0;
                                    can0_end_chk = 0;
                                    iRet = 2;
                                }
                            }
                            else
                            {
                                can0_end_chk = 0;
                            }
                        }

                    }
                }
            }
            // CAN-FD期間のサンプリング継続、最初のSOFを探す
            else if (can0_phase == 4)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can0_sample_cnt--;
                if (can0_sample_cnt == 0)
                {
                    // スタッフビット排除処理
                    if (can0_bef2 == can0_val)
                    {
                        can0_stf_cnt++;
                        // 2020.06.09 CAN-FD期間のスタッフィングエラー対応
                        if (can0_stf_cnt > 6) can0_fderr = 1;
                    }
                    else
                    {
                        if (can0_stf_cnt == 5)
                        {
                            // サンプル数を1bit期間とする
                            if (brs0_flag == 0)
                            {
                                can0_sample_cnt = can0_bit1_cnt;
                            }
                            else
                            {
                                can0_sample_cnt = canfd0_bit1_cnt;
                            }
                            can0_stf_cnt = 1;
                            can0_bef2 = can0_val;
                            goto CAN1_PHASE;
                        }
                        can0_stf_cnt = 1;
                        can0_bef2 = can0_val;
                    }
                    // ビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_0_arry[arry0_cnt++] = (int)can0_val;
                    // サンプル数を1bit期間とする
                    if(brs0_flag == 0)
                    {
                        can0_sample_cnt = can0_bit1_cnt;
                    }
                    else
                    {
                        can0_sample_cnt = canfd0_bit1_cnt;
                    }
                    // BRS期間のビットカウンタ更新
                    brs0_cnt++; // 1:ESI, 2-5:DLC
                    if (brs0_cnt == 1)
                    {
                    }
                    else if (brs0_cnt >= 2 && brs0_cnt <= 5)
                    {
                        brs0_dlc |= can0_val;
                        if (brs0_cnt != 5)
                        {
                            brs0_dlc <<= 1;
                        }
                        else
                        {
                            brs0_dlc_cnt = 0;
                            if (brs0_dlc == 9) brs0_dlc = 12;
                            else if (brs0_dlc == 10) brs0_dlc = 16;
                            else if (brs0_dlc == 11) brs0_dlc = 20;
                            else if (brs0_dlc == 12) brs0_dlc = 24;
                            else if (brs0_dlc == 13) brs0_dlc = 32;
                            else if (brs0_dlc == 14) brs0_dlc = 48;
                            else if (brs0_dlc == 15) brs0_dlc = 64;
                            // 2020.05.19 bug modify start
                            brs0_dlc2 = (brs0_dlc << 3); // DataFieldのみ
                            if (brs0_dlc > 16)
                            {
                                brs0_dlc  = (brs0_dlc << 3) + (21 + 4 + 7 + 1); // DataField+CRC+StuffCount+FixedStuff+CRC Delimiter
                            }
                            else
                            {
                                brs0_dlc = (brs0_dlc << 3) + (17 + 4 + 6 + 1); // DataField+CRC+StuffCount+FixedStuff+CRC Delimiter
                            }
                            // 2020.05.19 bug modify end
                        }
                    }
                    else
                    {
                        brs0_dlc_cnt++;
                        if(brs0_dlc_cnt >= brs0_dlc2)
                        {
                            can0_stf_cnt = 1;
                        }
                        if (brs0_dlc_cnt == brs0_dlc)
                        {
                            // CANFD期間のデータ領域、CRC領域をデコードしたらCAN期間に戻す
                            can0_phase = 3;
                            fdf0_flag = 0;
                            brs0_flag = 0;
                            can0_no_stuff_chk = 1;
                            can0_sample_cnt = can0_bit1_cnt;
                        }
                    }
                }
            }
            // 異常時または長いアイドル？
            else if (can0_phase == 5)
            {
                if (can0_val == 1)
                {
                    // ドミナントになるのを待つ
                    int i = 0;
                    int j = 0;
                    can0_packet[j] = -1;
                    can0_phase = 0;
                    arry0_cnt = 0;
                    fdf0_flag = 0;
                    brs0_flag = 0;
                    std0_dlc = 0;
                    can0_end_chk = 0;
                    can0_no_stuff_chk = 0;
                    iRet = 1;
                }
                else
                {
                    can0_stf_cnt = 1;
                }
            }

        CAN1_PHASE:
            //////////////////////////////////////////////////////////////////////////////
            /// CH1の処理
            //////////////////////////////////////////////////////////////////////////////

            // 最初のドミナントを探す、最初からドミナントであれば、次のドミナントを探す
            if (can1_phase == 0)
            {
                if (can1_val == 0)
                {
                }
                else
                {
                    // 最初の信号がレセッシブなので、次のドミナントを探す
                    can1_phase = 1;
                }
                // 2020.06.08 CAN-FD時のスタッフィングエラー対応
                can1_fderr = 0;
            }
            // 最初の信号がレセッシブなので、次のドミナントを探す
            else if (can1_phase == 1)
            {
                if (can1_val == 0)
                {
                    // ドミナント開始位置を保存
                    domi1_pos = can1_cnt - 1;
                    // サンプリング開始
                    can1_phase = 2;
                    //can1_sample_cnt = (can1_bit1_cnt >> 1) - 1; // 最初は半周期分
                    sof1_pos = can1_cnt - 1;
                    //debug        6356600000
                    if (sof1_pos > 2692857600)
                    {
                        can1_phase = 2;
                    }
                }
                else
                {
                    // 次のドミナントを探す
                }
            }
            // サンプリング開始、最初のSOFを探す
            else if (can1_phase == 2)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can1_sample_cnt--;
                if (can1_sample_cnt == 0)
                {
                    // CAN信号状態を保存
                    can1_bef2 = can1_val;
                    // 最初のビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_1_arry[arry1_cnt++] = (int)can1_val;
                    // サンプル数を1bit期間とする
                    can1_sample_cnt = can1_bit1_cnt;
                    can1_stf_cnt = 1;
                    can1_phase = 3;
                }
            }
            // サンプリング継続、最初のSOFを探す
            else if (can1_phase == 3)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can1_sample_cnt--;
                if (can1_sample_cnt == 0)
                {
                    // スタッフビット排除処理
                    if (can1_bef2 == can1_val)
                    {
                        can1_stf_cnt++;
                        if (can1_stf_cnt > (11 + 23))
                        {
                            // 異常時または長いアイドル？
                            can1_phase = 5;
                            can1_bef2 = can1_val;
                            goto RET_PHASE;
                        }
                    }
                    else
                    {
                        if (can1_stf_cnt == 5 && can1_no_stuff_chk == 0)
                        {
                            // サンプル数を1bit期間とする
                            can1_sample_cnt = can1_bit1_cnt;
                            can1_stf_cnt = 1;
                            can1_bef2 = can1_val;
                            goto RET_PHASE;
                        }
                        can1_stf_cnt = 1;
                        can1_bef2 = can1_val;
                    }
                    // ビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_1_arry[arry1_cnt++] = (int)can1_val;
                    // サンプル数を1bit期間とする
                    can1_sample_cnt = can1_bit1_cnt;
                    //  CAN   (SOF,ID,RTR,IDE,res,DLC,DATA,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
                    //         0   1  12  13  14  15  19
                    //  CAN-FD(SOF,ID,RRS,IDE,FDF,res,BRS,ESI,DLC,DATA,StuffCount,CRC,CRCデリミタ,ACK,ACKデリミタ,EOF,ITM)
                    //         0   1  12  13  14  15  16  17  18
                    // FDFかチェック
                    if (arry1_cnt == 15)
                    {
                        // 2020.05.23 bug リモートフレームの対応をする
                        if (can_1_arry[arry1_cnt - 1] == 0 || can_1_arry[12] == 1) // CAN or Remote Frameか？
                        // 2020.05.23 bug リモートフレームの対応をする
                        //if (can_1_arry[arry1_cnt - 1] == 0) // CANか？
                        {
                            fdf1_flag = 0;
                        }
                        else // CAN-FD
                        {
                            fdf1_flag = 1;
                        }
                    }
                    // DLCを取得
                    if (arry1_cnt >= 16 && arry1_cnt <= 19 && fdf1_flag == 0)
                    {
                        std1_dlc |= can1_val;
                        if (arry1_cnt == 19)
                        {
                            //std1_dlc = (std1_dlc << 3);
                            // 2020.05.23 bug リモートフレームの対応をする
                            std1_dlc = (std1_dlc << 3)+15;
                            if (can_1_arry[12] == 1) // Remote Frameか？
                            {
                                std1_dlc = 15;
                            }
                            // 2020.05.23 bug リモートフレームの対応をする
                        }
                        else
                        {
                            std1_dlc <<= 1;
                        }
                    }
                    // CAN-FD時BRS ONかチェック
                    if (fdf1_flag == 1)
                    {
                        if (arry1_cnt == 17)
                        {
                            if (can_1_arry[arry1_cnt - 1] == 0) // 速度変更なしか？
                            {
                                brs1_flag = 0;
                                can1_sample_cnt = can1_bit1_cnt;
                            }
                            else // 速度変更あり
                            {
                                brs1_flag = 1;
                                // サンプリングポイントを設定
                                //can1_sample_cnt = (can_brs_cnt + canfd_brs_cnt) - (can1_bit1_cnt >> 1) + (canfd1_bit1_cnt >> 1);
                                can1_sample_cnt = (can_brs_cnt + canfd_brs_cnt) - (can_bit_sample_point) + (canfd_bit_sample_point);
                            }
                            brs1_cnt = 0;
                            brs1_dlc = 0;
                            can1_phase = 4;
                        }
                    }
                    // CAN時パケット終了を探す
                    else
                    {
                        if (fdf1_flag == 0 && arry1_cnt > 19)
                        {
                            if (std1_dlc > 0)
                            {
                                std1_dlc--;
                                // 2020.05.19 bug modify start
                                if (std1_dlc == 0)
                                {
                                    // DATA領域の後ろからはスタッフィングチェックを行わない
                                    can1_no_stuff_chk = 1;
                                }
                                // 2020.05.19 bug modify end
                            }
                        }
                        if (std1_dlc == 0)
                        {
                            if (can1_sts == 1)
                            {
                                can1_end_chk++;
                                if (can1_end_chk == 11)
                                {
                                    // 2020.05.23 bug リモートフレームの対応をする
                                    if (can_1_arry[12] == 1) // Remote Frameか？
                                    {
                                        // 異常時または長いアイドル？
                                        can1_phase = 5;
                                        can1_bef2 = can1_val;
                                        goto RET_PHASE;
                                    }
                                    // 2020.06.09 add CAN-FD期間にエラーが立っていたらデコードエラーとする
                                    if (can1_fderr != 0)
                                    {
                                        // 異常時または長いアイドル？
                                        can1_phase = 5;
                                        can1_bef2 = can1_val;
                                        goto RET_PHASE;
                                    }
                                    // 2020.05.23 bug リモートフレームの対応をする
                                    // 最後のビット位置を保存
                                    eof1_pos = can1_cnt - 1;
                                    // 上位へCAN情報を返すためにスタッフビットを削除しCAN情報を生成
                                    // CAN   標準フォーマットの最大ビット数　1+11+1+6+64+15+1+1+1+7+3=111
                                    // CAN   拡張フォーマットの最大ビット数　1+29+1+6+64+15+1+1+1+7+3=129
                                    // CAN-FD標準フォーマットの最大ビット数　1+11+1+9+512+4+21+1+1+1+7+3=572
                                    // CAN-FD拡張フォーマットの最大ビット数　1+29+1+9+512+4+21+1+1+1+7+3=590
                                    int i = 0;
                                    int j = 0;
                                    can1_packet[j++] = sof1_pos;
                                    for (i = 0; i < arry1_cnt; i++)
                                    {
                                        can1_packet[j++] = can_1_arry[i];
                                    }
                                    can1_packet[j++] = -1;
                                    // 2020.05.25 NVcalfil対応でEOFポジションを格納するようにする
                                    can1_packet[j++] = eof1_pos;
                                    // 2020.05.25 NVcalfil対応でEOFポジションを格納するようにする
                                    can1_phase = 0;
                                    arry1_cnt = 0;
                                    fdf1_flag = 0;
                                    brs1_flag = 0;
                                    std1_dlc = 0;
                                    can1_end_chk = 0;
                                    can1_no_stuff_chk = 0;
                                    iRet2 = 2;
                                }
                            }
                            else
                            {
                                can1_end_chk = 0;
                            }
                        }

                    }
                }
            }
            // CAN-FD期間のサンプリング継続、最初のSOFを探す
            else if (can1_phase == 4)
            {
                // サンプリング数を-1ずつ減らし、0になったところ1bit期間の中間地点とする
                can1_sample_cnt--;
                if (can1_sample_cnt == 0)
                {
                    // スタッフビット排除処理
                    if (can1_bef2 == can1_val)
                    {
                        can1_stf_cnt++;
                        // 2020.06.09 CAN-FD期間のスタッフィングエラー対応
                        if (can1_stf_cnt > 6) can1_fderr = 1;
                    }
                    else
                    {
                        if (can1_stf_cnt == 5)
                        {
                            // サンプル数を1bit期間とする
                            if(brs1_flag == 0)
                            {
                                can1_sample_cnt = can1_bit1_cnt;
                            }
                            else
                            {
                                can1_sample_cnt = canfd1_bit1_cnt;
                            }
                            can1_stf_cnt = 1;
                            can1_bef2 = can1_val;
                            goto RET_PHASE;
                        }
                        can1_stf_cnt = 1;
                        can1_bef2 = can1_val;
                    }
                    // ビット情報（0:ドミナント, 1:レセッシブ）を保存
                    can_1_arry[arry1_cnt++] = (int)can1_val;
                    // サンプル数を1bit期間とする
                    if (brs1_flag == 0)
                    {
                        can1_sample_cnt = can1_bit1_cnt;
                    }
                    else
                    {
                        can1_sample_cnt = canfd1_bit1_cnt;
                    }
                    // BRS期間のビットカウンタ更新
                    brs1_cnt++; // 1:ESI, 2-5:DLC
                    if (brs1_cnt == 1)
                    {
                    }
                    else if (brs1_cnt >= 2 && brs1_cnt <= 5)
                    {
                        brs1_dlc |= can1_val;
                        if (brs1_cnt != 5)
                        {
                            brs1_dlc <<= 1;
                        }
                        else
                        {
                            brs1_dlc_cnt = 0;
                            if (brs1_dlc == 9) brs1_dlc = 12;
                            else if (brs1_dlc == 10) brs1_dlc = 16;
                            else if (brs1_dlc == 11) brs1_dlc = 20;
                            else if (brs1_dlc == 12) brs1_dlc = 24;
                            else if (brs1_dlc == 13) brs1_dlc = 32;
                            else if (brs1_dlc == 14) brs1_dlc = 48;
                            else if (brs1_dlc == 15) brs1_dlc = 64;
                            // 2020.05.19 bug modify start
                            brs1_dlc2 = (brs1_dlc << 3); // DataFieldのみ
                            if (brs1_dlc > 16)
                            {
                                brs1_dlc = (brs1_dlc << 3) + (21 + 4 + 7 + 1); // CRC+StuffCount+FixedStuff+CRC Delimiter
                            }
                            else
                            {
                                brs1_dlc = (brs1_dlc << 3) + (17 + 4 + 6 + 1); // CRC+StuffCount+FixedStuff+CRC Delimiter
                            }
                            // 2020.05.19 bug modify end
                        }
                    }
                    else
                    {
                        brs1_dlc_cnt++;
                        if (brs1_dlc_cnt >= brs1_dlc2)
                        {
                            can1_stf_cnt = 1;
                        }
                        if (brs1_dlc_cnt == brs1_dlc)
                        {
                            // CANFD期間のデータ領域、CRC領域をデコードしたらCAN期間に戻す
                            can1_phase = 3;
                            fdf1_flag = 0;
                            brs1_flag = 0;
                            can1_no_stuff_chk = 1;
                            can1_sample_cnt = can1_bit1_cnt;
                        }
                    }
                }
            }
            // 異常時または長いアイドル？
            else if (can1_phase == 5)
            {
                if(can1_val == 1)
                {
                    // ドミナントになるのを待つ
                    int i = 0;
                    int j = 0;
                    can1_packet[j] = -1;
                    can1_phase = 0;
                    arry1_cnt = 0;
                    fdf1_flag = 0;
                    brs1_flag = 0;
                    std1_dlc = 0;
                    can1_end_chk = 0;
                    can1_no_stuff_chk = 0;
                    iRet2 = 1;
                }
                else
                {
                    can1_stf_cnt = 1;
                }
            }
            RET_PHASE:
            return (iRet | iRet2);
        }

        /// <summary>
        /// SVImonCanでコールされMonitoring用CANデータデコーダー
        /// </summary>
        /// <param name="can_data_buf"></param>
        /// <param name="buf_size"></param>
        /// <param name="can0_packet"></param>
        /// <param name="can1_packet"></param>
        /// <returns></returns>
        public int Can_mon_decode(ref UInt16[] can_data_buf, uint buf_size, ref long[] can0_packet, ref long[] can1_packet)
        {
            int iRet = 0;
            int iCh0 = 0;
            int iCh1 = 0;
            int j = 0;
            uint buf_cnt = 0;
            int iCh0_Off = 40960 - 1000;
            int iCh1_Off = iCh0_Off;
            int iVal0 = 0;
            int iVal1 = 0;
            int iPhase0 = 0;
            int iPhase1 = 0;
            uint iCnt0Tgt = (uint)(can0_bit1_cnt * 10);
            uint iCnt1Tgt = (uint)(can1_bit1_cnt * 10);
            uint iCnt0 = 0;
            uint iCnt1 = 0;
            ushort usVal = 0;

            can_0_mon_pkt[0] = -1;
            can_1_mon_pkt[0] = -1;
            can0_packet[0] = -1;
            can1_packet[0] = -1;

            // 最初のアイドルを探す
            for (; buf_cnt < (buf_size >> 1); buf_cnt++)
            {
                iVal0 = (can_data_buf[buf_cnt] >> 14) & 0x1;
                iVal1 = (can_data_buf[buf_cnt] >> 15) & 0x1;
                if (iPhase0 == 0)
                {
                    if (iVal0 == 0)
                    {
                        iCnt0 = 0;
                    }
                    else
                    {
                        iCnt0++;
                        if (iCnt0 > iCnt0Tgt)
                        {
                            iCnt0Tgt = buf_cnt;
                            iPhase0 = 1;
                        }
                    }
                }
                if (iPhase1 == 0)
                {
                    if (iVal1 == 0)
                    {
                        iCnt1 = 0;
                    }
                    else
                    {
                        iCnt1++;
                        if (iCnt1 > iCnt1Tgt)
                        {
                            iCnt1Tgt = buf_cnt;
                            iPhase1 = 1;
                        }
                    }
                }
                if (iPhase0 == 1 && iPhase1 == 1) break;
            }

            // 最初のアイドルまでアイドル化する
            for (buf_cnt=0; buf_cnt < (buf_size >> 1); buf_cnt++)
            {
                usVal = can_data_buf[buf_cnt];
                if (buf_cnt < iCnt0Tgt)
                {
                    usVal |= 0x4000;
                }
                if (buf_cnt < iCnt1Tgt)
                {
                    usVal |= 0x8000;
                }
                can_data_buf[buf_cnt] = usVal;
                if (buf_cnt > iCnt0Tgt && buf_cnt > iCnt0Tgt) break;
            }

            for (buf_cnt = 0; buf_cnt < (buf_size >> 1); buf_cnt++)
            {
                if (buf_cnt == 237427)
                {
                    buf_cnt = 237427;
                }
                iRet = Can_serch_data(0, can_data_buf[buf_cnt], ref can_0_mon_pkt, ref can_1_mon_pkt);
                if ((iRet & 0x6) != 0)
                {
                    if (can_0_mon_pkt[0] != -1 && iCh0 < iCh0_Off)
                    {
                        for (j = 0; ; iCh0++, j++)
                        {
                            can0_packet[iCh0] = can_0_mon_pkt[j];
                            if (can_0_mon_pkt[j] == -1)
                            {
                                iCh0++;
                                break;
                            }
                        }
                    }
                    if (can_1_mon_pkt[0] != -1 && iCh1 < iCh1_Off)
                    {
                        for (j = 0; ; iCh1++, j++)
                        {
                            can1_packet[iCh1] = can_1_mon_pkt[j];
                            if (can_1_mon_pkt[j] == -1)
                            {
                                iCh1++;
                                break;
                            }
                        }
                    }
                }
            }
            iRet = 1;
            if (can0_packet[0] != -1) iRet = 2;
            if (can1_packet[0] != -1) iRet |= 4;
            return iRet;
        }
    }
}
