﻿namespace SVCanData
{
    using System;
    using System.Runtime.InteropServices;
    
    /// <summary>
    /// Frame I/O
    /// </summary>
    public class SvCanRecFrame : SVCanFileIO
    {
        #region フィールド
        /// <summary>
        /// 拡張子
        /// </summary>
        private const string ext = ".frame";

        /// <summary>
        /// サイズ
        /// </summary>
        private int recordSize = Marshal.SizeOf(typeof(Record));

        /// <summary>
        /// フレーム最大
        /// </summary>
        private Int32 frameMax;

        /// <summary>
        /// フレームデータ(ReadOpen時)
        /// </summary>
        private Record[] frameData;

        /// <summary>
        /// レコードデータ
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public class Record
        {
            /// <summary>
            /// 開始レコード
            /// </summary>
            public Int64 Start;
            /// <summary>
            /// 終了レコード
            /// </summary>
            public Int64 End;
            /// <summary>
            /// フレーム番号
            /// </summary>
            public Int32 FrameNo;
        }

        #endregion

        /// <summary>
        /// ファイル名を取得する
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static public string GetFileName(string file_name)
        {
            return System.IO.Path.ChangeExtension(file_name, ext);
        }

        #region 書き込み

        /// <summary>
        /// フレームファイルの書込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public override bool WriteOpen(string file_name)
        {
            // 書き込みファイルオープン
            if (!base.WriteOpen(GetFileName(file_name)))
            {
                return false;
            }
            this.frameMax = 0;
            return true;
        }

        #endregion


        #region 書き込み
        /// <summary>
        /// フレームファイルの読込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        /// <returns></returns>
        public override bool ReadOpen(string file_name)
        {
            // 書き込みファイルオープン
            if (!base.ReadOpen(GetFileName(file_name)))
            {
                return false;
            }

            // 総フレーム数の取得
            this.frameMax = (int)(this.binaryReader.BaseStream.Length / recordSize);

            // フレーム確保
            this.frameData = new Record[this.frameMax];
            for( int i=0; i < this.frameData.Length; i++)
            {
                this.frameData[i] = this.ReadFrom<Record>();
            }

            //　閉じる
            this.ReadClose();

            return true;
        }

        /// <summary>
        /// 指定されたフレーム番号のレコードを取得する
        /// </summary>
        /// <param name="frame_no">フレーム番号</param>
        /// <returns>取得したレコード</returns>
        public Record GetFrameRecord(int frame_no)
        {
            Int64 low = 0;
            Int64 high = this.frameData.Length - 1;

            while(low <= high)
            {
                // 中心位置
                Int64 im = (high + low) / 2;

                Record rec = this.frameData[im];

                if (rec.FrameNo == frame_no)
                {
                    return rec;
                }

                if (rec.FrameNo > frame_no)
                {
                    high = im - 1;
                }
                else
                {
                    low = im + 1;
                }
            }
            return null;
        }

        /// <summary>
        /// 指定されたレコード位置のフレームを取得する
        /// </summary>
        /// <param name="offset">オフセット位置</param>
        /// <returns>取得したレコード</returns>
        public Record GetRecord(Int64 offset)
        {
            Int64 low = 0;
            Int64 high = this.frameData.Length - 1;

            while (low <= high)
            {
                // 中心位置
                Int64 im = (high + low) / 2;

                Record rec = this.frameData[im];

                if (rec.Start >= offset && rec.End <= offset)
                {
                    return rec;
                }

                if (rec.Start > offset)
                {
                    high = im - 1;
                }
                else
                {
                    low = im + 1;
                }
            }
            return null;
        }

        #endregion

    }
}
