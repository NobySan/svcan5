﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVCanData
{
    /// <summary>
    /// CANレコード
    /// </summary>
    public class SvCanRecord : ICloneable
    {
        /// <summary>
        /// 経過時間(us)
        /// </summary>
        public string time;
        /// <summary>
        /// フレームタイプ
        /// </summary>
        public SvCanFrameType frame_type;
        /// <summary>
        /// ID
        /// </summary>
        public int id=0;
        /// <summary>
        /// DLC
        /// </summary>
        public int dlc=0;
        /// <summary>
        /// Data
        /// </summary>
        public byte[] data;
        /// <summary>
        /// CRC
        /// </summary>
        public ushort crc = 0;

        /// <summary>
        /// メンバーのクローン処理
        /// </summary>
        /// <returns></returns>
        object ICloneable.Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// dataメンバのダンプ
        /// </summary>
        public string dump_data
        {
            get
            {
                string ret = "";
                if (data != null)
                {
                    foreach( var s in data)
                    {
                        if ( ret != "")
                        {
                            ret += " ";
                        }
                        ret += s.ToString("X2");
                    }
                }
                return (ret);
            }
        }
    }

    /// <summary>
    /// フレームタイプ
    /// </summary>
    public enum SvCanFrameType
    {
        /// <summary>
        /// 標準
        /// </summary>
        Standard=0,
        /// <summary>
        /// 拡張
        /// </summary>
        Expansion,
        /// <summary>
        /// リモート
        /// </summary>
        Remote,
        /// <summary>
        /// IDLE状態
        /// </summary>
        IDLE,
        /// <summary>
        /// CAN-FD
        /// </summary>
        Standard_FD,
        /// <summary>
        /// CAN-FD2
        /// </summary>
        Standard_FD2,
    }

}
