﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVOgenCAN
{
    /// <summary>
    /// About Window
    /// </summary>
    public partial class AbotWindow : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AbotWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void AbotWindow_Load(object sender, EventArgs e)
        {
            string exe_name = System.IO.Path.GetFileName(Assembly.GetEntryAssembly().Location);
            labelVersion.Text = string.Format("{0} V{1}", exe_name, Application.ProductVersion); // ファイルバージョン
        }
    }
}
