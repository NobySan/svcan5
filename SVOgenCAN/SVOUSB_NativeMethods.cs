﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace svousb_dll
{
    /// <summary>
    /// DllImport属性を用いて定義したメソッドを格納するクラス
    /// </summary>
    public static class NativeMethods_SvoUsb
    {
        /// <summary>
        /// SVOUSB30 API戻り値定義
        /// </summary>
        public const UInt32 RET_NORMAL = 0x1;
        public const UInt32 RET_ERROR_DEVICE = 0xe0000000;
        public const UInt32 RET_ERROR_DEVOPEN = 0xf0000001;
        public const UInt32 RET_ERROR_MULTIOPEN = 0xf0000002;
        public const UInt32 RET_ERROR_NOOPEN = 0xf0000003;
        public const UInt32 RET_ERROR_PARAMETER = 0xf0000004;
        public const UInt32 RET_ERROR_SVO = 0xf1000000;

        /// <summary>
        /// SVOUSB30_GetBoardStatus用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct GET_BOARD_STATUS
        {
            public UInt32 ulHWVersion;          // FPGAのバージョン番号
            public UInt32 ulFWVersion;          // ファームウェアのバージョン番号
            public UInt32 ulOPStatus;           // 現在の動作モード
                                                // bit31    : エラー状態
                                                // bit30-05 : 予約（０）
                                                // bit04    : SDRAMに画像データあり
                                                // bit03-00 :
                                                // 　0x8　アップデート中
                                                // 　0x1　出力中
                                                // 　0x0　アイドル中
            public UInt32 ulHWEdition;          // FPGAのエディション番号
            public UInt32 ulH_Period;           // H有効PCLK数
            public UInt32 ulH_SyncStart;        // H同期開始PCLK数
            public UInt32 ulH_SyncEnd;          // H同期終了PCLK数
            public UInt32 ulDIP_SW2;            // DIPスイッチS2モニター
            public UInt32 ulV_Period;           // V有効ライン数
            public UInt32 ulV_SyncStart;        // V同期開始ライン数
            public UInt32 ulV_SyncEnd;          // V同期終了ライン数
            public UInt32 ulSyncInfo;           // 同期信号設定情報
                                                // bit31-02 : 予約（０）
                                                // bit01    : ピクセルクロック極性（0：↑、1:↓）
                                                // bit00    : 同期信号極性（0：Low Active、1:High Active）
            public UInt32 ulSyncMonitor;        // 同期信号モニター
                                                // bit31-08 : 予約（０）
                                                // bit07    : HSYNC (Output)
                                                // bit06    : VSYNC (Output)
                                                // bit05-03 : 予約（０）
                                                // bit02    : ターゲットボード電源情報（0：OFF、1:ON）
                                                // bit01    : 予約（０）
                                                // bit00    : ターゲットボードリセット情報（0：解除、1:リセット中）
            public UInt32 ulSdramInfo;          // SDRAM容量（128 or 256 単位MB）
            public UInt32 ulBoardVer;           // SVIボードバージョン番号（SVO-02：02h）
            public UInt32 ulPllClock;			// PLL Clock 設定値（単位KHｚ）
        }

        /// <summary>
        /// SVIUSB30_SetParamEx用構造体定義
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct SET_PARAM_EX
        {
            public UInt32 ulPixelFormat;    // MIPI CSI-2 Pixel Format
            public UInt32 ulDataRate;       // MIPI CSI-2 Data Rate
            public UInt32 ulLaneCount;      // MIPI CSI-2 Lane Count
            public UInt32 ulContinuousClk;  // MIPI CSI-2 Continuous Clock Mode
            public UInt32 ulCanMode;        // CANデータ出力モード
                                            //  bit31（0:CAN-0 Not sending, 1:CAN-0 Sending）
                                            //  bit30（0:CAN-1 Not sending, 1:CAN-1 Sending）
                                            //  bit29（0:画像データLSB詰め, 1:MSB詰め）
                                            //  bit28-bit00（予約）
        }

        /// <summary>
        /// SVOUSB30_Init API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_Init")]
        public static extern void Init();

        /// <summary>
        /// SVOUSB30_End API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_End")]
        public static extern void End();

        /// <summary>
        /// SVOUSB30_Open API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_Open")]
        public static extern UInt32 Open(UInt32 ulBoardNum);

        /// <summary>
        /// SVIUSB20_Close API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_Close")]
        public static extern UInt32 Close(UInt32 ulBoardNum);

        /// <summary>
        /// SVOUSB30_GetBoardStatus API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_GetBoardStatus")]
        public static extern UInt32 GetBoardStatus(UInt32 ulBoardNum, ref GET_BOARD_STATUS pStatus);

        /// <summary>
        /// SVOUSB30_GetOutputStatusEx API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_GetOutputStatusEx")]
        public static extern UInt32 GetOutputStatusEx(UInt32 ulBoardNum, ref UInt32 pulFrameNum, ref UInt32 pulTotalNum, ref UInt32 pulFreeMem);

        /// <summary>
        /// SVOUSB30_SetParamEx API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_SetParamEx")]
        public static extern UInt32 SetParamEx(UInt32 ulBoardNum, ref SET_PARAM_EX pParam);

        /// <summary>
        /// SVOUSB30_OfflineOutputStart API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_OfflineOutputStart")]
        public static extern UInt32 OfflineOutputStart(UInt32 ulBoardNum, UInt32 ulStartFrameNum, UInt32 ulEndFrameNum, UInt32 ulCycleCount);

        /// <summary>
        /// SVOUSB30_OfflineOutputStop API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_OfflineOutputStop")]
        public static extern UInt32 OfflineOutputStop(UInt32 ulBoardNum);

        /// <summary>
        /// SVOUSB30_OfflinePictureSendEx2 API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_OfflinePictureSendEx2")]
        public static extern UInt32 OfflinePictureSendEx2(UInt32 ulBoardNum, IntPtr lpFrameBuf, UInt32 ulLen, UInt32 ulAddr, UInt32 ulFlag, ref UInt32 pulRetLen);

        /// <summary>
        /// SVOUSB30_ReadReg API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_ReadReg")]
        public static extern UInt32 ReadReg(UInt32 ulBoardNum, UInt32 ulAddr, UInt32 ulDataWidth, ref UInt32 pulReadVal);

        /// <summary>
        /// SVOUSB30_WriteReg API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_WriteReg")]
        public static extern UInt32 WriteReg(UInt32 ulBoardNum, UInt32 ulAddr, UInt32 ulDataWidth, UInt32 ulWriteData);

        /// <summary>
        /// SVOUSB30_WriteReg_Burst API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_WriteReg_Burst")]
        public static extern UInt32 WriteReg_Burst(UInt32 ulBoardNum, UInt32 ulAddr, UInt32 ulRegNum, UInt32[] pulWriteData);

        /// <summary>
        /// SVOUSB30_GetVersion API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_GetVersion")]
        public static extern UInt32 GetVersion(byte[] pucVerBuf);

        /// <summary>
        /// SVOUSB30_EnumDevice API呼び出し定義
        /// </summary>
        [DllImport("SVO09USB30.dll", EntryPoint = "SVOUSB30_EnumDevice")]
        public static extern UInt32 EnumDevice(ref UInt32 pulSVO_Num, UInt32[] pulSVO_NumTable);

    }
}
