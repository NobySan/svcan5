﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVOgenCAN
{
    /// <summary>
    /// メインウィンドウ
    /// </summary>
    public partial class MainWindow : Form
    {
        /// <summary>
        /// CAN Window 0
        /// </summary>
        CanWindow.CanWindow mCanWindow0 = new CanWindow.CanWindow(Program.mEnv, 0);
        /// <summary>
        /// CAN Window 1
        /// </summary>
        CanWindow.CanWindow mCanWindow1 = new CanWindow.CanWindow(Program.mEnv, 1);
        /// <summary>
        /// ダンプウィンドウ
        /// </summary>
        DumpWindow.DumpWindow mDumpWindow = new DumpWindow.DumpWindow(Program.mEnv);
        /// <summary>
        /// 波形表示ウィンドウ
        /// </summary>
        EasyLogicAnalyzerWindow.EasyLogicAnalyzerWindow mEasyLogicAnalyzerWindow = new EasyLogicAnalyzerWindow.EasyLogicAnalyzerWindow(Program.mEnv);
        /// <summary>
        /// ジャンプウィンドウ
        /// </summary>
        Jump mJump = new Jump();
        /// <summary>
        /// プレイウィンドウ(通常)
        /// </summary>
        PlayControl mPlayControl = new PlayControl();

        /// <summary>
        /// CAN Data
        /// </summary>
        SVCanData.SvCanData mSVCanData = null;
        /// <summary>
        /// フレームビットマップ
        /// </summary>
        IntPtr mFrameBmp = IntPtr.Zero;
        /// <summary>
        /// 縮尺値
        /// </summary>
        double mScale = 1.0;
        /// <summary>
        /// フレームサイズ
        /// </summary>
        Size mFrameSize;
        /// <summary>
        /// 表示フレーム位置
        /// </summary>
        int mFramePos = 0;
        /// <summary>
        /// 表示中止
        /// </summary>
        bool mRunAbort = false;

        /// <summary>
        /// ファイルのみを開いた場合はtrue,通常false
        /// </summary>
        bool mOpenOnly = false;

        /// <summary>
        /// Width Height フォーマット
        /// </summary>
        const string STATUS_SIZE_FORMAT = "Width : {0} Height : {1}";
        /// <summary>
        /// FPS フォーマット
        /// </summary>
        const string STATUS_FPS_FORMAT = "fps : {0:N2}/{1:N2}";
        /// <summary>
        /// fpos フォーマット
        /// </summary>
        const string STATUS_FPOS_FORMAT = "fpos : {0}";
        /// <summary>
        /// fmax フォーマット
        /// </summary>
        const string STATUS_FMAX_FORMAT = "fmax : {0}";

        /// <summary>
        /// 実行中モード
        /// </summary>
        enum RunningMode
        {
            eSTOP = 0,
            eRUNNING,
        }
        /// <summary>
        /// 実行モード
        /// </summary>
        RunningMode mRunningMode = RunningMode.eSTOP;

        /// <summary>
        /// 再表示用共有ロック
        /// </summary>
        object mPaintLock = new object();

        /// <summary>
        /// ピクチャビットマップ
        /// </summary>
        SVUtil.DIBitmap mPictureBmp = null;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MainWindow_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;

            // CSVファイルイベント
            mCanWindow0.CanCSVEvent += MCanWindowX_CanCSVEvent;
            mCanWindow0.SetContentMenu();
            mCanWindow1.CanCSVEvent += MCanWindowX_CanCSVEvent;
            mCanWindow1.SetContentMenu();
            mEasyLogicAnalyzerWindow.FramePosEvent += MEasyLogicAnalyzerWindow_FramePosEvent;

            mJump.JumpEvent += Jump_JumpEvent;

            statusLabelFPS.Text = string.Format(STATUS_FPS_FORMAT, 0.0, Program.mEnv.mPlayOption.mDisplayFPS);
        }


        /// <summary>
        /// 閉じる処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 実行中はWindowを閉じない
            if (this.mRunningMode == RunningMode.eRUNNING)
            {
                e.Cancel = true;
                return;
            }

            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }

            if (mEasyLogicAnalyzerWindow.Visible) mEasyLogicAnalyzerWindow.Close();
            if (mDumpWindow.Visible) mDumpWindow.Close();
            if (mCanWindow0.Visible) mCanWindow0.Close();
            if (mCanWindow1.Visible) mCanWindow1.Close();
            if (mJump.Visible) mJump.Close();
            if (mPlayControl.Visible) mPlayControl.Close();

            Program.mEnv.SetWindowRect(this.Name, rc);

            e.Cancel = false;
        }

        /// <summary>
        /// データ読込有無
        /// </summary>
        /// <returns>true:読込中 false:読込無</returns>
        bool IsLoadData
        {
            get
            {
                if (mSVCanData != null) return (true);
                return (false);
            }
        }

        /// <summary>
        /// ファイル操作確認
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFile_DropDownOpened(object sender, EventArgs e)
        {
            bool enable = IsLoadData;

            menuFileOpen.Enabled = true;
            menuFileOption.Enabled = true;

            // 読み込まれている場合且つ実行中はカスケード
            if (enable)
            {
                if (mRunningMode != RunningMode.eSTOP || mPlayControl.Visible)
                {
                    enable = false;
                    menuFileOpen.Enabled = enable;
                    menuFileOption.Enabled = enable;
                    menuOpenPlayOnly.Enabled = enable;
                }
            }

            menuFileExit.Enabled = enable;
            menuFileClipToFile.Enabled = enable;

            if (this.mOpenOnly)
            {
                menuFileClipToFile.Enabled = false;
            }
        }

        /// <summary>
        /// Preview操作
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreView_DropDownOpened(object sender, EventArgs e)
        {
            bool enable = IsLoadData;

            // 読み込まれている場合且つ実行中はカスケード
            if (enable)
            {
                if (mRunningMode != RunningMode.eSTOP || (mPlayControl.Visible))
                {
                    enable = false;
                }
            }
            menuPreviewEnd.Enabled = enable;
            menuPreviewJump.Enabled = enable;
            menuPreviewNext.Enabled = enable;
            menuPreviewPrev.Enabled = enable;
            menuPreviewRun.Enabled = enable;
            menuPreviewStop.Enabled = enable;
            menuPreviewTop.Enabled = enable;

            if (mRunningMode == RunningMode.eRUNNING)
            {
                menuPreviewStop.Enabled = true;
            }
        }

        /// <summary>
        /// Play操作
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPlay_DropDownOpened(object sender, EventArgs e)
        {
            bool enable = IsLoadData;
            if (enable)
            {
                if (mRunningMode != RunningMode.eSTOP)
                {
                    enable = false;
                }
            }
            if (enable)
            {
                menuPlayStart.Enabled = !(mPlayControl.Visible);
                menuPlayStop.Enabled = false; //(mPlayControl.IsRunning || mPlayControl2.IsRunning);
            }
            else
            {
                menuPlayStart.Enabled = enable;
                menuPlayStop.Enabled = false;// enable;
            }
        }

        /// <summary>
        /// ツール操作
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuTool_DropDownOpened(object sender, EventArgs e)
        {
            bool enable = IsLoadData;
            menuToolEasyLogicAnalyzer.Enabled = enable;
            menuToolCanCH0Window.Enabled = enable;
            menuToolCanCH1Window.Enabled = enable;
            menuToolDump.Enabled = enable;
        }

        /// <summary>
        /// 再表示処理
        /// </summary>
        /// <param name="g">グラフィック</param>
        void RedrawBitmap(Graphics g)
        {
            // 排他ロック
            lock (mPaintLock)
            {
                IntPtr dc = Win32.User32.GetDC(IntPtr.Zero);
                IntPtr src = Win32.Gdi32.CreateCompatibleDC(dc);
                IntPtr dst = g.GetHdc();

                // ビットマップを張り付ける
                if (mScale < 1.0)
                {
                    Win32.Gdi32.SetStretchBltMode(dst, Win32.Gdi32.StrechModeFlags.HALFTONE);
                }
                Win32.Gdi32.SelectObject(src, mFrameBmp);
                Win32.Gdi32.StretchBlt(dst, 0, 0, (int)Math.Round(mFrameSize.Width * mScale), (int)Math.Round(mFrameSize.Height * mScale),
                                        src, 0, 0, mFrameSize.Width, mFrameSize.Height, Win32.Gdi32.TernaryRasterOperations.SRCCOPY);

                Win32.Gdi32.DeleteDC(src);
                g.ReleaseHdc(dst);

                Win32.User32.ReleaseDC(IntPtr.Zero, dc);
            }
        }

        /// <summary>
        /// ファイルオープン
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.FileName = "*.dat";
                dlg.Filter = "dat file(*.dat)|*.dat|All Files(*.*)|*.*";
                dlg.FilterIndex = 1;
                dlg.CheckFileExists = true; // 存在しないパスはエラー
                dlg.CheckPathExists = true;
                if ( dlg.ShowDialog() == DialogResult.OK)
                {
                    if (mSVCanData!=null)
                    {
                        mSVCanData.Close();
                        if (mEasyLogicAnalyzerWindow.mSvCanDat!= null)
                        {
                            mEasyLogicAnalyzerWindow.mSvCanDat.Close();
                        }
                    }

                    // 読込成功
                    mSVCanData = new SVCanData.SvCanData();
                    mSVCanData.SetBitmapMode(Program.mEnv.mPlayOption.mDisplayReverse, Program.mEnv.mPlayOption.mDisplayLsbPack);

                    int[] threshold = new int[] { Program.mEnv.mPlayOption.mCanThreshold1, Program.mEnv.mPlayOption.mCanThreshold2};
                    int[] bps = new int[] { Program.mEnv.mPlayOption.mCanbps, Program.mEnv.mPlayOption.mCanFDbps };

                    if (!mSVCanData.Open(dlg.FileName, threshold, bps))
                    {
                        //                        pictureBox.Image = new Bitmap(panelView.Width,panelView.Height);
                        this.Text = "SVOgenCAN [no data]";
                        Win32.SVMessageBox.Show(this, mSVCanData.ErrMessage, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        mSVCanData = null;
                    }
                    else
                    {
                        // ビットマップ等
                        mFramePos = 0;
                        mFrameSize = mSVCanData.FrameSize;
                        mFrameBmp = mSVCanData.GetFrameData(0);

                        mEasyLogicAnalyzerWindow.mSvCanDat = mSVCanData.Clone();

                        mPictureBmp = new SVUtil.DIBitmap();
                        mPictureBmp.CreateDIB((int)Math.Round(mFrameSize.Width * mScale), (int)Math.Round(mFrameSize.Height * mScale), 8);

                        pictureBox.Width = (int)Math.Round(mFrameSize.Width * mScale);
                        pictureBox.Height= (int)Math.Round(mFrameSize.Height * mScale);

                        statusLabelSize.Text = string.Format(STATUS_SIZE_FORMAT, mFrameSize.Width, mFrameSize.Height);
                        statusLabelFPS.Text = string.Format(STATUS_FPS_FORMAT, mSVCanData.FPS, Program.mEnv.mPlayOption.mDisplayFPS);
                        statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                        statusLabelFMAX.Text = string.Format(STATUS_FMAX_FORMAT, mSVCanData.FrameMax);

                        mCanWindow0.mFrameMax = mSVCanData.FrameMax;
                        mCanWindow1.mFrameMax = mSVCanData.FrameMax;

                        // CANレコードの設定
                        if (mSVCanData.mCan0Record != null)
                        {
                            mCanWindow0.SetCanRecord(mSVCanData.mCan0Record);
                        }
                        if (mSVCanData.mCan1Record != null)
                        {
                            mCanWindow1.SetCanRecord(mSVCanData.mCan1Record);
                        }

                        mEasyLogicAnalyzerWindow.SetupGraph();

                        this.Text = string.Format("SVOgenCAN [{0}]", dlg.FileName);

                        pictureBox.Refresh();

                        this.menuPreView.Enabled = true;
                        this.menuTool.Enabled = true;
                        this.mOpenOnly = false;
                    }
                }
            }
        }

        /// <summary>
        /// 画像のスケール
        /// </summary>
        void ScalePicture()
        {
            // ロック
            lock (mPaintLock)
            {
                pictureBox.Width = (int)Math.Round(mFrameSize.Width * mScale);
                pictureBox.Height = (int)Math.Round(mFrameSize.Height * mScale);
                pictureBox.Refresh();
            }
        }

        /// <summary>
        /// 1/4縮尺
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_4_Click(object sender, EventArgs e)
        {
            mScale = 1.0 / 4.0;
            ScalePicture();
        }

        /// <summary>
        /// 1/2縮尺
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_2_Click(object sender, EventArgs e)
        {
            mScale = 1.0 / 2.0;
            ScalePicture();
        }

        /// <summary>
        /// 1/1
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_1_Click(object sender, EventArgs e)
        {
            mScale = 1;
            ScalePicture();
        }

        /// <summary>
        /// 2倍拡大
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_2_Click(object sender, EventArgs e)
        {
            mScale = 2;
            ScalePicture();
        }

        /// <summary>
        /// 4倍拡大
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuZoom_4_Click(object sender, EventArgs e)
        {
            mScale = 4;
            ScalePicture();
        }

        /// <summary>
        /// 各種ウィンドウの更新
        /// </summary>
        public void UpdateOtherWindow()
        {
            // Dump Window更新
            if (mDumpWindow.Visible)
            {
                mDumpWindow.SetDumpBuffer(mSVCanData.GetDumpData(mFramePos));
            }
            // Easy frame
            if (mEasyLogicAnalyzerWindow.Visible)
            {
                mEasyLogicAnalyzerWindow.SetFrame(mFramePos);
            }
        }

        /// <summary>
        /// CANウィンドウの更新
        /// </summary>
        public void UpdateCanWindow()
        {
            if (mCanWindow0.Visible)
            {
                if (mSVCanData.mCan0Record != null)
                {
                    mCanWindow0.SetCanRecord(mSVCanData.mCan0Record);
                }
            }
            if (mCanWindow1.Visible)
            {
                if (mSVCanData.mCan1Record != null)
                {
                    mCanWindow1.SetCanRecord(mSVCanData.mCan1Record);
                }
            }
        }

        /// <summary>
        /// 次フレーム
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewNext_Click(object sender, EventArgs e)
        {
            if ((mFramePos+1) < mSVCanData.FrameMax)
            {
                mFrameBmp = mSVCanData.GetFrameData(++mFramePos);
                // Other Window Update
                UpdateOtherWindow();
                // CAN Window Update
                UpdateCanWindow();
                statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                pictureBox.Refresh();
            }
        }

        /// <summary>
        /// 前フレーム
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewPrev_Click(object sender, EventArgs e)
        {
            if (mFramePos>0)
            {
                mFrameBmp = mSVCanData.GetFrameData(--mFramePos);
                // Other Window Update
                UpdateOtherWindow();
                // CAN Window Update
                UpdateCanWindow();
                statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                pictureBox.Refresh();
            }
        }

        /// <summary>
        /// トップへ戻る
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewTop_Click(object sender, EventArgs e)
        {
            mFramePos = 0;
            mFrameBmp = mSVCanData.GetFrameData(mFramePos);
            // Other Window Update
            UpdateOtherWindow();
            // CAN Window Update
            UpdateCanWindow();
            statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
            pictureBox.Refresh();
        }

        /// <summary>
        /// 最後のフレーム
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewEnd_Click(object sender, EventArgs e)
        {
            mFramePos = mSVCanData.FrameMax-1;
            mFrameBmp = mSVCanData.GetFrameData(mFramePos);
            // Other Window Update
            UpdateOtherWindow();
            // CAN Window Update
            UpdateCanWindow();
            statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
            pictureBox.Refresh();
        }

        /// <summary>
        /// 再生表示(RUN)
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewRun_Click(object sender, EventArgs e)
        {
            mRunAbort = false;
            //
            // フレームリストスレッド作成
            //
            System.Threading.Tasks.Task.Run(() =>
            {
                DateTime start = DateTime.Now;
                DateTime now_time;
                TimeSpan sp;
                int one_frame_time = (int)Math.Round(1.0 / Program.mEnv.mPlayOption.mDisplayFPS * 1000);
                int cnt = 0;

                // 実行中
                mRunningMode = RunningMode.eRUNNING;

                using (var g = pictureBox.CreateGraphics())
                {
                    now_time = DateTime.Now;

                    // 開始
                    while (!mRunAbort)
                    {
                        // フレーム数カウントアップ
                        mFramePos++;
                        cnt++;
                        if (mFramePos >= mSVCanData.FrameMax)
                        {
                            mFramePos = 0;
                        }
                        mFrameBmp = mSVCanData.GetFrameData(mFramePos);

                        sp = DateTime.Now - now_time;
                        if ( sp.TotalMilliseconds < one_frame_time)
                        {
                            System.Threading.Thread.Sleep(one_frame_time - (int)sp.TotalMilliseconds);
                        }

                        now_time = DateTime.Now;

                        // ウィンドウ表示更新処理
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            // 再表示
                            RedrawBitmap(g);

                            statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);

                            // Other Window Update
                            UpdateOtherWindow();

                            // Can Data Update
                            UpdateCanWindow();
                        });

                        if (mFramePos == 0) break;
                    }
                }
                mRunningMode = RunningMode.eSTOP;
            }
            );
        }

        /// <summary>
        /// 停止処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewStop_Click(object sender, EventArgs e)
        {
            mRunAbort = true;
        }

        /// <summary>
        /// 表示イベント
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (mPictureBmp != null)
            {
                // 再表示
                RedrawBitmap(e.Graphics);
            }
        }

        /// <summary>
        /// ダンプ表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolDump_Click(object sender, EventArgs e)
        {
            if (!mDumpWindow.Visible)
            {
                mDumpWindow.Show();
            }
            if (IsLoadData)
            {
                mDumpWindow.SetDumpBuffer(mSVCanData.GetDumpData(mFramePos));
            }
        }

        /// <summary>
        /// クリップファイル
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileClipToFile_Click(object sender, EventArgs e)
        {
            using (var dlg = new ClipFile(mSVCanData))
            {
                dlg.ShowDialog();
            }
        }

        /// <summary>
        /// EasyLogicAnalyzer表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolEasyLogicAnalyzer_Click(object sender, EventArgs e)
        {
            if (mEasyLogicAnalyzerWindow.Visible)
            {
            }
            else
            {
                mEasyLogicAnalyzerWindow.Show();
            }
        }

        /// <summary>
        /// CAN0表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolCanCH0Window_Click(object sender, EventArgs e)
        {
            if (mCanWindow0.Visible)
            {
            }
            else
            {
                mCanWindow0.Show();
            }
        }

        /// <summary>
        /// CAN1表示
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuToolCanCH1Window_Click(object sender, EventArgs e)
        {
            if (mCanWindow1.Visible)
            {
            }
            else
            {
                mCanWindow1.Show();
            }
        }

        /// <summary>
        /// Can Window より出力処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MCanWindowX_CanCSVEvent(object sender, CanWindow.CanCSVEventArgs e)
        {
            int frame_start = 0, frame_end = 0;

            switch( e.mode)
            {
                case CanWindow.CanCSVEventArgs.CanCSVMode.AllTime:
                    frame_start = 0;
                    frame_end = mSVCanData.FrameMax - 1;
                    break;
                case CanWindow.CanCSVEventArgs.CanCSVMode.AnyTime:
                    frame_start = e.frame_start-1;
                    frame_end = e.frame_end-1;
                    break;
                case CanWindow.CanCSVEventArgs.CanCSVMode.Current:
                    frame_start = mFramePos;
                    frame_end = mFramePos;
                    break;
            }

            try
            {
                List<SVCanData.SvCanRecord> can_record = new List<SVCanData.SvCanRecord>();
                using ( var sw = new System.IO.StreamWriter(e.file_name, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {

                    sw.WriteLine("Time,Frame Type,ID,DLC,Data,CRC(h)");

                    for( int frame_no=frame_start; frame_no <= frame_end; frame_no++)
                    {
                        mSVCanData.GetCanData(frame_no, e.can_no, can_record);

                        for( int i=0; i < can_record.Count; i++)
                        {
                            sw.WriteLine("{0},{1},{2},{3},{4},{5}",
                                can_record[i].time,
                                can_record[i].frame_type,
                                can_record[i].id,
                                can_record[i].dlc,
                                can_record[i].dump_data,
                                can_record[i].crc.ToString("X4")
                            );
                        }
                    }
                }
                Win32.SVMessageBox.Show((Form)sender, "Can Data CSV Output!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                string err = string.Format("Can Data CSV Output Error!\n{0}", ex.Message);
                Win32.SVMessageBox.Show((Form)sender, err, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// オプション
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileOption_Click(object sender, EventArgs e)
        {
            using (var dlg = new PlayOption())
            {
                dlg.mPlayOption = Program.mEnv.mPlayOption.Clone() as PlayOptionData;
                if ( dlg.ShowDialog() == DialogResult.OK )
                {
                    Program.mEnv.mPlayOption = dlg.mPlayOption;

                    if (mSVCanData != null)
                    {
                        mSVCanData.SetBitmapMode(Program.mEnv.mPlayOption.mDisplayReverse, Program.mEnv.mPlayOption.mDisplayLsbPack);
                        if(!this.mOpenOnly) mEasyLogicAnalyzerWindow.mSvCanDat.SetBitmapMode(Program.mEnv.mPlayOption.mDisplayReverse, Program.mEnv.mPlayOption.mDisplayLsbPack);
                        mFrameBmp = mSVCanData.GetFrameData(mFramePos);
                        UpdateCanWindow();
                        pictureBox.Refresh();
                        statusLabelFPS.Text = string.Format(STATUS_FPS_FORMAT, mSVCanData.FPS, Program.mEnv.mPlayOption.mDisplayFPS);
                    }
                    else
                    {
                        statusLabelFPS.Text = string.Format(STATUS_FPS_FORMAT, 0.0, Program.mEnv.mPlayOption.mDisplayFPS);
                    }
                }
            }
        }

        /// <summary>
        /// ヘルプ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuHelpAbout_Click(object sender, EventArgs e)
        {
            using (var dlg = new AbotWindow())
            {
                dlg.ShowDialog();
            }
        }


        /// <summary>
        /// ジャンプ処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPreviewJump_Click(object sender, EventArgs e)
        {
            if (mSVCanData != null)
            {
                mJump.FrameMax = mSVCanData.FrameMax;
                mJump.FramePos = mFramePos + 1;

                if (!mJump.Visible)
                {
                    mJump.Show();
                }
                mJump.BringToFront();
            }
        }

        /// <summary>
        /// ジャンプイベント
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void Jump_JumpEvent(object sender, JumpEventArgs e)
        {
            if (mSVCanData != null)
            {
                mFramePos = e.FrameNo - 1;
                mSVCanData.SetBitmapMode(Program.mEnv.mPlayOption.mDisplayReverse, Program.mEnv.mPlayOption.mDisplayLsbPack);
                mFrameBmp = mSVCanData.GetFrameData(mFramePos);
                // Other Window Update
                UpdateOtherWindow();
                // Can Window Update
                UpdateCanWindow();
                statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                pictureBox.Refresh();
            }
        }

        /// <summary>
        /// プレイコントロール開始処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPlayStart_Click(object sender, EventArgs e)
        {
            // データポインタの設定
            this.mPlayControl.mSVCanData = mSVCanData;
            // 非表示なら表示
            if (!this.mPlayControl.Visible)
            {
                this.mPlayControl.Show(this);
            }
        }

        /// <summary>
        /// プレイコントロール停止処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuPlayStop_Click(object sender, EventArgs e)
        {
            mPlayControl.Stop();
        }

        /// <summary>
        /// 画像範囲内でのマウスクリック処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            // マウス左ボタンクリック
            if (e.Button == MouseButtons.Left)
            {
                if (mSVCanData != null && mEasyLogicAnalyzerWindow.Visible )
                {
                    int dump_pos;
                    mEasyLogicAnalyzerWindow.SetFrameXY(mFramePos, new Point((int)(e.X/mScale), (int)(e.Y/mScale)), out dump_pos);
                    if ( mDumpWindow.Visible )
                    {
                        mDumpWindow.SetPosition(dump_pos);
                    }
                }

                int y_pixel = (int)(e.Y / mScale);

                // CAN暫定
                if (this.mCanWindow0.Visible && this.mSVCanData.mCan0Record.Count != 0)
                {
                    this.mCanWindow0.SelectIndex = (int)((y_pixel / (double)mFrameSize.Height) * this.mSVCanData.mCan0Record.Count);
                }
                if (this.mCanWindow1.Visible && this.mSVCanData.mCan1Record.Count != 0)
                {
                    this.mCanWindow1.SelectIndex = (int)((y_pixel / (double)mFrameSize.Height) * this.mSVCanData.mCan1Record.Count);
                }
            }
        }

        /// <summary>
        /// フレーム表示変更
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void MEasyLogicAnalyzerWindow_FramePosEvent(object sender, EasyLogicAnalyzerWindow.FramePosEventHandlerArgs e)
        {
            if ( mFramePos != e.mFrameNo)
            {
                mFramePos = e.mFrameNo;
                mFrameBmp = mSVCanData.GetFrameData(mFramePos);
                UpdateCanWindow();
                statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                pictureBox.Refresh();
                // Dump Window更新
                if (mDumpWindow.Visible)
                {
                    mDumpWindow.SetDumpBuffer(mSVCanData.GetDumpData(mFramePos));
                }
            }
        }

        /// <summary>
        /// Open Play Only 処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuOpenPlayOnly_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.FileName = "*.*";
                dlg.Filter = "Supported Format(*.dat,*.lst,*.txt)|*.dat;*.lst;*.txt|All Files(*.*)|*.*";
                dlg.FilterIndex = 1;
                dlg.CheckFileExists = true; // 存在しないパスはエラー
                dlg.CheckPathExists = true;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    this.mOpenOnly = false;

                    if (this.mSVCanData != null)
                    {
                        this.mSVCanData.Close();
                        if (this.mEasyLogicAnalyzerWindow.mSvCanDat != null)
                        {
                            this.mEasyLogicAnalyzerWindow.mSvCanDat.Close();
                        }
                        if (this.mEasyLogicAnalyzerWindow.Visible)
                        {
                            this.mEasyLogicAnalyzerWindow.Hide();
                        }
                        if (this.mDumpWindow.Visible)
                        {
                            this.mDumpWindow.Hide();
                        }
                        if (this.mCanWindow0.Visible)
                        {
                            this.mCanWindow0.Hide();
                        }
                        if (this.mCanWindow1.Visible)
                        {
                            this.mCanWindow1.Hide();
                        }
                    }

                    this.mSVCanData = null;

                    LstFile.LstFileInfo lstFileInfo = null;
                    List<string> datFiles = null;
                    string ext = System.IO.Path.GetExtension(dlg.FileName).ToUpper();

                    if (ext == ".DAT")
                    {
                        datFiles = new List<string>();
                        datFiles.Add(dlg.FileName);
                    }
                    else
                    {
                        string msg;

                        LstFile.STATUS sts = LstFile.GetListInfo(dlg.FileName, out lstFileInfo, out msg);
                        if (sts != LstFile.STATUS.OK)
                        {
                            Win32.SVMessageBox.Show(this, msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            // SVUtil.Log.Write(msg);
                            return;
                        }
                        datFiles = lstFileInfo.DatList;
                    }

                    // 読込成功
                    mSVCanData = new SVCanData.SvCanData();
                    mSVCanData.SetBitmapMode(Program.mEnv.mPlayOption.mDisplayReverse, Program.mEnv.mPlayOption.mDisplayLsbPack);

                    if (!mSVCanData.OpenNone(datFiles))
                    {
                        //                        pictureBox.Image = new Bitmap(panelView.Width,panelView.Height);
                        this.Text = "SVOgenCAN [no data]";
                        Win32.SVMessageBox.Show(this, mSVCanData.ErrMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        // SVUtil.Log.Write(mSVCanData.ErrMessage);
                        mSVCanData = null;
                        return;
                    }
                    else
                    {
                        // ビットマップ等
                        this.mFramePos = 0;
                        this.mFrameSize = new Size(this.mSVCanData.Header.picture_width, this.mSVCanData.Header.picture_height);
                        mFrameBmp = mSVCanData.GetFrameData(0);

                        mPictureBmp = new SVUtil.DIBitmap();
                        mPictureBmp.CreateDIB((int)Math.Round(mFrameSize.Width * mScale), (int)Math.Round(mFrameSize.Height * mScale), 8);

                        pictureBox.Width = (int)Math.Round(mFrameSize.Width * mScale);
                        pictureBox.Height = (int)Math.Round(mFrameSize.Height * mScale);

                        statusLabelSize.Text = string.Format(STATUS_SIZE_FORMAT, mFrameSize.Width, mFrameSize.Height);
                        statusLabelFPS.Text = string.Format(STATUS_FPS_FORMAT, mSVCanData.FPS, Program.mEnv.mPlayOption.mDisplayFPS);
                        statusLabelFPOS.Text = string.Format(STATUS_FPOS_FORMAT, mFramePos + 1);
                        statusLabelFMAX.Text = string.Format(STATUS_FMAX_FORMAT, mSVCanData.FrameMax);

                        this.Text = string.Format("SVOgenCAN [{0}]", dlg.FileName);

                        pictureBox.Refresh();

                        this.menuPreView.Enabled = false;
                        this.menuTool.Enabled = false;
                        this.mOpenOnly = true;
                    }
                }
            }
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void menuFileExit_Click(object sender, EventArgs e)
        {
            // 実行中はWindowを閉じない
            if (this.mRunningMode == RunningMode.eRUNNING)
            {
                return;
            }
            this.Close();
        }
    }
}
