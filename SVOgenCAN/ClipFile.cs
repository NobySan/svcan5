﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVOgenCAN
{
    /// <summary>
    /// クリップファイル
    /// </summary>
    public partial class ClipFile : Form
    {
        /// <summary>
        /// データポインタ
        /// </summary>
        public SVCanData.SvCanData mVCanData;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ClipFile(SVCanData.SvCanData data)
        {
            mVCanData = data;
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void ClipFile_Load(object sender, EventArgs e)
        {
            textFileName.Text = DateTime.Now.ToString("yyyyMMddHHmmss") + ".dat";

            numericStartFrame.Maximum = mVCanData.FrameMax;
            numericEndFrame.Maximum = mVCanData.FrameMax;
        }

        /// <summary>
        /// ファイル保存
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonRefer_Click(object sender, EventArgs e)
        {
            // ファイルを開く
            using (var dlg = new SaveFileDialog())
            {
                dlg.FileName = textFileName.Text;
                dlg.DefaultExt = ".dat";
                dlg.Filter = "SVI documents (.dat)|*.dat";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textFileName.Text = dlg.FileName;
                }
            }
        }

        /// <summary>
        /// 実行処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonExecute_Click(object sender, EventArgs e)
        {
            // ファイル名が存在しない
            if (textFileName.Text == string.Empty)
            {
                Win32.SVMessageBox.Show(this, "Save filename Empty.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // フレームの開始～終了が反転している
            if (numericStartFrame.Value > numericEndFrame.Value)
            {
                Win32.SVMessageBox.Show(this, "Set the Start frame smaller than the End frame.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            //
            // 書込みスレッド作成
            //
            System.Threading.Tasks.Task.Run(() =>
            {
                string msg;
                if ( mVCanData.Clip2File(textFileName.Text, (int)numericStartFrame.Value-1, (int)numericEndFrame.Value-1, ProgressPos, out msg) )
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "clip complete!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    });
                }
                else
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, string.Format("clip error!\r\n{0}", msg), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    });

                }

            });
        }

        /// <summary>
        /// 進捗表示
        /// </summary>
        /// <param name="pos">進捗位置</param>
        private void ProgressPos(int pos)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                progressBar.Value = pos;
            });
        }
    }
}
