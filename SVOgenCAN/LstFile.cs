﻿namespace SVOgenCAN
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    /// <summary>
    /// .lst,.txt解析
    /// </summary>
    public static class LstFile
    {

        /// <summary>
        /// リストファイル情報
        /// </summary>
        public class LstFileInfo
        {
            /// <summary>
            /// .datリスト一覧
            /// </summary>
            public List<string> DatList = new List<string>();

            /// <summary>
            /// AVIリストファイル
            /// </summary>
            public Dictionary<int, string> AviFiles = new Dictionary<int, string>();
        }

        /// <summary>
        /// 処理結果
        /// </summary>
        public enum STATUS : int
        {
            /// <summary>
            /// エラー無し
            /// </summary>
            OK = 0,
            /// <summary>
            /// ファイルオープンエラー
            /// </summary>
            FILE_OPEN,
            /// <summary>
            /// DATファイル無し
            /// </summary>
            DAT_ERROR,
            /// <summary>
            /// AVIファイル無し
            /// </summary>
            AVI_ERROR,
            /// <summary>
            /// プログラムエラー
            /// </summary>
            PROGRAM_ERROR,
        }

        /// <summary>
        /// 指定された(lst or txt)ファイルを解析し、設定する
        /// </summary>
        /// <param name="fileName">解析ファイル名</param>
        /// <param name="lstFileInfo">解析結果</param>
        /// <param name="message">エラー時のメッセージ</param>
        /// <returns></returns>
        public static STATUS GetListInfo(string fileName, out LstFileInfo lstFileInfo, out string message)
        {
            Dictionary<int, List<string>> aviList = new Dictionary<int, List<string>>();

            lstFileInfo = new LstFileInfo();
            message = string.Empty;

            // 解析ファイルを開く
            if (!System.IO.File.Exists(fileName))
            {
                message = string.Format("Error : The specified file [{0}] does not exist.", fileName);
                return STATUS.FILE_OPEN;
            }

            // 作業パス変更
            string save_folder = System.Environment.CurrentDirectory;
            System.Environment.CurrentDirectory = System.IO.Path.GetDirectoryName(fileName);

            try
            {
                // シフトJISで読む
                string[] texts = System.IO.File.ReadAllLines(fileName, System.Text.Encoding.GetEncoding("shift_jis"));

                int mode = -1;

                foreach (var text in texts)
                {
                    // 文字列無し
                    if (text.Length == 0)
                    {
                        continue;
                    }

                    // コメント
                    if (text[0] == ';')
                    {
                        continue;
                    }

                    if (text[0] == '[')
                    {
                        switch(text.Trim().ToUpper())
                        {
                            case "[CH0]": mode = 0; break;
                            case "[CH1]": mode = 1; break;
                            case "[CH2]": mode = 2; break;
                            case "[CH3]": mode = 3; break;
                            case "[DAT]": mode = 1000; break;
                        }
                        continue;
                    }

                    // DAT?
                    if (mode == 1000)
                    {
                        string filename = System.IO.Path.GetFullPath(text);

                        // 解析ファイルを開く
                        if (!System.IO.File.Exists(filename))
                        {
                            message = string.Format("Error : The DAT file [{0}] does not exist.", text);
                            return STATUS.FILE_OPEN;
                        }
                        lstFileInfo.DatList.Add(filename);
                    }
                    else if (mode >=0 && mode <= 3)
                    {
                        string filename = System.IO.Path.GetFullPath(text);

                        // 解析ファイルを開く
                        if (!System.IO.File.Exists(filename))
                        {
                            message = string.Format("Error : The AVI CH{0} file [{1}] does not exist.", mode, text);
                            return STATUS.FILE_OPEN;
                        }

                        if (!aviList.ContainsKey(mode))
                        {
                            aviList[mode] = new List<string>();
                        }
                        aviList[mode].Add(filename);
                    }
                }

                if (lstFileInfo.DatList.Count == 0)
                {
                    message = string.Format("Error : DAT file empty.\r\nFile:{0}", fileName);
                    return STATUS.FILE_OPEN;
                }

                // AVI不可　2020/9/24
#if false
                if (aviList.Count == 0)
                {
                    message = string.Format("Error : CH0～CH3 AVI file empty.\r\nFile:{0}", fileName);
                    return STATUS.FILE_OPEN;
                }

                for( int ch=0; ch < 4; ch++)
                {
                    if (!aviList.ContainsKey(ch))
                    {
                        message = string.Format("Error : AVI CH{0} file empty.\r\nFile:{1}", ch, fileName);
                        return STATUS.FILE_OPEN;
                    }
                }


                // aviファイルリスト作成(temp)
                string path = System.Environment.GetEnvironmentVariable("temp");
                foreach (var avi in aviList)
                {
                    string name = System.IO.Path.Combine(path, string.Format("SVOGenerator_ch{0}.avit", avi.Key));

                    System.IO.File.Delete(name);

                    using (var sw = new System.IO.StreamWriter(name, false, System.Text.Encoding.GetEncoding("shift_jis")))
                    {
                        foreach (var item in avi.Value)
                        {
                            sw.WriteLine("{0}", item);
                        }
                    }
                    lstFileInfo.AviFiles[avi.Key] = name;
                }
#endif
            }
            catch (Exception ex)
            {
                message = string.Format("Error : GetListInfo Program Error : {0}", ex.Message);
                return STATUS.PROGRAM_ERROR;
            }
            finally
            {
                System.Environment.CurrentDirectory = save_folder;
            }

            return STATUS.OK;
        }
    }
}
