﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using SVUtil;

namespace SVOgenCAN
{
    static class Program
    {
        /// <summary>
        /// プログラム環境
        /// </summary>
        static public SVOgenCANEnv mEnv = new SVOgenCANEnv();
        /// <summary>
        /// 環境変数ファイル名
        /// </summary>
        static public string mEnvFileName;

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Console.Writeをデバッグ画面へ表示
            System.Console.SetOut(Console.Out);
            Console.WriteLine("SVOgenCAN Start");

            // exe.jsonのパス名作成
            mEnvFileName = Assembly.GetEntryAssembly().Location + ".json";

            // 環境の読込
            LoadEnv(mEnvFileName);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());

            // 環境の書込み
            SaveEnv(mEnvFileName);
        }

        /// <summary>
        /// 環境変数の読込
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static void LoadEnv(string file_name)
        {
            try
            {
                using (var file = new FileStream(file_name, FileMode.Open, FileAccess.Read))
                {
                    mEnv = JsonSerializer.SerializerList<SVOgenCANEnv>().ReadObject(file) as SVOgenCANEnv;
                }
                // 必ずON
                mEnv.mPlayOption.mCanSettingCAN0Sending = true;
                mEnv.mPlayOption.mCanSettingCAN1Sending = true;
            }
            catch
            {
            }
        }

        /// <summary>
        /// 環境変数の保存
        /// </summary>
        /// <param name="file_name">ファイル名</param>
        static void SaveEnv(string file_name)
        {
            try
            {
                using (var file = new FileStream(file_name, FileMode.Create, FileAccess.Write))
                {
                    JsonSerializer.SerializerList<SVOgenCANEnv>().WriteObject(file, mEnv);
                }
            }
            catch
            {
            }
        }
    }
}
