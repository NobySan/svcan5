﻿namespace SVOgenCAN
{
    partial class PlayOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkMIPIContinuousClock = new System.Windows.Forms.CheckBox();
            this.comboMIPILaneCount = new System.Windows.Forms.ComboBox();
            this.comboMIPIDataRate = new System.Windows.Forms.ComboBox();
            this.comboMIPIPixelFormat = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericCanbps = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericCanFDbps = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericThreshold1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericThreshold2 = new System.Windows.Forms.NumericUpDown();
            this.checkCanSettingCAN1Sending = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkCanSettingCAN0Sending = new System.Windows.Forms.CheckBox();
            this.buttonApply = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericBuffer = new System.Windows.Forms.NumericUpDown();
            this.numericFPS = new System.Windows.Forms.NumericUpDown();
            this.checkReverse = new System.Windows.Forms.CheckBox();
            this.checkLsbPack = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCanbps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCanFDbps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuffer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFPS)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkMIPIContinuousClock);
            this.groupBox4.Controls.Add(this.comboMIPILaneCount);
            this.groupBox4.Controls.Add(this.comboMIPIDataRate);
            this.groupBox4.Controls.Add(this.comboMIPIPixelFormat);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(225, 126);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "MIPI CSI-2 Setting";
            // 
            // checkMIPIContinuousClock
            // 
            this.checkMIPIContinuousClock.AutoSize = true;
            this.checkMIPIContinuousClock.Location = new System.Drawing.Point(17, 99);
            this.checkMIPIContinuousClock.Name = "checkMIPIContinuousClock";
            this.checkMIPIContinuousClock.Size = new System.Drawing.Size(114, 16);
            this.checkMIPIContinuousClock.TabIndex = 13;
            this.checkMIPIContinuousClock.Text = "Continuous Clock";
            this.checkMIPIContinuousClock.UseVisualStyleBackColor = true;
            // 
            // comboMIPILaneCount
            // 
            this.comboMIPILaneCount.FormattingEnabled = true;
            this.comboMIPILaneCount.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboMIPILaneCount.Location = new System.Drawing.Point(94, 71);
            this.comboMIPILaneCount.Name = "comboMIPILaneCount";
            this.comboMIPILaneCount.Size = new System.Drawing.Size(119, 20);
            this.comboMIPILaneCount.TabIndex = 2;
            // 
            // comboMIPIDataRate
            // 
            this.comboMIPIDataRate.FormattingEnabled = true;
            this.comboMIPIDataRate.Items.AddRange(new object[] {
            "200.000000",
            "300.000000",
            "400.000000",
            "600.000000",
            "800.000000",
            "900.000000",
            "1000.000000",
            "1100.000000",
            "396.000000",
            "100.000000",
            "648.000000"});
            this.comboMIPIDataRate.Location = new System.Drawing.Point(94, 46);
            this.comboMIPIDataRate.Name = "comboMIPIDataRate";
            this.comboMIPIDataRate.Size = new System.Drawing.Size(119, 20);
            this.comboMIPIDataRate.TabIndex = 1;
            // 
            // comboMIPIPixelFormat
            // 
            this.comboMIPIPixelFormat.FormattingEnabled = true;
            this.comboMIPIPixelFormat.Items.AddRange(new object[] {
            "YUV4:2:2 8bit",
            "RGB888",
            "Raw 10",
            "Raw 12",
            "Raw 20"});
            this.comboMIPIPixelFormat.Location = new System.Drawing.Point(94, 21);
            this.comboMIPIPixelFormat.Name = "comboMIPIPixelFormat";
            this.comboMIPIPixelFormat.Size = new System.Drawing.Size(119, 20);
            this.comboMIPIPixelFormat.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 12);
            this.label11.TabIndex = 6;
            this.label11.Text = "Lane Count";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 12);
            this.label13.TabIndex = 3;
            this.label13.Text = "Data Rate";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "Pixel Format";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericCanbps);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.numericCanFDbps);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.numericThreshold1);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.numericThreshold2);
            this.groupBox6.Controls.Add(this.checkCanSettingCAN1Sending);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.checkCanSettingCAN0Sending);
            this.groupBox6.Location = new System.Drawing.Point(12, 144);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(225, 173);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Can Setting";
            // 
            // numericCanbps
            // 
            this.numericCanbps.Location = new System.Drawing.Point(125, 63);
            this.numericCanbps.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericCanbps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCanbps.Name = "numericCanbps";
            this.numericCanbps.Size = new System.Drawing.Size(70, 19);
            this.numericCanbps.TabIndex = 4;
            this.numericCanbps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "CAN Bitrate";
            // 
            // numericCanFDbps
            // 
            this.numericCanFDbps.Location = new System.Drawing.Point(125, 112);
            this.numericCanFDbps.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericCanFDbps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCanFDbps.Name = "numericCanFDbps";
            this.numericCanFDbps.Size = new System.Drawing.Size(70, 19);
            this.numericCanFDbps.TabIndex = 5;
            this.numericCanFDbps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "CAN-FD Bitrate";
            // 
            // numericThreshold1
            // 
            this.numericThreshold1.Location = new System.Drawing.Point(124, 137);
            this.numericThreshold1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericThreshold1.Name = "numericThreshold1";
            this.numericThreshold1.Size = new System.Drawing.Size(70, 19);
            this.numericThreshold1.TabIndex = 2;
            this.numericThreshold1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "BRS Sample Point";
            // 
            // numericThreshold2
            // 
            this.numericThreshold2.Location = new System.Drawing.Point(124, 87);
            this.numericThreshold2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericThreshold2.Name = "numericThreshold2";
            this.numericThreshold2.Size = new System.Drawing.Size(70, 19);
            this.numericThreshold2.TabIndex = 3;
            this.numericThreshold2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkCanSettingCAN1Sending
            // 
            this.checkCanSettingCAN1Sending.AutoSize = true;
            this.checkCanSettingCAN1Sending.Enabled = false;
            this.checkCanSettingCAN1Sending.Location = new System.Drawing.Point(17, 40);
            this.checkCanSettingCAN1Sending.Name = "checkCanSettingCAN1Sending";
            this.checkCanSettingCAN1Sending.Size = new System.Drawing.Size(97, 16);
            this.checkCanSettingCAN1Sending.TabIndex = 1;
            this.checkCanSettingCAN1Sending.Text = "CAN1 sending";
            this.checkCanSettingCAN1Sending.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Bit Sample Point";
            // 
            // checkCanSettingCAN0Sending
            // 
            this.checkCanSettingCAN0Sending.AutoSize = true;
            this.checkCanSettingCAN0Sending.Enabled = false;
            this.checkCanSettingCAN0Sending.Location = new System.Drawing.Point(17, 18);
            this.checkCanSettingCAN0Sending.Name = "checkCanSettingCAN0Sending";
            this.checkCanSettingCAN0Sending.Size = new System.Drawing.Size(97, 16);
            this.checkCanSettingCAN0Sending.TabIndex = 0;
            this.checkCanSettingCAN0Sending.Text = "CAN0 sending";
            this.checkCanSettingCAN0Sending.UseVisualStyleBackColor = true;
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Location = new System.Drawing.Point(165, 476);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 3;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericBuffer);
            this.groupBox1.Controls.Add(this.numericFPS);
            this.groupBox1.Controls.Add(this.checkReverse);
            this.groupBox1.Controls.Add(this.checkLsbPack);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 331);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 128);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Setting";
            // 
            // numericBuffer
            // 
            this.numericBuffer.Location = new System.Drawing.Point(81, 97);
            this.numericBuffer.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.numericBuffer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericBuffer.Name = "numericBuffer";
            this.numericBuffer.Size = new System.Drawing.Size(70, 19);
            this.numericBuffer.TabIndex = 3;
            this.numericBuffer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericFPS
            // 
            this.numericFPS.DecimalPlaces = 2;
            this.numericFPS.Location = new System.Drawing.Point(81, 70);
            this.numericFPS.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericFPS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFPS.Name = "numericFPS";
            this.numericFPS.Size = new System.Drawing.Size(70, 19);
            this.numericFPS.TabIndex = 2;
            this.numericFPS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkReverse
            // 
            this.checkReverse.AutoSize = true;
            this.checkReverse.Location = new System.Drawing.Point(15, 47);
            this.checkReverse.Name = "checkReverse";
            this.checkReverse.Size = new System.Drawing.Size(66, 16);
            this.checkReverse.TabIndex = 1;
            this.checkReverse.Text = "Reverse";
            this.checkReverse.UseVisualStyleBackColor = true;
            // 
            // checkLsbPack
            // 
            this.checkLsbPack.AutoSize = true;
            this.checkLsbPack.Location = new System.Drawing.Point(15, 25);
            this.checkLsbPack.Name = "checkLsbPack";
            this.checkLsbPack.Size = new System.Drawing.Size(74, 16);
            this.checkLsbPack.TabIndex = 0;
            this.checkLsbPack.Text = "LSB_Pack";
            this.checkLsbPack.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(157, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "MB";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "Buffer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "FPS";
            // 
            // PlayOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 512);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PlayOption";
            this.Text = "PlayOption";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayOption_FormClosing);
            this.Load += new System.EventHandler(this.PlayOption_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCanbps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCanFDbps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericThreshold2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuffer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFPS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkMIPIContinuousClock;
        private System.Windows.Forms.ComboBox comboMIPILaneCount;
        private System.Windows.Forms.ComboBox comboMIPIDataRate;
        private System.Windows.Forms.ComboBox comboMIPIPixelFormat;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkCanSettingCAN1Sending;
        private System.Windows.Forms.CheckBox checkCanSettingCAN0Sending;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkReverse;
        private System.Windows.Forms.CheckBox checkLsbPack;
        private System.Windows.Forms.NumericUpDown numericThreshold2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericFPS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericThreshold1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericCanbps;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericCanFDbps;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericBuffer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}