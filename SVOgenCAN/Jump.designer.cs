﻿namespace SVOgenCAN
{
    partial class Jump
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelFMax = new System.Windows.Forms.Label();
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.numericFrameNo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFrameNo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "1";
            // 
            // labelFMax
            // 
            this.labelFMax.AutoSize = true;
            this.labelFMax.Location = new System.Drawing.Point(171, 26);
            this.labelFMax.Name = "labelFMax";
            this.labelFMax.Size = new System.Drawing.Size(35, 12);
            this.labelFMax.TabIndex = 1;
            this.labelFMax.Text = "99999";
            // 
            // trackBar
            // 
            this.trackBar.Location = new System.Drawing.Point(19, 56);
            this.trackBar.Minimum = 1;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(187, 45);
            this.trackBar.TabIndex = 0;
            this.trackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBar.Value = 1;
            this.trackBar.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "Frame";
            // 
            // numericFrameNo
            // 
            this.numericFrameNo.Location = new System.Drawing.Point(223, 62);
            this.numericFrameNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFrameNo.Name = "numericFrameNo";
            this.numericFrameNo.Size = new System.Drawing.Size(71, 19);
            this.numericFrameNo.TabIndex = 1;
            this.numericFrameNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFrameNo.ValueChanged += new System.EventHandler(this.numericFrameNo_ValueChanged);
            // 
            // Jump
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 104);
            this.Controls.Add(this.numericFrameNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.trackBar);
            this.Controls.Add(this.labelFMax);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Jump";
            this.Text = "Jump";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Jump_FormClosing);
            this.Load += new System.EventHandler(this.Jump_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFrameNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFMax;
        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericFrameNo;
    }
}