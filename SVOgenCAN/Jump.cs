﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVOgenCAN
{
    /// <summary>
    /// ジャンプフレーム
    /// </summary>
    public partial class Jump : Form
    {
        /// <summary>
        /// ジャンプ
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        public delegate void JumpEventHandler(object sender, JumpEventArgs e);

        /// <summary>
        /// ジャンプイベント
        /// </summary>
        public event JumpEventHandler JumpEvent;

        /// <summary>
        /// 最大フレーム数
        /// </summary>
        public int FrameMax
        {
            set
            {
                labelFMax.Text = value.ToString();
                trackBar.Maximum = value;
                numericFrameNo.Maximum = value;
            }
        }

        /// <summary>
        /// フレーム位置(1～)
        /// </summary>
        public int FramePos
        {
            set
            {
                trackBar.Value = value;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Jump()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Jump_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);

            // 配置
            this.Bounds = rc;
        }

        /// <summary>
        /// クローズ処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void Jump_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            Program.mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。
                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// トラックバー移動
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            numericFrameNo.Value = trackBar.Value;
        }

        /// <summary>
        /// フレーム番号変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericFrameNo_ValueChanged(object sender, EventArgs e)
        {
            trackBar.Value = (int)numericFrameNo.Value;

            if (JumpEvent!=null)
            {
                JumpEvent(this, new JumpEventArgs { FrameNo = trackBar.Value });
            }
        }
    }

    /// <summary>
    /// フレーム番号
    /// </summary>
    public class JumpEventArgs : EventArgs
    {
        /// <summary>
        /// フレーム番号
        /// </summary>
        public int FrameNo;
    }

}
