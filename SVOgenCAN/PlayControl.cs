﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _SVOControl;
using svousb_dll;

namespace SVOgenCAN
{
    /// <summary>
    /// 再生データ転送
    /// </summary>
    public partial class PlayControl : Form
    {

        /// <summary>
        /// ブロックバッファ
        /// </summary>
        class BlockBuffer
        {
            /// <summary>
            /// バイトバッファ
            /// </summary>
            public byte[] mBuf;

            /// <summary>
            /// バッファ確報
            /// </summary>
            /// <param name="size">バイトサイズ</param>
            public BlockBuffer(int size)
            {
                mBuf = new byte[size];
            }
        }

        /// <summary>
        /// ブロッキングキューの作成
        /// </summary>
        BlockingCollection<BlockBuffer> mBlockingCollection = null;
        /// <summary>
        /// 転送ブロック数(32M:1)
        /// </summary>
        long mBlockMax;

        /// <summary>
        /// 中止フラグ
        /// </summary>
        volatile bool mAbort = false;

        /// <summary>
        /// エラーステータス
        /// </summary>
        int mErrorStatus = 0;
        /// <summary>
        /// 実行中
        /// </summary>
        volatile bool mRunning = false;
        /// <summary>
        /// 実行中かどうか
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return mRunning;
            }
        }

        /// <summary>
        /// CANデータ
        /// </summary>
        public SVCanData.SvCanData mSVCanData;

        /// <summary>
        /// 最大10秒間のウェイト
        /// </summary>
        const int WAIT_READ_MAX = 10 * 1000;
        /// <summary>
        /// 合計１Gのサイズ
        /// </summary>
        const int QUE_STACK_MAX = 32;
        /// <summary>
        /// 送信先最大
        /// </summary>
        const int WAIT_SEND_MAX = 10000;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void PlayControl_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);
            // 配置
            this.Bounds = rc;
        }

        /// <summary>
        /// クローズ処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void PlayControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            Program.mEnv.SetWindowRect(this.Name, rc);

            if (e.CloseReason == CloseReason.UserClosing)
            {
                // × ボタンの場合は、Hide に変える。

                if (this.mRunning)
                {
                    this.mAbort = true;
                    do
                    {
                        System.Threading.Thread.Sleep(200);
                    }
                    while (this.mRunning == false);
                }

                this.labelErrCnt.Text = "0";
                this.lblFrameNo.Text = "0";

                e.Cancel = true;
                this.Hide();
            }
        }

        /// <summary>
        /// 停止
        /// </summary>
        public void Stop()
        {
            mAbort = true;
        }

        /// <summary>
        /// 送信データをキューに詰め込む
        /// </summary>
        void PlayReadData()
        {
            int read_size;

            // 先頭に戻す
            mSVCanData.SeekToTop();

            for (long i = 0; i < mBlockMax; i++)
            {
                if (this.mAbort) break;

                // 読込サイズ
                read_size = Program.mEnv.mPlayOption.mBuffer * 1024 * 1024;
                if ((i + 1) == mBlockMax)
                {
                    // 最後のバッファサイズ
                    read_size = (int)(mSVCanData.DataLength % (Program.mEnv.mPlayOption.mBuffer * 1024 * 1024));
                    if (read_size == 0) read_size = (Program.mEnv.mPlayOption.mBuffer * 1024 * 1024);
                }

                BlockBuffer data = new BlockBuffer(read_size);

                // ブロックバッファの読込
                int cnt = mSVCanData.ReadBlock(data.mBuf, read_size);
                if (cnt != read_size)
                {
                    mErrorStatus = -1;
                    break;
                }

                // 空きができるのを最大10秒待つ
                if ( mBlockingCollection.TryAdd( data, WAIT_READ_MAX) == false )
                {
                    Console.WriteLine("SVCan Readfile timeout = {0} msec", WAIT_READ_MAX);
                    mErrorStatus = -999;
                    break;
                }
            }
        }

        /// <summary>
        /// バッファ読込
        /// </summary>
        /// <param name="buffer">読込バッファ</param>
        /// <returns>成功ならtrueそれ以外はfalseを返却</returns>
        bool ReadBuffer(out byte [] buffer)
        {
            BlockBuffer data;

            bool ret = mBlockingCollection.TryTake(out data, WAIT_SEND_MAX);
            if ( ret )
            {
                buffer = data.mBuf;
            }
            else
            {
                buffer = null;
                Console.WriteLine("SVCan ReadBuffer timeout = {0} msec", WAIT_SEND_MAX);
            }
            return (ret);
        }

        /// <summary>
        /// 実行開始
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonPlay_Click(object sender, EventArgs e)
        {
            // ボタンを無効化
            buttonPlay.Enabled = false;
            progressBar.Value = 0;
            mErrorStatus = 0;
            mAbort = false;

            // SVOコントロールクラスのオブジェクトを作成
            SVOControl svo_control = new SVOControl();

            // SVOドライバーをオープンする
            int iret = SVOControl.Init();
            if (iret != 0)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVOControl.Init() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                buttonPlay.Enabled = true;
                return;
            }

            mRunning = true;

            // SVボードの情報を取得する
            NativeMethods_SvoUsb.GET_BOARD_STATUS gSts = new NativeMethods_SvoUsb.GET_BOARD_STATUS();
            UInt32 uret = SVOControl.GetStatus(out gSts);
            if (uret != NativeMethods_SvoUsb.RET_NORMAL)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVOControl.GetStatus() ret=" + iret.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVOControl.End();
                buttonPlay.Enabled = true;
                return;
            }
            // ボードのアイドルと画像入力の有無を検証する
            if (gSts.ulOPStatus != 0)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    Win32.SVMessageBox.Show(this, "Error SVO board not idle ulOPStatus=" + gSts.ulOPStatus.ToString("x8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
                SVOControl.End();
                buttonPlay.Enabled = true;
                return;
            }

            //
            // スレッド実行
            //
            System.Threading.Tasks.Task.Run(() =>
            {

                // 転送ブロック数の算出
                mBlockMax = (mSVCanData.DataLength - 1) / (Program.mEnv.mPlayOption.mBuffer * 1024 * 1024) + 1;

                // Que作成
                //int alloc_size = (int)mBlockMax;
                mBlockingCollection = new BlockingCollection<BlockBuffer>(QUE_STACK_MAX);
                //mBlockingCollection = new BlockingCollection<BlockBuffer>(alloc_size);

                // 読込バッファスレッド処理
                System.Threading.Tasks.Task.Run(() =>
                {
                    PlayReadData();
                });
                System.Threading.Thread.Sleep(1000);

                // 送信開始
                PlaySendData();

                // 終了
                mRunning = false;

                // SVOボードの終了
                SVOControl.End();

                // 終了処理
                this.Invoke((MethodInvoker)delegate ()
                {
                    if (mAbort)
                    {
                        Win32.SVMessageBox.Show(this, "play aborted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (mErrorStatus != 0)
                        {
                            Win32.SVMessageBox.Show(this, string.Format("play error[{0}].", mErrorStatus), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            Win32.SVMessageBox.Show(this, "play complete.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    // 進捗バーを戻す
                    progressBar.Value = 0;

                    // ボタンを有効化
                    buttonPlay.Enabled = true;

                    this.labelErrCnt.Text = "0";
                    this.lblFrameNo.Text = "0";

                    // 自身を非表示
                    this.Hide();
                });

                // メモリを強制解放する(2020/3/18)
                this.mBlockingCollection.Dispose();
            });
        }

        /// <summary>
        /// 転送ブロックサイズ(32M)
        /// </summary>
        const int BLOCK_SIZE = 32 * 1024 * 1024;
        const int SEND_SIZE = BLOCK_SIZE + 20;

        /// <summary>
        /// データの送信
        /// </summary>
        void PlaySendData()
        {
            try
            {
                UInt32 uiRcvLen = 0;
                UInt32 uiRet = 0;
                UInt32 ulSubFrameNo = 0;
                UInt32 uiErrCnt = 0;
                UInt32 ul70000200 = 0;
                UInt32 ul70000204 = 0;
                UInt32 ul70000208 = 0;
                UInt32 ul70000200_bef = 0;

                int frame_size = this.mSVCanData.FrameDataSize;
                long copy_bytes = 0;

                // Program.mEnv.mPlayOptionにてオプション変数を取得
                // Program.mEnv.mPlayOption.mMIPIPixelFormat;


                // 送信用バッファの確保
                IntPtr SubFrameBuffer = Marshal.AllocHGlobal((IntPtr)(Program.mEnv.mPlayOption.mBuffer * 1024 * 1024 + 20));

                // SVOボードへ送信開始準備を通知します
                uiRet = SVOControl.SendStart0((uint)Program.mEnv.mPlayOption.mBuffer);
                if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVOControl.SendStart0() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }

                // 最初にボード上のフレームメモリーへDATデータのデータ部先頭256MBを転送する
                long loop =0;
                for (loop = 0; loop < mBlockMax; loop++)
                {
                    // 中断
                    if (mAbort) break;

                    byte[] buf = null;
                    // ブロックバッファの読込
                    if ( !ReadBuffer(out buf) )
                    {
                        mErrorStatus = -1;
                        break;
                    }

                    // 読込サイズ
                    int read_size = buf.Length;

                    Marshal.Copy(buf, 0, SubFrameBuffer+20, read_size);

                    // SVOボードへレコーディングデータを転送する
                    uiRet = SVOControl.SendPutData(SubFrameBuffer, (uint)read_size+20, (uint)read_size, ulSubFrameNo, (uint)Program.mEnv.mPlayOption.mBuffer, ref uiRcvLen);
                    if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVOControl.SendPutData() ret=" + uiRet.ToString("x8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }
                    ulSubFrameNo++;
                    if ((256 / Program.mEnv.mPlayOption.mBuffer) == ulSubFrameNo) ulSubFrameNo = 0;

                    //
                    // 進捗バーの更新
                    //
                    int progress = (int)(((loop + 1) * 100) / mBlockMax);
                    if (progressBar.Value != progress)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            // 進捗バーのアップデート
                            if (progress <= progressBar.Maximum)
                            {
                                progressBar.Value = progress;
                            }

                            // フレーム番号
                            this.lblFrameNo.Text = (copy_bytes / frame_size).ToString();
                            
                        });
                    }

                    copy_bytes += read_size;

                    // 256MB転送したかを確認
                    if ((loop + 1) == (256 / Program.mEnv.mPlayOption.mBuffer)) break;
                }
                loop++; // ループ回数を更新

#if false // for debug
                uiRet = SVOControl.GetOutputStatus(ref ul70000200, ref ul70000204, ref ul70000208);
#endif
                // SVOボードへ送信開始を通知します
                //SVOControl.SendStart((int)Program.mEnv.mPlayOption.mCanSettingCAN0Sending,
                //                            (int)Program.mEnv.mPlayOption.mCanSettingCAN1Sending);
                uiRet = SVOControl.SendStart(0x1, 0x1);
                if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVOControl.SendStart() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }

                // ブロック数の回し
                for (; loop < mBlockMax; )
                {
                    // 中断
                    if (mAbort) break;

#if false // for debug
                    uint ulTemp=0;
                    uiRet = SVOControl.GetOutputStatus(ref ulTemp, ref ul70000204, ref ul70000208);
#endif

                    // SVOボードより出力ステータスを受信しレコーディングデータを補充できるか確認する
                    uiRet = SVOControl.GetOutputStatus(ref ul70000200, ref ul70000204, ref ul70000208);
                    if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVOControl.GetOutputStatus() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }
                    Console.WriteLine("GetOutputStatus() 200=" + ul70000200.ToString("x8") + " 204=" + ul70000204.ToString("x8"));
                    // F-Info Read/Write Common Status Regsiterのbit31-24-Errorを確認
                    //if ((ul70000200 & 0xff010101) != 0x0)
                    if ((ul70000200 & 0xff000000) != 0x0)
                    {
                        uiErrCnt++;

                        // エラー数を画面更新
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            //labelErrCnt.Text = uiErrCnt.ToString();
                            labelErrCnt.Text = ul70000200.ToString("x8");
                        });

                        //this.Invoke((MethodInvoker)delegate ()
                        //{
                        //    Win32.SVMessageBox.Show(this, "Error SVOControl.GetOutputStatus() ul70000200=" + ul70000200.ToString("x8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //});
                        //break;
                    }
                    ul70000200_bef = ul70000200;

                    // F-Info Read/Write Common Status Regsiterのbit12-FWRDYを確認
                    if ((ul70000200 & 0x1000) == 0x0)
                    {
                        //this.Invoke((MethodInvoker)delegate ()
                        //{
                        //    Win32.SVMessageBox.Show(this, "Error SVOControl.GetOutputStatus() ul70000200=" + ul70000200.ToString("x8"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //});
                        // FWRDYが立っていなかったらちょっと待って再度ステータスを取得する
                        // 1ミリ秒withウェイト
                        //System.Threading.Thread.Sleep(1);
                        continue;
                    }

                    // FWRDYが立っていたらデータを次のデータを補充できる
                    loop++;

                    byte[] buf = null;
                    // ブロックバッファの読込
                    if (!ReadBuffer(out buf))
                    {
                        mErrorStatus = -1;
                        break;
                    }

                    // 読込サイズ
                    int read_size = buf.Length;

                    Marshal.Copy(buf, 0, SubFrameBuffer + 20, read_size);

                    // SVOボードへレコーディングデータを転送する
                    //var sw = new System.Diagnostics.Stopwatch();
                    //sw.Start();
                    uiRet = SVOControl.SendPutData(SubFrameBuffer, (uint)read_size+20, (uint)read_size, ulSubFrameNo, (uint)Program.mEnv.mPlayOption.mBuffer, ref uiRcvLen);
                    if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVOControl.SendPutData() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }
                    //sw.Stop();
                    //TimeSpan ts = sw.Elapsed;
                    //Console.WriteLine($"{ts.Milliseconds}msec");
                    ulSubFrameNo++;
                    if ((256 / Program.mEnv.mPlayOption.mBuffer) == ulSubFrameNo) ulSubFrameNo = 0;

                    //
                    // 進捗バーの更新
                    //
                    int progress = (int)(((loop + 1) * 100) / mBlockMax);
                    if (progressBar.Value != progress)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            // 進捗バーのアップデート
                            if (progress <= progressBar.Maximum)
                            {
                                progressBar.Value = progress;
                            }

                            // フレーム番号
                            this.lblFrameNo.Text = (copy_bytes / frame_size).ToString();
                        });
                    }

                    copy_bytes += read_size;
                }

                // 最後のフレーム番号
                this.Invoke((MethodInvoker)delegate ()
                {
                    // フレーム番号
                    this.lblFrameNo.Text = this.mSVCanData.FrameMax.ToString();

                });


                // 最後のSubFrameが転送するまで待つ
                for (; ; )
                {
                    // 中断
                    if (mAbort) break;

                    // SVOボードより出力ステータスを受信しレコーディングデータを補充できるか確認する
                    uiRet = SVOControl.GetOutputStatus(ref ul70000200, ref ul70000204, ref ul70000208);
                    if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            Win32.SVMessageBox.Show(this, "Error SVOControl.GetOutputStatus() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        });
                        break;
                    }
                    // F-Info Read/Write Common Status Regsiterのbit31-24-Errorを確認
                    if ((ul70000200 & 0x1) != 0x0) break;
                }

                // SVOボードへ送信停止を通知します
                uiRet = SVOControl.SendStop();
                if (uiRet != NativeMethods_SvoUsb.RET_NORMAL)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Win32.SVMessageBox.Show(this, "Error SVOControl.SendStop() ret=" + uiRet.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    });
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("exception error[{0}]", ex.Message);
            }
        }

        /// <summary>
        /// フォーム表示／非表示処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void PlayControl_VisibleChanged(object sender, EventArgs e)
        {
            if ( this.Visible)
            {
                // 表示された場合はエラーカウントを0へ
                labelErrCnt.Text = "0";
            }
            else
            {
            }
        }
    }
}
