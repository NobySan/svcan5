﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SVOgenCAN
{
    /// <summary>
    /// 環境設定
    /// </summary>
    [DataContract]
    public class SVOgenCANEnv : SVUtil.SVEnv
    {

        /// <summary>
        /// 再生オプション
        /// </summary>
        [DataMember(Name = "PlayOption")]
        PlayOptionData _playoption = null;
        public PlayOptionData mPlayOption
        {
            get
            {
                if ( _playoption == null)
                {
                    _playoption = new PlayOptionData();
                }
                return (_playoption);
            }
            set
            {
                _playoption = value;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SVOgenCANEnv()
        {
        }

    }


    /// <summary>
    /// 再生-設定
    /// </summary>
    [DataContract]
    public class PlayOptionData : ICloneable
    {
        /// <summary>
        /// MIPI CSI-2 Setting Pixel Format
        /// </summary>
        [DataMember(Name = "MIPIPixelFormat")]
        public int mMIPIPixelFormat;
        /// <summary>
        /// MIPI CSI-2 Setting Data Rate
        /// </summary>
        [DataMember(Name = "MIPIDataRate")]
        public int mMIPIDataRate;
        /// <summary>
        /// MIPI CSI-2 Setting Lane Count
        /// </summary>
        [DataMember(Name = "MIPILaneCount")]
        public int mMIPILaneCount;
        /// <summary>
        /// MIPI CSI-2 Setting Continuous Clock
        /// </summary>
        [DataMember(Name = "MIPIContinuousClock")]
        public bool mMIPIContinuousClock;
        /// <summary>
        /// Can Setting CAN0 sending
        /// </summary>
        [DataMember(Name = "CanSettingCAN0Sending")]
        public bool mCanSettingCAN0Sending=true;
        /// <summary>
        /// Can Setting CAN1 sending
        /// </summary>
        [DataMember(Name = "CanSettingCAN1Sending")]
        public bool mCanSettingCAN1Sending=true;
        /// <summary>
        /// Threshold1 BRS Sample Point
        /// </summary>
        [DataMember(Name = "CanThreshold1")]
        public int mCanThreshold1=68;
        /// <summary>
        /// Threshold2 Bit Sample Point
        /// </summary>
        [DataMember(Name = "CanThreshold2")]
        public int mCanThreshold2 = 78;
        /// <summary>
        /// CAN bps
        /// </summary>
        [DataMember(Name = "CANbps")]
        public int mCanbps = 500;
        /// <summary>
        /// CAN-FD bps
        /// </summary>
        [DataMember(Name = "CAN-FDbps")]
        public int mCanFDbps = 2000;
        /// <summary>
        /// LSB_Pack
        /// </summary>
        [DataMember(Name = "DisplayLsbPack")]
        public bool mDisplayLsbPack = false;
        /// <summary>
        /// Reverse
        /// </summary>
        [DataMember(Name = "DisplayReverse")]
        public bool mDisplayReverse = false;
        /// <summary>
        /// FPS
        /// </summary>
        [DataMember(Name = "DisplayFPS")]
        public double mDisplayFPS = 30;
        /// <summary>
        /// Buffer
        /// </summary>
        [DataMember(Name = "Buffer")]
        public int mBuffer = 32;

        /// <summary>
        /// デシリアライズ
        /// </summary>
        /// <param name="sc">コンテキスト</param>
        [OnDeserializing]
        internal void OnDeserializing(StreamingContext sc)
        {
            mMIPIPixelFormat = 0;
            mMIPIDataRate = 0;
            mMIPILaneCount = 0;
            mMIPIContinuousClock = false;
            mCanThreshold1 = 68; // BRS Sample Point
            mCanThreshold2 = 70; // Bit Sample Point
            mCanSettingCAN0Sending = true;
            mCanSettingCAN1Sending = true;
            mCanbps = 500;
            mCanFDbps = 2000;
            mDisplayLsbPack = false;
            mDisplayReverse = false;
            mDisplayFPS = 30.0;
            mBuffer = 32;
        }
        
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayOptionData()
        {

        }

        /// <summary>
        /// クローン
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }

}
