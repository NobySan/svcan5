﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using svousb_dll;

namespace _SVOControl
{
    /// <summary>
    /// SVO-03-MIPIアクセスモジュール
    /// </summary>
    public class SVOControl
    {
        /// <summary>
        /// クラス内変数定義
        /// </summary>
        private static bool bOpen = false;          // デバイスドライバオープンフラグ
        private const UInt32 m_BufMax = (64 * 1024 * 1024 * 4);  // 64MBx4の受信バッファサイズ
        private static NativeMethods_SvoUsb.GET_BOARD_STATUS sts;
        private static NativeMethods_SvoUsb.SET_PARAM_EX param;
        private static UInt32 fm_addr = 0; // SVOボードフレームメモリ書き込みアドレス
        private static UInt32 uiCan = 0; // SVO-03-MIPI-CANのボード番号

        /// <summary>
        /// SVOコントロールクラスのコンストラクタ
        /// </summary>
        public SVOControl()
        {
            // 画像入力ライブラリーの初期化
            NativeMethods_SvoUsb.Init();
        }

        /// <summary>
        /// SVO初期化、SVOデバイスドライバーオープン
        /// </summary>
        /// <param name="stFileName"></param>
        /// <returns></returns>
        public static int Init()
        {
            UInt32 uiRet;
            UInt32 uiNum=0;
            var uiTable = new uint[8];

            ////////////////////////////////////////////////////////
            // Board ID=0のSVO-03-MIPIを探す
            ////////////////////////////////////////////////////////
            uiRet = NativeMethods_SvoUsb.EnumDevice(ref uiNum, null);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL || uiNum == 0) return -1;
            uiRet = NativeMethods_SvoUsb.EnumDevice(ref uiNum, uiTable);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return -1;
            for(int i=0;i<8;i++)
            {
                if ((uiTable[i] >> 16) == 0x0)
                {
                    uiCan = (UInt32)i;
                    break;
                }
            }

            ////////////////////////////////////////////////////////
            // SVOデバイスドライバのオープン
            ////////////////////////////////////////////////////////
            uiRet = NativeMethods_SvoUsb.Open(uiCan);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return -1;
            bOpen = true;   // オープンフラグをオープン済みにする

            return 0;
        }

        /// <summary>
        /// SVIライブラリーを終了します
        /// </summary>
        public static void End()
        {
            // デバイスドライバがオープン済みであればクローズする
            if (bOpen != false)
            {
                ////////////////////////////////////////////////
                // FPGA リコンフィグレーション BSB_FPGA_SELF_RCFG_EXE
                ////////////////////////////////////////////////
                // 8,	080000FC,	00000080	# BSB_FPGA_SELF_RCFG_EXE = 1
                //uint uiRet2 = NativeMethods_SvoUsb.WriteReg(uiCan, 0x080000fc, 0x4, 0x80);
                //if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
                // 250ミリ秒withウェイト
                System.Threading.Thread.Sleep(1000);

                UInt32 uiRet = NativeMethods_SvoUsb.Close(uiCan);
                bOpen = false;   // オープンフラグをオープン未にする
            }

            // SVIライブラリーを終了します
            NativeMethods_SvoUsb.End();
        }

        /// <summary>
        /// SVボードの情報を取得する
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static UInt32 GetStatus(out NativeMethods_SvoUsb.GET_BOARD_STATUS status)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;

            // SVO-03-MIPIボードの情報を取得する
            uiRet = NativeMethods_SvoUsb.GetBoardStatus(uiCan, ref sts);

            status = sts;

            return uiRet;
        }

        /// <summary>
        /// SVボードの出力情報を取得する
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static UInt32 GetOutputStatus(ref UInt32 pulFrameNum, ref UInt32 pulTotalNum, ref UInt32 pulFreeMem)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;

            // SVO-03-MIPIボードの出力情報を取得する
#if true
            UInt32[] uiArray = new UInt32[4];
            uiRet = NativeMethods_SvoUsb.ReadReg(0x0, 0x70000200, 0x4, ref pulFrameNum);
            uiRet = NativeMethods_SvoUsb.ReadReg(0x0, 0x70000204, 0x4, ref pulTotalNum);
            pulFreeMem = 0;
#else
            uiRet = NativeMethods_SvoUsb.GetOutputStatusEx(uiCan, ref pulFrameNum, ref pulTotalNum, ref pulFreeMem);
#endif
            return uiRet;
        }

        /// <summary>
        /// SVボードからVideo同期信号のモニター情報を取得する
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static UInt32 GetVideoMonitor(ref UInt32 pulTCB_CIP_SYNC_SIG)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;

            // SVO-03-MIPIボードの出力情報を取得する
            uiRet = NativeMethods_SvoUsb.ReadReg(uiCan, 0x10000004, 0x4, ref pulTCB_CIP_SYNC_SIG);
            return uiRet;
        }

        /// <summary>
        /// 画像出力開始の準備をする
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static UInt32 SendStart0(UInt32 ulSubFlameSize)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;

            //# Renewal F-Info Table Contents After Pushed "Test_SVO_CAN" Button
            //# BSB:
            //8,	08000048,	00000020	# Block Reset for DLB
            //8,	0800004C,	00000020	# Block Reset for UDPoB
            //8,	08000044,	00000040	# Block Reset for FITB
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x8000048, 0x4, 0x20);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x800004C, 0x4, 0x20);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x8000044, 0x4, 0x40);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            // 250ミリ秒withウェイト
            System.Threading.Thread.Sleep(250);

            ////////////////////////////////////////////////
            // Frame Info Tableの設定
            // # Renewal F-Info Table Contents After Pushed "Test_SVO_CAN" Button
            ////////////////////////////////////////////////
            // 8,	70000204,	80000000	# FACTMAX=8, FACTCNT=0, FWCNT=0, FRCNT=0
            uint uiVal = (256 / ulSubFlameSize) << 24;
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x70000204, 0x4, uiVal);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // 8,	70000214,	00000807	# FWRDY_NEG_THR=8, FWRDY_ASS_THR=7
            uiVal = (256 / ulSubFlameSize) << 8 | (256 / (ulSubFlameSize)-1) << 0;
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x70000214, 0x4, uiVal);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            return uiRet;
        }

        /// <summary>
        /// 画像出力開始を通知する
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static UInt32 SendStart(UInt32 can0_flag, UInt32 can1_flag)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;
reset:
            ////////////////////////////////////////////////
            // Target Connection Blockの設定
            // # TCB Init.
            ////////////////////////////////////////////////
            //8,	10000000,	00000101	# CAM_PWR_CTRL=1, TGT_RST_CTRL=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000000, 0x4, 0x101);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            ////////////////////////////////////////////////
            // Target Connection Blockの設定
            ////////////////////////////////////////////////
            // setting GPIO13,15 for CAN Tx pin
            // # TCB_XSS_SEL/SYNC_SIG_POL:
            // 8, 10000028, 00101100 # TCB_XSS_SEL_EXT_MAP_OUT=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000028, 0x4, 0x100101);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            // # TCB_EXT_BUF_CTRL1:
            // 8, 10000008, 00005fff # bit[15]=0, bit[13]=0
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000008, 0x4, 0x5fff);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // # Setting for Parallel Pixel Clock Eq. 100[MHz]
            // # TCB_TCS1:
            //8,	10000010,	00000001	# RST_CTRL=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000010, 0x4, 0x1);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            //8,	10000014,	001E01E0	# GEN_STG2_DIV=480, GEN_STG1_MUL=480
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000014, 0x4, 0x1E01E0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            //8,	10000018,	000601E0	# GEN_STG4_DIV= 96, GEN_STG3_MUL=480
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000018, 0x4, 0x601E0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            //8,	1000001C,	00000001	# GEN_STG5_DIV=  1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x1000001C, 0x4, 0x1);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            //8,	10000010,	00000000	# SEL_EXT_SRC=0, PCB_SEL_BYP_PLL=0, GEN_STG5_DIV_ENA=0, RST_CTRL=0
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000010, 0x4, 0x0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // # Setting for MIPI-CSI2-TX Clock Eq.  99[MHz](198[Mbps/Lane])
            // # TCB_TCS2:
            // 8,	10000050,	00000001	# RST_CTRL=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000050, 0x4, 0x1);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            // 8,	10000054,	001901B8	# GEN_STG2_DIV=400, GEN_STG1_MUL=440
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000054, 0x4, 0x1901B8);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            // 8,	10000058,	000601B0	# GEN_STG4_DIV= 96, GEN_STG3_MUL=432
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000058, 0x4, 0x601B0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            // 8,	10000050,	00000000	# RST_CTRL=0
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000050, 0x4, 0x0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // # TCB_MIPI_TX_CTRL
            // 8,	10000040,	001E1010	# DATA_TYPE=YUV422-8bit, CONT_HS_CK=1, LANE_CNT=0, FGATE_CTRL=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000040, 0x4, 0x1E1010);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // # TCB Init.
            // 8,	10000000,	00000100	# CAM_PWR_CTRL=1, TGT_RST_CTRL=0
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x10000000, 0x4, 0x100);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            // 250ミリ秒withウェイト
            System.Threading.Thread.Sleep(250);

            // TCS_1 Lock確認
            // # TCB_TCS1:
            //8,	10000010,	00000001	# RST_CTRL=1
            UInt32 ulReadVal = 0;
            uiRet = NativeMethods_SvoUsb.ReadReg(uiCan, 0x10000010, 0x4, ref ulReadVal);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            if ((ulReadVal & 0x2) == 0x0) goto reset;

            ////////////////////////////////////////////////
            // Data Loader Blockの設定
            ////////////////////////////////////////////////
            // 8,	A8000000,	00000001	# OPE_MODE=0(Normal), OPE_ENABLE=1
            // 8,	A8000000,	00000005	# OPE_MODE=1(Repeat), OPE_ENABLE=1
            // 8,	A8000000,	00000009	# OPE_MODE=2(Reenter), OPE_ENABLE=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0xA8000000, 0x4, 0x1);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            return uiRet;
        }

        /// <summary>
        /// 画像出力停止を通知する
        /// </summary>
        /// <param name="sts"></param>
        /// <returns></returns>
        public static UInt32 SendStop()
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;

            ////////////////////////////////////////////////
            // Data Loader Blockの設定
            ////////////////////////////////////////////////
            // 8,	A8000000,	00000000	# OPE_MODE=0, OPE_ENABLE=0
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0xA8000000, 0x4, 0x0);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;

            return uiRet;
        }

        /// <summary>
        /// レコーディングデータを出力します
        /// </summary>
        /// <param></param>
        /// <returns>
        /// </returns>
        public static UInt32 SendPutData(IntPtr pFrame, UInt32 uiLen, UInt32 uiRealLen, UInt32 ulSubFrameNo, UInt32 ulSubFrameSize, ref UInt32 uiRcvLen)
        {
            UInt32 uiRet = NativeMethods_SvoUsb.RET_NORMAL;
            UInt32 uiOffset = 0;

            // データ受信
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            unsafe
            {
                UInt32 ulGetSize2 = 0;
                fm_addr = ulSubFrameNo * (ulSubFrameSize * 1024 * 1024);
                uiRet = NativeMethods_SvoUsb.OfflinePictureSendEx2(uiCan, pFrame, uiLen, fm_addr, uiRealLen, ref ulGetSize2);
                uiRcvLen = ulGetSize2;
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            Console.WriteLine($"{ts.Milliseconds}msec");

            ////////////////////////////////////////////
            // Frame Info Tabel Setting
            ////////////////////////////////////////////
            //8,	70000100,	00000100	# FWSTA=1
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x70000100, 0x4, 0x100);
            if (uiRet != NativeMethods_SvoUsb.RET_NORMAL) return uiRet;
            //8,	70000104,	80000000	# FWEND(Push F-Info[0]: Adrs=00h[MByte],Size=32[MByte])
            uiOffset = ((uiRealLen >> 2) << 8) | (fm_addr >> 20);
            uiRet = NativeMethods_SvoUsb.WriteReg(uiCan, 0x70000104, 0x4, uiOffset);
            return uiRet;
        }
    }
}

