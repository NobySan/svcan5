﻿namespace SVOgenCAN
{
    partial class ClipFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textFileName = new System.Windows.Forms.TextBox();
            this.buttonRefer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.numericStartFrame = new System.Windows.Forms.NumericUpDown();
            this.numericEndFrame = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericStartFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEndFrame)).BeginInit();
            this.SuspendLayout();
            // 
            // textFileName
            // 
            this.textFileName.Location = new System.Drawing.Point(12, 29);
            this.textFileName.Name = "textFileName";
            this.textFileName.Size = new System.Drawing.Size(315, 19);
            this.textFileName.TabIndex = 0;
            // 
            // buttonRefer
            // 
            this.buttonRefer.Location = new System.Drawing.Point(330, 30);
            this.buttonRefer.Name = "buttonRefer";
            this.buttonRefer.Size = new System.Drawing.Size(28, 18);
            this.buttonRefer.TabIndex = 1;
            this.buttonRefer.Text = "...";
            this.buttonRefer.UseVisualStyleBackColor = true;
            this.buttonRefer.Click += new System.EventHandler(this.buttonRefer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "Save filename";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Start frame";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(166, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "-- End frame";
            // 
            // buttonExecute
            // 
            this.buttonExecute.Location = new System.Drawing.Point(283, 96);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(75, 23);
            this.buttonExecute.TabIndex = 5;
            this.buttonExecute.Text = "Execute";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(15, 96);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(262, 23);
            this.progressBar.TabIndex = 4;
            // 
            // numericStartFrame
            // 
            this.numericStartFrame.Location = new System.Drawing.Point(91, 64);
            this.numericStartFrame.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericStartFrame.Name = "numericStartFrame";
            this.numericStartFrame.Size = new System.Drawing.Size(69, 19);
            this.numericStartFrame.TabIndex = 2;
            this.numericStartFrame.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericEndFrame
            // 
            this.numericEndFrame.Location = new System.Drawing.Point(245, 64);
            this.numericEndFrame.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericEndFrame.Name = "numericEndFrame";
            this.numericEndFrame.Size = new System.Drawing.Size(69, 19);
            this.numericEndFrame.TabIndex = 3;
            this.numericEndFrame.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ClipFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 134);
            this.Controls.Add(this.numericEndFrame);
            this.Controls.Add(this.numericStartFrame);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.buttonExecute);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonRefer);
            this.Controls.Add(this.textFileName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ClipFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ClipFile";
            this.Load += new System.EventHandler(this.ClipFile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericStartFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEndFrame)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textFileName;
        private System.Windows.Forms.Button buttonRefer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.NumericUpDown numericStartFrame;
        private System.Windows.Forms.NumericUpDown numericEndFrame;
    }
}