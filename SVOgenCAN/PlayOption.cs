﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVOgenCAN
{
    /// <summary>
    /// play option
    /// </summary>
    public partial class PlayOption : Form
    {
        /// <summary>
        /// Play Option Data
        /// </summary>
        public PlayOptionData mPlayOption;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayOption()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void PlayOption_Load(object sender, EventArgs e)
        {
            Rectangle rc = this.Bounds;
            Program.mEnv.GetWindowRect(this.Name, ref rc);
            // 配置
            this.Location = rc.Location;

            InitPlayOption();
        }

        /// <summary>
        /// 閉じる
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void PlayOption_FormClosing(object sender, FormClosingEventArgs e)
        {
            Rectangle rc = new Rectangle(this.Location, this.Size);
            if (this.WindowState == FormWindowState.Minimized)
            {
                rc = this.RestoreBounds;
            }
            else
            {
                rc = this.Bounds;
            }
            Program.mEnv.SetWindowRect(this.Name, rc);
        }

        /// <summary>
        /// オプションデータ
        /// </summary>
        void InitPlayOption()
        {

            comboMIPIPixelFormat.SelectedIndex = mPlayOption.mMIPIPixelFormat;
            comboMIPIDataRate.SelectedIndex = mPlayOption.mMIPIDataRate;
            comboMIPILaneCount.SelectedIndex = mPlayOption.mMIPILaneCount;

            checkMIPIContinuousClock.Checked = mPlayOption.mMIPIContinuousClock;

            checkCanSettingCAN0Sending.Checked = mPlayOption.mCanSettingCAN0Sending;
            checkCanSettingCAN1Sending.Checked = mPlayOption.mCanSettingCAN1Sending;

            numericThreshold1.Value = mPlayOption.mCanThreshold1;
            numericThreshold2.Value = mPlayOption.mCanThreshold2;
            numericCanbps.Value = mPlayOption.mCanbps;
            numericCanFDbps.Value = mPlayOption.mCanFDbps;

            checkLsbPack.Checked = mPlayOption.mDisplayLsbPack;
            checkReverse.Checked = mPlayOption.mDisplayReverse;
            numericFPS.Value = (decimal)mPlayOption.mDisplayFPS;
            numericBuffer.Value = (decimal)mPlayOption.mBuffer;
        }

        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="sender">送信先</param>
        /// <param name="e">イベント情報</param>
        private void buttonApply_Click(object sender, EventArgs e)
        {
            // チェック
            try
            {
                // 数値チェック
                foreach (var ctrl in GetSelfAndChildrenRecursive(this, typeof(TextBox)))
                {
                    TextBox text = ctrl as TextBox;
                    double value;
                    if (!double.TryParse(text.Text, out value))
                    {
                        Win32.SVMessageBox.Show(this, "Invalid number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        text.Focus();
                        return;
                    }
                }

                // 設定

                mPlayOption.mMIPIPixelFormat = comboMIPIPixelFormat.SelectedIndex;
                mPlayOption.mMIPIDataRate = comboMIPIDataRate.SelectedIndex;
                mPlayOption.mMIPILaneCount = comboMIPILaneCount.SelectedIndex;
                mPlayOption.mMIPIContinuousClock = checkMIPIContinuousClock.Checked;

                mPlayOption.mCanSettingCAN0Sending = checkCanSettingCAN0Sending.Checked;
                mPlayOption.mCanSettingCAN1Sending = checkCanSettingCAN1Sending.Checked;
                mPlayOption.mCanThreshold1 = (int)numericThreshold1.Value;
                mPlayOption.mCanThreshold2 = (int)numericThreshold2.Value;
                mPlayOption.mCanbps = (int)numericCanbps.Value;
                mPlayOption.mCanFDbps = (int)numericCanFDbps.Value;

                mPlayOption.mDisplayLsbPack = checkLsbPack.Checked;
                mPlayOption.mDisplayReverse = checkReverse.Checked;
                mPlayOption.mDisplayFPS = (double)numericFPS.Value;
                mPlayOption.mBuffer = (int)numericBuffer.Value;

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Console.WriteLine("error = {0}", ex.Message);
            }
        }

        /// <summary>
        /// 配下チェック
        /// </summary>
        /// <param name="parent">親ウィンドウ</param>
        /// <param name="type_find">チェックするコントロール</param>
        /// <returns></returns>
        IEnumerable<Control> GetSelfAndChildrenRecursive(Control parent, Type type_find)
        {
            List<Control> controls = new List<Control>();

            foreach (Control child in parent.Controls)
            {
                if (child.GetType() == type_find)
                {
                    controls.Add(child);
                }

                controls.AddRange(GetSelfAndChildrenRecursive(child, type_find));
            }
            return controls;
        }
    }
}
