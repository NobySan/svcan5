﻿namespace SVOgenCAN
{
    partial class MainWindow
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenPlayOnly = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileClipToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOption = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreView = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewRun = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewStop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewNext = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewPrev = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewTop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewEnd = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviewJump = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPlay = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPlayStart = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPlayStop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolEasyLogicAnalyzer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolDump = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolCanCH0Window = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolCanCH1Window = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabelSize = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelFPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelFPOS = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelFMAX = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuZoom_1_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_1_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoom_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.menuBar.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.menuBar.AutoSize = false;
            this.menuBar.Dock = System.Windows.Forms.DockStyle.None;
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuPreView,
            this.menuPlay,
            this.menuTool,
            this.menuHelp});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1024, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileOpen,
            this.menuOpenPlayOnly,
            this.menuFileClipToFile,
            this.menuFileOption,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(51, 20);
            this.menuFile.Text = "File(&F)";
            this.menuFile.DropDownOpened += new System.EventHandler(this.menuFile_DropDownOpened);
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.menuFileOpen.Size = new System.Drawing.Size(180, 22);
            this.menuFileOpen.Text = "Open...";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuOpenPlayOnly
            // 
            this.menuOpenPlayOnly.Name = "menuOpenPlayOnly";
            this.menuOpenPlayOnly.Size = new System.Drawing.Size(180, 22);
            this.menuOpenPlayOnly.Text = "Open Play only...";
            this.menuOpenPlayOnly.Click += new System.EventHandler(this.menuOpenPlayOnly_Click);
            // 
            // menuFileClipToFile
            // 
            this.menuFileClipToFile.Name = "menuFileClipToFile";
            this.menuFileClipToFile.Size = new System.Drawing.Size(180, 22);
            this.menuFileClipToFile.Text = "Clip to file (&C) ...";
            this.menuFileClipToFile.Click += new System.EventHandler(this.menuFileClipToFile_Click);
            // 
            // menuFileOption
            // 
            this.menuFileOption.Name = "menuFileOption";
            this.menuFileOption.Size = new System.Drawing.Size(180, 22);
            this.menuFileOption.Text = "Option (&O) ...";
            this.menuFileOption.Click += new System.EventHandler(this.menuFileOption_Click);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.Size = new System.Drawing.Size(180, 22);
            this.menuFileExit.Text = "End (&X)";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // menuPreView
            // 
            this.menuPreView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPreviewRun,
            this.menuPreviewStop,
            this.menuPreviewNext,
            this.menuPreviewPrev,
            this.menuPreviewTop,
            this.menuPreviewEnd,
            this.menuPreviewJump});
            this.menuPreView.Name = "menuPreView";
            this.menuPreView.Size = new System.Drawing.Size(75, 20);
            this.menuPreView.Text = "Preview(&P)";
            this.menuPreView.DropDownOpened += new System.EventHandler(this.menuPreView_DropDownOpened);
            // 
            // menuPreviewRun
            // 
            this.menuPreviewRun.Name = "menuPreviewRun";
            this.menuPreviewRun.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.menuPreviewRun.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewRun.Text = "Run";
            this.menuPreviewRun.Click += new System.EventHandler(this.menuPreviewRun_Click);
            // 
            // menuPreviewStop
            // 
            this.menuPreviewStop.Name = "menuPreviewStop";
            this.menuPreviewStop.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.menuPreviewStop.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewStop.Text = "Stop";
            this.menuPreviewStop.Click += new System.EventHandler(this.menuPreviewStop_Click);
            // 
            // menuPreviewNext
            // 
            this.menuPreviewNext.Name = "menuPreviewNext";
            this.menuPreviewNext.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.menuPreviewNext.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewNext.Text = "Next";
            this.menuPreviewNext.Click += new System.EventHandler(this.menuPreviewNext_Click);
            // 
            // menuPreviewPrev
            // 
            this.menuPreviewPrev.Name = "menuPreviewPrev";
            this.menuPreviewPrev.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.menuPreviewPrev.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewPrev.Text = "Prev";
            this.menuPreviewPrev.Click += new System.EventHandler(this.menuPreviewPrev_Click);
            // 
            // menuPreviewTop
            // 
            this.menuPreviewTop.Name = "menuPreviewTop";
            this.menuPreviewTop.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.menuPreviewTop.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewTop.Text = "Top";
            this.menuPreviewTop.Click += new System.EventHandler(this.menuPreviewTop_Click);
            // 
            // menuPreviewEnd
            // 
            this.menuPreviewEnd.Name = "menuPreviewEnd";
            this.menuPreviewEnd.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.menuPreviewEnd.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewEnd.Text = "End";
            this.menuPreviewEnd.Click += new System.EventHandler(this.menuPreviewEnd_Click);
            // 
            // menuPreviewJump
            // 
            this.menuPreviewJump.Name = "menuPreviewJump";
            this.menuPreviewJump.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.menuPreviewJump.Size = new System.Drawing.Size(130, 22);
            this.menuPreviewJump.Text = "Jump...";
            this.menuPreviewJump.Click += new System.EventHandler(this.menuPreviewJump_Click);
            // 
            // menuPlay
            // 
            this.menuPlay.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPlayStart,
            this.menuPlayStop});
            this.menuPlay.Name = "menuPlay";
            this.menuPlay.Size = new System.Drawing.Size(58, 20);
            this.menuPlay.Text = "Play(&O)";
            this.menuPlay.DropDownOpened += new System.EventHandler(this.menuPlay_DropDownOpened);
            // 
            // menuPlayStart
            // 
            this.menuPlayStart.Name = "menuPlayStart";
            this.menuPlayStart.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.menuPlayStart.Size = new System.Drawing.Size(126, 22);
            this.menuPlayStart.Text = "Start...";
            this.menuPlayStart.Click += new System.EventHandler(this.menuPlayStart_Click);
            // 
            // menuPlayStop
            // 
            this.menuPlayStop.Name = "menuPlayStop";
            this.menuPlayStop.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.menuPlayStop.Size = new System.Drawing.Size(126, 22);
            this.menuPlayStop.Text = "Stop";
            this.menuPlayStop.Click += new System.EventHandler(this.menuPlayStop_Click);
            // 
            // menuTool
            // 
            this.menuTool.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolEasyLogicAnalyzer,
            this.menuToolDump,
            this.menuToolCanCH0Window,
            this.menuToolCanCH1Window});
            this.menuTool.Name = "menuTool";
            this.menuTool.Size = new System.Drawing.Size(55, 20);
            this.menuTool.Text = "Tool(&T)";
            this.menuTool.DropDownOpened += new System.EventHandler(this.menuTool_DropDownOpened);
            // 
            // menuToolEasyLogicAnalyzer
            // 
            this.menuToolEasyLogicAnalyzer.Name = "menuToolEasyLogicAnalyzer";
            this.menuToolEasyLogicAnalyzer.Size = new System.Drawing.Size(205, 22);
            this.menuToolEasyLogicAnalyzer.Text = "Easy Logic Analyzer (&A)...";
            this.menuToolEasyLogicAnalyzer.Click += new System.EventHandler(this.menuToolEasyLogicAnalyzer_Click);
            // 
            // menuToolDump
            // 
            this.menuToolDump.Name = "menuToolDump";
            this.menuToolDump.Size = new System.Drawing.Size(205, 22);
            this.menuToolDump.Text = "Dump (&D)...";
            this.menuToolDump.Click += new System.EventHandler(this.menuToolDump_Click);
            // 
            // menuToolCanCH0Window
            // 
            this.menuToolCanCH0Window.Name = "menuToolCanCH0Window";
            this.menuToolCanCH0Window.Size = new System.Drawing.Size(205, 22);
            this.menuToolCanCH0Window.Text = "CAN ch0 window (&0)...";
            this.menuToolCanCH0Window.Click += new System.EventHandler(this.menuToolCanCH0Window_Click);
            // 
            // menuToolCanCH1Window
            // 
            this.menuToolCanCH1Window.Name = "menuToolCanCH1Window";
            this.menuToolCanCH1Window.Size = new System.Drawing.Size(205, 22);
            this.menuToolCanCH1Window.Text = "CAN ch1 window (&1)...";
            this.menuToolCanCH1Window.Click += new System.EventHandler(this.menuToolCanCH1Window_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(61, 20);
            this.menuHelp.Text = "Help(&H)";
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Name = "menuHelpAbout";
            this.menuHelpAbout.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.menuHelpAbout.Size = new System.Drawing.Size(135, 22);
            this.menuHelpAbout.Text = "About...";
            this.menuHelpAbout.Click += new System.EventHandler(this.menuHelpAbout_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelSize,
            this.statusLabelFPS,
            this.statusLabelFPOS,
            this.statusLabelFMAX});
            this.statusBar.Location = new System.Drawing.Point(0, 723);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1024, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabelSize
            // 
            this.statusLabelSize.AutoSize = false;
            this.statusLabelSize.Name = "statusLabelSize";
            this.statusLabelSize.Size = new System.Drawing.Size(146, 17);
            this.statusLabelSize.Text = "Width : 0000 Height : 0000";
            // 
            // statusLabelFPS
            // 
            this.statusLabelFPS.AutoSize = false;
            this.statusLabelFPS.Name = "statusLabelFPS";
            this.statusLabelFPS.Size = new System.Drawing.Size(90, 17);
            this.statusLabelFPS.Text = "fps : 00.00";
            // 
            // statusLabelFPOS
            // 
            this.statusLabelFPOS.AutoSize = false;
            this.statusLabelFPOS.Name = "statusLabelFPOS";
            this.statusLabelFPOS.Size = new System.Drawing.Size(72, 17);
            this.statusLabelFPOS.Text = "fpos : 0";
            this.statusLabelFPOS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusLabelFMAX
            // 
            this.statusLabelFMAX.AutoSize = false;
            this.statusLabelFMAX.Name = "statusLabelFMAX";
            this.statusLabelFMAX.Size = new System.Drawing.Size(72, 17);
            this.statusLabelFMAX.Text = "fmax : 0";
            this.statusLabelFMAX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuZoom_1_4,
            this.menuZoom_1_2,
            this.menuZoom_1,
            this.menuZoom_2,
            this.menuZoom_4});
            this.contextMenu.Name = "contextMenuStrip1";
            this.contextMenu.Size = new System.Drawing.Size(92, 114);
            // 
            // menuZoom_1_4
            // 
            this.menuZoom_1_4.Name = "menuZoom_1_4";
            this.menuZoom_1_4.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1_4.Text = "1/4";
            this.menuZoom_1_4.Click += new System.EventHandler(this.menuZoom_1_4_Click);
            // 
            // menuZoom_1_2
            // 
            this.menuZoom_1_2.Name = "menuZoom_1_2";
            this.menuZoom_1_2.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1_2.Text = "1/2";
            this.menuZoom_1_2.Click += new System.EventHandler(this.menuZoom_1_2_Click);
            // 
            // menuZoom_1
            // 
            this.menuZoom_1.Name = "menuZoom_1";
            this.menuZoom_1.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_1.Text = "1/1";
            this.menuZoom_1.Click += new System.EventHandler(this.menuZoom_1_Click);
            // 
            // menuZoom_2
            // 
            this.menuZoom_2.Name = "menuZoom_2";
            this.menuZoom_2.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_2.Text = "2";
            this.menuZoom_2.Click += new System.EventHandler(this.menuZoom_2_Click);
            // 
            // menuZoom_4
            // 
            this.menuZoom_4.Name = "menuZoom_4";
            this.menuZoom_4.Size = new System.Drawing.Size(91, 22);
            this.menuZoom_4.Text = "4";
            this.menuZoom_4.Click += new System.EventHandler(this.menuZoom_4_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 693);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox.Location = new System.Drawing.Point(0, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1, 1);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 745);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.menuBar);
            this.MainMenuStrip = this.menuBar;
            this.Name = "MainWindow";
            this.Text = "SVOgenCAN [no data]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileOption;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.ToolStripMenuItem menuPreView;
        private System.Windows.Forms.ToolStripMenuItem menuTool;
        private System.Windows.Forms.ToolStripMenuItem menuToolEasyLogicAnalyzer;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuHelpAbout;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelSize;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewTop;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewEnd;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewNext;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewPrev;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewJump;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1_4;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1_2;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_1;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_2;
        private System.Windows.Forms.ToolStripMenuItem menuZoom_4;
        private System.Windows.Forms.ToolStripMenuItem menuToolDump;
        private System.Windows.Forms.ToolStripMenuItem menuToolCanCH0Window;
        private System.Windows.Forms.ToolStripMenuItem menuToolCanCH1Window;
        private System.Windows.Forms.ToolStripMenuItem menuPlay;
        private System.Windows.Forms.ToolStripMenuItem menuPlayStart;
        private System.Windows.Forms.ToolStripMenuItem menuPlayStop;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewRun;
        private System.Windows.Forms.ToolStripMenuItem menuPreviewStop;
        private System.Windows.Forms.ToolStripMenuItem menuFileClipToFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFPS;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFPOS;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFMAX;
        private System.Windows.Forms.ToolStripMenuItem menuOpenPlayOnly;
    }
}

